#version 420

$GLMatrices

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec4 colorVert[3];
out vec4 color;

uniform float cylinderRadius;   // radio del cilindro
uniform float cylinderHeight;   // altura del cilindro
uniform vec3 cylinderAxis;      // dirección del eje (vector unitario)
uniform vec3 cylinderPos;       // centro de la base del cilindro

bool isInside(vec3 vertex, vec3 cylPos, vec3 cylAxi) {
    vec3 cylPosExtreme = cylPos + cylAxi * cylinderHeight;
    // https://stackoverflow.com/questions/47932955/how-to-check-if-a-3d-point-is-inside-a-cylinder
    // Comprobar que el punto esté entre las dos bases
    bool check1 = dot(vertex - cylPos, cylPosExtreme - cylPos) >= 0.0;
    bool check2 = dot(vertex - cylPosExtreme, cylPos - cylPosExtreme) <= 0.0;
    // Comprobar que el punto esté entre la parte curva
    bool check3 = (length(cross(vertex - cylPos, cylPosExtreme - cylPos)) / length(cylPosExtreme - cylPos)) < cylinderRadius;

    return check1 && check2 && check3;
}

void main() {
    // vec3 cylAxi = vec3(projMatrix * viewMatrix * vec4(cylinderAxis, 1.0));
    // vec3 cylPos = vec3(projMatrix * viewMatrix * vec4(cylinderPos, 1.0));
    vec3 cylAxi = cylinderAxis;
    vec3 cylPos = cylinderPos;

    int numVertInCyl = 0;
    for (int i = 0; i < gl_in.length(); i++) {
        if (isInside(gl_in[i].gl_Position.xyz, cylPos, cylAxi)) {
            numVertInCyl++;
        }
    }

    if (numVertInCyl == 0) {
        for (int i = 0; i < gl_in.length(); i++) {
            // Asignamos el color de nuevo para que la variable no se quede inutilizada
            color = colorVert[i];
            gl_Position = gl_in[i].gl_Position;
            EmitVertex();
        }
    }
    else if (numVertInCyl == 1) {
        color = vec4(1.0, 0.0, 0.0, 1.0);
        for (int i = 0; i < gl_in.length(); i++) {
            // Asignamos el color de nuevo para que la variable no se quede inutilizada
            gl_Position = gl_in[i].gl_Position;
            EmitVertex();
        }
    }
    else if (numVertInCyl == 2) {
        color = vec4(0.0, 1.0, 0.0, 1.0);
        for (int i = 0; i < gl_in.length(); i++) {
            // Asignamos el color de nuevo para que la variable no se quede inutilizada
            gl_Position = gl_in[i].gl_Position;
            EmitVertex();
        }
    }
}