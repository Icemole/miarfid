
#include <glm/gtc/matrix_transform.hpp>

#include "PGUPV.h"
#include "GUI3.h"

using namespace PGUPV;

using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;
using std::vector;

/*

Muestra una esfera texturada con una foto de la Tierra.

*/

class MyRender : public Renderer {
public:
    MyRender(){};
    void setup(void);
    void render(void);
    void reshape(uint w, uint h);
    void update(uint ms);

private:
    const int MAX_NUM_SPHERES = 5;
    int numSpheres = 3;
    std::shared_ptr<GLMatrices> mats;
    std::shared_ptr<Program> program;
    std::vector<std::shared_ptr<Texture2D>> textures;
    std::vector<std::shared_ptr<Sphere>> sph;
    std::vector<float> angles;
    std::vector<float> angularVelocity;

    // UI
    float rotationSpeed;
    glm::vec4 colorThreshold;
    std::shared_ptr<Button> addSphereButton, removeSphereButton;
    int currentSphereSelected;

    void buildGUI();
    void updateRotationSpeeds(float newValue);
    void addSphere();
    void removeSphere();
};

void MyRender::setup() {
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    /**********
     *** UI ***
     **********/
    rotationSpeed = 15;
    colorThreshold = glm::vec4(0, 0, 0.5, 1);

    /* Este shader necesita las coordenadas de los vértices y sus 
    coordenadas de textura */
    program = std::make_shared<Program>();
    program->addAttributeLocation(Mesh::VERTICES, "position");
    program->addAttributeLocation(Mesh::TEX_COORD0, "texCoord");

    mats = GLMatrices::build();
    program->connectUniformBlock(mats, UBO_GL_MATRICES_BINDING_INDEX);

    program->loadFiles("../p4b/textureReplace");
    program->compile();

    // Localización de los uniform (unidad de textura)
    GLint texUnitLoc = program->getUniformLocation("texUnit");
    // Comunicamos la unidad de textura al shader
    program->use();
    glUniform1i(texUnitLoc, 0);

    for (int i = 0; i < numSpheres; i++) {
        sph.push_back(std::make_shared<Sphere>());
        // Setear las texturas
        textures.push_back(std::make_shared<Texture2D>());
        textures[i]->loadImage("../p4b/textures/water.png");
        // Setear los ángulos
        angles.push_back(0);
        // Setear la velocidad de rotación de las esferas
        angularVelocity.push_back(rotationSpeed * (i + 1));
    }

    setCameraHandler(std::make_shared<OrbitCameraHandler>());

    buildGUI();
    App::getInstance().getWindow().showGUI(true);
}

void MyRender::render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    mats->setMatrix(GLMatrices::VIEW_MATRIX, getCamera().getViewMatrix());

    for (int i = 0; i < numSpheres; i++) {
        textures[i]->bind(GL_TEXTURE0);
        mats->pushMatrix(GLMatrices::MODEL_MATRIX);
        mats->rotate(GLMatrices::MODEL_MATRIX, angles[i], glm::vec3(0.1, 1.0, -0.1));
        mats->scale(GLMatrices::MODEL_MATRIX, glm::vec3((i + 1) / (float)numSpheres));
        sph[i]->render();
        mats->popMatrix(GLMatrices::MODEL_MATRIX);
    }
    
    CHECK_GL();
}

void MyRender::reshape(uint w, uint h) {
    glViewport(0, 0, w, h);
    mats->setMatrix(GLMatrices::PROJ_MATRIX, getCamera().getProjMatrix());
}

void MyRender::update(uint ms) {
    for (int i = 0; i < numSpheres; i++) {
        angles[i] += glm::radians(angularVelocity[i]) * ms / 1000.0f;
        if (angles[i] > TWOPIf) {
            angles[i] -= TWOPIf;
        }
    }
}

void MyRender::buildGUI() {
    auto panel = addPanel("Fragmentos");

    panel->setPosition(5, 5);
    panel->setSize(400, 300);

    // Los widgets ya se comunican con el shader sin necesidad
    // de implementar esa comunicación a mano

    auto discardByThresholdWidget = std::make_shared<CheckBoxWidget>(
        "Descartar por umbral",
        true,
        program,
        "discardByThreshold"
    );
    panel->addWidget(discardByThresholdWidget);

    auto thresholdWidget = std::make_shared<RGBAColorWidget>(
        "Umbral",
        colorThreshold,
        program,
        "colorThreshold");
    panel->addWidget(thresholdWidget);

    auto angularVelocityWidget = std::make_shared<FloatSliderWidget>(
        "Vel. rotación",
        rotationSpeed,
        0,
        90
    );
    auto rotationSpeedUpdater = [this](const float newValue) { updateRotationSpeeds(newValue); };
    angularVelocityWidget->getValue().addListener(rotationSpeedUpdater);
    panel->addWidget(angularVelocityWidget);
    
    addSphereButton = std::make_shared<Button>(
        "Añadir esfera",
        std::bind(&MyRender::addSphere, this)
    );
    panel->addWidget(addSphereButton);

    removeSphereButton = std::make_shared<Button>(
        "Quitar esfera",
        std::bind(&MyRender::removeSphere, this)
    );
    panel->addWidget(removeSphereButton);

    auto sphereSelectorWidget = std::make_shared<IntSliderWidget>(
        "Esfera i-ésima",
        currentSphereSelected,
        0,
        MAX_NUM_SPHERES - 1
    );
    // auto currentSphereUpdater = [textureSelector, this](const int newValue) {
    //     currentSphereSelected = newValue;
    //     textureSelector->setSelected(false);
    // };
    // sphereSelectorWidget->getValue().addListener(currentSphereUpdater);
    // panel->addWidget(sphereSelectorWidget);

    std::vector<std::string> filenames = listFiles("../p4b/textures/", false, std::vector < std::string > {"*.png", "*.jpg"});
    std::vector<std::string> names;
    for (auto name : filenames) {
        names.push_back(getFilenameFromPath(name));
    }
    auto textureSelector = std::make_shared<ListBoxWidget<>>("Tex. esf. i-ésima", names);
    auto updater = [textureSelector, this](const int &) {
        if (currentSphereSelected < numSpheres) {
            textures[currentSphereSelected]->loadImage("../p4b/textures/" + textureSelector->getSelectedElement());
        }
    };
    textureSelector->getValue().addListener(updater);

    // Callback del selector de esfera
    // Se declara aquí abajo porque también modifica el selector de texturas
    auto currentSphereUpdater = [textureSelector, this](const int newValue) {
        currentSphereSelected = newValue;
        textureSelector->setSelected(false);
    };
    sphereSelectorWidget->getValue().addListener(currentSphereUpdater);
    panel->addWidget(sphereSelectorWidget);

    panel->addWidget(textureSelector);
}

void MyRender::updateRotationSpeeds(float newValue) {
    rotationSpeed = newValue;
    for (int i = 0; i < numSpheres; i++) {
        angularVelocity[i] = newValue * (i + 1);
    }
}

void MyRender::addSphere() {
    if (numSpheres == MAX_NUM_SPHERES) {
        return;
    }
    numSpheres++;
    sph.push_back(std::make_shared<Sphere>());
    textures.push_back(std::make_shared<Texture2D>());
    textures[numSpheres - 1]->loadImage("../p4b/textures/water.png");
    angles.push_back(0);
    angularVelocity.push_back(rotationSpeed * numSpheres);
    if (numSpheres == MAX_NUM_SPHERES) {
        addSphereButton->setDisabled(true);
    }
    else if (numSpheres == 2) {
        removeSphereButton->setDisabled(false);
    }
}

void MyRender::removeSphere() {
    if (numSpheres == 0) {
        return;
    }
    numSpheres--;
    sph.pop_back();
    textures.pop_back();
    angles.pop_back();
    angularVelocity.pop_back();
    if (numSpheres == 1) {
        removeSphereButton->setDisabled(true);
    }
    else if (numSpheres == MAX_NUM_SPHERES - 1) {
        addSphereButton->setDisabled(false);
    }
}

int main(int argc, char *argv[]) {
    App &myApp = App::getInstance();
    myApp.initApp(argc, argv, PGUPV::DOUBLE_BUFFER | PGUPV::DEPTH_BUFFER |
                                    PGUPV::MULTISAMPLE);
    myApp.getWindow().setRenderer(std::make_shared<MyRender>());
    return myApp.run();
}
