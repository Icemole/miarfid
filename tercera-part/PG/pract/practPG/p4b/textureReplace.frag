#version 420 core

uniform sampler2D texUnit;

in vec2 texCoordFrag;
uniform vec4 colorThreshold;
uniform bool discardByThreshold;
out vec4 fragColor;

void main() { 
    fragColor = texture(texUnit, texCoordFrag);

    if (discardByThreshold && any(lessThan(fragColor, colorThreshold))) {
        discard;
    }
}
