
#include "PGUPV.h"

#include "GameObject.h"
#include "ObjectPool.h"

using namespace PGUPV;

using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;
using std::vector;

class MyRender : public Renderer {
public:
	MyRender() {};
	void setup(void) override;
	void render(void) override;
	void reshape(uint w, uint h) override;
	void update(uint ms) override;
private:
	std::shared_ptr<GLMatrices> mats;
	Model luna;
	ObjectPool treePool;

	void drawUI();
};

void MyRender::setup() {
	glClearColor(1.f, 1.f, 1.f, 1.0f);
	glEnable(GL_DEPTH_TEST);

	mats = GLMatrices::build();

	setCameraHandler(std::make_shared<OrbitCameraHandler>(5.0f));

	// Preparar el blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Luna
	std::vector<glm::vec3> verticesLuna = {
		// Coordenadas en el cubo canónico 2x2x2
		glm::vec3(-0.75, -0.5, 0), glm::vec3(-0.5, 0.5, 0),
		glm::vec3(0.5, 0.5, 0), glm::vec3(0.75, -0.5, 0)
	};
	std::vector<GLuint> indicesLuna = {0, 3, 2, 1};
	auto mesh = std::make_shared<Mesh>();
	mesh->addVertices(verticesLuna);
	mesh->addIndices(indicesLuna);
	mesh->setColor(glm::vec4(0, 0.67, 1, 0.125));
	mesh->addDrawCommand(new PGUPV::DrawElements(GL_TRIANGLE_FAN, 4, GL_UNSIGNED_INT, (GLvoid *)0));
	luna.addMesh(mesh);

	// Preparar el buffer de stencil
	glEnable(GL_STENCIL_TEST);

	// Instalar el shader para dibujar los ejes, las normales y la luz
	ConstantIllumProgram::use();
}

/*
Tienes que completar esta función para dibujar la escena.

*/
void MyRender::render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// mats->setMatrix(GLMatrices::VIEW_MATRIX, getCamera().getViewMatrix());

	treePool.render(mats);

	drawUI();

	CHECK_GL();
}

void MyRender::reshape(uint w, uint h) {
	glViewport(0, 0, w, h);

	/***************
	 *** STENCIL ***
	 ***************/
	// No queremos que la profundidad tenga nada que ver con la modificación
	// del buffer de stencil
	glDisable(GL_DEPTH_TEST);
	
	// Modificar stencil
	glClear(GL_STENCIL_BUFFER_BIT);
	glStencilFunc(GL_ALWAYS, 0x1, 0XFF);
	glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);

	drawUI();
	
	// De ahora en adelante, dibujar únicamente por encima de la luna
	glStencilFunc(GL_EQUAL, 0x1, 0xFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	glEnable(GL_DEPTH_TEST);
	mats->setMatrix(GLMatrices::PROJ_MATRIX, getCamera().getProjMatrix());
}

void MyRender::update(uint ms) {
	treePool.update(ms);
}

void MyRender::drawUI() {
	// Preparar el cubo canónico 2x2x2
	mats->loadIdentity(GLMatrices::PROJ_MATRIX);
	// Ajustar la vista
	mats->pushMatrix(GLMatrices::VIEW_MATRIX);
	mats->setMatrix(GLMatrices::VIEW_MATRIX,
			glm::lookAt(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f),
					glm::vec3(0.0f, 1.0f, 0.0f)));
	
	luna.render();

	// Dejar las matrices como estaban
	mats->popMatrix(GLMatrices::VIEW_MATRIX);
	mats->setMatrix(GLMatrices::PROJ_MATRIX, getCamera().getProjMatrix());
}

int main(int argc, char* argv[]) {
	App& myApp = App::getInstance();
	myApp.setInitWindowSize(800, 600);
	myApp.initApp(argc, argv, PGUPV::DOUBLE_BUFFER | PGUPV::DEPTH_BUFFER | PGUPV::MULTISAMPLE
			| PGUPV::STENCIL_BUFFER);
	myApp.getWindow().setRenderer(std::make_shared<MyRender>());
	return myApp.run();
}
