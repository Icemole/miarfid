
#include "PGUPV.h"

#ifndef GameObject_H
#include "GameObject.h"
#define GameObject_H
#endif

using namespace PGUPV;

class ObjectPool {
public:
    const uint MAX_TREES_IN_SCENE = 30;  // TreePool.h
    const float MAX_RENDER_DISTANCE = 100.0f;  // TreePool.h

    ObjectPool();
    void update(uint ms);
    void render(std::shared_ptr<PGUPV::GLMatrices> mats);
    void addOrUpdateTree(int i = -1);  // TreePool.h

    glm::vec3 velocity = glm::vec3(0, 0, 1);  // SomewhereElse.h
private:
    uint currentActiveObjects;
    std::vector<std::shared_ptr<Model>> models;
    std::vector<std::shared_ptr<GameObject>> gameObjects;
    glm::vec3 origin;

    void setup();
    uint getNumModels();
    void crearCilindro(float r_bottom, float r_top, float height, float offsetY,
            uint stacks, uint slices,
            std::vector<glm::vec3> &vertices, std::vector<glm::vec4> &colores,
            std::vector<GLuint> &indices,
            glm::vec4 color);  // TreePool.h
    void crearEsfera(float radius, float offsetY, uint stacks, uint slices,
            std::vector<glm::vec3> &vertices, std::vector<glm::vec4> &colores,
            std::vector<GLuint> &indices,
            glm::vec4 colorInicio, glm::vec4 colorCopa);  // TreePool.h
    std::shared_ptr<Model> crearArbol(float troncoRBottom, float troncoRTop, float troncoHeight,
            float troncoOffsetY, uint troncoStacks, uint troncoSlices, glm::vec4 troncoColor,
            float hojasRadio, float hojasOffsetY, uint hojasStacks, uint hojasSlices,
            glm::vec4 hojasColorAbajo, glm::vec4 hojasColorArriba);  // TreePool.h
};