
#include "PGUPV.h"

#define GameObject_H

using namespace PGUPV;

/**
 * Clase que modela los objetos en la escena con
 * un modelo, una posición y una velocidad.
 */
class GameObject {
public:
    GameObject() {};
    GameObject(std::shared_ptr<Model> m, glm::vec3 pos, glm::vec3 vel);
    // Getters
    std::shared_ptr<Model> getModel();
    glm::vec3 getPosition();
    glm::vec3 getVelocity();
    // Setters
    void setModel(std::shared_ptr<Model> m);
    void setPosition(glm::vec3 pos);
    void setVelocity(glm::vec3 vel);
	void update(uint ms);
    void render();
private:
    std::shared_ptr<Model> model;
	glm::vec3 pos;
    glm::vec3 vel;
};