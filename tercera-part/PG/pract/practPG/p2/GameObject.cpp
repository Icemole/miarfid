
#include "GameObject.h"

GameObject::GameObject(std::shared_ptr<PGUPV::Model> m, glm::vec3 pos, glm::vec3 vel) {
    setModel(m);
    setPosition(pos);
    setVelocity(vel);
}

std::shared_ptr<Model> GameObject::getModel() {
    return this->model;
}

glm::vec3 GameObject::getPosition() {
    return this->pos;
}

glm::vec3 GameObject::getVelocity() {
    return this->vel;
}

void GameObject::setModel(std::shared_ptr<PGUPV::Model> m) {
    this->model = m;
}

void GameObject::setPosition(glm::vec3 pos) {
    this->pos = pos;
}

void GameObject::setVelocity(glm::vec3 vel) {
    this->vel = vel;
}

void GameObject::update(uint ms) {
    this->pos += this->vel * (ms / 1000.0f);
}

void GameObject::render() {
    model->render();
}