- He creado dos clases:
    - GameObject: para usar un puntero a un modelo
      y que el objeto tenga una posición y una velocidad.
    - ObjectPool para trabajar con diferentes GameObjects de manera eficiente,
      replicando muchos objetos con un único modelo en memoria.
- En main.cpp he creado un objeto de la clase ObjectPool que replica 100 árboles
  con solo 3 modelos (los desarrollados en la práctica anterior) en memoria.
    - Los árboles van hacia la cámara, y cuando pasan la coordenada z de la cámara
      (es decir, quedan detrás de la vista), pongo su posición al final de la escena de nuevo.
- He creado el modelo de un parabrisas y lo he dibujado en el render(), modificando la vista
  al cubo canónico 2x2x2 para más facilidad y realismo.
- En el reshape() también he dibujado el parabrisas, modificando el buffer de stencil en el proceso.