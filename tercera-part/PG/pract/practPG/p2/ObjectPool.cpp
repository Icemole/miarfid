
#include "ObjectPool.h"

using namespace PGUPV;

ObjectPool::ObjectPool() {
	setup();
}

void ObjectPool::update(uint ms) {
	for (uint i = 0; i < gameObjects.size(); i++) {
		gameObjects[i]->update(ms);
		if (gameObjects[i]->getPosition().z > origin.z) {
			// Actualizar árbol (posición negativa, van hacia el origen)
			addOrUpdateTree(i);
		}
	}
}

void ObjectPool::render(std::shared_ptr<PGUPV::GLMatrices> mats) {
	for (auto gameObject : gameObjects) {
		mats->pushMatrix(GLMatrices::MODEL_MATRIX);
		mats->translate(GLMatrices::MODEL_MATRIX, gameObject->getPosition());
		gameObject->render();
		mats->popMatrix(GLMatrices::MODEL_MATRIX);
	}
}

uint ObjectPool::getNumModels() {
	return models.size();
}

void ObjectPool::crearCilindro(float r_bottom, float r_top, float height, float offsetY,
		uint stacks, uint slices,
		std::vector<glm::vec3> &vertices, std::vector<glm::vec4> &colores,
		std::vector<GLuint> &indices,
		glm::vec4 color) {
	
    float delta = (float)(TWOPIf / slices);
	for (uint j = 0; j <= stacks; j++) {
		float angle = 0.0;
		float ring_y = height * ((float)j / stacks - 0.5f);
		float radius_ring = r_bottom - j * (r_bottom - r_top) / stacks;
		for (uint i = 0; i <= slices; i++, angle += delta) {
			float sin_a = sin(angle);
			float cos_a = cos(angle);

			vertices.push_back(
				glm::vec3(radius_ring * sin_a, ring_y + offsetY, radius_ring * cos_a));
			colores.push_back(color);
			// normalesArbol1.push_back(
			// 	vec3(glm::rotate(glm::mat4(1.0f), angle, vec3(0.0f, 1.0f, 0.0f)) *
			// 		glm::vec4(0.0f, 0.0f, 1.0f, 0.0f)));
		}
	}

	// Dibujar la parte de abajo
	// Esto asume que la base es al menos triangular (buena asunción para un cilindro)
	uint beginningIndexBottom = slices / 2;
	indices.push_back(beginningIndexBottom);
	uint decreasingIndexBottom = beginningIndexBottom - 1;
	uint increasingIndexBottom = beginningIndexBottom + 1;
	for (uint i = 0; i <= (slices - 1) / 2; i++) {
		indices.push_back(decreasingIndexBottom--);
		indices.push_back(increasingIndexBottom++);
	}
	if ((slices / 2) % 2 == 0) {
		indices.push_back((uint)0);
	}
	indices.push_back((uint)-1);

	for (uint j = 0; j < stacks; j++) {
		uint topring = (j + 1) * (slices + 1);
		uint botring = j * (slices + 1);
		for (uint i = 0; i <= slices; i++) {
			indices.push_back(topring++);
			indices.push_back(botring++);
		}
		indices.push_back((uint)-1);
	}

	// Dibujar la parte de arriba
	// Esto asume que la base es al menos triangular (buena asunción para un cilindro)
	uint beginningIndexTop = (slices + 1) * stacks + slices / 2;
	indices.push_back(beginningIndexTop);
	uint decreasingIndexTop = beginningIndexTop - 1;
	uint increasingIndexTop = beginningIndexTop + 1;
	for (uint i = 0; i <= (slices - 1) / 2; i++) {
		indices.push_back(increasingIndexTop++);
		indices.push_back(decreasingIndexTop--);
	}
	if ((slices / 2) % 2 == 0) {
		indices.push_back((uint)((slices + 1) * stacks));
	}
	indices.push_back((uint)-1);
}

void ObjectPool::crearEsfera(float radius, float offsetY, uint stacks, uint slices,
		std::vector<glm::vec3> &vertices, std::vector<glm::vec4> &colores,
		std::vector<GLuint> &indices,
		glm::vec4 colorInicio, glm::vec4 colorCopa) {
	
    if (stacks < 2)
		stacks = 2;

	float deltalong = (float)(2.0 * M_PI / slices);
	float deltalat = (float)(M_PI / stacks);

	glm::vec4 colorInterp = colorCopa - colorInicio;

	// Regular stacks
	float latitude = (float)(-M_PI / 2.0);
	for (uint j = 0; j <= stacks; j++, latitude += deltalat) {
		float sinlat = sin(latitude);
		float coslat = cos(latitude);
		float longitude = 0.0;
		float ring_y = radius * sinlat;
		for (uint i = 0; i <= slices; i++, longitude += deltalong) {
			float sinlon = sin(longitude);
			float coslon = cos(longitude);

			// vertices[k] =
			vertices.push_back(
				glm::vec3(radius * sinlon * coslat, ring_y + offsetY, radius * coslon * coslat));
			if (j == 0 && i == 0)
				colores.push_back(colorInicio);
			else if (j == stacks && i == slices)
				colores.push_back(colorCopa);
			else
				colores.push_back(colorInicio + colorInterp * (j / (float)stacks));
		}
	}

	// # of indices per stack:
	//  each stack has 2*slices triangles
	//  a triangle strip requires 2 + #triangles = 2 + 2*slices
	//  we end each slice with a -1 index to ask OpenGL to start a new
	//  tri-strip (+1)
	// uint indices_per_stack = 2 + 2 * slices + 1;

	for (uint j = 0; j < stacks; j++) {
		uint topring = (j + 1) * (slices + 1);
		uint botring = j * (slices + 1);
		for (uint i = 0; i <= slices; i++) {
			indices.push_back(topring++);
			indices.push_back(botring++);
		}
		indices.push_back((uint)-1);
	}
}

std::shared_ptr<Model> ObjectPool::crearArbol(float troncoRBottom, float troncoRTop, float troncoHeight,
		float troncoOffsetY, uint troncoStacks, uint troncoSlices, glm::vec4 troncoColor,
		float hojasRadio, float hojasOffsetY, uint hojasStacks, uint hojasSlices,
		glm::vec4 hojasColorAbajo, glm::vec4 hojasColorArriba) {

    auto model = std::make_shared<Model>();

	/**********
	 * TRONCO *
	 **********/
	auto meshTronco = std::make_shared<Mesh>();
	std::vector<glm::vec3> verticesTronco;
	std::vector<glm::vec4> coloresTronco;
	std::vector<GLuint> indicesTronco;
	crearCilindro(troncoRBottom, troncoRTop, troncoHeight, troncoOffsetY,
				troncoStacks, troncoSlices,
				verticesTronco, coloresTronco, indicesTronco,
				troncoColor);
	meshTronco->addIndices(indicesTronco);
	meshTronco->addVertices(verticesTronco);
	meshTronco->addColors(coloresTronco);
	// Draw command para el tronco
	auto dc = new DrawElements(GL_TRIANGLE_STRIP, static_cast<GLsizei>(indicesTronco.size()), GL_UNSIGNED_INT, (void *)0);
	dc->setPrimitiveRestart();
	dc->setRestartIndex((uint)-1);
	meshTronco->addDrawCommand(dc);

	/*********
	 * HOJAS *
	 *********/
	auto meshHojas = std::make_shared<Mesh>();
	std::vector<glm::vec3> verticesHojas;
	std::vector<glm::vec4> coloresHojas;
	std::vector<GLuint> indicesHojas;
	crearEsfera(hojasRadio, hojasOffsetY, hojasStacks, hojasSlices,
				verticesHojas, coloresHojas, indicesHojas,
				hojasColorAbajo, hojasColorArriba);
	meshHojas->addIndices(indicesHojas);
	meshHojas->addVertices(verticesHojas);
	meshHojas->addColors(coloresHojas);
	// // Draw command para las hojas
	auto dc2 = new DrawElements(GL_TRIANGLE_STRIP, static_cast<GLsizei>(indicesHojas.size()), GL_UNSIGNED_INT, (void *)0);
	dc2->setPrimitiveRestart();
	dc2->setRestartIndex((uint)-1);
	meshHojas->addDrawCommand(dc2);

	// Añadir las mallas del árbol al modelo del árbol
	model->addMesh(meshTronco);
	model->addMesh(meshHojas);

    return model;
}

/**
 * Añade un nuevo árbol o actualiza alguno ya existente.
 * Si i es -1 añade un nuevo árbol; si no, actualiza uno ya existente.
 */
void ObjectPool::addOrUpdateTree(int i) {
	std::shared_ptr<GameObject> obj;
	if (i == -1 && currentActiveObjects < MAX_TREES_IN_SCENE) {
		// Añadir un nuevo árbol (setup de la pool)
		obj = std::make_shared<GameObject>();
		gameObjects.push_back(obj);
		currentActiveObjects++;
	}
	else if (0 <= i && i < (int)gameObjects.size()) {
		// Coger el objeto de la pool
		obj = gameObjects[i];
	}
	else {
		return;
	}
	
	// Actualizar el modelo
	obj->setModel(models[rand() % 3]);
	// Actualizar la posición
	float streetSide = (rand() % 2)? 1: -1;  // Lado de la carretera
	float streetDisp = (rand() % 2)? 1: -1;  // Pequeño desplazamiento izq/der
	float x = streetSide;  // Lado de la carretera
	x += streetDisp * (rand() % 5) / 10.0f;  // Pequeño desplazamiento izq/der
	float y = -0.5;
	float z;
	if (i == -1) {
		z = -rand() % int(MAX_RENDER_DISTANCE - 3);  // Situar en cualquier punto
	}
	else {
		z = -(MAX_RENDER_DISTANCE - 3);  // Situar al final
	}
	z += (rand() % 300) / 100.0f;  // Pequeño desplazamiento adelante/atrás
	glm::vec3 pos = glm::vec3(x, y, z);
	obj->setPosition(pos);
	// Actualizar la velocidad
	obj->setVelocity(velocity);
}

/**
 * Inicializa la pool con los modelos de árbol predefinidos.
 */
void ObjectPool::setup() {
	// Establecer el origen (ojo de la pantalla aproximado)
	origin = glm::vec3(0, 0, 5);

	// Crear los modelos de los árboles
	models.push_back(crearArbol(0.4, 0.2, 1, 0.5, 2, 5, glm::vec4(0.83f, 0.57f, 0.16f, 1.0f),
			0.6, 1.25, 5, 8, glm::vec4(0.2f, 0.64f, 0.16f, 1.0f), glm::vec4(0.4f, 0.94f, 0.3f, 1.0f)));
	models.push_back(crearArbol(0.2, 0.01, 1, 0.5, 2, 5, glm::vec4(0.83f, 0.57f, 0.16f, 1.0f),
			0.6, 1.25, 4, 6, glm::vec4(1.0f, 0.0f, 1.0f, 1.0f), glm::vec4(1.0f, 1.0f, 0.0f, 1.0f)));
	models.push_back(crearArbol(0.1, 0.07, 1, 0.5, 2, 5, glm::vec4(0.6f, 0.7f, 0.6f, 1.0f),
			0.6, 1.25, 4, 6, glm::vec4(0.95f, 0.57f, 0.25f, 1.0f), glm::vec4(0.95f, 0.57f, 0.25f, 1.0f)));

	// Crear las instancias de la pool
	currentActiveObjects = 0;
	srand(time(NULL));
	for (uint i = 0; i < MAX_TREES_IN_SCENE; i++) {
		addOrUpdateTree();
	}
}