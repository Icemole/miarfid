#version 420

$GLMatrices
$Lights
//$Material

in vec4 position;
in vec3 normal;

out vec3 N, L[4], V;


void main() {
    // Normal en el espacio de la c�mara
    N = normalize(normalMatrix * normal);
    // V�rtice en el espacio de la c�mara
    vec3 eposition = vec3(modelviewMatrix * position);
    // Vector vista (desde v�rtice a la c�mara)
    V = normalize(-eposition.xyz);
    // Vector vertice-fuente
    for (int i = 0; i < lights.length(); i++) {
        if (lights[i].directional != 0.0) {
            // Luz direccional
            L[i] = normalize(lights[i].positionEye.xyz);
        }
        else {
            // Luz no direccional
            L[i] = normalize(vec3(lights[i].positionEye) - eposition);
        }
    }
    gl_Position = modelviewprojMatrix * position;
}
