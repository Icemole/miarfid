#version 420

$GLMatrices//?
$Lights
$Material

in vec3 N, L[4], V;

out vec4 fragColor;


// Los vectores N, L, V se entregan normalizados
vec4 iluminacion(vec3 N, vec3 L[4], vec3 V) {
    // Componente emisiva del material
    vec4 color = emissive;
    for (int i = 0; i < lights.length(); i++) {
        // Si la fuente est� apagada, no tenerla en cuenta
        if (lights[i].enabled == 0) {
            continue;
        }
        // Vector iluminaci�n (desde v�rtice a la fuente)
        // Multiplicador de la componente difusa
        float diffuseMult = max(0.0, dot(N, L[i]));
        float specularMult = 0.0;
        if (diffuseMult > 0.0) {
            // Multiplicador de la componente especular
            vec3 R = reflect(-L[i], N);
            specularMult = max(0.0, dot(R, V));
            specularMult = pow(specularMult, shininess);
        }
        float dist = length(L[i]);
        float attenDist = lights[i].attenuation[0] +
                lights[i].attenuation[1] * dist +
                lights[i].attenuation[2] * dist * dist;
        float attenFactor = 1 / max(1, attenDist);
        float spotlightEffect;
        if (lights[i].directional == 0 && lights[i].spotCutoff < 180.0) {
            // Luz focal
            // Se considera la dirección en el espacio de la cámara
            float spotlightBase = max(dot(-L[i], lights[i].spotDirectionEye), 0.0);
            if (spotlightBase >= lights[i].spotCosCutoff) {
                // El vértice está dentro del haz de luz
                spotlightEffect = pow(spotlightBase, lights[i].spotExponent);
            }
            else {
                spotlightEffect = 0.0;
            }
        }
        else {
            // Luz no focal
            spotlightEffect = 1.0;
            if (lights[i].directional != 0) {
                // Luz direccional
                attenFactor = 1.0;
            }
        }
        vec4 lightContrib = lights[i].ambient * ambient +
                lights[i].diffuse * diffuse * diffuseMult +
                lights[i].specular * specular * specularMult;
        lightContrib *= attenFactor * spotlightEffect;

        color += lightContrib;
    }

    return color;
}

void main() {
    vec3 Nnorm = normalize(N);
    vec3 Lnorm[4];
    for (int i = 0; i < lights.length(); i++) {
        Lnorm[i] = normalize(L[i]);
    }
    vec3 Vnorm = normalize(V);

    fragColor = iluminacion(Nnorm, Lnorm, Vnorm);
}
