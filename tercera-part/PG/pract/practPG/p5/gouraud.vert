#version 420

$GLMatrices
$Lights
$Material

in vec4 position;
in vec3 normal;

out vec4 color;


vec4 iluminacion(vec3 pos, vec3 N, vec3 V) {
    // Componente emisiva del material
    vec4 color = emissive;
    for (int i = 0; i < lights.length(); i++) {
        // Si la fuente est� apagada, no tenerla en cuenta
        if (lights[i].enabled == 0) {
            continue;
        }
        // Vector iluminaci�n (desde v�rtice a la fuente)
        vec3 L;
        if (lights[i].directional != 0.0) {
            // Luz direccional
            L = normalize(lights[i].positionEye.xyz);
        }
        else {
            // Luz no direccional
            L = normalize(vec3(lights[i].positionEye) - pos);
        }
        
        // Multiplicador de la componente difusa
        float diffuseMult = max(dot(N, L), 0.0);
        float specularMult = 0.0;
        if (diffuseMult > 0.0) {
            // Multiplicador de la componente especular
            vec3 R = reflect(-L, N);
            specularMult = max(0.0, dot(R, V));
            specularMult = pow(specularMult, shininess);
        }
        float dist = length(L);
        float attenDist = lights[i].attenuation[0] +
                lights[i].attenuation[1] * dist +
                lights[i].attenuation[2] * dist * dist;
        float attenFactor = 1 / max(1, attenDist);
        float spotlightEffect;
        if (lights[i].directional == 0 && lights[i].spotCutoff < 180.0) {
            // Luz focal
            // Se considera la dirección en el espacio de la cámara
            float spotlightBase = max(0.0, dot(-L, lights[i].spotDirectionEye));
            if (spotlightBase >= lights[i].spotCosCutoff) {
                // El vértice está dentro del haz de luz
                spotlightEffect = pow(spotlightBase, lights[i].spotExponent);
            }
            else {
                spotlightEffect = 0.0;
            }
        }
        else {
            // Luz no focal
            spotlightEffect = 1.0;
            if (lights[i].directional != 0) {
                // Luz direccional
                attenFactor = 1.0;
            }
        }
        vec4 lightContrib = lights[i].ambient * ambient +
                lights[i].diffuse * diffuse * diffuseMult +
                lights[i].specular * specular * specularMult;
        lightContrib *= attenFactor * spotlightEffect;

        color += lightContrib;
    }

    return color;
}

void main() {
    // Normal en el espacio de la c�mara
    vec3 eN = normalize(normalMatrix * normal);
    // V�rtice en el espacio de la c�mara
    vec3 eposition = vec3(modelviewMatrix * position);
    // Vector vista (desde v�rtice a la c�mara)
    vec3 V = normalize(-eposition.xyz);
    // C�lculo de la iluminaci�n
    color = iluminacion(eposition, eN, V);
    gl_Position = modelviewprojMatrix * position;
}
