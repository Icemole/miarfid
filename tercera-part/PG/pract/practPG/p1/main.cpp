#include "PGUPV.h"
#include "GUI3.h"

using namespace PGUPV;

using glm::vec3;
using std::vector;

class MyRender : public Renderer {
public:
	MyRender() = default;
	void setup(void) override;
	void render(void) override;
	void reshape(uint w, uint h) override;
	void update(uint) override;
private:
	void buildModels();
	std::shared_ptr<GLMatrices> mats;
	Axes axes;
	std::vector<std::shared_ptr<Model>> models;
	
	void buildGUI();
	std::shared_ptr<IntSliderWidget> modelSelector;
};

/**
 * Crea un cilindro, insertando los vértices correspondientes y sus respectivos colores
 * en los vectores pasados como parámetros.
 * Devuelve el número de vértices del cilindro.
 */
void makeCylinder(float r_bottom, float r_top, float height, float offsetY,
					uint stacks, uint slices,
					std::vector<glm::vec3> &vertices, std::vector<glm::vec4> &colores,
					std::vector<GLuint> &indices,
					glm::vec4 color)
{
	float delta = (float)(TWOPIf / slices);
	for (uint j = 0; j <= stacks; j++) {
		float angle = 0.0;
		float ring_y = height * ((float)j / stacks - 0.5f);
		float radius_ring = r_bottom - j * (r_bottom - r_top) / stacks;
		for (uint i = 0; i <= slices; i++, angle += delta) {
			float sin_a = sin(angle);
			float cos_a = cos(angle);

			vertices.push_back(
				glm::vec3(radius_ring * sin_a, ring_y + offsetY, radius_ring * cos_a));
			colores.push_back(
				color);	// Marroncito
			// normalesArbol1.push_back(
			// 	vec3(glm::rotate(glm::mat4(1.0f), angle, vec3(0.0f, 1.0f, 0.0f)) *
			// 		glm::vec4(0.0f, 0.0f, 1.0f, 0.0f)));
		}
	}

	// Dibujar la parte de abajo
	// Esto asume que la base es al menos triangular (buena asunción para un cilindro)
	uint beginningIndexBottom = slices / 2;
	indices.push_back(beginningIndexBottom);
	uint decreasingIndexBottom = beginningIndexBottom - 1;
	uint increasingIndexBottom = beginningIndexBottom + 1;
	for (uint i = 0; i <= (slices - 1) / 2; i++) {
		indices.push_back(decreasingIndexBottom--);
		indices.push_back(increasingIndexBottom++);
	}
	if ((slices / 2) % 2 == 0) {
		indices.push_back((uint)0);
	}
	indices.push_back((uint)-1);

	for (uint j = 0; j < stacks; j++) {
		uint topring = (j + 1) * (slices + 1);
		uint botring = j * (slices + 1);
		for (uint i = 0; i <= slices; i++) {
			indices.push_back(topring++);
			indices.push_back(botring++);
		}
		indices.push_back((uint)-1);
	}

	// Dibujar la parte de arriba
	// Esto asume que la base es al menos triangular (buena asunción para un cilindro)
	uint beginningIndexTop = (slices + 1) * stacks + slices / 2;
	indices.push_back(beginningIndexTop);
	uint decreasingIndexTop = beginningIndexTop - 1;
	uint increasingIndexTop = beginningIndexTop + 1;
	for (uint i = 0; i <= (slices - 1) / 2; i++) {
		indices.push_back(increasingIndexTop++);
		indices.push_back(decreasingIndexTop--);
	}
	if ((slices / 2) % 2 == 0) {
		indices.push_back((uint)((slices + 1) * stacks));
	}
	indices.push_back((uint)-1);
}

void makeSphere(float radius, float offsetY, uint stacks, uint slices,
					std::vector<glm::vec3> &vertices, std::vector<glm::vec4> &colores,
					std::vector<GLuint> &indices,
					glm::vec4 colorInicio, glm::vec4 colorCopa)
{
	if (stacks < 2)
		stacks = 2;

	float deltalong = (float)(2.0 * M_PI / slices);
	float deltalat = (float)(M_PI / stacks);

	glm::vec4 colorInterp = colorCopa - colorInicio;

	// Regular stacks
	float latitude = (float)(-M_PI / 2.0);
	for (uint j = 0; j <= stacks; j++, latitude += deltalat) {
		float sinlat = sin(latitude);
		float coslat = cos(latitude);
		float longitude = 0.0;
		float ring_y = radius * sinlat;
		for (uint i = 0; i <= slices; i++, longitude += deltalong) {
			float sinlon = sin(longitude);
			float coslon = cos(longitude);

			// vertices[k] =
			vertices.push_back(
				glm::vec3(radius * sinlon * coslat, ring_y + offsetY, radius * coslon * coslat));
			if (j == 0 && i == 0)
				colores.push_back(colorInicio);
			else if (j == stacks && i == slices)
				colores.push_back(colorCopa);
			else
				colores.push_back(colorInicio + colorInterp *(j / (float)stacks));
		}
	}

	// # of indices per stack:
	//  each stack has 2*slices triangles
	//  a triangle strip requires 2 + #triangles = 2 + 2*slices
	//  we end each slice with a -1 index to ask OpenGL to start a new
	//  tri-strip (+1)
	// uint indices_per_stack = 2 + 2 * slices + 1;

	for (uint j = 0; j < stacks; j++) {
		uint topring = (j + 1) * (slices + 1);
		uint botring = j * (slices + 1);
		for (uint i = 0; i <= slices; i++) {
			indices.push_back(topring++);
			indices.push_back(botring++);
		}
		indices.push_back((uint)-1);
	}
}

std::shared_ptr<Model> crearArbol(float troncoRBottom, float troncoRTop, float troncoHeight,
		float troncoOffsetY, uint troncoStacks, uint troncoSlices, glm::vec4 troncoColor,
		float hojasRadio, float hojasOffsetY, uint hojasStacks, uint hojasSlices,
		glm::vec4 hojasColorAbajo, glm::vec4 hojasColorArriba) {
	
	auto model = std::make_shared<Model>();

	/***************
	 ***************
	 **** C P U ****
	 ***************
	 ***************/
	/**********
	 * TRONCO *
	 **********/
	auto meshTronco = std::make_shared<Mesh>();
	std::vector<glm::vec3> verticesTronco;
	std::vector<glm::vec4> coloresTronco;
	std::vector<GLuint> indicesTronco;
	makeCylinder(troncoRBottom, troncoRTop, troncoHeight, troncoOffsetY,
				troncoStacks, troncoSlices,
				verticesTronco, coloresTronco, indicesTronco,
				troncoColor);
	meshTronco->addIndices(indicesTronco);
	meshTronco->addVertices(verticesTronco);
	meshTronco->addColors(coloresTronco);
	// Draw command para el tronco
	auto dc = new DrawElements(GL_TRIANGLE_STRIP, static_cast<GLsizei>(indicesTronco.size()), GL_UNSIGNED_INT, (void *)0);
	dc->setPrimitiveRestart();
	dc->setRestartIndex((uint)-1);
	meshTronco->addDrawCommand(dc);

	/*********
	 * HOJAS *
	 *********/
	auto meshHojas = std::make_shared<Mesh>();
	std::vector<glm::vec3> verticesHojas;
	std::vector<glm::vec4> coloresHojas;
	std::vector<GLuint> indicesHojas;
	makeSphere(hojasRadio, hojasOffsetY, hojasStacks, hojasSlices,
				verticesHojas, coloresHojas, indicesHojas,
				hojasColorAbajo, hojasColorArriba);
	meshHojas->addIndices(indicesHojas);
	meshHojas->addVertices(verticesHojas);
	meshHojas->addColors(coloresHojas);
	// Draw command para las hojas
	auto dc2 = new DrawElements(GL_TRIANGLE_STRIP, static_cast<GLsizei>(indicesHojas.size()), GL_UNSIGNED_INT, (void *)0);
	dc2->setPrimitiveRestart();
	dc2->setRestartIndex((uint)-1);
	meshHojas->addDrawCommand(dc2);

	// Añadir las mallas del árbol al modelo del árbol
	model->addMesh(meshTronco);
	model->addMesh(meshHojas);
	
	/***************
	 ***************
	 **** G P U ****
	 ***************
	 ***************/
	// auto mesh = std::make_shared<Mesh>();
	// std::vector<glm::vec3> vertices;
	// std::vector<glm::vec4> colores;
	// std::vector<GLuint> indices;
	// /**********
	//  * TRONCO *
	//  **********/
	// makeCylinder(troncoRBottom, troncoRTop, troncoHeight, troncoOffsetY,
	// 			troncoStacks, troncoSlices,
	// 			vertices, colores, indices,
	// 			troncoColor);
	// GLint numVerticesTronco = vertices.size();
	// GLint numIndicesTronco = indices.size();
	// /*********
	//  * HOJAS *
	//  *********/
	// makeSphere(hojasRadio, hojasOffsetY, hojasStacks, hojasSlices,
	// 			vertices, colores, indices,
	// 			hojasColorAbajo, hojasColorArriba);
	// GLint numVerticesHojas = vertices.size() - numVerticesTronco;
	// GLint numIndicesHojas = indices.size() - numIndicesTronco;
	// /***************************
	//  * RESERVAR MEMORIA EN GPU *
	//  ***************************/
	// GLuint buffers[2];
	// glGenBuffers(2, buffers);
	// GLuint vertexBuffer = buffers[0], indexBuffer = buffers[1];
	// // GLuint vertexBuffer, indexBuffer;
	// // glGenBuffers(1, &vertexBuffer);
	// // glGenBuffers(1, &indexBuffer);
	// // Bindear y cargar el buffer de indices en memoria de GPU
	// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	// glBufferData(indexBuffer, indices.size() * sizeof(GLuint),
	// 		indices.data(), GL_STATIC_DRAW);
	// // Bindear y cargar el buffer de vertices en memoria de GPU
	// glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	// glBufferData(vertexBuffer, vertices.size() * sizeof(glm::vec3),
	// 		vertices.data(), GL_STATIC_DRAW);

	// GLint drawCount[] = {numIndicesTronco, numIndicesHojas};
	// GLvoid *drawIndices[2] = {0, (GLvoid *)(numIndicesTronco * sizeof(GLuint))};
	// // Poblar la mesh
	// mesh->addIndices(indices);
	// mesh->addVertices(vertices);
	// mesh->addColors(colores);
	// auto multidc = new MultiDrawElements(GL_TRIANGLE_STRIP, drawCount,
	// 		GL_UNSIGNED_INT, drawIndices, static_cast<GLsizei>(2));
	// multidc->setPrimitiveRestart();
	// multidc->setRestartIndex((uint)-1);
	// mesh->addDrawCommand(multidc);
	// model->addMesh(mesh);

	return model;
}

/**
Construye tus modelos y añádelos al vector models (quita los ejemplos siguientes). Recuerda
que no puedes usar directamente las clases predefinidas (tienes que construir los meshes y los
models a base de vértices, colores, etc.)
*/
void MyRender::buildModels()
{
	auto modeloArbol1 = crearArbol(0.4, 0.2, 1, 0.5, 2, 5, glm::vec4(0.83f, 0.57f, 0.16f, 1.0f),
			0.6, 1.25, 5, 8, glm::vec4(0.2f, 0.64f, 0.16f, 1.0f), glm::vec4(0.4f, 0.94f, 0.3f, 1.0f));
	models.push_back(modeloArbol1);

	auto modeloArbol2 = crearArbol(0.2, 0.01, 1, 0.5, 2, 5, glm::vec4(0.83f, 0.57f, 0.16f, 1.0f),
			0.6, 1.25, 4, 6, glm::vec4(1.0f, 0.0f, 1.0f, 1.0f), glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
	models.push_back(modeloArbol2);

	auto modeloArbol3 = crearArbol(0.1, 0.07, 1, 0.5, 2, 5, glm::vec4(0.6f, 0.7f, 0.6f, 1.0f),
			0.6, 1.25, 4, 6, glm::vec4(0.95f, 0.57f, 0.25f, 1.0f), glm::vec4(0.95f, 0.57f, 0.25f, 1.0f));
	models.push_back(modeloArbol3);
}


void MyRender::setup() {
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	// Habilitamos el z-buffer
	glEnable(GL_DEPTH_TEST);
	// Habilitamos el back face culling. ¡Cuidado! Si al dibujar un objeto hay 
	// caras que desaparecen o el modelo se ve raro, puede ser que estés 
	// definiendo los vértices del polígono del revés (en sentido horario)
	// Si ese es el caso, invierte el orden de los vértices.
	// Puedes activar/desactivar el back face culling en cualquier aplicación 
	// PGUPV pulsando las teclas CTRL+B
	glEnable(GL_CULL_FACE);

	mats = PGUPV::GLMatrices::build();

	buildModels();

	// Establecemos una cámara que nos permite explorar el objeto desde cualquier
	// punto
	setCameraHandler(std::make_shared<OrbitCameraHandler>());

	// Construimos la interfaz de usuario
	buildGUI();
}

void MyRender::render() {
	// Borramos el buffer de color y el zbuffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Le pedimos a la cámara que nos de la matriz de la vista, que codifica la
	// posición y orientación actuales de la cámara.
	mats->setMatrix(GLMatrices::VIEW_MATRIX, getCamera().getViewMatrix());

	// Activamos un shader que dibuja cada vértice con su atributo color
	ConstantIllumProgram::use();

	// Dibujamos los ejes
	axes.render();

	// Dibujamos los objetos
	if (!models.empty()) {
		models[modelSelector->get()]->render();
	}

	// Si la siguiente llamada falla, quiere decir que OpenGL se encuentra en
	// estado erróneo porque alguna de las operaciones que ha ejecutado
	// recientemente (después de la última llamada a CHECK_GL) ha tenido algún
	// problema. Revisa tu código.
	CHECK_GL();
}

void MyRender::reshape(uint w, uint h) {
	glViewport(0, 0, w, h);
	mats->setMatrix(GLMatrices::PROJ_MATRIX, getCamera().getProjMatrix());
}

// Este método se ejecuta una vez por frame, antes de llamada a render. Recibe el 
// número de milisegundos que han pasado desde la última vez que se llamó, y se suele
// usar para hacer animaciones o comprobar el estado de los dispositivos de entrada
void MyRender::update(uint) {
	// Si el usuario ha pulsado el espacio, ponemos la cámara en su posición inicial
	if (App::isKeyUp(PGUPV::KeyCode::Space)) {
		getCameraHandler()->resetView();
	}
}


/**
En éste método construimos los widgets que definen la interfaz de usuario. En esta
práctica no tienes que modificar esta función.
*/
void MyRender::buildGUI() {
	auto panel = addPanel("Modelos");
	modelSelector = std::make_shared<IntSliderWidget>("Model", 0, 0, static_cast<int>(models.size()-1));

	if (models.size() <= 1) {
		panel->addWidget(std::make_shared<Label>("Introduce algún modelo más en el vector models..."));
	}
	else {
		panel->addWidget(modelSelector);
	}
	App::getInstance().getWindow().showGUI();
}

int main(int argc, char* argv[]) {
	App& myApp = App::getInstance();
	myApp.initApp(argc, argv, PGUPV::DOUBLE_BUFFER | PGUPV::DEPTH_BUFFER |
		PGUPV::MULTISAMPLE);
	myApp.getWindow().setRenderer(std::make_shared<MyRender>());
	return myApp.run();
}
