Para hacer esta práctica he seguido los siguientes pasos:
- Me he basado en el código de stockModels.cpp para dibujar
  un cilindro que actuara a modo de tronco, y una esfera que
  actuara a modo de hojas del árbol.
- Con este código, he hecho tres modelos:
    - Un árbol normal: tronco marrón ancho, hojas verdes.
    - Un árbol fantástico: tronco marrón de tamaño medio por abajo estrechándose
      a medida que va hacia arriba, hojas con un degradado de rosa a amarillo
      (también he aplicado la interpolación de color
      entre los vértices del inicio de las hojas a la copa).
    - Un árbol otoñal: tronco gris estrecho, hojas naranjas.

Nota: he trasladado todos los árboles hacia arriba un poco para que
      empezasen en el (0, 0, 0)