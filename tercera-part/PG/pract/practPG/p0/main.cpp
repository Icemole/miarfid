#include "PGUPV.h"

using namespace PGUPV;

/* 
Rellena las funciones setup y render tal y como se explica en el enunciado de la pr�ctica.
�Cuidado! NO uses las llamadas de OpenGL directamente (glGenVertexArrays, glBindBuffer, etc.).
Usa las clases Model y Mesh de PGUPV.
*/

class MyRender : public Renderer {
public:
	void setup(void);
	void render(void);
	void reshape(uint w, uint h);

private:
	Program program;
	Model model;
};

void MyRender::setup() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//GLfloat vertices[] = { -0.8, -0.8, 0.0, 0.5, -0.5, 0.0, 0.0, 0.5, 0.0 };
  	//GLfloat colores[] = { 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0 };

	// "vPosition" empieza en el número 0
	program.addAttributeLocation(0, "vPosition");

	// Cargar los ficheros de shader "triangles"
	program.loadFiles("../p0/triangles");

	// Compilar el programa
	program.compile();

	// Instalación del programa
  	program.use();

	// Declarar la mesh
	std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();
	glm::vec3 vertices[] = {
		//glm::vec3(-1, -1, 0.0), glm::vec3(-1, 1, 0.0), glm::vec3(1, 1, 0.0),
		glm::vec3(-0.95, -0.9, 0.0), glm::vec3(0.85, 0.9, 0.0), glm::vec3(-0.95, 0.9, 0.0),
		glm::vec3(-0.85, -0.9, 0.0), glm::vec3(0.95, -0.9, 0.0), glm::vec3(0.95, 0.9, 0.0)
	};
	// glm::vec4 colores[] = {
	// 	glm::vec4(0.0, 0.0, 1.0, 1.0), glm::vec4(0.0, 0.0, 1.0, 1.0), glm::vec4(0.0, 0.0, 1.0, 1.0),
	// 	glm::vec4(0.0, 0.0, 1.0, 1.0), glm::vec4(0.0, 0.0, 1.0, 1.0), glm::vec4(0.0, 0.0, 1.0, 1.0)
	// };
	mesh->addVertices(vertices, 6, GL_STATIC_DRAW);
	//mesh->addColors(colores, 6, GL_STATIC_DRAW);
	mesh->addDrawCommand(new PGUPV::DrawArrays(GL_TRIANGLES, 0, 6));
	model.addMesh(mesh);
}

void MyRender::render() {
	glClear(GL_COLOR_BUFFER_BIT);

	// Instalación del programa
  	// program.use();
	
	// Dibujo del modelo
	model.render();
}

void MyRender::reshape(uint w, uint h) {
	glViewport(0, 0, w, h);
}

int main(int argc, char *argv[]) {
	App &myApp = App::getInstance();
	myApp.initApp(argc, argv, PGUPV::DOUBLE_BUFFER);
	myApp.getWindow().setRenderer(std::make_shared<MyRender>());
	return myApp.run();
}