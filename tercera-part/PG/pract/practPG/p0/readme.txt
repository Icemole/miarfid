Esta práctica estaba guiada. Esencialmente, he hecho lo que dice en el boletín:
- He cargado el shader, lo he compilado y lo he usado.
- He declarado una mesh con dos triángulos.
- He declarado un modelo al que le he añadido la mesh anterior (dibujada mediante GL_TRIANGLES).
- En el método render() he dibujado el modelo de los dos triángulos.