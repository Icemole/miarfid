#version 420 core

uniform sampler2D texUnit;

in vec2 texCoordFrag;
out vec4 fragColor;

void main() { 
    fragColor = texture(texUnit, texCoordFrag);
    // Pasar a escala de grises según la fórmula de la práctica
    float myColor = 0.3 * fragColor.r + 0.59 * fragColor.g + 0.11 * fragColor.b;
    // Manipular el color dependiendo de la posición en la ventana
    uint pixelNum = int(gl_FragCoord.y) % 4;
    if (pixelNum == 0 || pixelNum == 1) {
        myColor *= 0.2;
    }
    else if (pixelNum == 2) {
        myColor *= 0.8;
    }
    fragColor = vec4(myColor, myColor, myColor, 1);
}
