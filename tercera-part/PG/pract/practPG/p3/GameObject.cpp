
#include "GameObject.h"

GameObject::GameObject(std::shared_ptr<PGUPV::Model> m, glm::vec3 pos, glm::vec3 vel)
        : GameObject::GameObject(m, pos, vel, nullptr, 0, glm::vec3(1), glm::vec3(1)) {}

GameObject::GameObject(std::shared_ptr<PGUPV::Model> m, glm::vec3 pos,
        glm::vec3 vel, std::shared_ptr<Texture2D> tex)
        : GameObject::GameObject(m, pos, vel, tex, 0, glm::vec3(1), glm::vec3(1)) {}


GameObject::GameObject(std::shared_ptr<PGUPV::Model> m, glm::vec3 pos,
        glm::vec3 vel, std::shared_ptr<Texture2D> tex,
        float rotAng, glm::vec3 rotAxis, glm::vec3 scaleFactor) {
    setModel(m);
    setPosition(pos);
    setVelocity(vel);
    setTexture(tex);
    setRotationAngle(rotAng);
    setRotationAxis(rotAxis);
    setScaleFactor(scaleFactor);
}

std::shared_ptr<Model> GameObject::getModel() {
    return this->model;
}

glm::vec3 GameObject::getPosition() {
    return this->pos;
}

glm::vec3 GameObject::getVelocity() {
    return this->vel;
}

std::shared_ptr<Texture2D> GameObject::getTexture() {
    return this->tex;
}

float GameObject::getRotationAngle() {
    return this->rotAng;
}

glm::vec3 GameObject::getRotationAxis() {
    return this->rotAxis;
}

glm::vec3 GameObject::getScaleFactor() {
    return this->scaleFactor;
}

void GameObject::setModel(std::shared_ptr<PGUPV::Model> m) {
    this->model = m;
}

void GameObject::setPosition(glm::vec3 pos) {
    this->pos = pos;
}

void GameObject::setVelocity(glm::vec3 vel) {
    this->vel = vel;
}

void GameObject::setTexture(std::shared_ptr<Texture2D> tex) {
    this->tex = tex;
}

void GameObject::setRotationAngle(float rotAng) {
    this->rotAng = rotAng;
}

void GameObject::setRotationAxis(glm::vec3 rotAxis) {
    this->rotAxis = rotAxis;
}

void GameObject::setScaleFactor(glm::vec3 scaleFactor) {
    this->scaleFactor = scaleFactor;
}

void GameObject::update(uint ms) {
    this->pos += this->vel * (ms / 1000.0f);
}

void GameObject::render() {
    model->render();
}