
#include "PGUPV.h"

#define GameObject_H

using namespace PGUPV;

/**
 * Clase que modela los objetos en la escena con
 * un modelo, una posición y una velocidad.
 */
class GameObject {
public:
    GameObject() {};
    GameObject(std::shared_ptr<Model> m, glm::vec3 pos, glm::vec3 vel);
    GameObject(std::shared_ptr<PGUPV::Model> m, glm::vec3 pos,
        glm::vec3 vel, std::shared_ptr<Texture2D> tex);
    GameObject(std::shared_ptr<Model> m, glm::vec3 pos, glm::vec3 vel,
            std::shared_ptr<Texture2D> tex, float rotAng, glm::vec3 rotAxis,
            glm::vec3 scaleFactor);
    // Getters
    std::shared_ptr<Model> getModel();
    glm::vec3 getPosition();
    glm::vec3 getVelocity();
    std::shared_ptr<Texture2D> getTexture();
    float getRotationAngle();
    glm::vec3 getRotationAxis();
    glm::vec3 getScaleFactor();
    // Setters
    void setModel(std::shared_ptr<Model> m);
    void setPosition(glm::vec3 pos);
    void setVelocity(glm::vec3 vel);
    void setTexture(std::shared_ptr<Texture2D> tex);
    void setRotationAngle(float rotAng);
    void setRotationAxis(glm::vec3 rotAxis);
    void setScaleFactor(glm::vec3 scaleFactor);
	void update(uint ms);
    void render();
private:
    std::shared_ptr<Model> model;
	glm::vec3 pos;
    glm::vec3 vel;
    std::shared_ptr<Texture2D> tex;
    float rotAng;
    glm::vec3 rotAxis;
    glm::vec3 scaleFactor;
};