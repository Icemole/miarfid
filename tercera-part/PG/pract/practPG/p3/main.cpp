
#include "PGUPV.h"

#include "GameObject.h"
#include "ObjectPool.h"


using namespace PGUPV;

using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;
using std::vector;

class MyRender : public Renderer {
public:
	MyRender()
    	: anisotropyExtAvailable(isGLExtensionAvailable(
      	"GL_EXT_texture_filter_anisotropic")) {};
	void setup(void) override;
	void render(void) override;
	void reshape(uint w, uint h) override;
	void update(uint ms) override;
private:
	std::shared_ptr<GLMatrices> mats;
	// Indica si la extensión está disponible en la máquina
	bool anisotropyExtAvailable;

	// Todos los objetos que están en el pool no van dentro de gameObjects
	ObjectPool treePool;
	std::vector<std::shared_ptr<GameObject>> gameObjects;
	// Las carreteras necesitan estar fuera de gameObjects
	// ya que tienen un update() especial.
	std::shared_ptr<GameObject> carreteraCerca, carreteraLejos;
	std::shared_ptr<Texture2D> texturaCarretera;

	// UI
	std::vector<std::shared_ptr<GameObject>> gameObjectsUI;
	std::shared_ptr<Texture2D> texturaUICoche;
	std::shared_ptr<GameObject> panelUICoche;
	// El parabrisas necesita estar fuera de gameObjectsUI
	// ya que tiene un tratamiento especial (dibujado del stencil).
	std::shared_ptr<GameObject> parabrisas;
	std::shared_ptr<Texture2D> texturaTetraedro;
	std::shared_ptr<GameObject> tetraedro;
	float tetraedroRotAngle = 0.0f;
	// Ejercicio opcional: insertar un vídeo
	// Esto va fuera del gameObjectsUI porque GameObject no soporta TextureVideo
	// Program ashader;
	GLint texUnitLoc, frameUnitLoc;
	uint frame;
	std::shared_ptr<GameObject> video;
	std::shared_ptr<TextureVideo> videoTexture;

	void drawUI();
	void buildCanonicalView();
	void destroyCanonicalView();
};

void MyRender::setup() {
	glClearColor(1.f, 1.f, 1.f, 1.0f);
	glEnable(GL_DEPTH_TEST);

	mats = GLMatrices::build();

	// Instalar el shader para dibujar los ejes, las normales y la luz
	ConstantIllumProgram::use();

	// Instalamos un shader que dibuja los modelos aplicándoles la textura activa
	// Por defecto usa la unidad de textura 0, pero puedes cambiarla con la llamada
	// TextureReplaceProgram::setTextureUnit(1), para usar la textura en GL_TEXTURE1
	// TextureReplaceProgram::use();

	setCameraHandler(std::make_shared<OrbitCameraHandler>(5.0f));

	// Preparar el blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/**********
	 **  UI  **
	 **********/
	// Tetraedro (Wikipedia ftw)
	std::vector<glm::vec3> verticesTetr = {
        glm::vec3(0, 1.333, 0) * 0.125f,
        glm::vec3(0.943, 0, 0) * 0.125f,
        glm::vec3(-0.471, 0, 0.816) * 0.125f,
        glm::vec3(-0.471, 0, -0.816) * 0.125f,
		// Vértice 0 repetido dos veces (para variar las coordenadas de textura)
		glm::vec3(0, 1.333, 0) * 0.125f,
		glm::vec3(0, 1.333, 0) * 0.125f
    };
    std::vector<GLuint> indicesTetr = {
		1, 2, 0, 4, 3, 1, 3, 5, 2, 2, 1, 3
    };
	std::vector<glm::vec4> coloresTetr = {
		glm::vec4(1, 0, 0, 1), glm::vec4(0, 1, 0, 1), glm::vec4(0, 0, 1, 1),
		glm::vec4(1, 1, 1, 1), glm::vec4(1, 0, 0, 1), glm::vec4(1, 0, 0, 1)
	};
	// Coordenadas de textura
	std::vector<glm::vec2> tcTetr = {
		glm::vec2(0.5, 1), glm::vec2(0.75, 0.5), glm::vec2(0.25, 0.5),
		glm::vec2(0.5, 0), glm::vec2(1, 0), glm::vec2(0, 0)
	};
	// Modelo del tetraedro
	auto modelTetraedro = std::make_shared<Model>();
	// Mesh del tetraedro
	// Mi implementación anterior con 4 meshes: https://pbs.twimg.com/media/EPesTVpWsAAm2gh.png
	std::shared_ptr<Mesh> meshTetr;
	meshTetr = std::make_shared<Mesh>();
	meshTetr->addVertices(verticesTetr);
	meshTetr->addIndices(indicesTetr);
	meshTetr->addTexCoord(0, tcTetr);
	meshTetr->addColors(coloresTetr);
	meshTetr->addDrawCommand(new PGUPV::DrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid *)0));
	modelTetraedro->addMesh(meshTetr);
	// Textura del tetraedro
	texturaTetraedro = std::make_shared<Texture2D>();
	texturaTetraedro->loadImage("../p3/tetraedro.png", GL_RGBA);
	tetraedro = std::make_shared<GameObject>(modelTetraedro, glm::vec3(0.75f, 0.5f, 0.5f), glm::vec3(0), texturaTetraedro, 0, glm::vec3(1, 0, 0), glm::vec3(1));
	gameObjectsUI.push_back(tetraedro);

	// Parabrisas
	std::vector<glm::vec3> verticesParabrisas = {
		// Coordenadas en el cubo canónico 2x2x2
		glm::vec3(1.0f, 0.78378f, 1), glm::vec3(1.0f, -0.021337f, 1),
		glm::vec3(-0.55197f, -0.03841f, 1), glm::vec3(-0.79941f, -0.06401f, 1),
		glm::vec3(-0.87262f, 0.49360f, 1), glm::vec3(-0.91801f, 0.68421f, 1),
		glm::vec3(-0.40264f, 0.74111f, 1)
	};
	std::vector<GLuint> indicesParabrisas = {
		0, 6, 5, 4, 3, 2, 1
	};
	auto meshParabrisas = std::make_shared<Mesh>();
	meshParabrisas->addVertices(verticesParabrisas);
	meshParabrisas->addIndices(indicesParabrisas);
	meshParabrisas->setColor(glm::vec4(0, 0.67, 1, 0.125));
	meshParabrisas->addDrawCommand(new PGUPV::DrawElements(GL_TRIANGLE_FAN, 7, GL_UNSIGNED_INT, (GLvoid *)0));
	auto modelParabrisas = std::make_shared<Model>();
	modelParabrisas->addMesh(meshParabrisas);
	parabrisas = std::make_shared<GameObject>(modelParabrisas, glm::vec3(0, 0, 0), glm::vec3(0, 0, 0));
	gameObjectsUI.push_back(parabrisas);

	// Texturas UI
	texturaUICoche = std::make_shared<Texture2D>();
	texturaUICoche->loadImage("../p3/car.png", GL_RGBA);
	glm::vec2 tc[] = {
		glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 0.0f),
	  	glm::vec2(1.0f, 1.0f), glm::vec2(0.0f, 1.0f)
	};
	auto r = std::make_shared<Rect>(2, 2, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	r->getMesh(0).addTexCoord(0, tc, 4);
	panelUICoche = std::make_shared<GameObject>(r, glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), texturaUICoche);
	gameObjectsUI.push_back(panelUICoche);

	// Vídeo texturado
	videoTexture = std::make_shared<TextureVideo>("../p3/duck.mp4");
	std::shared_ptr<Rect> v = std::make_shared<Rect>(2, 2);
	v->getMesh(0).addTexCoord(0, tc, 4);
	video = std::make_shared<GameObject>(v, glm::vec3(0.825, 0.1, 0), glm::vec3(0), nullptr, 0, glm::vec3(1), glm::vec3(0.125f));

	/**************
	 *** ESCENA ***
	 **************/
	// Figuras adicionales
	// ...
	
	// Texturas adicionales
	texturaCarretera = std::make_shared<Texture2D>();
	texturaCarretera->loadImage("../recursos/imagenes/asfalto.png");
	// Texturizar la carretera
	glm::vec2 tcdouble[] = {
		// El .png está girado 90 grados -> empezamos primero por la T
		glm::vec2(0.0f, 0.0f), glm::vec2(0.0f, 1.0f),
	  	glm::vec2(6.0f, 1.0f), glm::vec2(6.0f, 0.0f)
	};
	Rect carretera = Rect(treePool.TRACK_WIDTH, 50, glm::vec4(0.0f, 0.0f, 1.0f, 0.5f));
	carretera.getMesh(0).addTexCoord(0, tcdouble, 4);
	auto ptrCarretera = std::make_shared<Rect>(carretera);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	carreteraCerca = std::make_shared<GameObject>(ptrCarretera, glm::vec3(0, -treePool.EYE_POS.y, -25), treePool.velocity, texturaCarretera, -PIf / 2.0f, glm::vec3(1, 0, 0), glm::vec3(1));
	carreteraLejos = std::make_shared<GameObject>(ptrCarretera, glm::vec3(0, -treePool.EYE_POS.y, -75), treePool.velocity, texturaCarretera, -PIf / 2.0f, glm::vec3(1, 0, 0), glm::vec3(1));
	gameObjects.push_back(carreteraCerca);
	gameObjects.push_back(carreteraLejos);

	// Aplicar anisotropía si está disponible
	if (anisotropyExtAvailable) {
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 2.0f);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);  // GL_LINEAR_MIPMAP_LINEAR
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	// Preparar el buffer de stencil
	glEnable(GL_STENCIL_TEST);
}

/*
Tienes que completar esta función para dibujar la escena.

*/
void MyRender::render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	treePool.render(mats);
	
	for (size_t i = 0; i < gameObjects.size(); i++) {
		mats->pushMatrix(GLMatrices::MODEL_MATRIX);
		mats->translate(GLMatrices::MODEL_MATRIX, gameObjects[i]->getPosition());
		mats->rotate(GLMatrices::MODEL_MATRIX, gameObjects[i]->getRotationAngle(), gameObjects[i]->getRotationAxis());
		mats->scale(GLMatrices::MODEL_MATRIX, gameObjects[i]->getScaleFactor());
		if (gameObjects[i]->getTexture() == nullptr) {
			ConstantIllumProgram::use();
		}
		else {
			TextureReplaceProgram::use();
			gameObjects[i]->getTexture()->bind(GL_TEXTURE0);
		}
		gameObjects[i]->render();
		mats->popMatrix(GLMatrices::MODEL_MATRIX);
	}

	drawUI();

	CHECK_GL();
}

void MyRender::buildCanonicalView() {
	// Preparar el cubo canónico 2x2x2
	mats->loadIdentity(GLMatrices::PROJ_MATRIX);
	// Ajustar la vista
	mats->pushMatrix(GLMatrices::VIEW_MATRIX);
	mats->setMatrix(GLMatrices::VIEW_MATRIX,
			glm::lookAt(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f),
					glm::vec3(0.0f, 1.0f, 0.0f)));
}

void MyRender::destroyCanonicalView() {
	// Dejar las matrices como estaban
	mats->popMatrix(GLMatrices::VIEW_MATRIX);
	mats->setMatrix(GLMatrices::PROJ_MATRIX, getCamera().getProjMatrix() + glm::mat4(glm::vec4(0, 0, 0, 0), glm::vec4(0, 0, 0, 0), glm::vec4(0, -0.125f, 0, 0), glm::vec4(-0.325f, -0.25f, 0, 0)));
	// Reverse engineering time
	// getCamera().getProjMatrix():
	// - Primera fila: ancho -> width
	// - Segunda fila: largo -> height
	// - Tercera fila: punto de mira
	// - Cuarta fila: posición del ojo
}

void MyRender::reshape(uint w, uint h) {
	glViewport(0, 0, w, h);

	/***************
	 *** STENCIL ***
	 ***************/
	// No queremos que la profundidad tenga nada que ver con la modificación
	// del buffer de stencil
	glDisable(GL_DEPTH_TEST);
	
	// Modificar stencil
	glClear(GL_STENCIL_BUFFER_BIT);
	glStencilFunc(GL_ALWAYS, 0x1, 0XFF);
	glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
	
	buildCanonicalView();
	
	parabrisas->render();  // El protagonista

	destroyCanonicalView();

	// De ahora en adelante, dibujar únicamente por encima de la parabrisas
	glStencilFunc(GL_EQUAL, 0x1, 0xFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	glEnable(GL_DEPTH_TEST);
}

void MyRender::update(uint ms) {
	treePool.update(ms);

	// carreteraCerca->update(ms);
	// carreteraLejos->update(ms);

	for (size_t i = 0; i < gameObjects.size(); i++) {
		gameObjects[i]->update(ms);
	}
	if (carreteraCerca->getPosition().z > 0.0f) {
		// El coche ha pasado el borde de la primera carretera y se dirige a la segunda.
		// Se reinician las posiciones de las carreteras
		carreteraCerca->setPosition(carreteraCerca->getPosition() + glm::vec3(0, 0, -treePool.TRACK_WIDTH));
		carreteraLejos->setPosition(carreteraLejos->getPosition() + glm::vec3(0, 0, -treePool.TRACK_WIDTH));
	}

	tetraedroRotAngle += ms / 2000.0f;
	if (tetraedroRotAngle > TWOPIf) {
		tetraedroRotAngle -= TWOPIf;
	}
	tetraedro->setRotationAngle(tetraedroRotAngle);

	// static uint total = 0;
	// total += ms;
	// // Si han pasado 250 ms desde la última actualización, cambiamos de frame
	// if (total > 250) {
	// 	total = 0;
	// 	frame++;
	// 	if (frame == videoTexture->getDepth())
	// 	frame = 0;
	// 	glUniform1i(frameUnitLoc, frame);
	// }
}

/**
 * Sale con el programa ConstantIllumProgram activo.
 */
void MyRender::drawUI() {
	glDisable(GL_STENCIL_TEST);
	// No se puede desactivar la profundidad
	// porque el tetraedro se dibuja aquí
	// glDisable(GL_DEPTH_TEST);

	buildCanonicalView();

	TextureReplaceProgram::use();
	// ashader.use();
	mats->pushMatrix(GLMatrices::MODEL_MATRIX);
	mats->translate(GLMatrices::MODEL_MATRIX, video->getPosition());
	mats->rotate(GLMatrices::MODEL_MATRIX, video->getRotationAngle(), video->getRotationAxis());
	mats->scale(GLMatrices::MODEL_MATRIX, video->getScaleFactor());
	videoTexture->bind(GL_TEXTURE0);
	video->render();
	mats->popMatrix(GLMatrices::MODEL_MATRIX);
	for (size_t i = 0; i < gameObjectsUI.size(); i++) {
		mats->pushMatrix(GLMatrices::MODEL_MATRIX);
		mats->translate(GLMatrices::MODEL_MATRIX, gameObjectsUI[i]->getPosition());
		mats->rotate(GLMatrices::MODEL_MATRIX, gameObjectsUI[i]->getRotationAngle(), gameObjectsUI[i]->getRotationAxis());
		mats->scale(GLMatrices::MODEL_MATRIX, gameObjectsUI[i]->getScaleFactor());
		if (gameObjectsUI[i]->getTexture() == nullptr) {
			ConstantIllumProgram::use();
		}
		else {
			TextureReplaceProgram::use();
			gameObjectsUI[i]->getTexture()->bind(GL_TEXTURE0);
		}
		gameObjectsUI[i]->render();
		mats->popMatrix(GLMatrices::MODEL_MATRIX);
	}

	destroyCanonicalView();

	// glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	ConstantIllumProgram::use();
}

int main(int argc, char* argv[]) {
	App& myApp = App::getInstance();
	myApp.setInitWindowSize(800, 600);
	myApp.initApp(argc, argv, PGUPV::DOUBLE_BUFFER | PGUPV::DEPTH_BUFFER | PGUPV::MULTISAMPLE
			| PGUPV::STENCIL_BUFFER);
	myApp.getWindow().setRenderer(std::make_shared<MyRender>());
	return myApp.run();
}
