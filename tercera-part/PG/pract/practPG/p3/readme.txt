- Me he bajado una textura car.jpg y le he aplicado transparencia.
  He adecuado el parabrisas a esta textura, y he adecuado el código de dibujo en el stencil.
  Esta textura la dibujo después de renderizar toda la escena (sin modificar el stencil).
- He usado la clase GameObject de p2 para hacer dos (y solo dos) trozos de carretera, a los cuales
  les he puesto coordenadas de textura y les he dibujado la carretera.
    - Estos trozos de carretera se van desplazando hacia la cámara. Cuando la cámara
      se encuentra entre una carretera y otra, se reinician las posiciones de las dos carreteras.
- Con la ayuda de wikipedia y stockModels2.cpp he desarrollado un tetraedro
  con cuatro meshes, a las cuales les he aplicado coordenadas de textura.
  He dibujado una textura muy básica (tetraedro.png) y se la he aplicado al tetraedro.
- También he hecho el ejercicio adicional de dibujar una textura animada, he usado la clase
  TextureVideo. La textura se puede encontrar en la parte derecha de la escena.