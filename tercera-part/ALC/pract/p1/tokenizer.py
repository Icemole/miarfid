import re, sys


# hola | pseudo-código
regexGeneric = r"\w+(-\w+)?"

# 12,34 | 69.420
regexFloats = r"\d+[.,\/]\d+"

regexMonth = r"(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre)"
# 12/04 | 11-09 | 02-02-2020 | 32 de febrero de 2022
regexDates = r"\d{2}[\/-]\d{2}([\/-]\d{2,4})?|\d{2} de " + regexMonth + " de \d{4}" #TODO: sustituir primer [/-] por (/|-) y segundo por \1 (?)

# 9:30 | 20:42
regexTime = r"\d{1,2}:\d{1,2}"

# https://miweb.otrascosas.muchasmascosas.org | www.miweb.com
regexWebsites = r"(http(s)?://)(www.)?(\w+.)+\w+(\/(~)?\w+)*/?"

# myemail@mydomain.org
regexEmails = r"\w+@(\w+\.)+\w+"

# @myusername | #bless | #bless-you
regexTwitterSpecific = r"[@#]\w+((-\w+)*)?"

# K.E.K. | KK.EE.KK.
regexAcronyms = r"([A-Z]+\.)+"

# Emoji range: \U00010000-\U0010ffff
regexEmojis = r"[\U00010000-\U0010ffff]"

# ... | . | , ETC
regexSeparator = r"\.\.\.|[().,'\"?¿!¡;:%]"

regexes = [
    regexFloats,
    regexDates,
    regexTime,
    regexWebsites,
    regexEmails,
    regexTwitterSpecific,
    regexAcronyms,
    regexEmojis,
    regexSeparator,
    regexGeneric
]
regex = re.compile("|".join(regexes), re.U)

with open(sys.argv[1], "r") as f:
    for line in f:
        print(line)
        result = regex.finditer(line)
        for i in result:
            print (line[i.start():i.end()])
    print()