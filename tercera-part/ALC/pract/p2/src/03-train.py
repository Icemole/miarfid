from sklearn import svm, linear_model, neural_network
from sklearn.metrics import classification_report
import pickle
import argparse


def load_data(data_dir, polarized_data):
    train_data = []
    dev_data = []

    polarized_suffix = ""
    if polarized_data:
        polarized_suffix = "_polarized"
    
    with open(data_dir + "/train" + polarized_suffix + ".vec", "rb") as f:
        train_data, train_labels = pickle.load(f)
    
    with open(data_dir + "/dev" + polarized_suffix + ".vec", "rb") as f:
        dev_data, dev_labels = pickle.load(f)
    
    return train_data, train_labels, dev_data, dev_labels


def train_classifier(train_data, train_labels, c, degree, model_out):
    classifier = svm.LinearSVC(C=c)
    # classifier = svm.SVC(C=1000.0, kernel="rbf")
    # classifier = linear_model.Perceptron(alpha=c)
    # classifier = neural_network.MLPClassifier(alpha=1e-5, hidden_layer_sizes=(512, 128, 32))
    classifier.fit(train_data, train_labels)

    with open(model_out, "wb") as f:
        pickle.dump(classifier, f)

    return classifier


def predict_data(predict_data, predict_labels, classifier):
    prediction = classifier.predict(predict_data)
    print(classification_report(predict_labels, prediction))


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_dir", default="data/vec/",
            help="Directory where to get vectorized data from.")
    parser.add_argument("--c", type=float, default=0.1,
            help="C parameter for SVM.")
    parser.add_argument("--degree", type=int,
            help="Degree for poly SVM.")
    parser.add_argument("--model_out", default="models/svm.bin",
            help="File where to store the resulting classifier.")
    parser.add_argument("--polarized", type=str2bool, nargs='?',
            const=True, default=False,
            help="Set this if you want to train with polarized data." + 
                    "NOTE: this only looks for a '[part]_polarized.vec file.")

    args = parser.parse_args()

    train_data, train_labels, dev_data, dev_labels = load_data(args.data_dir, args.polarized)
    classifier = train_classifier(train_data, train_labels, args.c, args.degree, args.model_out)
    predict_data(dev_data, dev_labels, classifier)


if __name__ == "__main__":
    main()