import re
from sklearn.feature_extraction.text import CountVectorizer, HashingVectorizer, TfidfVectorizer
import numpy as np
import scipy
import pickle
import argparse


def get_polarity_dicts(polarity_dict_file):
    positive_words = set()
    negative_words = set()

    with open(polarity_dict_file, "r") as f:
        for line in f:
            try:
                word, polarity = line.strip().split("\t")
                if polarity == "positive":
                    positive_words.add(word)
                else:
                    negative_words.add(word)
            except ValueError:
                pass
    
    return positive_words, negative_words


def load_data(data_dir, polarity_dict_file):
    add_polarity_info = polarity_dict_file != ""
    if add_polarity_info:
        positive_words, negative_words = get_polarity_dicts(polarity_dict_file)
    else:
        positive_words = negative_words = None

    train_text = []
    train_labels = []
    with open(data_dir + "/train.tok.txt", "r") as f:
        for line in f:
            label, content = line.strip().split("\t")
            train_text.append(content)
            train_labels.append(label)
    
    dev_text = []
    dev_labels = []
    with open(data_dir + "/dev.tok.txt", "r") as f:
        for line in f:
            label, content = line.strip().split("\t")
            dev_text.append(content)
            dev_labels.append(label)
    
    test_text = []
    test_ids = []
    with open(data_dir + "/test.tok.txt", "r") as f:
        for line in f:
            tweet_id, content = line.strip().split("\t")
            test_text.append(content)
            test_ids.append(tweet_id)
    
    return train_text, train_labels, dev_text, dev_labels, test_text, test_ids, \
            positive_words, negative_words
    

def vectorize_data(train_text, dev_text, test_text,
        positive_words, negative_words, vectorization_type, ngram_upper_limit, analyzer):

    add_polarity_info = positive_words is not None and negative_words is not None

    if vectorization_type == "count":
        vectorizer = CountVectorizer(ngram_range=(1, ngram_upper_limit), analyzer=analyzer)
    elif vectorization_type == "hashing":
        vectorizer = HashingVectorizer(ngram_range=(1, ngram_upper_limit), analyzer=analyzer)
    elif vectorization_type == "tfidf":
        vectorizer = TfidfVectorizer(ngram_range=(1, ngram_upper_limit), analyzer=analyzer)

    train_vectors = vectorizer.fit_transform(train_text)
    dev_vectors = vectorizer.transform(dev_text)
    test_vectors = vectorizer.transform(test_text)

    if add_polarity_info:
        train_polarities = np.zeros(shape=(len(train_text), 2))
        for i, tweet in enumerate(train_text):
            for word in tweet.strip().split():
                if word in positive_words:
                    train_polarities[i, 0] += 1
                elif word in negative_words:
                    train_polarities[i, 1] += 1
        train_mat = scipy.sparse.hstack((train_vectors, train_polarities))

        dev_polarities = np.zeros(shape=(len(dev_text), 2))
        for i, tweet in enumerate(dev_text):
            for word in tweet.strip().split():
                if word in positive_words:
                    dev_polarities[i, 0] += 1
                elif word in negative_words:
                    dev_polarities[i, 1] += 1
        dev_mat = scipy.sparse.hstack((dev_vectors, dev_polarities))

        test_polarities = np.zeros(shape=(len(test_text), 2))
        for i, tweet in enumerate(test_text):
            for word in tweet.strip().split():
                if word in positive_words:
                    test_polarities[i, 0] += 1
                elif word in negative_words:
                    test_polarities[i, 1] += 1
        test_mat = scipy.sparse.hstack((test_vectors, test_polarities))
    else:
        train_mat = train_vectors
        dev_mat = dev_vectors
        test_mat = test_vectors
    
    return train_mat, dev_mat, test_mat


def write_data(train_mat, train_labels, dev_mat, dev_labels, test_mat, test_ids, write_to):
    # TODO: write data with pickle grouped in (train_mat, train_labels) -> train.vec
    with open(write_to + "/train.vec", "wb") as f:
        pickle.dump((train_mat, train_labels), f)
    
    with open(write_to + "/dev.vec", "wb") as f:
        pickle.dump((dev_mat, dev_labels), f)
    
    with open(write_to + "/test.vec", "wb") as f:
        pickle.dump((test_mat, test_ids), f)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_dir", default="data/tok/",
            help="Directory where to read tokenized data in txt format.")
    parser.add_argument("--write_to", default="data/vec/",
            help="Directory where to write data in vec format.")
    parser.add_argument("--polarity_dict_file", default="", # other/ElhPolar_esV1.lex
            help="File that represents the polarity dictionary.")
    parser.add_argument("--vectorization_type", choices=["count", "hashing", "tfidf"],
            default="count", help="Type of vectorization applied to the data.")
    parser.add_argument("--ngram_upper_limit", type=int, default=1,
            help="Upper boundary of ngram to consider in the vectorization.")
    parser.add_argument("--analyzer", choices=["word", "char", "char_wb"], default="word",
            help="What to consider when extracting the vectors.")
    args = parser.parse_args()

    train_text, train_labels, dev_text, dev_labels, test_text, test_ids, \
            positive_words, negative_words = load_data(args.data_dir, args.polarity_dict_file)
    
    train_mat, dev_mat, test_mat = vectorize_data(train_text, dev_text, test_text, 
            positive_words, negative_words, args.vectorization_type,
            args.ngram_upper_limit, args.analyzer)
    
    write_data(train_mat, train_labels, dev_mat, dev_labels, test_mat, test_ids, args.write_to)


if __name__ == "__main__":
    main()