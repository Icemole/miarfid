import argparse
from nltk.tokenize import TweetTokenizer
from bs4 import BeautifulSoup
import re


def extract_data_from_xmls(data_dir):
    train_data = []
    dev_data = []
    test_data = []

    f = open(data_dir + "/TASS2017_T1_training.xml", "r")
    train_contents = f.read()
    f.close()
    for tweet in BeautifulSoup(train_contents, "lxml").find_all("tweet"):
        content = tweet.content.string
        polarity = tweet.polarity.value.string
        data = polarity + "\t" + content
        train_data.append(data)
    
    f = open(data_dir + "/TASS2017_T1_development.xml", "r")
    dev_contents = f.read()
    f.close()
    for tweet in BeautifulSoup(dev_contents, "lxml").find_all("tweet"):
        content = tweet.content.string
        polarity = tweet.polarity.value.string
        data = polarity + "\t" + content
        dev_data.append(data)
    
    f = open(data_dir + "/TASS2017_T1_test.xml", "r")
    test_contents = f.read()
    f.close()
    for tweet in BeautifulSoup(test_contents, "lxml").find_all("tweet"):
        tweet_id = tweet.tweetid.string
        content = tweet.content.string
        data = tweet_id + "\t" + content
        test_data.append(data)

    return train_data, dev_data, test_data


def generalize_content(content):
    generalized_content = []

    for word in content.split():
        word = re.sub("@.*", "<arroba>", word)
        word = re.sub("#(.*)", "<hashtag>", word)
        word = re.sub("http.*", "<http>", word)
        word = re.sub("[0-9].*", "<num>", word)
        generalized_content.append(word)
    
    return " ".join(generalized_content)

def tokenize_data(train_data, dev_data, test_data):
    tokenizer = TweetTokenizer(strip_handles=False, reduce_len=True, preserve_case=False)
    
    for i in range(len(train_data)):
        polarity, content = train_data[i].split("\t")
        content = " ".join(tokenizer.tokenize(content))
        generalized_content = generalize_content(content)
        data = polarity + "\t" + generalized_content
        train_data[i] = data
    
    for i in range(len(dev_data)):
        polarity, content = dev_data[i].split("\t")
        content = " ".join(tokenizer.tokenize(content))
        generalized_content = generalize_content(content)
        data = polarity + "\t" + generalized_content
        dev_data[i] = data
    
    for i in range(len(test_data)):
        tweet_id, content = test_data[i].split("\t")
        content = " ".join(tokenizer.tokenize(content))
        generalized_content = generalize_content(content)
        data = tweet_id + "\t" + generalized_content
        test_data[i] = data

    return train_data, dev_data, test_data


def print_to_file(train_data, dev_data, test_data, write_to):
    with open(write_to + "/train.tok.txt", "w") as f:
        for tweet in train_data:
            f.write(tweet + "\n")
    
    with open(write_to + "/dev.tok.txt", "w") as f:
        for tweet in dev_data:
            f.write(tweet + "\n")
    
    with open(write_to + "/test.tok.txt", "w") as f:
        for tweet in test_data:
            f.write(tweet + "\n")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_dir", default="data/original/",
            help="Directory where to get xml data.")
    parser.add_argument("--write_to", default="data/tok/",
            help="Directory where to write tokenized output in txt format.")
    args = parser.parse_args()

    train_data, dev_data, test_data = extract_data_from_xmls(args.data_dir)
    train_data, dev_data, test_data = tokenize_data(train_data, dev_data, test_data)
    print_to_file(train_data, dev_data, test_data, args.write_to)


if __name__ == "__main__":
    main()