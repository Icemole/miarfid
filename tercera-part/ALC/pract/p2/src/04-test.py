import pickle
import argparse

def predict_data(predict_data, classifier):
    predictions = classifier.predict(predict_data)

    return predictions


def write_predictions(predictions, test_ids, write_to):
    with open(write_to + "/test_predictions.txt", "w") as f:
        for test_id, pred in zip(test_ids, predictions):
            f.write(str(test_id) + "\t" + str(pred) + "\n")


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_dir", default="data/vec/",
            help="Directory where to get vectorized test data from.")
    parser.add_argument("--write_to", default="test_results/",
            help="Directory where to write output in txt format.")
    parser.add_argument("--polarized", type=str2bool, nargs='?',
            const=True, default=False,
            help="Set this if you want to test with polarized data.")
    parser.add_argument("--classifier", default="",
            help="File where the classifier is stored in binary format.")
    args = parser.parse_args()

    polarized_suffix = ""
    if args.polarized:
        polarized_suffix = "_polarity"
    
    with open(args.data_dir + "/test" + polarized_suffix + ".vec", "rb") as f:
        test_data, test_ids = pickle.load(f)
    
    with open(args.classifier, "rb") as f:
        classifier = pickle.load(f)
    
    predictions = predict_data(test_data, classifier)
    write_predictions(predictions, test_ids, args.write_to)


if __name__ == "__main__":
    main()