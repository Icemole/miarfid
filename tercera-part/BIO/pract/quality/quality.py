import math
import sys
import matplotlib.pyplot as plt
import numpy as np


## Prints help
def help():
    print("python quality.py <CLIENT-SCORE> <IMPOSTOR-SCORE> <X> [-h]")
    print("-h: prints this help and exits")

## Gets the client and impostor scores from the files passed as a parameter.
##
## Returns the sorted client and impostor scores.
def get_scores_from_files():
    client_mean = 0
    client_var = 0
    client_scores = []
    with open(sys.argv[1], "r") as f_client_scores:
        num_clients = 1
        for score in f_client_scores:
            sc = float(score.split()[1])
            client_scores.append(sc)
        #     ## Incrementally compute mean
        #     client_mean_current = (sc + client_mean * (num_clients - 1)) / num_clients
        #     ## Welford's algorithm for online variance computation
        #     client_var = client_var + (sc - client_mean) * (sc - client_mean_current)
        #     client_mean = client_mean_current
        #     num_clients += 1
        # client_var /= num_clients
    client_scores.sort()

    impostor_mean = 0
    impostor_var = 0
    impostor_scores = []
    with open(sys.argv[2], "r") as f_impostor_scores:
        num_impostors = 1
        for score in f_impostor_scores:
            sc = float(score.split()[1])
            impostor_scores.append(sc)
        #     ## Incrementally compute mean
        #     impostor_mean_current = (sc + impostor_mean * (num_impostors - 1)) / num_impostors
        #     ## Welford's algorithm for online variance computation
        #     impostor_var = impostor_var + (sc - impostor_mean) * (sc - impostor_mean_current)
        #     impostor_mean = impostor_mean_current
        #     num_impostors += 1
        # impostor_var /= num_impostors
    impostor_scores.sort()

    # print("D-prime:", (client_mean - impostor_mean) / math.sqrt(client_var + impostor_var))
    # print()

    return (client_scores, impostor_scores)


## Draws the ROC curve to a file passed as a parameter.
## If no parameter is passed, prints the graph on screen.
def get_data(client_scores, impostor_scores, filename = None):
    X = float(sys.argv[3])
    # roc_plot = plt.plot()

    num_clients = len(client_scores)
    num_impostors = len(impostor_scores)

    index_client = 0  ## Acts like 1 - FN (= TN)
    index_impostor = 0  ## Acts like FP (actually TP = 1 - FP)
    # prev_index_client = 0
    # prev_index_impostor = 0

    ## Used to plot the ROC curve
    latest_val_client = 1
    latest_val_impostor = 1

    ## Starting state of the ROC curve
    roc_curve = [(1, 1)]

    ## Area below the ROC curve
    rect_point = (1, 1) ## This point and the current one have the same Y (area calculation)
    area = 0

    ## FP == FN
    fp_eq_fn_point = (1, 1)
    ## Strategy: get the last three points and check when the ratios
    ## stop converging and start diverging (i.e. ratios' min distance)
    ## When this distance is minimal, set the point to the one
    ## that yielded the value used in ratios_distance_previous
    ##
    ## Distances between ratios
    ratios_distance_previous_previous = 1
    ratios_distance_previous = 1
    ratios_distance = 1
    ## Last values of the clients and the impostor
    ## (used to get the previous point)
    last_val_client = 1
    last_val_impostor = 1

    ## FP(FN == X)
    fp_x_index = 0
    fp_x_point = (1, 1)

    ## FN(FP == X)
    fn_x_index = 0
    fn_x_point = (1, 1)

    ## Mean and variance calculation
    client_mean = client_mean_current = client_var = 0
    impostor_mean = impostor_mean_current = impostor_var = 0

    ## Mergesort-style: go through the lists until one is finished
    ## then go through the other until the end
    while index_client < len(client_scores) and index_impostor < len(impostor_scores):
        if client_scores[index_client] < impostor_scores[index_impostor]:
            ## Incrementally compute mean
            client_mean_current = (client_scores[index_client] + client_mean * index_client) / (index_client + 1)
            ## Welford's algorithm for online variance computation
            client_var = client_var + (client_scores[index_client] - client_mean) * (client_scores[index_client] - client_mean_current)
            client_mean = client_mean_current

            index_client += 1
            ## Update the ROC curve
            latest_val_client = 1 - index_client / num_clients
            roc_curve.append((latest_val_impostor, latest_val_client))
        else:
            ## Incrementally compute mean
            impostor_mean_current = (impostor_scores[index_impostor] + impostor_mean * index_impostor) / (index_impostor + 1)
            ## Welford's algorithm for online variance computation
            impostor_var = impostor_var + (impostor_scores[index_impostor] - impostor_mean) * (impostor_scores[index_impostor] - impostor_mean_current)
            impostor_mean = impostor_mean_current

            index_impostor += 1
            ## Update the ROC curve
            latest_val_impostor = 1 - index_impostor / num_impostors
            roc_curve.append((latest_val_impostor, latest_val_client))
        
        ## Area calculation: only update area if Y changes (i.e. new rectangle)
        if rect_point[1] != latest_val_client:
            area += (rect_point[0] - latest_val_impostor) * latest_val_client
            rect_point = (latest_val_impostor, latest_val_client)
        
        ## FP == FN when the ratios are closer than never
        ## By mathematics laws, they will converge and diverge
        ## We only have to know the point where they stop "converging"
        ## and start "diverging"
        ## Also, there is no way that these ratios are the same
        ## because on each iteration either a client or an impostor is processed
        ratios_distance = abs((1 - index_client / num_clients) - index_impostor / num_impostors)
        if ratios_distance > ratios_distance_previous \
                and ratios_distance_previous_previous > ratios_distance_previous:
            ## Set the point on the values that were given by ratios_distance_previous
            fp_eq_fn_point = (last_val_impostor, last_val_client)
        ## Update the values
        last_val_client = latest_val_client
        last_val_impostor = latest_val_impostor
        ## Update the ratios distances
        ratios_distance_previous_previous = ratios_distance_previous
        ratios_distance_previous = ratios_distance
        
        ## FP(FN == X) when FN has crossed X
        if (index_client - 1) / num_clients <= X and X <= index_client / num_clients:
            fp_x_index = index_impostor
            fp_x_point = (latest_val_impostor, latest_val_client)
        ## FN(FP == X) when FP has crossed X
        if 1 - (index_impostor - 1) / num_impostors >= X and X >= 1 - index_impostor / num_impostors:
            fn_x_index = index_client
            fn_x_point = (latest_val_impostor, latest_val_client)
        
        # prev_index_client = index_client
        # prev_index_impostor = index_impostor
    
    while index_client < num_clients:
        ## Incrementally compute mean
        client_mean_current = (client_scores[index_client] + client_mean * index_client) / (index_client + 1)
        ## Welford's algorithm for online variance computation
        client_var = client_var + (client_scores[index_client] - client_mean) * (client_scores[index_client] - client_mean_current)
        client_mean = client_mean_current
        
        index_client += 1
        ## Update the ROC curve
        latest_val_client = 1 - index_client / num_clients
        roc_curve.append((latest_val_impostor, latest_val_client))
    
    while index_impostor < num_impostors:
        ## Incrementally compute mean
        impostor_mean_current = (impostor_scores[index_impostor] + impostor_mean * index_impostor) / (index_impostor + 1)
        ## Welford's algorithm for online variance computation
        impostor_var = impostor_var + (impostor_scores[index_impostor] - impostor_mean) * (impostor_scores[index_impostor] - impostor_mean_current)
        impostor_mean = impostor_mean_current

        index_impostor += 1
        ## Update the ROC curve
        latest_val_impostor = 1 - index_impostor / num_impostors
        roc_curve.append((latest_val_impostor, latest_val_client))

    client_var /= num_clients
    impostor_var /= num_impostors

    print("D-prime:", (client_mean - impostor_mean) / math.sqrt(client_var + impostor_var))
    print()

    ## Print FP(FN == X) and threshold
    print("FP(FN == X):", fp_x_point[0])
    print("FP(FN == X) (threshold):", impostor_scores[fp_x_index])
    print()

    ## Print FN(FP == X) and threshold
    print("FN(FP == X):", 1 - fn_x_point[1])
    print("FN(FP == X) (threshold):", client_scores[fn_x_index])
    print()

    ## Print FP == FN and threshold
    ## For simplicity, we only print the client data
    ## but it should be the same as the impostor data
    fp_fn_client = 1 - fp_eq_fn_point[1]
    fp_fn_threshold_client = client_scores[int((fp_fn_client) * num_clients)]
    #fp_fn_impostor = 1 - fp_eq_fn_point[0]
    #fp_fn_threshold_impostor = impostor_scores[int(fp_fn_impostor * num_impostors)]
    print("FP == FN:", fp_fn_client)  ## Same with fp_fn_threshold_impostor
    print("FP == FN (threshold):", fp_fn_threshold_client)
    print()

    ## Print area below the ROC curve
    print("Area below the ROC curve:", area)
    print()

    x, y = zip(*roc_curve)
    # plt.scatter(x, y, s = [1] * len(y))
    plt.plot(x, y, label = "ROC")
    plt.scatter(fp_eq_fn_point[0], fp_eq_fn_point[1], color = "red", label = "FP == FN")
    plt.scatter(fp_x_point[0], fp_x_point[1], color = "green", label = "FP(FN == X)")
    plt.scatter(fn_x_point[0], fn_x_point[1], color = "yellow", label = "FN(FP == X)")
    plt.legend(loc = "center")
    if filename == None:
        plt.show()
    else:
        plt.savefig(filename)


# Main
def main():
    if len(sys.argv) != 4 or "-h" in sys.argv:
        help()
        sys.exit(1)
    
    client_scores, impostor_scores = get_scores_from_files()

    get_data(client_scores, impostor_scores)

if __name__ == "__main__":
    main()