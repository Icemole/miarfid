sed -e 's/\^{\S*}//g' -e 's/[[][^]]*[]]//g' \
	data/text/transcriptions.txt | \
gawk -v MAP=data/lists/unitsMap.lst 'BEGIN {
	while ((getline < MAP) > 0) {
		if (NF == 2) DICT[$1] = $2
	}
	DICT[" "] = " ";
}{
	printf("%s ", $1)
	for(w=2; w<=NF; w++) {
		N=split(tolower($w), CHAR, "")
		for(i=1; i <= N; i++)
			if (CHAR[i] in DICT)
				printf("%s ", DICT[CHAR[i]])
		printf("<space> ")
	}
	printf("\n")
}' > data/text/transcriptions_char.txt
