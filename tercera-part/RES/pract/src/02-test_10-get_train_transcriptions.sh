gawk -v TR_LIST=data/lists/train.lst \
-v VAL_LIST=data/lists/val.lst 'BEGIN{
while ((getline < TR_LIST) > 0) DICT[$1] = 1
while ((getline < VAL_LIST) > 0) DICT[$1] = 1
}{
if ($1 in DICT) {
$1="";
N=split($0,CHAR,"")
for(i=1; i<=N; i++){
if (CHAR[i] ~ /[(),.:;?]/)
printf(" %s ",CHAR[i])
else
printf("%s",CHAR[i])
}
printf ("\n")
}
}' data/text/transcr_words.txt > data/text/train_words.txt
