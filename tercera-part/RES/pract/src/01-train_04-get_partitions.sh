for file in `cut -d" " -f1 data/text/transcriptions.txt`
do
	[ -e data/Corpus_clean_lines/${file}.jpg ] && echo $file
done | head -15000 > data/lists/train.lst

for file in `cut -d" " -f1 data/text/transcriptions.txt`
do
        [ -e data/Corpus_clean_lines/${file}.jpg ] && echo $file
done | head -16000 | tail -1000 > data/lists/val.lst

for file in `cut -d" " -f1 data/text/transcriptions.txt`
do
        [ -e data/Corpus_clean_lines/${file}.jpg ] && echo $file
done | tail -3359 > data/lists/test.lst
