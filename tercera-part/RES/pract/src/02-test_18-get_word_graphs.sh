mkdir -p results/lattices/words

N_FEAT=`feat-to-dim ark:models/Optical/ConfMats.ark -`
./scripts/mkTopo.sh ${N_FEAT} > models/WFST/topology.hmm

gmm-init-mono --print-args=false models/WFST/topology.hmm \
${N_FEAT} models/WFST/model.hmm /tmp/auxTree

latgen-faster-mapped --beam=25.0 --max-active=5000 \
--acoustic-scale=1.0 models/WFST/model.hmm \
models/WFST/TLG.fst ark:models/Optical/ConfMats.ark \
"ark,t:|gzip -c > results/lattices/test-word-lat.gz" \
ark,t:results/lattices/test-3gram-words-1bestLat.hyp
