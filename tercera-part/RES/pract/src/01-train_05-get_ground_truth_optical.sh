for part in train val test
do
	gawk -v ListFiles=data/lists/${part}.lst '
	BEGIN {
		while ((getline file < ListFiles) > 0) FILES[file]=1
	}{
		if ($1 in FILES) print
	}' data/text/transcriptions_char.txt > data/text/${part}_char.txt
done
