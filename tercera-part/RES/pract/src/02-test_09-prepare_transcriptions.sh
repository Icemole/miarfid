sed -e "s/\w*\^{\w*}//g" -e "s/\w*\[//g" -e "s/\]//g" \
data/text/transcriptions.txt | \
gawk -v MAP_UNITS=data/lists/unitsMap.lst \
'BEGIN{ while ((getline < MAP_UNITS) > 0)
MAP[$1] = $2
}{
printf ("%s ",$1)
for(w=2; w<=NF;w++) {
N=split(tolower($w),CHAR,"")
for(i=1; i<=N; i++)
if (CHAR[i] in MAP)
if (CHAR[i] ~ /[,;.:?()]/)
printf(" %s ", MAP[CHAR[i]])
else
printf("%s", MAP[CHAR[i]])
printf (" ")
}
printf("\n")
}' > data/text/transcr_words.txt
