decode-faster --print-args=false \
--beam=30.0 --max-active=5000 \
--acoustic-scale=1.0 --allow-partial=true \
--word_symbol-table=models/WFST/wordsMap.txt \
models/WFST/TLG.fst ark:models/Optical/ConfMats.ark \
ark,t:results/test_TLG_numbers.hyp
