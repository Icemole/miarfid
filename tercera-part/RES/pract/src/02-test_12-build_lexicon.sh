gawk 'BEGIN{print "<s> <space> #1"; print "</s> <space> #2";
}{
N=split($1,CHARS,"");
if ($1 !~ /<s>/ && $1 !~ /<\/s>/ &&
$1 !~ /<eps>/ && $1 !~ /#0/){
printf("%s ", $1);
for(i=1; i<=N; i++)
printf("%s ",CHARS[i]);
printf ("<space>\n")
}
}' models/WFST/wordsMap.txt > models/WFST/lexicon.txt
