#!/bin/bash

N_WG=`ls results/lattices/words/*.lat.gz | wc -l | cut -d ' ' -f 1`
# ---> 3322 lines

./Exp-KWS/KwsEvalTool/kws-assessment \
-t -s -a -m Exp-KWS/IDX_p.dat -w Exp-KWS/keywords.lst -l ${N_WG}
# MAP = 0.897 ( #Rel-Wrds = 2792 )
# AP = 0.919
