mkdir data/text
for f in data/txt/*
do
	gawk -v fileName=$(basename $f .txt) '{printf("%s_r0_l%02i ", fileName, NR-1); gsub("[␣_]"," "); print}' $f
done | sed -e "s/\s+/ /g" \
	-e "s/^\s+//g" \
	-e "s/[\$\t]/ /g" > data/text/transcriptions.txt
