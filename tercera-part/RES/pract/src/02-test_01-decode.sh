#!/bin/bash

mkdir results
conda activate res

pylaia-htr-decode-ctc --trainer.gpus 1 \
--common.model_filename models/Optical/Rodrigo.net \
--common.experiment_dirname models/Optical \
--img_dirs [data/Corpus_clean_lines] \
--data.batch_size 40 \
data/lists/symbols_train.lst data/lists/test.lst | \
sort -k1 > results/test_chars.hyp
