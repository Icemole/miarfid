mkdir -p models/Optical

pylaia-htr-create-model \
--fixed_input_height 64 \
--save_model true \
--common.train_path ./models/Optical \
--common.model_filename Rodrigo.net \
--logging.level INFO --crnn.rnn_units 512 \
--crnn.rnn_layers 3 --crnn.use_masks true \
--crnn.cnn_batchnorm [true,true,true,true] \
--crnn.cnn_kernel_size [3,3,3,3] \
--crnn.cnn_num_features [16,16,32,32] \
--crnn.cnn_dilation [1,1,1,1] --crnn.num_input_channels 1 \
--crnn.cnn_poolsize [2,2,0,2] --crnn.rnn_type LSTM \
--crnn.lin_dropout 1 \
data/lists/symbols_train.lst

pylaia-htr-train-ctc --trainer.gpus 1 \
--logging.level INFO --logging.to_stderr_level INFO \
--logging.filepath train-crnn.log \
--common.experiment_dirname . \
--common.train_path models/Optical \
--common.model_filename Rodrigo.net \
--data.batch_size 32 --optimizer.learning_rate 0.0003 \
--trainer.max_epochs 60 --train.delimiters ["<space>"] \
data/lists/symbols_train.lst [data/Corpus_clean_lines/] \
data/text/train_char.txt data/text/val_char.txt
