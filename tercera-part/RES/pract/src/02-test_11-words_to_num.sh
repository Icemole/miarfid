gawk '{for (w=1; w<=NF; w++){print $w}}' \
data/text/train_words.txt | sort -u | \
gawk 'BEGIN{print "<eps> 0"; print "<s> "1; print "</s> 2"}
{print $0, NR+2} END{ print "#0",NR+3; }' \
> models/WFST/wordsMap.txt
