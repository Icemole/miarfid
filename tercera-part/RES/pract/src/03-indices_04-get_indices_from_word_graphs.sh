for f in results/lattices/words/*.lat.gz
do
	F=$(basename $f .lat.gz); echo "Processing $F..."; \

	Exp-KWS/WordGraph2Index/WordGraph2Index -i $f -z w | \
	awk '!/^#|NULL/{print $2,$3,$4,$5,$6}' > \
	Exp-KWS/WGIDX/${F}_p.idx; \
done
