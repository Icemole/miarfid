#!/bin/bash

ls Exp-KWS/WGIDX/* > Exp-KWS/indx.lst

awk -v TR=data/text/test_words.ref 'BEGIN{
	while ((getline < TR) > 0)
		for (w=2; w <= NF; w++) REF[$1][$w]=1;
}{
	lineId=$1;
	gsub("_p.idx", "", lineId)
	gsub(".*/", "", lineId)
	while ((getline word_line < $1) > 0) {
		split(word_line, word);
		words_in_WG[word[1]]=1;
		result=0;
		if (word[1] in REF[lineId])
			result=1;
			print lineId,word[1],result,word[2]
	}
	for (w in REF[lineId]) #Words in REF but not in the WordGraph
		if (!(w in words_in_WG)) {print lineId,w,"1","-1"}
	delete words_in_WG;
}' Exp-KWS/indx.lst > Exp-KWS/IDX_p.dat
