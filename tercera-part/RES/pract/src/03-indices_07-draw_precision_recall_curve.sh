#!/bin/bash

./Exp-KWS/KwsEvalTool/kws-assessment -t -s Exp-KWS/IDX_p.dat \
-w Exp-KWS/keywords.lst -l 5011 > Exp-KWS/r-p_data.dat

cp ./Exp-KWS/KwsEvalTool/egs/plot-R-P.gnp Exp-KWS;
cd Exp-KWS/
gnuplot plot-R-P.gnp
evince R-P.pdf
