gawk -v TEST_LIST=data/lists/test.lst '
BEGIN{
while ((getline < TEST_LIST) > 0) DICT[$1] = 1
}{
if ($1 in DICT) {
printf("%s ",$1)
$1="";
N=split($0,CHAR,"")
for(i=1; i<=N; i++){
if (CHAR[i] ~ /[(),.:;?]/)
printf(" %s ",CHAR[i])
else
printf("%s",CHAR[i])
}
printf ("\n")
}
}' data/text/transcr_words.txt | \
sed "s/\s\s*/ /g" > data/text/test_words.ref
