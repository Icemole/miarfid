gawk -v TEST_LIST=data/lists/test.lst \
'BEGIN{while ((getline < TEST_LIST) > 0){TL[$1]=1}
}{
if ($1 in TL) {
printf("%s ",$1);
for (i=2; i<=NF; i++)
printf("%s",$i);printf("\n")
}
}' data/text/transcriptions_char.txt | \
sed 's/<space>/ /g'> data/text/test.ref
