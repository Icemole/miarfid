source /usr/local/anaconda3/etc/profile.d/conda.sh
# If there is some second parameter just create this
[ "$#" -eq 1 ] && conda create -n res python=3.7
conda activate res

# If there is some second parameter just install this
([ "$#" -eq 1 ] || [ "$#" -eq 2 ]) && git clone https://github.com/jpuigcerver/PyLaia.git && cd PyLaia && pip install -e . && cd ..
