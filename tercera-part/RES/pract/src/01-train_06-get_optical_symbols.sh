sort -u data/lists/unitsMap.lst | gawk 'BEGIN {
	cont=0;
	print "<eps>", cont++
	print "<ctc>", cont++
	print "<space>", cont++
}{ if (NF==2 && !($2 in AP)) {AP[$2]=1; print $2,cont++}}' \
> data/lists/symbols_train.lst
