scripts/int2sym.pl -f 2-200 models/WFST/wordsMap.txt \
results/lattices/test-3gram-words-1bestLat.hyp | \
gawk -v FILE_LIST=data/text/test_words.ref 'BEGIN{
while ((getline < FILE_LIST) > 0) DICT[$1] = $0
}{
if ($1 in DICT) {
N=split(DICT[$1],DIC)
for (i=2; i<NF; i++) printf("%s ", $i)
printf("%s#",$NF);
for (i=2; i<N; i++) printf("%s ",DIC[i])
printf("%s\n",DIC[N]);
}
}' > results/lattices/test_WG_hyp2ref

echo -e "WER= "`tasas/tasas -f "#" -s " " \
results/lattices/test_WG_hyp2ref`
echo -e "CER= "`tasas/tasas -f "#" \
results/lattices/test_WG_hyp2ref`
