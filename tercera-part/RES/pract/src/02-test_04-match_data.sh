gawk -v TEST_LIST=data/text/test.ref '
BEGIN{
while ((getline < TEST_LIST) > 0)
TL[$1] = $0
} {
if ($1 in TL) {
N=split(TL[$1],DIC)
for (i=2; i<NF; i++) printf("%s ", $i)
printf("%s#",$NF);
for (i=2; i<N; i++) printf("%s ",DIC[i])
printf("%s\n",DIC[N]);
}
}' results/test_words.hyp > results/test_words_hyp2ref
