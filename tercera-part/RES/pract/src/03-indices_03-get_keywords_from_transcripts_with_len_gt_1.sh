#!/bin/bash

awk '{if (length($1) > 1 && $1 !~ /[<#]/ ) print $1}' \
	models/WFST/wordsMap.txt > Exp-KWS/keywords.lst
