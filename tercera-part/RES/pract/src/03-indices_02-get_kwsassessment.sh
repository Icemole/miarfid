#!/bin/bash

git clone https://github.com/PRHLT/KwsEvalTool.git
cd KwsEvalTool
gcc -Wall -O4 -o kws-assessment kws-assessment.c
cd ../..
