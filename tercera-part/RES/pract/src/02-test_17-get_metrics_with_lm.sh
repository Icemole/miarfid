scripts/int2sym.pl -f 2-200 models/WFST/wordsMap.txt \
results/test_TLG_numbers.hyp | \
gawk -v TEST_LIST=data/text/test_words.ref 'BEGIN{
while ((getline < TEST_LIST) > 0) DICT[$1] = $0
} {
if ($1 in DICT) {
N=split(DICT[$1],DIC)
for (i=2; i<NF; i++) printf("%s ", $i)
printf("%s#",$NF);
for (i=2; i<N; i++) printf("%s ",DIC[i])
printf("%s\n",DIC[N]);
}
}' > results/test_TLG_hyp2ref

echo -e "WER= "`tasas/tasas -f "#" -s " " results/test_TLG_hyp2ref`
echo -e "CER= "`tasas/tasas -f "#" results/test_TLG_hyp2ref`
