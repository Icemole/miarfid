source /usr/local/anaconda3/etc/profile.d/conda.sh
conda create -n res python=3.7
conda activate res

cd PyLaia
pip install -e .
cd ..
