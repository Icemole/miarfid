mkdir -p data/lists

gawk '{
	$0=tolower($0)
	for(i=2;i<=NF;i++) {
		for(j=1;j<=length($i);j++) {
			print substr($i, j, 1);
		}
	}
}' data/text/transcriptions.txt  |\
sort -u > data/lists/symbols.lst
