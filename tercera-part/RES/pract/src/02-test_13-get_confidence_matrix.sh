pylaia-htr-netout --logging.level INFO \
--logging.to_stderr_level INFO --logging.filepath CMs-crnn.log \
--trainer.gpus 1 --common.train_path ./models/Optical \
--common.model_filename Rodrigo.net \
--common.experiment_dirname . --data.batch_size 40 \
--netout.output_transform log_softmax \
--netout.matrix ConfMats.ark \
--img_dirs [data/Corpus_clean_lines] data/lists/test.lst
