mkdir -p models/WFST
gawk 'BEGIN{
cont=0;
print "<eps>",cont++;
print "<ctc>",cont++;
print "<blk>",cont++; getline; getline;
}{ print $1,cont++; }
END{
print "#0",cont++;
print "#1",cont++;
print "#2",cont++;
print "#3",cont++;
}' data/lists/symbols_train.lst > models/WFST/tokensMap.txt
