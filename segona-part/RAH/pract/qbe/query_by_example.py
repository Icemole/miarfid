# -*- coding: utf-8 -*-

# Author: Icemole

import numpy as np
from sys import *
from distances import get_distance_matrix
from matplotlib.pyplot import matshow, show, cm, plot
import math
import heapq	# Efficient sorting haha

# Useful variables
# epsilon = 0.00000001	# Comparison of floats for equality
normalization = "-n" in argv
help = "-h" in argv or "--help" in argv
if help:
	print("python query_by_example.py [-n] [-h]")
	print("-n\tApply distance normalization when comparing the minimum path (disabled by default)")
	print("-h\tShow this help dialog and exit")
	exit(0)


def lee_fichero(fichero):
	matriz=[]
	fichero=open(fichero,"r")
	lineas=fichero.readlines()
	matriz=[linea.split() for linea in lineas] 
	fichero.close()
	return np.array(matriz).astype(np.float)

#Implementar a partir de aqui
#sintaxis get_distance_matrix:
#distancias=get_distance_matrix(npmatriz1,npmatriz2,'cos')
#donde npmatriz1 y npmatriz2 son los vectores de caracteristicas de los dos audios
words_to_segments = {'acuerdo': 'SEG-0000', 'amigos': 'SEG-0001', 'analizar': 'SEG-0002', 'andalucía': 'SEG-0003', 'bruselas': 'SEG-0004', 'cajas': 'SEG-0005', 'capacidad': 'SEG-0006', 'centro': 'SEG-0007', 'ciudad': 'SEG-0008', 'claramente': 'SEG-0009', 'comunicar': 'SEG-0010', 'concejal': 'SEG-0011', 'condición': 'SEG-0012', 'corrupción': 'SEG-0013', 'crecimiento': 'SEG-0014', 'dato': 'SEG-0015', 'decidir': 'SEG-0016', 'defender': 'SEG-0017', 'democracia': 'SEG-0018', 'diario': 'SEG-0019', 'diferencia': 'SEG-0020', 'discapacidad': 'SEG-0021', 'documento': 'SEG-0022', 'durante': 'SEG-0023', 'economía': 'SEG-0024', 'elecciones': 'SEG-0025', 'empezar': 'SEG-0026', 'estados': 'SEG-0027', 'estatuto': 'SEG-0028', 'euro': 'SEG-0029', 'europa': 'SEG-0030', 'exactamente': 'SEG-0031', 'fácil': 'SEG-0032', 'familia': 'SEG-0033', 'fecha': 'SEG-0034', 'final': 'SEG-0035', 'finalmente': 'SEG-0036', 'fiscales': 'SEG-0037', 'francia': 'SEG-0038', 'fraude': 'SEG-0039', 'fundamental': 'SEG-0040', 'gasolina': 'SEG-0041', 'gracias': 'SEG-0042', 'hacienda': 'SEG-0043', 'historia': 'SEG-0044', 'importantes': 'SEG-0045', 'información': 'SEG-0046', 'inmediato': 'SEG-0047', 'inmovilismo': 'SEG-0048', 'izquierda': 'SEG-0049', 'jubilación': 'SEG-0050', 'leche': 'SEG-0051', 'legalidad': 'SEG-0052', 'legislatura': 'SEG-0053', 'líder': 'SEG-0054', 'libertad': 'SEG-0055', 'mantener': 'SEG-0056', 'mandato': 'SEG-0057', 'mensaje': 'SEG-0058', 'miedo': 'SEG-0059', 'ministerio': 'SEG-0060', 'mordaza': 'SEG-0061', 'música': 'SEG-0062', 'necesario': 'SEG-0063', 'norma': 'SEG-0064', 'noviembre': 'SEG-0065', 'opinión': 'SEG-0066', 'oportunidad': 'SEG-0067', 'oposición': 'SEG-0068', 'palabra': 'SEG-0069', 'pequeño': 'SEG-0070', 'pizarra': 'SEG-0071', 'planeta': 'SEG-0072', 'plazos': 'SEG-0073', 'pobreza': 'SEG-0074', 'policía': 'SEG-0075', 'pregunta': 'SEG-0076', 'pueblos': 'SEG-0077', 'razonable': 'SEG-0078', 'reforma': 'SEG-0079', 'responsabilidad': 'SEG-0080', 'reunión': 'SEG-0081', 'sanidad': 'SEG-0082', 'seguridad': 'SEG-0083', 'semana': 'SEG-0084', 'siglo': 'SEG-0085', 'significa': 'SEG-0086', 'siguiente': 'SEG-0087', 'sistema': 'SEG-0088', 'situación': 'SEG-0089', 'socialista': 'SEG-0090', 'supuesto': 'SEG-0091', 'televisión': 'SEG-0092', 'unidos': 'SEG-0093', 'vasco': 'SEG-0094', 'venido': 'SEG-0095', 'viernes': 'SEG-0096', 'vigor': 'SEG-0097', 'voluntad': 'SEG-0098', 'votación': 'SEG-0099'}
segments_to_words = {'SEG-0000': 'acuerdo', 'SEG-0001': 'amigos', 'SEG-0002': 'analizar', 'SEG-0003': 'andalucía', 'SEG-0004': 'bruselas', 'SEG-0005': 'cajas', 'SEG-0006': 'capacidad', 'SEG-0007': 'centro', 'SEG-0008': 'ciudad', 'SEG-0009': 'claramente', 'SEG-0010': 'comunicar', 'SEG-0011': 'concejal', 'SEG-0012': 'condición', 'SEG-0013': 'corrupción', 'SEG-0014': 'crecimiento', 'SEG-0015': 'dato', 'SEG-0016': 'decidir', 'SEG-0017': 'defender', 'SEG-0018': 'democracia', 'SEG-0019': 'diario', 'SEG-0020': 'diferencia', 'SEG-0021': 'discapacidad', 'SEG-0022': 'documento', 'SEG-0023': 'durante', 'SEG-0024': 'economía', 'SEG-0025': 'elecciones', 'SEG-0026': 'empezar', 'SEG-0027': 'estados', 'SEG-0028': 'estatuto', 'SEG-0029': 'euro', 'SEG-0030': 'europa', 'SEG-0031': 'exactamente', 'SEG-0032': 'fácil', 'SEG-0033': 'familia', 'SEG-0034': 'fecha', 'SEG-0035': 'final', 'SEG-0036': 'finalmente', 'SEG-0037': 'fiscales', 'SEG-0038': 'francia', 'SEG-0039': 'fraude', 'SEG-0040': 'fundamental', 'SEG-0041': 'gasolina', 'SEG-0042': 'gracias', 'SEG-0043': 'hacienda', 'SEG-0044': 'historia', 'SEG-0045': 'importantes', 'SEG-0046': 'información', 'SEG-0047': 'inmediato', 'SEG-0048': 'inmovilismo', 'SEG-0049': 'izquierda', 'SEG-0050': 'jubilación', 'SEG-0051': 'leche', 'SEG-0052': 'legalidad', 'SEG-0053': 'legislatura', 'SEG-0054': 'líder', 'SEG-0055': 'libertad', 'SEG-0056': 'mantener', 'SEG-0057': 'mandato', 'SEG-0058': 'mensaje', 'SEG-0059': 'miedo', 'SEG-0060': 'ministerio', 'SEG-0061': 'mordaza', 'SEG-0062': 'música', 'SEG-0063': 'necesario', 'SEG-0064': 'norma', 'SEG-0065': 'noviembre', 'SEG-0066': 'opinión', 'SEG-0067': 'oportunidad', 'SEG-0068': 'oposición', 'SEG-0069': 'palabra', 'SEG-0070': 'pequeño', 'SEG-0071': 'pizarra', 'SEG-0072': 'planeta', 'SEG-0073': 'plazos', 'SEG-0074': 'pobreza', 'SEG-0075': 'policía', 'SEG-0076': 'pregunta', 'SEG-0077': 'pueblos', 'SEG-0078': 'razonable', 'SEG-0079': 'reforma', 'SEG-0080': 'responsabilidad', 'SEG-0081': 'reunión', 'SEG-0082': 'sanidad', 'SEG-0083': 'seguridad', 'SEG-0084': 'semana', 'SEG-0085': 'siglo', 'SEG-0086': 'significa', 'SEG-0087': 'siguiente', 'SEG-0088': 'sistema', 'SEG-0089': 'situación', 'SEG-0090': 'socialista', 'SEG-0091': 'supuesto', 'SEG-0092': 'televisión', 'SEG-0093': 'unidos', 'SEG-0094': 'vasco', 'SEG-0095': 'venido', 'SEG-0096': 'viernes', 'SEG-0097': 'vigor', 'SEG-0098': 'voluntad', 'SEG-0099': 'votación'}

# Distance to use
valid_distance = False
while not valid_distance:
	print("Select the distance that you would use. [cos, euc, corr, dot, kl, pearson]")
	distance_method = input()
	if distance_method not in ["cos", "euc", "corr", "dot", "kl", "pearson"]:
		print("The distance you have chosen is not recognized. Please choose another distance.")
	else:
		valid_distance = True

valid_choice = False
while not valid_choice:
	print("Would you like to search by segment or by word? [segment, word]")
	choice = input()
	if choice not in ["segment", "word"]:
		print("Wrong choice. Please try again.")
	else:
		valid_choice = True

word_to_search = segment_to_search = None
if choice == "word":
	# Word to look for
	valid_word = False
	while not valid_word:
		print("Select the word that you would like to search in the audio: [just look at the poliformat file; beware of accents]")
		word_to_search = input()
		if word_to_search not in words_to_segments:
			print("The word you are trying to look for is not in the dictionary.")
		else:
			valid_word = True
elif choice == "segment":
	# Segment to look for
	valid_segment = False
	while not valid_segment:
		print("Select the segment that you would like to search: [00, 01, ..., 99]")
		segment_to_search = input()
		if not (0 <= int(segment_to_search) and int(segment_to_search) <= 99):
			print("The segment you are trying to look for has not been found.")
			print("Please check that the segment you provided is between 00 and 99.")
		else:
			valid_segment = True

print("Corresponding audio found. Loading datasets...")
long_audio = lee_fichero("data/long_audio/largo250000.mfc.raw")
if word_to_search is not None:	# 
	query = lee_fichero("data/segments/" + words_to_segments[word_to_search] + ".mfc.raw")
else:	# segment_to_search is not None
	query = lee_fichero("data/segments/SEG-00" + segment_to_search + ".mfc.raw")

print("Calculating distance matrix between both audios...")
distances = get_distance_matrix(query, long_audio, distance_method)

print("Comparing audios...")
# SDTW algorithm
data_matrix = {}
# Structure of data_matrix: matrix of elements (cost, path_length, initial_position)
for j in range(len(long_audio)):
	data_matrix[(0, j)] = (0, 1, j)

for i in range(1, len(query)):
	prev_element = data_matrix[(i - 1, 0)]
	data_matrix[(i, 0)] = (prev_element[0] + distances[i, 0], prev_element[1] + 1, prev_element[2])

for i in range(1, len(query)):
	print("Progress:", str(round(i / len(query) * 100, 2)) + "%")
	for j in range(1, len(long_audio)):
		bottom = data_matrix[(i - 1, j)]
		distance_from_bottom = bottom[0] + distances[(i, j)]
		
		left = data_matrix[(i, j - 1)]
		distance_from_left = left[0] + distances[(i, j)]
		
		diagonal = data_matrix[(i - 1, j - 1)]
		distance_from_diagonal = diagonal[0] + distances[(i, j)]

		if normalization:
			min_distance = min(distance_from_bottom / bottom[1], distance_from_left / left[1], distance_from_diagonal / diagonal[1])
			if min_distance == distance_from_bottom / bottom[1]:
				data_matrix[(i, j)] = (min_distance * bottom[1], bottom[1] + 1, bottom[2])
			elif min_distance == distance_from_left / left[1]:
				data_matrix[(i, j)] = (min_distance * left[1], left[1] + 1, left[2])
			elif min_distance == distance_from_diagonal / diagonal[1]:
				data_matrix[(i, j)] = (min_distance * diagonal[1], diagonal[1] + 1, diagonal[2])
		else:
			min_distance = min(distance_from_bottom, distance_from_left, distance_from_diagonal)
			if min_distance == distance_from_bottom:
				data_matrix[(i, j)] = (min_distance, bottom[1] + 1, bottom[2])
			elif min_distance == distance_from_left:
				data_matrix[(i, j)] = (min_distance, left[1] + 1, left[2])
			elif min_distance == distance_from_diagonal:
				data_matrix[(i, j)] = (min_distance, diagonal[1] + 1, diagonal[2])
		# print("bot:", distance_from_bottom / bottom[1])
		# print("left:", distance_from_left / left[1])
		# print("diag:", distance_from_diagonal / diagonal[1])
		# print("min:", min_distance)

print("Comparison finished!")
print("Storing best values...")
min_values = []
for j in range(len(long_audio)):
	aux_element = data_matrix[(len(query) - 1, j)]
	aux_element = (aux_element[0], aux_element[1], aux_element[2], j)
	heapq.heappush(min_values, aux_element)

begin_unique = {}
min_values_unique = []
for index in range(len(min_values)):
	aux_element = heapq.heappop(min_values)
	if aux_element[2] not in begin_unique:
		heapq.heappush(min_values_unique, aux_element)
		begin_unique[aux_element[2]] = True

finished = False
while not finished:
	print()
	print("Extract the N best values (type 0 to finish)")

	n_best = int(input("N: "))
	if n_best > 0:
		print("Retrieving best values...")
		aux_list = []
		for i in range(n_best):
			aux_list.append(heapq.heappop(min_values_unique))
		# 	print("apzpenidign ", aux_list)
		# print("saiz of aux list = ", len(aux_list), "???? n_best = ", n_best)
		print("N best values in ascending order (first means best):")
		for element in aux_list:
			print("Distance:", element[0])
			print("Length from the beginning:", element[1])
			print("Audio frame where the element started:", element[2])
			print("Audio frame where the element ends:", element[3])
			print("---------------------")
			heapq.heappush(min_values_unique, element)
	else:
		finished = True

print()
print("Thank you for using this program! Please be patient while memory is emptied...")
