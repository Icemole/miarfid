import math
import sys

log_prob = 0
ppl_prob = 0
n = 0
with open(sys.argv[1], "r") as f:
    for line in f:
        try:
            log_prob += math.log(float(line))
            ppl_prob += math.log(float(line), 2)
            n += 1
        except ValueError:
            pass
perplexity = 2 ** (-ppl_prob / n)

print("Log-prob:", log_prob)
print("Perplexity:", perplexity)