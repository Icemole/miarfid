import sys
from sklearn.metrics import confusion_matrix
from numpy import argmax
import math

num_grammar = sys.argv[1]
viterbi = len(sys.argv) > 2 and sys.argv[2] == "-v"

confus = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
# Rows:     known classification of the sample
# Columns:  class given to the sample
pred_labels = []
real_labels = []


if viterbi:
    # ==========================
    # Testing with grammar EQ
    eq = []
    with open("experiments/G" + num_grammar + "-EQ-TS-EQ-vit", "r") as eq_file:
        for line in eq_file:
            eq.append(float(line))

    iso = []
    with open("experiments/G" + num_grammar + "-IS-TS-EQ-vit", "r") as iso_file:
        for line in iso_file:
            iso.append(float(line))

    sc = []
    with open("experiments/G" + num_grammar + "-SC-TS-EQ-vit", "r") as sc_file:
        for line in sc_file:
            sc.append(float(line))

    # The real class is EQ (0)
    for i in range(1000):
        real_labels.append(0)

    # Find the most probable class
    for i in range(1000):
        pred_labels.append(argmax([eq[i], iso[i], sc[i]]))


    # ==========================
    # Testing with grammar IS
    eq = []
    with open("experiments/G" + num_grammar + "-EQ-TS-IS-vit", "r") as eq_file:
        for line in eq_file:
            eq.append(float(line))

    iso = []
    with open("experiments/G" + num_grammar + "-IS-TS-IS-vit", "r") as iso_file:
        for line in iso_file:
            iso.append(float(line))

    sc = []
    with open("experiments/G" + num_grammar + "-SC-TS-IS-vit", "r") as sc_file:
        for line in sc_file:
            sc.append(float(line))

    # The real class is IS (1)
    for i in range(1000):
        real_labels.append(1)

    # Find the most probable class
    for i in range(1000):
        pred_labels.append(argmax([eq[i], iso[i], sc[i]]))


    # ==========================
    # Testing with grammar SC
    eq = []
    with open("experiments/G" + num_grammar + "-EQ-TS-SC-vit", "r") as eq_file:
        for line in eq_file:
            eq.append(float(line))

    iso = []
    with open("experiments/G" + num_grammar + "-IS-TS-SC-vit", "r") as iso_file:
        for line in iso_file:
            iso.append(float(line))

    sc = []
    with open("experiments/G" + num_grammar + "-SC-TS-SC-vit", "r") as sc_file:
        for line in sc_file:
            sc.append(float(line))

    # The real class is SC (2)
    for i in range(1000):
        real_labels.append(2)

    # Find the most probable class
    for i in range(1000):
        pred_labels.append(argmax([eq[i], iso[i], sc[i]]))

else:
    # ==========================
    # Testing with grammar EQ
    eq = []
    with open("experiments/G" + num_grammar + "-EQ-TS-EQ", "r") as eq_file:
        for line in eq_file:
            eq.append(float(line))

    iso = []
    with open("experiments/G" + num_grammar + "-IS-TS-EQ", "r") as iso_file:
        for line in iso_file:
            iso.append(float(line))

    sc = []
    with open("experiments/G" + num_grammar + "-SC-TS-EQ", "r") as sc_file:
        for line in sc_file:
            sc.append(float(line))

    # The real class is EQ (0)
    for i in range(1000):
        real_labels.append(0)

    # Find the most probable class
    for i in range(1000):
        pred_labels.append(argmax([eq[i], iso[i], sc[i]]))


    # ==========================
    # Testing with grammar IS
    eq = []
    with open("experiments/G" + num_grammar + "-EQ-TS-IS", "r") as eq_file:
        for line in eq_file:
            eq.append(float(line))

    iso = []
    with open("experiments/G" + num_grammar + "-IS-TS-IS", "r") as iso_file:
        for line in iso_file:
            iso.append(float(line))

    sc = []
    with open("experiments/G" + num_grammar + "-SC-TS-IS", "r") as sc_file:
        for line in sc_file:
            sc.append(float(line))

    # The real class is IS (1)
    for i in range(1000):
        real_labels.append(1)

    # Find the most probable class
    for i in range(1000):
        pred_labels.append(argmax([eq[i], iso[i], sc[i]]))


    # ==========================
    # Testing with grammar SC
    eq = []
    with open("experiments/G" + num_grammar + "-EQ-TS-SC", "r") as eq_file:
        for line in eq_file:
            eq.append(float(line))

    iso = []
    with open("experiments/G" + num_grammar + "-IS-TS-SC", "r") as iso_file:
        for line in iso_file:
            iso.append(float(line))

    sc = []
    with open("experiments/G" + num_grammar + "-SC-TS-SC", "r") as sc_file:
        for line in sc_file:
            sc.append(float(line))

    # The real class is SC (2)
    for i in range(1000):
        real_labels.append(2)

    # Find the most probable class
    for i in range(1000):
        pred_labels.append(argmax([eq[i], iso[i], sc[i]]))


# ==========================
# Print the confusion matrix
confus = confusion_matrix(real_labels, pred_labels)
correct = 0
for i in range(3):
    correct = confus[i, i]
    if i == 0:
        print("EQ")
    elif i == 1:
        print("IS")
    else:
        print("SC")
    print("Accuracy:", correct / 10, "%")
    acc = correct / 1000
    # Ready to print to overleaf hehe
    print("Confidence interval: +- (" + str(1.96 * math.sqrt(acc * (1 - acc) / 1000))[0:6] + ", -" + str(1.96 * math.sqrt(acc * (1 - acc) / 1000))[0:6] + ")")
print(confus)