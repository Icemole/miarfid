# Task 0: calculate everything
echo "Calculating probabilities of each triangle according to every grammar..."
for i in EQ IS SC
do
    for j in EQ IS SC
    do
        for k in 1 2 3
        do
            scfg-toolkit/scfg_prob -g models/G${k}-${i} -m corpus/TS-${j} > experiments/G${k}-${i}-TS-${j} && python calculate-everything.py experiments/G${k}-${i}-TS-${j} > results/ppl-G${k}-${i}-TS-${j}
            scfg-toolkit/scfg_prob -g models/G${k}-${i} -m corpus/TS-${j} -v > experiments/G${k}-${i}-TS-${j}-vit && python calculate-everything.py experiments/G${k}-${i}-TS-${j}-vit > results/ppl-G${k}-${i}-TS-${j}-vit
        done
    done
done

# Task 1: extract log-probabilities and perplexity
echo "INSIDE PROBABILITIES"
for j in 1 2 3
do
    for i in EQ IS SC
    do
        echo ""
        echo "Log-prob and perplexity of model G${j}-${i} applied to TS-${i}:"
        cat results/ppl-G${j}-${i}-TS-${i}
        echo "--------------------------"
    done
done

echo ""
echo "VITERBI PROBABILITIES"
for j in 1 2 3
do
    for i in EQ IS SC
    do
        echo ""
        echo "Log-prob and perplexity of model G${j}-${i} applied to TS-${i}:"
        cat results/ppl-G${j}-${i}-TS-${i}-vit
        echo "--------------------------"
    done
done

# Task 2: extract the confusion matrix
echo ""
echo "INSIDE CONFUSION MATRICES"
echo ""
python calculate-confusion.py 1
echo ""
python calculate-confusion.py 2
echo ""
python calculate-confusion.py 3
echo ""
echo "VITERBI CONFUSION MATRICES"
echo ""
python calculate-confusion.py 1 -v
echo ""
python calculate-confusion.py 2 -v
echo ""
python calculate-confusion.py 3 -v
