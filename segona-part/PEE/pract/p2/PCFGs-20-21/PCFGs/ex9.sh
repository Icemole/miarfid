for iovit in "" -v
do
    for brnonbr in Tr Ts
    do
        # train models
        scfg-toolkit/scfg_learn -g MODELS/G-0 -f MODELS/right -m DATA/Tr-right -i 100
        scfg-toolkit/scfg_learn -g MODELS/G-0 -f MODELS/equil -m DATA/Tr-equil -i 100
        scfg-toolkit/scfg_learn -g MODELS/G-0 -f MODELS/isosc -m DATA/Tr-isosc -i 100

        # classify with the trained models and get results
        scfg-toolkit/scfg_prob -g MODELS/right -m DATA/Ts-right > r
        scfg-toolkit/scfg_prob -g MODELS/equil -m DATA/Ts-right > e
        scfg-toolkit/scfg_prob -g MODELS/isosc -m DATA/Ts-right > i
        paste r e i | awk '{m=$1;argm="right"; if ($2>m) {m=$2;argm="equil";}if ($3>m) {m=$3;argm="isosc";}printf("right %s\n",argm);}' > results-${iovit}-${brnonbr}

        scfg-toolkit/scfg_prob -g MODELS/right -m DATA/Ts-equil > r
        scfg-toolkit/scfg_prob -g MODELS/equil -m DATA/Ts-equil > e
        scfg-toolkit/scfg_prob -g MODELS/isosc -m DATA/Ts-equil > i
        paste r e i | awk '{m=$2;argm="equil"; if ($1>m) {m=$1;argm="right";}if ($3>m) {m=$3;argm="isosc";} printf("equil %s\n",argm);}' >> results-${iovit}-${brnonbr}

        scfg-toolkit/scfg_prob -g MODELS/right -m DATA/Ts-isosc > r
        scfg-toolkit/scfg_prob -g MODELS/equil -m DATA/Ts-isosc > e
        scfg-toolkit/scfg_prob -g MODELS/isosc -m DATA/Ts-isosc > i
        paste r e i | awk '{m=$3;argm="isosc"; if ($1>m) {m=$1;argm="right";}if ($2>m) {m=$2;argm="equil";} printf("isosc %s\n",argm);}' >> results-${iovit}-${brnonbr}

        echo "if -v appears here -> viterbi: ${iovit}"
        echo "if Tr appears here -> bracketed: ${brnonbr}"

        cat results-${iovit}-${brnonbr} | scfg-toolkit/confus
    done
done