
#include <fstream>
#include <iostream>
#include <iomanip>

#include "shaders.h"

using std::cout;
using std::cerr;
using std::string;
using std::endl;
using std::setw;

/* 
Carga desde fichero el c�digo fuente del shader y lo compila. 
Devuelve el identificador del shader compilado, o 0 si se produce
alg�n error.
Par�metros de entrada:
	-filename: Nombre del fichero con el c�digo fuente del shader
	-shadertype: Tipo de shader (GL_VERTEX_SHADER � GL_FRAGMENT_SHADER)

*/
GLuint compileShader(const string filename, const GLuint shadertype) {
	std::ifstream file;
	string st;
	Strings ssrc;
	GLchar **src;
	GLsizei nlines;
	GLuint id;
	GLint compiled;

	file.open(filename.c_str());
	if (!file) {
		cerr << "No se ha podido abrir el fichero fuente " << filename << endl;
		return 0;
	} else 
		cerr << endl << "Compilando " << filename << "..." << endl;

	while (!file.eof()) {
		getline(file, st);
		st.push_back('\n');
		ssrc.push_back(st);
	}
	file.close();

	src=new GLchar* [ssrc.size()];
	if (src==NULL) 
		return 0;

	nlines=(GLsizei)ssrc.size();
	for (int i=0; i<nlines; i++)
		src[i]=(GLchar *)ssrc[i].c_str();

#ifdef _DEBUG
	cerr << endl << "Codigo fuente del Shader: "<< endl;
	for (GLsizei j=0; j<nlines; j++)
		cerr << setw(3) << j+1 << " |" << src[j]; // << endl;
#endif

	id=glCreateShader(shadertype);

	glShaderSource(id, nlines, (const GLchar **)src, NULL);
	glCompileShader(id);
	glGetShaderiv(id, GL_COMPILE_STATUS, &compiled);

	delete []src;

	if(compiled == GL_FALSE) {
		GLint length;
		GLchar *log;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		log=new GLchar [length];
		glGetShaderInfoLog(id, length, &length, log);
		cerr << "Error compilando el shader:" << endl;
		cerr << log << endl;
		delete [] log;
		glDeleteShader(id);
		return 0;
	}

	return id;
}


/*

Enlaza un programa compuesto por uno o m�s shaders.
Devuelve el identificador de programa, o 0 si se produce alg�n error.
Par�metros de entrada:
	-un std::vector de GLuint, con los identificadores de shader a enlazar.

*/
GLuint linkProgram(const Uints &shids) {
	
	GLint linked;
	GLint pid;

	pid=glCreateProgram();
	for (unsigned int i=0; i<shids.size(); i++)
		glAttachShader(pid, shids[i]);
	glLinkProgram(pid);
	
	glGetProgramiv(pid, GL_LINK_STATUS, &linked);

	if (linked == GL_FALSE) {
		GLint length;
		GLchar *log;
		glGetProgramiv(pid, GL_INFO_LOG_LENGTH, &length);
		log=new GLchar [length];
		glGetProgramInfoLog(pid, length, &length, log);
		cerr << "Error enlazando el programa:" << endl;
		cerr << log << endl;
		delete [] log;
		glDeleteProgram(pid);
		return 0;
	
	}

	return pid;
}



/* Compila varios ficheros de shader a la vez. Devuelve los identificadores correspondientes */
int compileShaders(const Strings &vshaders, const Strings &fshaders, Uints &vshids, Uints &fshids) {
	GLuint t;

	for (unsigned int i=0; i<vshaders.size(); i++) {
		t=compileShader(vshaders[i], GL_VERTEX_SHADER);
		if (t>0)
			vshids.push_back(t);
	}

	for (unsigned int i=0; i<fshaders.size(); i++) {
		t=compileShader(fshaders[i], GL_FRAGMENT_SHADER);
		if (t>0) 
			fshids.push_back(t);
	}

	return 0;
}
