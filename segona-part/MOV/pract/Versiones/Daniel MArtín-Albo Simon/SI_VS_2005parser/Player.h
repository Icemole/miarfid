/*	Definition of the class player
	Prefijo de clase CP_

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#ifndef CP_CLASS_PLAYER
#define CP_CLASS_PLAYER

class CPlayer
{
	//Attributes
public:
	bool	Alive;						// joueur vivant ?
	bool	Explosion;					// explosion en cours ?
	int		Explosion_life;				// dur�e de vie de l'explosion
	int		Type;						// type
	float	Energy;						// energie
	float	Hit_duration;				// dur�e d'affichage du vaisseau touch�
	float	x, y, z;					// coordonn�es
	float	yi;							// le vaisseau est secou� (passage � game 3D)
	float	yi_speed;					// le vaisseau est secou� (vitesse)
	float	yi_cos;						// le vaisseau est secou� (variable de cosinus)
	float	zi;							// le vaisseau 'flotte' (coord z, pour mode 3D)
	float	zi_speed;					// le vaisseau 'flotte' (vitesse)
	float	Size_x, Size_y;				// taille
	float	Movement;					// d�placement (pour le glissement)
	bool	Immortal;					// Player may not die

	//Weapons
	bool	laser_left;					// laser gauche vivant ?
	bool	laser_left_explosion;		// laser gauche explosion en cours ?
	float	laser_left_explosion_life;	// laser gauche dur�e de l'explosion
	bool	laser_right;				// laser droite vivant ?
	bool	laser_right_explosion;		// laser droite explosion en cours ?
	float	laser_right_explosion_life;	// laser droite dur�e de l'explosion

	//Methods
	bool Init ();	//Used when all the values are initialized by default
					//when reading the global initialization game file
					//Returns true if the initialization of the attirbute has been correctly done

	CPlayer();								//Constructor
};

//Definitions for the game
#define CP_MAX_PLAYERS 10	//Amount of Players that can play in different moments, one after another, 
							//in the same sesion of execution of the game
#endif