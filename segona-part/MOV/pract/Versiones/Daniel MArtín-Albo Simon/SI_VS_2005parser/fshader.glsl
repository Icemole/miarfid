 uniform sampler2D textunit;
 uniform float mode;

void main() {
	vec4 color = texture2D(textunit, gl_TexCoord[0].st);
	if(mode >= 1.0){
		vec4 green = vec4(0.0,1.0,0.0,1.0);  
	    float modulo = mod(gl_FragCoord.y,4.0);
	    if(modulo <= 1.5) gl_FragColor = color * 0.2;
	    else if(modulo <= 2.5) gl_FragColor = color * 0.8 * green;
	    else gl_FragColor = color * green;
	}else{
		gl_FragColor = color;
	}
}
