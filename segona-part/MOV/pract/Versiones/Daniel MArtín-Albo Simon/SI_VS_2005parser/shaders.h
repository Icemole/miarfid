#ifndef _SHADERS_H
#define _SHADERS_H 2008


#include <vector>
#include <string>
#include "gl/glee.h"
#include "gl/glut.h"


typedef std::vector<std::string> Strings;
typedef std::vector<GLuint> Uints; 

/* 
Carga desde fichero el c�digo fuente del shader y lo compila. 
Devuelve el identificador del shader compilado, o 0 si se produce
alg�n error.
Par�metros de entrada:
	-filename: Nombre del fichero con el c�digo fuente del shader
	-shadertype: Tipo de shader (GL_VERTEX_SHADER � GL_FRAGMENT_SHADER)

*/
GLuint compileShader(const std::string filename, const GLuint shadertype);

/*

Enlaza un programa compuesto por uno o m�s shaders.
Devuelve el identificador de programa, o 0 si se produce alg�n error.
Par�metros de entrada:
	-un std::vector de GLuint, con los identificadores de shader a enlazar.

*/
GLuint linkProgram(const Uints &shids);

#endif
