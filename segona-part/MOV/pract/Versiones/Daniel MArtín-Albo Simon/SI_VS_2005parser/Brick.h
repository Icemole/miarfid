/*	Definition of the clas brick

	Prefix: CB_

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#ifndef CB_BRICK
#define CB_BRICK

class CBrick
{
	//Atributos
public:
	
	bool	Active;						// brique active ?
	float	x, y, z;					// coordonn�es

	//Methods
	void Init ();

	CBrick(){Init();}		//Constructor of the class
};

//Definitions for the game
#define CB_MAX_BRICKS 500	//Amount of Bricks available during the same playing level

#endif