/*
	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "Ship.h"

//////////////////////////////////////////////////////////////////////////////////////
//
// Ship initialization
//
// These values are by default. They have to be overwritten in the initialization phase
// when reading the default values from the "initialization.html" file

void CShip::Init()
{
	Alive			=	true;
	Health			=	25;
	Hit_duration	=	500;
	Type			=	HIGH;
	Speed			=	20;
	x				=	0;
	y				=	4;
	z				=	0.05f;
	Size_x			=	0.5;
	Size_y			=	0.3f;
}

unsigned int CS_MaxShips,	//Maximum amount of ships avialable in a given level of game
			 CS_CurrentShip = CG_INIT_INITIAL;
