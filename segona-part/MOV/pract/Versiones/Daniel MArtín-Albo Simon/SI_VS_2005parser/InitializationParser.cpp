/*	Declaration of all the parser methods to understand the initialization files
	Author: Ram�n Moll�
	Last update: 2007-09-11
*/
#include "Character.h"
#include "InitializationParser.h"
#include "Game.h"
#include "Player.h"
#include "SupplyShip.h"
#include "Character.h"
#include <iostream>

//Externals
extern unsigned int MaxPlayers;
extern unsigned int CSS_MaxSupplyShips;
extern unsigned int	CSS_CurrentSupplyShip;
extern CSupplyShip	SupplyShip[CSS_MAX_SUPPLYSHIPS];
extern CPlayer Player[CP_MAX_PLAYERS];
extern unsigned int CS_CurrentShip;
extern int lives;      //Variable con las vidas actuales...
extern int DEF_lives;  //Para recordar las vidas de inicio...
extern int DEF_energy; //Para recordar la energ�a por defecto...
extern int DEF_SShealth; //Para recordar las vidas de la SupplyShip por defecto...
extern int DEF_Shealth;  //Para recordar las vidas de las Ships por defecto...

extern CHAR_CharacterType CharType;
extern char CHAR_Tags[MAX_CHARTYPE][CHAR_TAG_MAX_LONG ];

//Sensitive string tags
char CIP_Tags[MAXTAGS][CIP_TAG_MAX_LONG] = 
{
	"",
	"BODY",		//HTML tag
	"CHARACTER",
	"DIR",
	"EPS",		//Energy Per Shoot
	"EXPLOSION",
	"FILE",
	"FILE",
	"FILE",
	"GEOMETRY",
	"HAVE",		//Recursive definition of a character
	"HEAD",		//HTML tag
	"HEALTH",		//How strong is the character. How much has to be hurt before dieing
	"HTML",		//HTML tag
	"LANGUAGE",
	"LANGUAGES",
	"LIVES",		//Amount of ships the player has still before finishing the game
	"MAXBRICKS",
	"MAXPLAYERS",
	"MAXSHIPS",
	"MAXSUPPLYSHIPS",
	"MESH",
	"MUSIC",
	"NAME",
	"NAME",
	"NAME",
	"NAME",
	"NAME",
	"SCORE",
	"SCORES",
	"SHOOT",		//Kind of shooting
	"SPEED",
	"SPS",		//Shoots Per Second
	"TEXTURE",
	"TITLE",		//HTML tag
	"TYPE",
	"TYPE",
	"UNKNOWN",	//This tag has no matching. It is for management purpouses only. 
	"VERSION"
};

extern CGame Game;

//Stores the new state onto the stack
inline void CInitializationReader::PushState (CIP_TagType State) {StackState[++LastState] = State;}

//Take out the last state from the stack. Shorts the stack. Returns current state
inline CIP_TagType CInitializationReader::PopState  (void){return StackState[--LastState];}

//Translates from a string to the token that the string represents
CIP_TagType CInitializationReader::String2Tag (const char *Name)
{
	switch (Name[0])
	{
	case 'B': if (0 == strcmp(Name, CIP_Tags[BODY]))			return BODY;
		break;
	case 'C': if (0 == strcmp(Name, CIP_Tags[CHARACTER]))		return CHARACTER;	  
		break;
	case 'D': if (0 == strcmp(Name, CIP_Tags[DIR]))				return DIR;
		break;
	case 'E': if (strcmp(Name, CIP_Tags[EPS]))
				if (strcmp(Name, CIP_Tags[EXPLOSION]))			return UNKNOWN;
				else											return EXPLOSION;
			  else												return EPS;
		 break;
	case 'F': if (0 == strcmp(Name, CIP_Tags[FILE_TAG]))		return FILE_TAG;
		break;
	case 'G': if (0 == strcmp(Name, CIP_Tags[GEOMETRY]))		return GEOMETRY;
		break;
	case 'H': if ('E' == Name[1])
				  if (strcmp(Name, CIP_Tags[HEAD]))		
					  if (strcmp(Name, CIP_Tags[HEALTH]))		return UNKNOWN;
					   else										return HEALTH;
				  else											return HEAD;
			  else if (strcmp(Name, CIP_Tags[HAVE]))
						if (strcmp(Name, CIP_Tags[HTML]))		return UNKNOWN;
						else									return HTML;
				   else											return HAVE;
		break;
	case 'L': if (strcmp(Name, CIP_Tags[LANGUAGE]))
					if (strcmp(Name, CIP_Tags[LIVES]))			return UNKNOWN;
					else										return LIVES;
			  else												return LANGUAGE;
		break;
	case 'M': if ('A' == Name[1])
				  //Serie de MAX*
				  if ('S' == Name[3])
					   if (strcmp(Name, CIP_Tags[MAXSHIPS]))
							if (strcmp(Name, CIP_Tags[MAXSUPPLYSHIPS]))	return UNKNOWN;
					        else										return MAXSUPPLYSHIPS;
					   else												return MAXSHIPS;
				  else if (strcmp(Name, CIP_Tags[MAXBRICKS]))
							if (strcmp(Name, CIP_Tags[MAXPLAYERS]))		return UNKNOWN;
					        else										return MAXPLAYERS;
					   else												return MAXBRICKS;
			  else if (strcmp(Name, CIP_Tags[MESH]))
					  if (strcmp(Name, CIP_Tags[MUSIC]))				return UNKNOWN;
					  else												return MUSIC;
				   else													return MESH;
		break;
	case 'N': if (0 == strcmp(Name, CIP_Tags[NAME]))	return NAME;
		break;
	case 'S': if (strcmp(Name, CIP_Tags[SCORE]))
				if (strcmp(Name, CIP_Tags[SCORES]))
				  if (strcmp(Name, CIP_Tags[SHOOT]))
					if (strcmp(Name, CIP_Tags[SPEED]))
					  if (strcmp(Name, CIP_Tags[SPS]))	return UNKNOWN;
					  else								return SPS;
					else								return SPEED;
				  else									return SHOOT;			  
				else									return SCORES;
			  else										return SCORE;
		break;
	case 'T': if  (strcmp(Name, CIP_Tags[TEXTURE]))
				if (strcmp(Name, CIP_Tags[TITLE]))
					if (strcmp(Name, CIP_Tags[TYPE]))	return UNKNOWN;
					else								return TYPE;
				else									return TITLE;
			  else										return TEXTURE;
		break;
	case 'V': if (0 == strcmp(Name, CIP_Tags[VERSION]))	return VERSION;
		break;
	};
	return UNKNOWN;
};


//GAME INITIALIZATION

void CInitializationReader::Init(char *FileName)
{
	LogFile.open(FileName);
	if (!LogFile)	//NAME the file
		exit(0);
	//InitializeState();
};

void  CInitializationReader::BeginParse(DWORD dwAppData, bool &bAbort)
{
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
#ifdef CIP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Comienza la interpretaci�n del fichero de inicializaci�n.\n";
#endif
//	InitializeState();			//Stack reset
}

void CInitializationReader::StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	CString TagString;
	CIP_TagType Tag;

	TagString = pTag->getTagName();

#ifdef CIP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Etiqueta comienzo: " << TagString << "\n";
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	Tag = String2Tag(LPCTSTR(TagString));
	switch(StackState[LastState])
	{case NIL:		//Starting and ending states
				if (HTML == Tag)
					//It is a HTML file. Change to HTML state
					PushState(HTML);
				else ErrorParser("First tag was not an HTML tag.");
		break;
	 case BODY:		//HTML tag
					switch (Tag)
					{
					case DIR:
					case MAXPLAYERS:
					case MAXSHIPS:
					case MAXSUPPLYSHIPS:
					case MAXBRICKS:
					case LANGUAGES:
					case CHARACTER:
					case SCORES: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a BODY group.");
					}
		break;
	case CHARACTER:
					switch (Tag)
					{
					case NAME:
						PushState(NAME_CHARACTER); //Change to the state NAME used to identify a character
						break;
					case TYPE:					//JA: Segurament petar�...
						PushState(TYPE_CHARACTER);
						break;
					case EXPLOSION:
					case LIVES:
					case HEALTH:
					case SPEED:
					case MESH:
					case TEXTURE:
					case SHOOT:
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a CHARACTER group.");
					}
		break;
	case DIR:
					switch (Tag)
					{
					case MUSIC:
					case TEXTURE:
					case GEOMETRY:
					case LANGUAGE: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a DIR group.");
					}
		break;
	case HEAD:		//HTML tag
					switch (Tag)
					{
					case TITLE:
					case VERSION:
					case TYPE:
					case NAME:
					case LANGUAGE: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a HEAD group.");
					}
		break;		
	case HTML:		//HTML Tag
					//The parser is at the very beginning of the iniitalization file
					switch (Tag)
					{
					case HEAD:		//A HEAD Tag is expected
					case BODY: 
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Only the attribute HEAD or BODY may appear in a group HTML.");
					}					
		break;
	case LANGUAGE:
					if (NAME == Tag)
						//Change to NAME state
					PushState(NAME_LANGUAGE);

					//May be that no NAME tag appears or that the NAME section is finished.
					//There is a FILE part also
					else if (FILE_TAG == Tag)
						//Change to FILE state
						PushState(FILE_LANGUAGE);
					else ErrorParser("Only the attribute NAME or FILE may appear in a group NAME. Nothing more.");
		break;
	case LANGUAGES://A LANGUAGE Tag is expected
					if (LANGUAGE == Tag)
						//Change to NAME state
						PushState(LANGUAGE);
					else ErrorParser("Only a LANGUAGE command may appear in a LANGUAGES group.");
		break;
	case SCORES:	//A NAME Tag is expected
					if (NAME == Tag)
						//Change to NAME state
						PushState(NAME_SCORE);

					//May be that no NAME tag appears or that the NAME section is finished.
					//There is a SCORE part also
					else if (SCORE == Tag)
						//Change to SCORE state
						PushState(SCORE);
					else ErrorParser("Only a NAME or SCORE command may appear in a SCORES group.");
		break;
	case SHOOT:		//Kind of shooting
					switch (Tag)
					{
					case SPS:	//Change to SPS state
					case EPS:	//May be that no SPS tag appears or that the SPS section is finished.
								//There is a EPS part also
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Only the attribute SPS or EPS may appear in a SHOOT group. Nothing more.");
					}
		break;
	case TEXTURE:
					if (NAME == Tag)
						//Change to NAME state
						PushState(NAME_TEXTURE);

					//May be that no NAME tag appears or that the NAME section is finished.
					//There is a FILE part also
					else if (FILE_TAG == Tag)
						//Change to FILE state
						PushState(FILE_TEXTURE);
					else ErrorParser("Only the attribute NAME or FILE may appear in a group TEXTURE. Nothing more.");
		break;
	default:;
	};
}

void CInitializationReader::ErrorClosingTag(CString TagString) //Logs a parser error of not matching closing tags
{
	char msj[100];
	
	strcpy_s	(msj, TagString);
	strcat_s	(msj, " clossing tag without correspondent opening tag in a BODY segment.");
	ErrorParser	(msj);
}

void CInitializationReader::EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	CString TagString;
	CIP_TagType Tag;
	bool Error = false;
	char msj[128];

	TagString = pTag->getTagName();

#ifdef CIP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Etiqueta fin: " << pTag->getTagName() << std::endl;
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	Tag = String2Tag(LPCTSTR(TagString));
	switch(StackState[LastState])
	{case BODY:
	 case CHARACTER:
	 case DIR:
	 case EPS:		//Energy Per Shoot
	 case EXPLOSION:
	 case GEOMETRY:
	 case HAVE:		//Recursive definition of a character
	 case HEAD:		
	 case HEALTH:	//How strong is the character. How much has to be hurt before dieing
	 case HTML:		//The parser is at the very end of the initialization file					
	 case LANGUAGE:
	 case LANGUAGES:
	 case LIVES:	
	 case MAXBRICKS:
	 case MAXPLAYERS:
	 case MAXSHIPS:
	 case MAXSUPPLYSHIPS:
	 case MESH:
	 case MUSIC:
	 case SCORE:
	 case SCORES:	
	 case SHOOT:		
	 case SPEED:
	 case SPS:		//Shoots Per Second
	 case TEXTURE:
	 case TITLE:		
	 case TYPE:
	 case VERSION:
					if (StackState[LastState] == Tag) PopState();	//Change to the previous state
					else Error = true;
		break;
 	 case FILE_LANGUAGE:
	 case FILE_TEXTURE:
					if (FILE_TAG == Tag) PopState();	//Change to the previous state
					else Error = true;
		break;
	 case NAME_CHARACTER:
	 case NAME_LANGUAGE:
	 case NAME_SCORE:
	 case NAME_TEXTURE:
					if (NAME == Tag) PopState();	//Change to the previous state
					else Error = true;
		break;
	 case UNKNOWN:	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
					strcpy_s(msj, TagString);
					strcat_s(msj, " clossing tag in an unknown state.");
					ErrorParser(msj);
		break;
	 case NIL:
	 case MAXTAGS:	//Closing a label when all the lables are closed is an error
					strcpy_s(msj, TagString);
					strcat_s(msj, " clossing tag out from the HTML section.");
					ErrorParser(msj);
		break;
	default:;
	};
	if (Error)
	{	strcpy_s(msj, TagString);
		strcat_s(msj, " clossing tag without correspondent opening tag in a ");
		if (LastState)
			 strcat_s(msj, CIP_Tags[StackState[LastState-1]]);
		else strcat_s(msj, CIP_Tags[StackState[LastState]]);
		strcat_s(msj, " segment.");
		ErrorParser(msj);
	}
}

void CInitializationReader::Characters(const CString &rText, DWORD dwAppData, bool &bAbort)
{
char	msj[128], aux[20];
float	f;

#ifdef CIP_DEBUG	//Class HTML Parser Debug activated
LogFile << "Texto: " << rText << std::endl;
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	switch(StackState[LastState])
	{case EPS:		//Energy Per Shoot
					f = atof (rText);
		break;
	case EXPLOSION:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 SupplyShip[CSS_CurrentSupplyShip].Hit_duration = atof (rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case FILE_LANGUAGE:
					
		break;
	case FILE_TEXTURE:
					
		break;
	case GEOMETRY:
					strcpy(Game.GeometryDir, rText);
		break;
	case HEALTH:	//How strong is the character. How much has to be hurt before dieing
					switch (CharType)
					{case SHIP:
						 DEF_Shealth=atoi (rText);
						 break;
					 case SUPPLYSHIP:
						 SupplyShip[CSS_CurrentSupplyShip].Health = atoi (rText);
						 DEF_SShealth=atoi (rText);
						 break;
					 case PLAYER:
						 Player[CG_INIT_CURRENT].Energy=atoi (rText);
						 DEF_energy=Player[CG_INIT_CURRENT].Energy;
						 break;
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case LANGUAGE:
					strcpy(Game.LanguageDir, rText);
		break;
	case LIVES:		//Amount of ships the player has still before finishing the game
		if(atoi(rText)>10){
						strcpy(msj, "Cantidad m�xima de vidas indicada (");
						strcat(msj, rText);
						strcat(msj, ") supera la cantidad m�xima permitida por el programa(10). Valor truncado al m�ximo");
						ErrorParser (msj);
			lives=10;
			DEF_lives=10;
		}
		else{
			if(atoi(rText)<1){
						strcpy(msj, "Cantidad m�xima de vidas indicada (");
						strcat(msj, rText);
						strcat(msj, ") es menor a la cantidad m�nima permitida por el programa(1). Valor truncado al m�nimo");
						ErrorParser (msj);

				lives=1;
				DEF_lives=1;
			}
			else{
				lives=atoi(rText);
				DEF_lives=lives;
			}
		}
		break;
	case MAXBRICKS:
		break;
	case MAXPLAYERS:
					MaxPlayers = atoi(rText);
					//Max and Min truncation
					if (CP_MAX_PLAYERS < MaxPlayers)
					{	strcpy(msj, "Cantidad m�xima de jugadores indicada (");
						strcat(msj, rText);
						strcat(msj, ") supera la cantidad m�xima permitida por el programa(");
						strcat(msj, itoa(CP_MAX_PLAYERS, aux, 10));
						strcat(msj, "). Valor truncado al m�ximo.");
						ErrorParser (msj);
						MaxPlayers = CP_MAX_PLAYERS;
					}
					if (0 > MaxPlayers)
					{	strcpy(msj, "Cantidad m�xima de jugadores indicada (");
						strcat(msj, rText);
						strcat(msj, ") es inferior a la cantidad m�nima necesaria para jugar(1). Valor truncado al m�nimo.");
						ErrorParser (msj);
						MaxPlayers = 1;
					}
		break;
	case MAXSHIPS:
		break;
	case MAXSUPPLYSHIPS:
					CSS_MaxSupplyShips = atoi(rText);
					if (CSS_MAX_SUPPLYSHIPS < CSS_MaxSupplyShips)
					{	strcpy(msj, "Cantidad m�xima de supernaves indicada (");
						strcat(msj, rText);
						strcat(msj, ") supera la cantidad m�xima permitida por el programa(");
						strcat(msj, itoa(CSS_MAX_SUPPLYSHIPS, aux, 10));
						strcat(msj, "). Valor truncado al m�ximo.");
						ErrorParser (msj);
						CSS_MaxSupplyShips = CSS_MAX_SUPPLYSHIPS;
					}
					if (0 > CSS_MaxSupplyShips)
					{	strcpy(msj, "Cantidad m�xima de naves nodriza indicada (");
						strcat(msj, rText);
						strcat(msj, ") es inferior a la cantidad m�nima necesaria para jugar(1). Valor truncado al m�nimo.");
						ErrorParser (msj);
						CSS_MaxSupplyShips = 1;
					}
		break;
	case MESH:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 strcpy(SupplyShip[CSS_CurrentSupplyShip].Mesh, rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case MUSIC:
					strcpy(Game.MusicDir, rText);
		break;
	case NAME_CHARACTER:
			switch (rText[0])
			{
			case 'S':	if (strcmp(rText,CHAR_Tags[SHIP]))
							if (strcmp(rText,CHAR_Tags[SUPPLYSHIP]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
							else {
									CharType = SUPPLYSHIP;
									//Reset all the values by default in case the file does not cover every object aspect
									SupplyShip[CSS_CurrentSupplyShip].Init();
							}
						else CharType = SHIP;
				break;
			case 'B':	if (strcmp(rText,CHAR_Tags[BONUS]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
						else CharType = BONUS;
				break;
			default:	if (strcmp(rText,CHAR_Tags[PLAYER]))
							if (strcmp(rText,CHAR_Tags[WEAPON]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
							else CharType = WEAPON;
						else CharType = PLAYER;
			};
		break;
	case NAME_LANGUAGE:
		break;
	case NAME_SCORE:
		break;
	case NAME_TEXTURE:
		break;
	case SCORE:
		break;
	case SPEED:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 SupplyShip[CSS_CurrentSupplyShip].Speed = atof (rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case SPS:		//Shoots Per Second
		break;
	case TEXTURE:
					strcpy(Game.TextureDir, rText);
		break;
	case TITLE:		//HTML tag
					if (strcmp(rText, Game.Title))
					{ErrorParser ("El fichero no corresponde a este videojuego.");
					 bAbort = true;}
					else ;	//File content corresponds to the expected one for this videogame.
							//Go on. Nothing is done
		break;
	case TYPE:		if (strcmp(rText, "Initialization"))
					{
						strcpy(msj,"El fichero es de tipo \"");
						strcat(msj, rText);
						strcat(msj, "\". Se requiere un fichero de tipo \"inicializaci�n\".");
						ErrorParser (msj);
					 bAbort = true;}
					else ;	//File type is "Initialization"
							//Go on. Nothing is done
		break;
	case TYPE_CHARACTER:
		break;
	case VERSION:	if (strcmp(rText, Game.Version))
					{
						strcpy(msj, "La versi�n del fichero es la v");
						strcat(msj, rText);
						strcat(msj, ", que no corresponde con la v");
						strcat(msj, Game.Version);
						strcat(msj, " de este videojuego.");
						ErrorParser (msj);
					 bAbort = true;}
					else ;	//File version corresponds to the one from this videogame.
							//Go on. Nothing is done
		break;
	default:;		//Tags not supported are not managed, even if they have content associated
	};
}

void CInitializationReader::Comment(const CString &rComment, DWORD dwAppData, bool &bAbort)
{
#ifdef CIP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Comentarios: " << rComment << std::endl;
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CInitializationReader::EndParse(DWORD dwAppData, bool bIsAborted)
{
	UNUSED_ALWAYS(dwAppData);
#ifdef CIP_DEBUG	//Class HTML Parser Debug activated
	if (bIsAborted) LogFile << "Se ha acabado la interpretaci�n del fichero.\n";
#endif
}