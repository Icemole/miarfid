// si_Main.h - space invaders opengl

#ifndef _MAIN_H
#define _MAIN_H

#ifdef  _MSC_VER
#pragma warning (disable:4305 4244 4005)
//#pragma warning (disable:4305 4244)	// Disable warnings on precision truncations/conversions
									// 4305 : truncation
									// 4244 : conversion, possible loss of data
#endif

#define PI 3.1415926535

#include "gl\glaux.h"					// Header File For Some OpenGL Function
#include <math.h>						// Header File For sin(), cos(), _hypot()
#include <process.h>					// Header File For _beginthread, _endthread 
#include "resource.h"					// Header File For Game Resources
#include "si_Basecode.h"				// Header File For Game
#include "si_Fmod.h"					// Header File For FMOD (sounds & music)
#include "si_Fmod_errors.h"				// Header File For FMOD (sounds & music errors)
#include "si_Fps.h"						// Header File For Frame Per Second, TimerGetTime()
#include "si_Functions.h"				// Header File For Game
#include "si_Particles.h"				// Header File For Game
#include "si_Resources.h"				// Header File For Game
#include "si_World.h"					// Header File For Game
//#include "si_3DS.h"					// Header File For 3DS Objects
#include "si_Font.h"					// Header File For 2D Text
#include "Player.h"						// Header File class player
#include "SupplyShip.h"					// Header File class SupplyShip. Big ship that comands the whole enemy navy
#include "Ship.h"					// Header File class Ship. Ships that fight in the enemy navy
#include "Game.h"						// Header File for general information about the whole game
#include "Brick.h"						// Header File class B rick. What the bunkers are made of

//Avoids warnings of deprecated functions
#define _CRT_SECURE_NO_WARNINGS

// MACRO : affiche une messagebox avec un message
#define BOX(text) MessageBox(NULL,text,"Info",MB_OK | MB_TOPMOST);

// MACRO : affiche un message d'erreur avec le nom du fichier et la ligne
#define ERR(s) DisplayMsgBoxErrorAndExit(__FILE__, __LINE__, s)

// MACRO : affiche le dernier message d'erreur sur la sortie de Debug
#define LAST_ERROR() DisplayLastErrorDebug()

// MACRO : affiche un *entier* dans le debugger *une seule fois* (si diff�rent de 0)
#define DISPLAY_1INT(var)											\
 		static int my_number = 0;									\
		if (my_number == 0)											\
		{															\
			my_number = var;										\
			char my_string[300]="";									\
			OutputDebugString (_itoa(my_number, my_string, 10));	\
		}															\

// MACRO : affiche un *entier* dans le debugger
#define DISPLAY_INT(var)											\
			{														\
				char my_string[300]="";								\
				OutputDebugString (_itoa(var, my_string, 10));		\
			}														\

// MACRO : affiche une *cha�ne* dans le debugger
#define DISPLAY(var)												\
			OutputDebugString (var);								\


//////////////////////////////////////////////////////////////////////////////////////
//
// VARIABLES GLOBALES => Cr�ation des structures
//
//

extern CPlayer player			[CP_MAX_PLAYERS];			//Amount of players
extern CSupplyShip SupplyShip	[CSS_MAX_SUPPLYSHIPS];		// Amount of SupplyShips available in the game

extern CBrick brick[CB_MAX_BRICKS];		// nombre de briques

extern CShip ship[50];					// nombre de ships ennemis

typedef struct							// TIRS (JOUEUR ET ENNEMIS)
{
	int		Type;						// type (0 1 2 = tir player, 10 = tir ships)
	bool	Active;						// tir actif ?
	bool	Tail;						// train�e pour les particules (quand le tir a touch� un impact, la train�e doit �tre toujours pr�sente jusqu'� dissipation)
	float	x, y, z;					// coordonn�es
	int		mode;						// dependiendo del tipo de disparo viaja de una forma u otra
	//particles2 particle2[MAX_PARTICLES2];// tir 3D
}
tirs;
extern tirs	tir[51];					// nombre de tirs permis au m�me moment (index remis � z�ro si limite d�pass�e)

typedef enum {
	BONUS3D,
	BONUSLASER,
	BONUSWEAPON
} BONUS_TYPE;

typedef struct							// BONUS
{
	BONUS_TYPE		Type;			// type
	bool			Active;			// The bonus is flowing down in the screen
	bool			Explosion;		// Explosion en cours ?
	float			Explosion_life;	// dur�e de l'Explosion
	float			x, y, z;		// coordonn�es
	float			Shake1, Shake2;	// The scene is shaking
}
bonuses;
#define MAX_BONUSES		10
extern  bonuses			bonus[MAX_BONUSES];	// nombre de bonus

extern bonuses bonus_classic;
extern GLint color;

//////////////////////////////////////////////////////////////////////////////////////
//
// VARIABLES GLOBALES => Autres Variables
//
//

// Application - Options
extern GL_Window* g_window;				// infos de la fen�tre (variable globale)
extern CGame Game;

// Console
extern char		console_command[40];
extern char		console_command2[40];
extern char		console_output[40];
extern char		console_output2[40];
extern float	viewport_push;

// Musique
extern char		stream_name[1024];		// streamed music name
extern char		stream_name_user[1024];	// streamed music user
extern char		music_en_cours[1024];	// musique en cours
extern char		music_affichee[1024];	// musique affichee (longueur limitee)
extern Playlist* my_playlist;			// Shoutcast playlist

extern int		int_sound,				// numero channel pour les bruitages
				int_sound_mp3,			// numero channel pour les musiques mp3
				int_sound_mod;			// numero channel pour la musique mod
extern bool		music_on;				// musique mp3 on/off
extern int		music_type;				// musique type
extern int		id_my_music;			// identifiant pour la musique en cours : music[id_my_music].titre (0="." et 1="..")
extern FSOUND_DSPUNIT * dspUnitSounds;	// DSP utilis� pour les sons non "int�gr�s" dans le spectrum
extern FSOUND_DSPUNIT * dspUnitSpectrum;// DSP utilis� pour le spectrum (musiques streams, mp3 ...)
extern signed short *OscBuffer;
extern int           OscBlock;
extern FSOUND_STREAM * my_stream;		// musique streaming
extern FSOUND_STREAM * my_mp3;			// musique mp3
extern FMUSIC_MODULE * my_mod;			// musique mod
extern FMUSIC_MODULE * my_mod_about;	// musique mod about-dialogbox
extern FSOUND_SAMPLE * sound_tir,		// sons wav et mp3
				* sound_tir3d,
				* sound_touche,
				* sound_descente,
				* sound_applause,
				* sound_shield,
				* sound_cross,
				* sound_bonus,
				* sound_key;
extern bool		sound_cross_passed;		// son cross passage 2d->3d
typedef struct {char fichier[256];}		// nom de fichier d'une musique mp3
musics;
extern musics	music[300];				// 300 struct music
/*
typedef struct
{
    int    count;
    void **name;
    void **displayname;
} Playlist;								// Winamp playlist
*/

extern float	game_3d_ajust;			// 3D adjust
extern char		console_text[50][50];	// console text
extern int		console_line;			// console line
extern int		id_brick;				// index des briques
extern GLuint	texture[200];			// tableau des textures
extern int		game_level;				// fond du jeu
extern int		ships_choix_anim;		// anim des ships ennemis
extern const int max_tirs;				// nombre de tirs maximum affich�s en m�me temps
extern unsigned int	score;				// score
extern unsigned int	hiscore;			// highscore
extern int		lives;					// vies
extern bool		powerup;				// powerup (passage � game 3D)
extern bool		Fps2_firstpass;			// FPS M�thode n�2
extern double	angle_x_start;			// angle x utilisateur
extern double	angle_y_start;			// angle y utilisateur
extern int		fontsize;				// font size
extern int		fontspace;				// space between fonts
extern int		equalizer_x;			// equalizer coord x
extern int		equalizer_y;			// equalizer coord y
extern int		shoot_type;
extern int		ammo[3];
extern int		ini_mis_texturas;
/*
extern Model_3DS m;						// objet 3DS
extern Model_3DS m1;					// objet 3DS
extern Model_3DS m2;					// objet 3DS
extern Model_3DS m3;					// objet 3DS
//extern Model_3DS m4;					// objet 3DS
extern Model_3DS m5;					// objet 3DS
*/
extern GLUquadricObj*	quadratic;		// Storage For Our Quadratic Objects

#endif
