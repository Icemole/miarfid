/*Definition of the class game
  General class to support all the attribute general to all the levels, players, ships,...
  Prefix: CG_

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#ifndef CG_CLASS_GAME
#define CG_CLASS_GAME

//ANTI-ALIASING MODES
enum {AAx2, AAx2QUINQUX, AAx4, AAx4S};

//Game States
enum GameStates{INITIAL,	//The game is in the initialization phase
				FADING_IN,	//The game is appearing from black
				LOADING, 
				FADING_OUT, //The game is fading to black
				PLAYING,	//The player is playing now the game
				WON,		//The player has won the game
				LOST,		//The player has lost the game
				NOSTATES};	//Amount of different states in the game

#define CG_LONG_DIR_NAME 256

class CGame
{
	//Attributes
public:
	//Game general attributes
	char	Version	[9];		// VERSION
	char	Title	[20];		// TITLE
	int		Step;				// STAGE of the game
	int		Level;				// LEVEL

	//Visualization
	bool	Tridimensional;		// 3D GAME not ACTIVE
	bool	Lines3D;			// 3D GAME - MODE GL_LINES
	bool	Mipmap;				// MIPMAP available
	bool	Vsync;				// VSYNC available
	bool	Vsync_active;		// VSYNC ACTIVE
	bool	Billboard;			// BILLBOARDS 

	//Antialiasing
	bool	Antialiasing;		// ANTI-ALIASING available
	int		Antialiasing_mode;	// ANTI-ALIASING MODE
	bool	Antialiasing_active;// ANTI-ALIASING ACTIVE

	//Music
	bool	Stream;				// MUSIC STREAMING
	int		Bitrate;			// MUSIC BITRATE 

	//WIVIK KEYBOARD 
	bool	Wivik;				// HANDLING ACTIVATED
	int		Holddown_wivik;		// HOLD DOWN KEY DURATION IN ms

	bool	Text_2D;			// 2D TEXT AFFICHE ?
	char	Message[300];		// MESSAGE UTILISATEUR
	bool	Spectrum;			// SPECTRUM (BEAT DETECTION)?

	//Console
	bool	Console;			// CONSOLE INACTIVE
	bool	Console_come_in;	// CONSOLE on screen
	bool	Console_moving;		// CONSOLE EN DEPLACEMENT ?
	bool	Console_start;		// CONSOLE EN DEPLACEMENT ?

	bool	Loading;			// To make the Loading Thread called only once
	bool	Loading_finished;

	int		Lang;				// LANGUAGE (0 english, 1 french, 2 spanish)

	bool LevelReaded;		// Lectura de nivel realizada (No es que no sepa ingl�s, es para diferenciarlo mejor)
	bool ReadLevel;		// Activar/Desactivar lectura de nivel por parser

	enum GameStates State;	//The state in which the game may be running in a given moment
	//Directories
	char LanguageDir	[CG_LONG_DIR_NAME];
	char LevelDir		[CG_LONG_DIR_NAME];
	char GeometryDir	[CG_LONG_DIR_NAME];
	char TextureDir		[CG_LONG_DIR_NAME];
	char MusicDir		[CG_LONG_DIR_NAME];

	//TEXT of the game
	char	Quit	[20];
	char	Change	[50];		
	char	Rotate	[20];		
	char	Music	[20];		
	char	Fondo	[20];		
	char	Cam		[20];		
	char	Hide	[20];
	char	Fin		[20];		
	char	Won		[20];		
	char	Lose	[20];		
	char	Ancho	[20];		
	char	Alto	[20];		


//methods
	//Constructor
	CGame ();

	void InitializeLanguages ();
};

//Definitions for the game
#define CG_MAX_PLAYERS 10	//Amount of Players that can play in different moments, one after another, 
							//in the same sesion of execution of the game

#define CG_INIT_INITIAL 0	//Number in the array or elements that holds the configuration by default
							//defined in the "initialization.html" file during the initialization phase
#define CG_INIT_CURRENT 1	//Position of the current element used as default player, super ship,...

#endif