/*	Definition of the class Ship

	Prefix: CS_
	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "Game.h"
#include "Character.h"

#ifndef CS_SHIP
#define CS_SHIP

typedef enum {
	CS_NO_SHIP,
	HIGH,
	MIDDLE,
	LOW,
	CS_MAXTYPE
} CS_SHIP_TYPE;

class CShip
{
	//Atributos
public:
	bool			Alive;			// Ship is still on play
	int				Health;			// Health
	CS_SHIP_TYPE	Type;			// Class of enemy ship
	float			Hit_duration;	// dur�e d'affichage du supplyship touch�
	bool			Explosion;		// explosion is still active
	int				Explosion_life;	// Explosion life duration
	float			Speed;			// Velocity
	float			x, y, z;		// Coordinates
	float			Size_x, Size_y;	// Size
	float			zi;				// le vaisseau 'flotte' (coord z pour mode 3D)
	float			zi_speed;		// le vaisseau 'flotte' (vitesse)
	float			my_rand;		// nombre al�atoire

	//Methods
	void Init ();

	CShip(){Init();}		//Constructor of the class
};

//Definitions for the game
#define CS_MAX_SHIPS 50	//Amount of Ships available during the same playing level

#endif