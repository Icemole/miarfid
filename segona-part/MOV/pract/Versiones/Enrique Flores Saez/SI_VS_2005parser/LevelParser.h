/*	Class that loads the values of a given level of game 

	Prefix CLeP_: Class Level Parser

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "LiteHTMLReader.h"
#include <fstream>

#define CLeP_DEBUG				//Class HTML Parser Debug activated
#define CLeP_TAG_MAX_LONG 15	//Maximun length of a sensitive tag
#define CLeP_MAXSTACK	  16	//Maximun deepness of the stack

//Types of tags the initialization parser can match during the parsing
//They can be used also as states of of the stack machine (parser) during the parsing
typedef enum {
	NIL_L,		//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	BODY_L,		//HTML tag
	BUNKER_L,
	BUNKERDEF_L,
	CHARACTER_L,
	DIR_L,
	EPS_L,		//Energy Per Shoot
	EXPLOSION_L,
	FILE_TAG_L,
	FILE_TEXTURE_L,
	GEOMETRY_L,
	HAVE_L,		//Recursive definition of a character
	HEAD_L,		//HTML tag
	HEALTH_L,		//How strong is the character. How much has to be hurt before dieing
	HTML_L,		//HTML tag
	ID_L,
	LEVEL_L,
	LINE_L,
	LIVES_L,		//Amount of ships the player has still before finishing the game
	MAXBRICKS_L,
	MAXPLAYERS_L,
	MAXSHIPS_L,
	MAXSUPPLYSHIPS_L,
	MESH_L,
	MUSIC_L,
	NAME_L,
	NAME_CHARACTER_L,
	NAME_SCORE_L,
	NAME_TEXTURE_L,
	NUMSS_L,
	POSITION_L,		//Posici�n espec�fica de un personaje
	REGENERATION_L, //Regeneraci�n S�/No
	SCORES_L,
	SCORE_L,
	SHOOT_L,		//Kind of shooting
	SHOOTS_L,		//Max. n� de disparos en el nivel.
	SPEED_L,
	SPS_L,		//Shoots Per Second
	TEXTURE_L,
	TITLE_L,		//HTML tag
	TYPE_L,
	X_L,			//Pos. X
	Y_L,			//Pos. Y
	Z_L,			//Pos. Z
	UNKNOWN_L,	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	VERSION_L,
	MAXTAGS_L		//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
} CLeP_TagType;

/* Class that loads the values by default in the very beginning of every level in the game
*/

class CLevelReader : public ILiteHTMLReaderEvents
{

private:
    std::ofstream	LogFile;
	CLeP_TagType	StackState[CLeP_MAXSTACK];	//Stack automata. Stack deepness	
	unsigned char	LastState;					//Pointer to the last stack. Never negative

	inline void		InitializeState(){LastState = 0; StackState[LastState]=NIL_L;};	//Starts up the stack
	void			PushState (CLeP_TagType State);	//Stores the new state onto the stack
													//Overflow never happens
	CLeP_TagType	PopState  (void);	//Take out the last state from the stack. Shorts the stack
										//Returns current state
    void BeginParse(DWORD dwAppData, bool &bAbort);
    void StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void Characters(const CString &rText, DWORD dwAppData, bool &bAbort);
    void Comment(const CString &rComment, DWORD dwAppData, bool &bAbort);
    void EndParse(DWORD dwAppData, bool bIsAborted);
	void ErrorParser(char *msj) {LogFile << "Parser error: " << msj << std::endl;}
	void ErrorClosingTag(CString TagString); //Logs a parser error of not matching closing tags

	//Translates from a string to the token that the string represents
	CLeP_TagType String2Tag (const char *Name);

public:
	void Init(char *FileName);

	//Constructor and destructor
	CLevelReader() {InitializeState();};
	~CLevelReader(){if (NULL != LogFile) LogFile.close();};
};