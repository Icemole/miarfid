/*	Class that loads the values of a given level of game 

	Prefix CLaP_: Class Level Parser

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "LiteHTMLReader.h"
#include <fstream>
#define CLaP_DEBUG	//Class HTML Parser Debug activated
#define CLaP_TAG_MAX_LONG 15	//Maximun length of a sensitive tag
#define CLaP_MAXSTACK	  16	//Maximun deepness of the stack
//Types of tags the initialization parser can match during the parsing
//They can be used also as states of of the stack machine (parser) during the parsing
typedef enum {
	NIL_LA,
	BODY_LA,		//HTML tag
	CONTENT_LA,
	HEAD_LA,		//HTML tag
	HTML_LA,		//HTML tag
	ID_LA,
	NAME_LA,
	RESOURCE_LA,
	TITLE_LA,		//HTML tag
	TYPE_LA,
	UNKNOWN_LA,	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	VERSION_LA,
	MAXTAGS_LA
} CLaP_TagType;

/* Class that loads the values by default in the very beginning of every level in the game
*/

class CLanguageReader : public ILiteHTMLReaderEvents
{
    std::ofstream	LogFile;
	CLaP_TagType	StackState[CLaP_MAXSTACK];	//Stack automata. Stack deepness	
	unsigned char	LastState;					//Pointer to the last stack. Never negative

	inline void		InitializeState(){LastState = 0; StackState[LastState]=NIL_LA;};	//Starts up the stack
	void			PushState (CLaP_TagType State);	//Stores the new state onto the stack
													//Overflow never happens
	CLaP_TagType	PopState  (void);	//Take out the last state from the stack. Shorts the stack
public:
	void Init(char *FileName);
private:
	
    void BeginParse(DWORD dwAppData, bool &bAbort);
    void StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void Characters(const CString &rText, DWORD dwAppData, bool &bAbort);
    void Comment(const CString &rComment, DWORD dwAppData, bool &bAbort);
    void EndParse(DWORD dwAppData, bool bIsAborted);
void ErrorParser(char *msj) {LogFile << "Parser error: " << msj << std::endl;}
	//Translates from a string to the token that the string represents
	CLaP_TagType String2Tag (const char *Name);
};