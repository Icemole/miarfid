/*LEVEL READER
	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "Character.h"
#include "LevelParser.h"
#include "Game.h"
#include "Player.h"
#include "SupplyShip.h"
//#include "Character.h"
#include <iostream>

int G, actbun[4];
extern CHAR_CharacterType CharType;
//Externals
extern unsigned int MaxPlayers;
extern unsigned int CSS_MaxSupplyShips;
extern unsigned int	CSS_CurrentSupplyShip;
extern unsigned int LEV_NumSS;
extern bool regen_SS;
extern CSupplyShip	SupplyShip[CSS_MAX_SUPPLYSHIPS];
extern CPlayer Player[CP_MAX_PLAYERS];
extern unsigned int CS_CurrentShip;

extern int bunker_defecto;
extern int bun1[112];
extern int bun2[112];
extern int bun3[112];
extern int bun4[112];
extern int lives;
extern int LEV_energy;
extern int DEF_energy;
extern int LEV_SShealth;
extern int DEF_SShealth;
extern int LEV_Shealth;
extern int DEF_Shealth;

extern CHAR_CharacterType CharType;
extern char CHAR_Tags[MAX_CHARTYPE][CHAR_TAG_MAX_LONG ];

extern CGame Game;
//Sensitive string tags
int BActual;
char CLeP_Tags[MAXTAGS_L][CLeP_TAG_MAX_LONG] = 
{
	"",
	"BODY",		//HTML tag
	"BUNKER",
	"BUNKERDEF",
	"CHARACTER",
	"DIR",
	"EPS",		//Energy Per Shoot
	"EXPLOSION",
	"FILE",
	"FILE",
	"GEOMETRY",
	"HAVE",		//Recursive definition of a character
	"HEAD",		//HTML tag
	"HEALTH",	//How strong is the character. How much has to be hurt before dieing
	"HTML",		//HTML tag
	"ID",
	"LEVEL",
	"LINE",
	"LIVES",	//Amount of ships the player has still before finishing the game
	"MAXBRICKS",
	"MAXPLAYERS",
	"MAXSHIPS",
	"MAXSUPPLYSHIPS",
	"MESH",
	"MUSIC",
	"NAME",
	"NAME",
	"NAME",
	"NAME",
	"NUMSS",
	"POSITION",
	"REGENERATION",
	"SCORES",
	"SCORE",
	"SHOOT",		//Kind of shooting
	"SHOOTS",
	"SPEED",
	"SPS",		//Shoots Per Second
	"TEXTURE",
	"TITLE",		//HTML tag
	"TYPE",
	"X",
	"Y",
	"Z",
	"UNKNOWN",	//This tag has no matching. It is for management purpouses only. 
	"VERSION"
};

//Stores the new state onto the stack
void CLevelReader::PushState (CLeP_TagType State)
{	++LastState;
	if (CLeP_MAXSTACK == LastState) 
	{	//ERROR. Amount of states stored in the stack has outrun the maximun stack deepness
		//Report to the player

	}
	else StackState[LastState] = State;
}

//Take out the last state from the stack. Shorts the stack. Returns current state
CLeP_TagType CLevelReader::PopState  (void)
{	CLeP_TagType CurrentState = StackState[LastState];
	LastState--;
	if (0 > LastState)
	{	//First state reached. Reset stack
		LastState = 0;
		StackState[0]=NIL_L;
	}
	return CurrentState;
}

//Translates from a string to the token that the string represents
CLeP_TagType CLevelReader::String2Tag (const char *Name)
{
switch (Name[0])
	{
	case 'B': if (strcmp(Name, CLeP_Tags[BODY_L]))			
				  if (strcmp(Name, CLeP_Tags[BUNKER_L])) 
					  if (strcmp(Name, CLeP_Tags[BUNKERDEF_L])) return UNKNOWN_L;
					  else						return BUNKERDEF_L;
				  else							return BUNKER_L;
			  else				  			    return BODY_L;
		break;
	case 'C': if (0 == strcmp(Name, CLeP_Tags[CHARACTER_L]))		return CHARACTER_L;	  
		break;
	case 'D': if (0 == strcmp(Name, CLeP_Tags[DIR_L]))				return DIR_L;
		break;
	case 'E': if (strcmp(Name, CLeP_Tags[EPS_L]))
				if (strcmp(Name, CLeP_Tags[EXPLOSION_L]))			return UNKNOWN_L;
				else											return EXPLOSION_L;
			  else												return EPS_L;
		 break;
	case 'F':  if(strcmp(Name, CLeP_Tags[FILE_TAG_L]))
				   if(strcmp(Name, CLeP_Tags[FILE_TEXTURE_L])) return UNKNOWN_L;
				   else										return FILE_TEXTURE_L;
			   else											return FILE_TAG_L;
		//if (0 == strcmp(Name, CLeP_Tags[FILE_TAG_L]))		return FILE_TAG_L;
		break;
	case 'G': if (0 == strcmp(Name, CLeP_Tags[GEOMETRY_L]))		return GEOMETRY_L;
		break;
	case 'H': if ('E' == Name[1])
				  if (strcmp(Name, CLeP_Tags[HEAD_L]))		
					  if (strcmp(Name, CLeP_Tags[HEALTH_L]))		return UNKNOWN_L;
					   else										return HEALTH_L;
				  else											return HEAD_L;
			  else if (strcmp(Name, CLeP_Tags[HAVE_L]))
						if (strcmp(Name, CLeP_Tags[HTML_L]))		return UNKNOWN_L;
						else									return HTML_L;
				   else											return HAVE_L;
		break;
	case 'I': if (0 == strcmp(Name, CLeP_Tags[ID_L]))		 return ID_L;
		break;
	case 'L': if (strcmp(Name, CLeP_Tags[LIVES_L]))			
				  if (strcmp(Name, CLeP_Tags[LEVEL_L])) 
					  if (strcmp(Name, CLeP_Tags[LINE_L])) return UNKNOWN_L;
					  else return LINE_L;
				  else return LEVEL_L;
			else return LIVES_L;
		break;
	case 'M': if ('A' == Name[1])
				  //Serie de MAX*
				  if ('S' == Name[3])
					   if (strcmp(Name, CLeP_Tags[MAXSHIPS_L]))
							if (strcmp(Name, CLeP_Tags[MAXSUPPLYSHIPS_L]))	return UNKNOWN_L;
					        else										return MAXSUPPLYSHIPS_L;
					   else												return MAXSHIPS_L;
				  else if (strcmp(Name, CLeP_Tags[MAXBRICKS_L]))
							if (strcmp(Name, CLeP_Tags[MAXPLAYERS_L]))		return UNKNOWN_L;
					        else										return MAXPLAYERS_L;
					   else												return MAXBRICKS_L;
			  else if (strcmp(Name, CLeP_Tags[MESH_L]))
					  if (strcmp(Name, CLeP_Tags[MUSIC_L]))				return UNKNOWN_L;
					  else												return MUSIC_L;
				   else													return MESH_L;
		break;
	case 'N': if (strcmp(Name, CLeP_Tags[NAME_L]))	
			        if (strcmp(Name, CLeP_Tags[NUMSS_L]))				return UNKNOWN_L;
					else												return NUMSS_L;
			  else														return NAME_L;
		break;
	case 'P': if (0 == strcmp(Name, CLeP_Tags[POSITION_L])) return POSITION_L;
		break;
	case 'R': if (0 == strcmp(Name, CLeP_Tags[REGENERATION_L])) return REGENERATION_L;
		break;
	case 'S': if (strcmp(Name, CLeP_Tags[SCORE_L]))
				if (strcmp(Name, CLeP_Tags[SCORES_L]))
				  if (strcmp(Name, CLeP_Tags[SHOOT_L]))
					if (strcmp(Name, CLeP_Tags[SPEED_L]))
					  if (strcmp(Name, CLeP_Tags[SPS_L]))	
						if (strcmp(Name, CLeP_Tags[SHOOTS_L])) return UNKNOWN_L;
						else							return SHOOTS_L;
					  else								return SPS_L;
					else								return SPEED_L;
				  else									return SHOOT_L;			  
				else									return SCORES_L;
			  else										return SCORE_L;
		break;
	case 'T': if  (strcmp(Name, CLeP_Tags[TEXTURE_L]))
				if (strcmp(Name, CLeP_Tags[TITLE_L]))
					if (strcmp(Name, CLeP_Tags[TYPE_L]))	return UNKNOWN_L;
					else								return TYPE_L;
				else									return TITLE_L;
			  else										return TEXTURE_L;
		break;
	case 'V': if (0 == strcmp(Name, CLeP_Tags[VERSION_L]))	return VERSION_L;
		break;
	case 'X': if (0 == strcmp(Name, CLeP_Tags[X_L]))	return X_L;
		break;
	case 'Y': if (0 == strcmp(Name, CLeP_Tags[Y_L]))	return Y_L;
		break;
	case 'Z': if (0 == strcmp(Name, CLeP_Tags[Z_L]))	return Z_L;
		break;
	};
	return UNKNOWN_L;
};


/* Class that loads the values by default in the very beginning of every level in the game

   LEVEL INITIALIZATION
*/

void CLevelReader::Init(char *FileName)
{
	LogFile.open(FileName);
	if (!LogFile)	//NAME the file
		exit(0);
	InitializeState();
};

void CLevelReader::BeginParse(DWORD dwAppData, bool &bAbort)
{
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
#ifdef CLeP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Begining the initialization file parsing.\n";
#endif
}

void CLevelReader::StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	CString TagString;
	CLeP_TagType Tag;

	TagString = pTag->getTagName();

#ifdef CLeP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Start tag: " << TagString << "\n";
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	Tag = String2Tag(LPCTSTR(TagString));
	switch(StackState[LastState])
	{case NIL_L:		//Starting and ending states
				if (HTML_L == Tag)
					//It is a HTML file. Change to HTML state
					PushState(HTML_L);
				else ErrorParser("First tag was not an HTML tag.");
		break;
	 case BODY_L:		//HTML tag
					switch (Tag)
					{
					case BUNKER_L:
					case BUNKERDEF_L:
					case DIR_L:
					case MAXPLAYERS_L:
					case MAXSHIPS_L:
					case MAXSUPPLYSHIPS_L:
					case MAXBRICKS_L:
					case NUMSS_L:
					case CHARACTER_L:
					case REGENERATION_L:
					case SHOOTS_L:
					case SCORES_L: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a BODY group.");
					}
		break;
	 case BUNKER_L:   
		 switch (Tag){
			case ID_L:
			case LINE_L: PushState(Tag);
				break;
			default: ErrorParser("Tag not allowed in a BUNKER group.");
		 }
		break;

	 case HAVE_L:
				switch (Tag)
				{
					case EXPLOSION_L:
					case LIVES_L:
					case HEALTH_L:
					case POSITION_L:
					case SPEED_L:
					case MESH_L:
					case TEXTURE_L:
					case TYPE_L:
					case SHOOT_L:
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a HAVE group.");
				}
		 break;
	case CHARACTER_L:
					switch (Tag)
					{
					case NAME_L:
						PushState(NAME_CHARACTER_L); //Change to the state NAME used to identify a character
						break;
					case EXPLOSION_L:
					case LIVES_L:
					case HEALTH_L:
					case HAVE_L:
					case POSITION_L:
					case SPEED_L:
					case MESH_L:
					case TEXTURE_L:
					case TYPE_L:
					case SHOOT_L:
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a CHARACTER group.");
					}
		break;
	case DIR_L:
					switch (Tag)
					{
					case MUSIC_L:
					case TEXTURE_L:
					case GEOMETRY_L: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a DIR group.");
					}
		break;
	case HEAD_L:		//HTML tag
					switch (Tag)
					{
					case TITLE_L:
					case VERSION_L:
					case TYPE_L:
					case LEVEL_L: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a HEAD group.");
					}
		break;		
	case HTML_L:		//HTML Tag
					//The parser is at the very beginning of the iniitalization file
					switch (Tag)
					{
					case HEAD_L:		//A HEAD Tag is expected
					case BODY_L: 
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Only the attribute HEAD or BODY may appear in a group HTML.");
					}					
		break;

	case POSITION_L:
				switch (Tag)
				{
					case X_L:
					case Y_L:
					case Z_L: PushState(Tag);
							  break;
					default: ErrorParser("Tag not allowed in a POSITION group.");
				}
		break;
	case SCORES_L:	//A NAME Tag is expected
					if (NAME_L == Tag)
						//Change to NAME state
						PushState(NAME_SCORE_L);

					//May be that no NAME tag appears or that the NAME section is finished.
					//There is a SCORE part also
					else if (SCORE_L == Tag)
						//Change to SCORE state
						PushState(SCORE_L);
					else ErrorParser("Only a NAME or SCORE command may appear in a SCORES group.");
		break;
	case SHOOT_L:		//Kind of shooting
					switch (Tag)
					{
					case SPS_L:	//Change to SPS state
					case EPS_L:	//May be that no SPS tag appears or that the SPS section is finished.
								//There is a EPS part also
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Only the attribute SPS or EPS may appear in a SHOOT group. Nothing more.");
					}
		break;
	case TEXTURE_L:
					if (NAME_L == Tag)
						//Change to NAME state
						PushState(NAME_TEXTURE_L);

					//May be that no NAME tag appears or that the NAME section is finished.
					//There is a FILE part also
					else if (FILE_TAG_L == Tag)
						//Change to FILE state
						PushState(FILE_TEXTURE_L);
					else ErrorParser("Only the attribute NAME or FILE may appear in a group TEXTURE. Nothing more.");
		break;
	default:;
	};
}

void CLevelReader::EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	CString TagString;
	CLeP_TagType Tag;
	bool Error = false;
	char msj[128];

	TagString = pTag->getTagName();

	#ifdef CLeP_DEBUG	//Class HTML Parser Debug activated
		LogFile << "Etiqueta fin: " << pTag->getTagName() << std::endl;
	#endif
	//printf("Etiqueta fin: %s\n", pTag->getTagName());
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	Tag = String2Tag(LPCTSTR(TagString));
	switch(StackState[LastState])
	{case BODY_L:
	 case BUNKER_L:
	 case BUNKERDEF_L:
	 case CHARACTER_L:
	 case DIR_L:
	 case EPS_L:		//Energy Per Shoot
	 case EXPLOSION_L:
	 case FILE_TAG_L:
	 case GEOMETRY_L:
	 case HAVE_L:		//Recursive definition of a character
	 case HEAD_L:		
	 case HEALTH_L:	//How strong is the character. How much has to be hurt before dieing
	 case HTML_L:		//The parser is at the very end of the initialization file					
	 case ID_L:
	 case LEVEL_L:
	 case LINE_L:
	 case LIVES_L:
	 case MAXBRICKS_L:
	 case MAXPLAYERS_L:
	 case MAXSHIPS_L:
	 case MAXSUPPLYSHIPS_L:
	 case MESH_L:
	 case MUSIC_L:
	 case NAME_L:
	 case NUMSS_L:
	 case POSITION_L:
	 case REGENERATION_L:
	 case SCORES_L:
	 case SCORE_L:
	 case SHOOT_L:		
	 case SHOOTS_L:
	 case SPEED_L:
	 case SPS_L:		//Shoots Per Second
	 case TEXTURE_L:
	 case TITLE_L:		
	 case TYPE_L:
	 case X_L:			//Pos. X
	 case Y_L:			//Pos. Y
	 case Z_L:
	 case VERSION_L:
					if (StackState[LastState] == Tag) PopState();	//Change to the previous state
					else Error = true;
		break;
	 case FILE_TEXTURE_L:
					if (FILE_TAG_L == Tag) PopState();	//Change to the previous state
					else Error = true;
		break;
	 case NAME_CHARACTER_L:
	 case NAME_SCORE_L:
	 case NAME_TEXTURE_L:
					if (NAME_L == Tag) PopState();	//Change to the previous state
					else Error = true;
		break;
	 case UNKNOWN_L:	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
					strcpy_s(msj, TagString);
					strcat_s(msj, " clossing tag in an unknown state.");
					ErrorParser(msj);
		break;
	 case NIL_L:
	 case MAXTAGS_L:	//Closing a label when all the lables are closed is an error
					strcpy_s(msj, TagString);
					strcat_s(msj, " clossing tag out from the HTML section.");
					ErrorParser(msj);
		break;
	default:;
	};
	if (Error)
	{	strcpy_s(msj, TagString);
		strcat_s(msj, " clossing tag without correspondent opening tag in a ");
		if (LastState)
			 strcat_s(msj, CLeP_Tags[StackState[LastState-1]]);
		else strcat_s(msj, CLeP_Tags[StackState[LastState]]);
		strcat_s(msj, " segment.");
		ErrorParser(msj);
	}
}

void CLevelReader::Characters(const CString &rText, DWORD dwAppData, bool &bAbort)
{
char	msj[128], aux[20];
float	f;

if (bAbort) return;

#ifdef CLeP_DEBUG	//Class HTML Parser Debug activated
LogFile << "Texto: " << rText << std::endl;
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	switch(StackState[LastState])
	{case EPS_L:		//Energy Per Shoot
					f = atof (rText);
		break;

	case BUNKERDEF_L:
					if(atoi(rText)==0)
						bunker_defecto=0;
					else
						bunker_defecto=1;

		break;			
	case EXPLOSION_L:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 SupplyShip[CSS_CurrentSupplyShip].Hit_duration = atof (rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case FILE_TEXTURE_L:
					
		break;
	case GEOMETRY_L:
					strcpy(Game.GeometryDir, rText);
		break;
	case HEALTH_L:	//How strong is the character. How much has to be hurt before dieing
					switch (CharType)
					{case SHIP:
						 LEV_Shealth=atoi (rText);
						 if(LEV_Shealth<0)
							 LEV_Shealth=-1;
						 break;
					 case SUPPLYSHIP:
						 LEV_SShealth=atoi (rText);
						 if(LEV_SShealth>0)
						  SupplyShip[CSS_CurrentSupplyShip].Health = atoi (rText);
						 else{
						  LEV_SShealth=-1;
						  SupplyShip[CSS_CurrentSupplyShip].Health = DEF_SShealth;
						 }
						 break;
					 case PLAYER:
						 LEV_energy=atoi (rText); //Player[CG_INIT_CURRENT].Energy;
						 if(LEV_energy>0)
							Player[CG_INIT_CURRENT].Energy=LEV_energy;
						 else{
							LEV_energy=-1;
							Player[CG_INIT_CURRENT].Energy=DEF_energy;
							}
						 break;
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;

	case ID_L:
			G=atoi(rText);
			if(G>=0 && G<=3){
				actbun[G]=0;
				BActual=G;
			}
		break;

	case LEVEL_L:
		break;
	case LINE_L:
		char *cbun;
		int cbuncont;
		cbuncont=0;
		cbun=(char *) malloc(sizeof(char));
		strcpy(cbun, rText);
		if(strlen(cbun)>112){
			strcpy(msj, "El tama�o de la l�nea de bunker supera el l�mite, truncando a 14");
			ErrorParser (msj);
			strncpy(cbun,rText,14);
		}
		switch (BActual){
			case 0: 
				for(int cci=actbun[BActual];cci<actbun[BActual]+14;cci++){
					if(cbun[cbuncont]=='0')
					 bun1[cci]=0;
					else
					 bun1[cci]=1;
					cbuncont++;
				}
				break;
			case 1:
				for(int cci=actbun[BActual];cci<actbun[BActual]+14;cci++){
					if(cbun[cbuncont]=='0')
					 bun2[cci]=0;
					else
					 bun2[cci]=1;
					cbuncont++;
				}
				break;
			case 2:
				for(int cci=actbun[BActual];cci<actbun[BActual]+14;cci++){
					if(cbun[cbuncont]=='0')
					 bun3[cci]=0;
					else
					 bun3[cci]=1;
					cbuncont++;
				}
				break;
			case 3:
				for(int cci=actbun[BActual];cci<actbun[BActual]+14;cci++){
					if(cbun[cbuncont]=='0')
					 bun4[cci]=0;
					else
					 bun4[cci]=1;
					cbuncont++;
				}
				break;
			default: bun1[0]=0;
		}
		actbun[BActual]=actbun[BActual]+14;
		break;
		

	case LIVES_L:		//Amount of ships the player has still before finishing the game
		break;
	case MAXBRICKS_L:
		break;
	case MAXPLAYERS_L:
					MaxPlayers = atoi(rText);
					//Max and Min truncation
					if (CP_MAX_PLAYERS < MaxPlayers)
					{	strcpy(msj, "Cantidad m�xima de jugadores indicada (");
						strcat(msj, rText);
						strcat(msj, ") supera la cantidad m�xima permitida por el programa(");
						strcat(msj, itoa(CP_MAX_PLAYERS, aux, 10));
						strcat(msj, "). Valor truncado al m�ximo.");
						ErrorParser (msj);
						MaxPlayers = CP_MAX_PLAYERS;
					}
					if (0 > MaxPlayers)
					{	strcpy(msj, "Cantidad m�xima de jugadores indicada (");
						strcat(msj, rText);
						strcat(msj, ") es inferior a la cantidad m�nima necesaria para jugar(1). Valor truncado al m�nimo.");
						ErrorParser (msj);
						MaxPlayers = 1;
					}
		break;
	case MAXSHIPS_L:
		break;
	case MAXSUPPLYSHIPS_L:
					CSS_MaxSupplyShips = atoi(rText);
					if (CSS_MAX_SUPPLYSHIPS < CSS_MaxSupplyShips)
					{	strcpy(msj, "Cantidad m�xima de supernaves indicada (");
						strcat(msj, rText);
						strcat(msj, ") supera la cantidad m�xima permitida por el programa(");
						strcat(msj, itoa(CSS_MAX_SUPPLYSHIPS, aux, 10));
						strcat(msj, "). Valor truncado al m�ximo.");
						ErrorParser (msj);
						CSS_MaxSupplyShips = CSS_MAX_SUPPLYSHIPS;
					}
					if (0 > CSS_MaxSupplyShips)
					{	strcpy(msj, "Cantidad m�xima de naves nodriza indicada (");
						strcat(msj, rText);
						strcat(msj, ") es inferior a la cantidad m�nima necesaria para jugar(1). Valor truncado al m�nimo.");
						ErrorParser (msj);
						CSS_MaxSupplyShips = 1;
					}
		break;
	case MESH_L:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 strcpy(SupplyShip[CSS_CurrentSupplyShip].Mesh, rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case MUSIC_L:
					strcpy(Game.MusicDir, rText);
		break;
	case NAME_CHARACTER_L:
			switch (rText[0])
			{
			case 'S':	if (strcmp(rText,CHAR_Tags[SHIP]))
							if (strcmp(rText,CHAR_Tags[SUPPLYSHIP]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
							else {
									CharType = SUPPLYSHIP;
									//Reset all the values by default in case the file does not cover every object aspect
									SupplyShip[CSS_CurrentSupplyShip].Init();
							}
						else CharType = SHIP;
				break;
			case 'B':	if (strcmp(rText,CHAR_Tags[BONUS]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
						else CharType = BONUS;
				break;
			default:	if (strcmp(rText,CHAR_Tags[PLAYER]))
							if (strcmp(rText,CHAR_Tags[WEAPON]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
							else CharType = WEAPON;
						else CharType = PLAYER;
			};
		break;
	case NAME_SCORE_L:
		break;
	case NAME_TEXTURE_L:
		break;
	case NUMSS_L:
				LEV_NumSS=atoi(rText);
				if(LEV_NumSS<1){
						strcpy(msj,"N� de SupplyShips menor que el m�nimo. Restablecido a 1.");
						strcat(msj, rText);
						LEV_NumSS=1;
				}
				if(LEV_NumSS>3){
						strcpy(msj,"N� de SupplyShips mayor que el m�ximo. Restablecido a 3.");
						strcat(msj, rText);
						LEV_NumSS=3;
				}
		break;
	case POSITION_L:
		break;
	case REGENERATION_L:
			int M1;
			M1=atoi(rText);
			if(M1==1)
				regen_SS=true;
			else
				regen_SS=false;
		break;
	case SCORE_L:
		break;
	case SPEED_L:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 SupplyShip[CSS_CurrentSupplyShip].Speed = atof (rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case SPS_L:		//Shoots Per Second
		break;
	case TEXTURE_L:
					strcpy(Game.TextureDir, rText);
		break;
	case TITLE_L:		//HTML tag
					if (strcmp(rText, Game.Title))
					{ErrorParser ("El fichero no corresponde a este videojuego.");
					 bAbort = true;}
					else ;	//File content corresponds to the expected one for this videogame.
							//Go on. Nothing is done
		break;
	case TYPE_L:		if (strcmp(rText, "Level"))
					{
						strcpy(msj,"El fichero es de tipo \"");
						strcat(msj, rText);
						strcat(msj, "\". Se requiere un fichero de tipo \"inicializaci�n\".");
						ErrorParser (msj);
					 bAbort = true;}
					else ;	//File type is "Initialization"
							//Go on. Nothing is done
		break;
	case VERSION_L:	if (strcmp(rText, Game.Version))
					{
						strcpy(msj, "La versi�n del fichero es la v");
						strcat(msj, rText);
						strcat(msj, ", que no corresponde con la v");
						strcat(msj, Game.Version);
						strcat(msj, " de este videojuego.");
						ErrorParser (msj);
					 bAbort = true;}
					else ;	//File version corresponds to the one from this videogame.
							//Go on. Nothing is done
		break;
	case X_L:
		break;
	case Y_L:
		break;
	case Z_L:
		break;
	default:;		//Tags not supported are not managed, even if they have content associated
	};
}

void CLevelReader::Comment(const CString &rComment, DWORD dwAppData, bool &bAbort)
{
	printf("Comentarios: %s\n", rComment);
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CLevelReader::EndParse(DWORD dwAppData, bool bIsAborted)
{
	UNUSED_ALWAYS(dwAppData);
	if (bIsAborted) printf ("Se ha acabado la interpretaci�n del fichero.\n");
};
