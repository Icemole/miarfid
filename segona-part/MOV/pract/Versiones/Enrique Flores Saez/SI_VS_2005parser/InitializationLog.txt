Comienza la interpretaci�n del fichero de inicializaci�n.
Etiqueta comienzo: HTML
Texto: 

Etiqueta comienzo: HEAD
Texto: 

Etiqueta comienzo: TITLE
Texto: Space Invaders
Etiqueta fin: TITLE
Texto: 

Etiqueta comienzo: VERSION
Texto: 0.6.1
Etiqueta fin: VERSION
Texto: 

Etiqueta comienzo: TYPE
Texto: Initialization
Etiqueta fin: TYPE
Texto: 

Etiqueta fin: HEAD
Texto: 

Etiqueta comienzo: BODY
Texto: 

Etiqueta comienzo: DIR
Texto: 
	
Etiqueta comienzo: MUSIC
Texto: sounds
Etiqueta fin: MUSIC
Texto: 
	
Etiqueta comienzo: TEXTURE
Texto: images
Etiqueta fin: TEXTURE
Texto: 
	
Etiqueta comienzo: GEOMETRY
Texto: models
Etiqueta fin: GEOMETRY
Texto: 
	
Etiqueta comienzo: LANGUAGE
Texto: Valencia
Etiqueta fin: LANGUAGE
Texto: 

Etiqueta fin: DIR
Texto: 

Etiqueta comienzo: MAXPLAYERS
Texto: 2
Etiqueta fin: MAXPLAYERS
Texto: 

Etiqueta comienzo: MAXSHIPS
Texto: 50
Etiqueta fin: MAXSHIPS
Texto: 

Etiqueta comienzo: MAXSUPPLYSHIPS
Texto: 10
Etiqueta fin: MAXSUPPLYSHIPS
Texto: 

Etiqueta comienzo: MAXBRICKS
Texto: 500
Etiqueta fin: MAXBRICKS
Texto: 

Etiqueta comienzo: CHARACTER
Texto: 
	
Etiqueta comienzo: NAME
Texto: SHIP
Etiqueta fin: NAME
Texto: 
	
Etiqueta comienzo: EXPLOSION
Texto: 500
Etiqueta fin: EXPLOSION
Texto: 
	
Etiqueta comienzo: HEALTH
Texto: 20
Etiqueta fin: HEALTH
Texto: 
	
Etiqueta comienzo: SPEED
Texto: 0.007
Etiqueta fin: SPEED
Texto: 
	
Etiqueta comienzo: MESH
Texto: ship.3ds
Etiqueta fin: MESH
Texto: 

Etiqueta fin: CHARACTER
Texto: 

Etiqueta comienzo: CHARACTER
Texto: 
	
Etiqueta comienzo: NAME
Texto: SUPPLYSHIP
Etiqueta fin: NAME
Texto: 
	
Etiqueta comienzo: EXPLOSION
Texto: 500
Etiqueta fin: EXPLOSION
Texto: 
	
Etiqueta comienzo: HEALTH
Texto: 300
Etiqueta fin: HEALTH
Texto: 
	
Etiqueta comienzo: SPEED
Texto: 2
Etiqueta fin: SPEED
Texto: 
	
Etiqueta comienzo: MESH
Texto: supplyship.3ds
Etiqueta fin: MESH
Texto: 

Etiqueta fin: CHARACTER
Texto: 

Etiqueta comienzo: CHARACTER
Texto: 
	
Etiqueta comienzo: NAME
Texto: PLAYER
Etiqueta fin: NAME
Texto: 
	
Etiqueta comienzo: EXPLOSION
Texto: 500
Etiqueta fin: EXPLOSION
Texto: 
	
Etiqueta comienzo: LIVES
Texto: 2
Etiqueta fin: LIVES
Texto: 
	
Etiqueta comienzo: HEALTH
Texto: 20
Etiqueta fin: HEALTH
Texto: 
	
Etiqueta comienzo: SPEED
Texto: 0.01
Etiqueta fin: SPEED
Texto: 
	
Etiqueta comienzo: MESH
Texto: player.3ds
Etiqueta fin: MESH
Texto: 

Etiqueta fin: CHARACTER
Texto: 	

Etiqueta comienzo: CHARACTER
Texto: 
	
Etiqueta comienzo: NAME
Texto: BONUS
Etiqueta fin: NAME
Texto: 
	
Etiqueta comienzo: EXPLOSION
Texto: N�
Etiqueta fin: EXPLOSION
Texto: 
	
Etiqueta comienzo: HEALTH
Texto: N�
Etiqueta fin: HEALTH
Texto: 
	
Etiqueta comienzo: SPEED
Texto: N�
Etiqueta fin: SPEED
Texto: 

Etiqueta fin: CHARACTER
Texto: 

Etiqueta comienzo: CHARACTER
Texto: 
	
Etiqueta comienzo: NAME
Texto: WEAPON
Etiqueta fin: NAME
Texto: 
	
Etiqueta comienzo: TYPE
Texto: MISSILE
Etiqueta fin: TYPE
Texto: 
	
Etiqueta comienzo: EXPLOSION
Texto: N�
Etiqueta fin: EXPLOSION
Texto: 
	
Etiqueta comienzo: HEALTH
Texto: N�
Etiqueta fin: HEALTH
Texto: 
	
Etiqueta comienzo: SPEED
Texto: N�
Etiqueta fin: SPEED
Texto: 

Etiqueta fin: CHARACTER
Texto: 

Etiqueta comienzo: SCORES
Texto: 
	
Etiqueta comienzo: NAME
Texto: Promedio
Etiqueta fin: NAME
Texto: 
	
Etiqueta comienzo: SCORE
Texto: 15000
Etiqueta fin: SCORE
Texto: 

Etiqueta fin: SCORES
Texto: 

Etiqueta fin: BODY
Texto: 

Etiqueta fin: HTML
