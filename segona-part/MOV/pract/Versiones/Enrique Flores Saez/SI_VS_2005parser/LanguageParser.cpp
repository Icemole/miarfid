/*LANGUAGE READER
	Author: Ramón Mollá
	Last update: 2007-09-11
*/
#include "LanguageParser.h"
#include "Game.h"
int Gla;
extern CGame Game;
int BActuala;
//Sensitive string tags
char CLaP_Tags[MAXTAGS_LA][CLaP_TAG_MAX_LONG] = 
{	"",
	"BODY",		//HTML tag
	"CONTENT",
	"HEAD",		//HTML tag
	"HTML",		//HTML tag
	"ID",
	"NAME",
	"RESOURCE",
	"TITLE",	//HTML tag
	"TYPE",
	"UNKNOWN",	//This tag has no matching. It is for management purpouses only. 
	"VERSION"
};
//Stores the new state onto the stack
void CLanguageReader::PushState (CLaP_TagType State)
{	StackState[++LastState] = State;
}

//Take out the last state from the stack. Shorts the stack. Returns current state
CLaP_TagType CLanguageReader::PopState  (void)
{	return StackState[--LastState];
}
//Translates from a string to the token that the string represents
CLaP_TagType CLanguageReader::String2Tag (const char *Name)
{ 
	switch (Name[0])
	{
	case 'B': if (0 == strcmp(Name, CLaP_Tags[BODY_LA]))		return BODY_LA;
		break;
	case 'C': if (0 == strcmp(Name, CLaP_Tags[CONTENT_LA]))	return CONTENT_LA;	  
		break;
		case 'H': if ('E' == Name[1])
				  if (strcmp(Name, CLaP_Tags[HEAD_LA]))		
					  		return UNKNOWN_LA;
				  else					return HEAD_LA;
			  else if (strcmp(Name, CLaP_Tags[HTML_LA]))		return UNKNOWN_LA;
				else									return HTML_LA;
				   
		break;
	case 'I': if (0 == strcmp(Name, CLaP_Tags[ID_LA]))			return ID_LA;
		break;
	case 'N': if (0 == strcmp(Name, CLaP_Tags[NAME_LA]))		return NAME_LA;
		break;
	case 'R': if (0 == strcmp(Name, CLaP_Tags[RESOURCE_LA]))	return RESOURCE_LA;
		break;
	case 'T': if (strcmp(Name, CLaP_Tags[TITLE_LA]))
				if (strcmp(Name, CLaP_Tags[TYPE_LA]))			return UNKNOWN_LA;
				else										return TYPE_LA;
			  else											return TITLE_LA;
		break;
	case 'V': if (0 == strcmp(Name, CLaP_Tags[VERSION_LA]))	return VERSION_LA;
		break;
	};
	return UNKNOWN_LA;
};

/* Class that loads the values by default in the very beginning of every level in the game

   LANGUAGE INITIALIZATION
*/


void CLanguageReader::Init(char *FileName)
{
	LogFile.open(FileName);
	if (!LogFile)	//NAME the file
		exit(0);
	InitializeState();
};

void CLanguageReader::BeginParse(DWORD dwAppData, bool &bAbort)
{
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
	#ifdef CLaP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Comienza la interpretación del fichero de inicialización.\n";
	#endif
}

void CLanguageReader::StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	CString TagString;
	CLaP_TagType Tag;

	TagString = pTag->getTagName();

#ifdef CLaP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Start tag: " << TagString << "\n";
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	Tag = String2Tag(LPCTSTR(TagString));
	switch(StackState[LastState])
	{
		case NIL_LA:		//Starting and ending states
				if (HTML_LA == Tag)
					//It is a HTML file. Change to HTML state
					PushState(HTML_LA);
 				else ErrorParser("First tag was not an HTML tag.");
		break;
		case BODY_LA:		//HTML tag
					switch (Tag)
					{
					case RESOURCE_LA:
						PushState(Tag);
						break;
					default: ErrorParser("Tag not allowed in a BODY group.");
					}
		break;
		case HEAD_LA:
			switch (Tag)
					{
					case TITLE_LA:
					case VERSION_LA:
					case TYPE_LA:
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a HEAD group.");
					}
		break;
		case HTML_LA:		//HTML Tag
					//The parser is at the very beginning of the iniitalization file
					switch (Tag)
					{
					case HEAD_LA:		//A HEAD Tag is expected
					case BODY_LA: 
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Only the attribute HEAD or BODY may appear in a group HTML.");
					}					
		break;
		case RESOURCE_LA:
					switch(Tag)
					{
					case ID_LA:
					case CONTENT_LA:
						PushState(Tag);
						break;
					default: ErrorParser("Dentro de RESOURCE solo puede estar ID y CONTENT");
					}
		break;
		default:;
	};
}

void CLanguageReader::EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	CString TagString;
	CLaP_TagType Tag;
	bool Error = false;
	char msj[128];

	TagString = pTag->getTagName();

	#ifdef CLaP_DEBUG	//Class HTML Parser Debug activated
		LogFile << "Etiqueta fin: " << pTag->getTagName() << std::endl;
	#endif
	//printf("Etiqueta fin: %s\n", pTag->getTagName());
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	Tag = String2Tag(LPCTSTR(TagString));
	switch(StackState[LastState])
	{
	case BODY_LA:
	case HTML_LA:
	case HEAD_LA:
	case CONTENT_LA:
	case ID_LA:
	case NAME_LA:
	case RESOURCE_LA:
	case TITLE_LA:		
	case TYPE_LA:
	case VERSION_LA:
					if (StackState[LastState] == Tag) PopState();	//Change to the previous state
					else Error = true;
		break;
	 case UNKNOWN_LA:	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
					strcpy_s(msj, TagString);
					strcat_s(msj, " clossing tag in an unknown state.");
					ErrorParser(msj);
		break;
	 case NIL_LA:
	 case MAXTAGS_LA:	//Closing a label when all the lables are closed is an error
					strcpy_s(msj, TagString);
					strcat_s(msj, " clossing tag out from the HTML section.");
					ErrorParser(msj);
		break;
	default:;
	};
	if (Error)
	{	strcpy_s(msj, TagString);
		strcat_s(msj, " clossing tag without correspondent opening tag in a ");
		if (LastState)
			 strcat_s(msj, CLaP_Tags[StackState[LastState-1]]);
		else strcat_s(msj, CLaP_Tags[StackState[LastState]]);
		strcat_s(msj, " segment.");
		ErrorParser(msj);
	}

}


















void CLanguageReader::Characters(const CString &rText, DWORD dwAppData, bool &bAbort)
{
char	msj[128], aux[20];
float	f;

if (bAbort) return;

#ifdef CLaP_DEBUG	//Class HTML Parser Debug activated
LogFile << "Texto: " << rText << std::endl;
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	switch(StackState[LastState])
	{
		case ID_LA:
			Gla=atoi(rText);
			if(Gla>0 && Gla<=12){
				BActuala=Gla;
			}
		break;
		case CONTENT_LA:
			switch(BActuala){
		case 1:strcpy(Game.Quit,rText);
		break;
		case 2:strcpy(Game.Change,rText);
		break;
		case 3:strcpy(Game.Rotate,rText);
		break;
		case 4:strcpy(Game.Music,rText);
		break;
		case 5:strcpy(Game.Fondo,rText);
		break;
		case 6:strcpy(Game.Cam,rText);
		break;
		case 7:strcpy(Game.Hide,rText);
		break;
		case 8:strcpy(Game.Fin,rText);
		break;
		case 9:strcpy(Game.Won,rText);
		break;
		case 10:strcpy(Game.Lose,rText);
		break;
		case 11:strcpy(Game.Ancho,rText);
		break;
		case 12:strcpy(Game.Alto,rText);
		break;
		default:ErrorParser ("No existe el identitificador");
			}
		break;
		case TYPE_LA:		if (strcmp(rText, "Language"))
					{
						strcpy(msj,"El fichero es de tipo \"");
						strcat(msj, rText);
						strcat(msj, "\". Se requiere un fichero de tipo \"Language\".");
						ErrorParser (msj);
					 bAbort = true;}
					else ;	
		break;
		
		default:;
	}

}

void CLanguageReader::Comment(const CString &rComment, DWORD dwAppData, bool &bAbort)
{
	printf("Comentarios: %s\n", rComment);
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CLanguageReader::EndParse(DWORD dwAppData, bool bIsAborted)
{
	UNUSED_ALWAYS(dwAppData);
	if (bIsAborted) printf ("Se ha acabado la interpretación del fichero.\n");
};