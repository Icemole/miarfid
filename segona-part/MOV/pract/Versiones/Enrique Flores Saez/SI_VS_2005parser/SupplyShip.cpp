/*/////////////////////////////////////////////////////////////////////////////////////
//
// SupplyShip initialization
//
// These values are by default. They have to be overwritten in the initializatio phase
// when reading the default values from the "initialization.html" file

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "SupplyShip.h"

void CSupplyShip::Init()
{
	Alive			=	true;
	Health			=	500;
	Hit_duration	=	500;
	Explosion		=	false; //At the beginning, the supplyship is not exploding
	Type			=	0;
	Speed			=	2;
	x				=	0;
	y				=	4;
	z				=	0.05f;
	Size_x			=	0.5;
	Size_y			=	0.3f;
}

//Variables globales de control del juego

unsigned int CSS_MaxSupplyShips,	//Maximum amount of supplyships avialable in a given level of game
			 CSS_CurrentSupplyShip = CG_INIT_INITIAL;
