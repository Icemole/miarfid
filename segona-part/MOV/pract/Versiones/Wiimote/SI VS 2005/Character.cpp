/*	Character.cpp - Clases, types, definitions, constants,..
	related with any kind of characters that may appear in the game.

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "Character.h"

//Sensitive string tags
char CHAR_Tags[MAX_CHARTYPE][CHAR_TAG_MAX_LONG] = 
{
	"",
	"SHIP",
	"SUPPLYSHIP",
	"PLAYER",
	"BONUS",
	"WEAPON"
};

//Different character that may be loaded during the parsing
CHAR_CharacterType CharType = CHARACTER_UNKNOWN;
