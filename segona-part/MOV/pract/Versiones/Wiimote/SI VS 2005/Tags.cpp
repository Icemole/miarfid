/*Tags.cpp
	Author: Ram�n Moll�
	Last update: 2009-05-01
*/

#include "Tags.h"

//Sensitive string tags
char Tags[MAXTAGS][TAG_MAX_LONG] = 
{
	"",
	"BODY",		//HTML tag
	"CHARACTER",
	"CONTENT",
	"DIR",
	"EPS",		//Energy Per Shoot
	"EXPLOSION",
	"FILE",
	"FILE",
	"FILE"
	"GEOMETRY",
	"HAVE",		//Recursive definition of a character
	"HEAD",		//HTML tag
	"HEALTH",	//How strong is the character. How much has to be hurt before dieing
	"HTML",		//HTML tag
	"ID",
	"LANGUAGE",
	"LANGUAGES",
	"LEVEL",
	"LIVES",	//Amount of ships the player has still before finishing the game
	"MAXBRICKS",
	"MAXPLAYERS",
	"MAXSHIPS",
	"MAXSUPPLYSHIPS",
	"MESH",
	"MUSIC",
	"NAME",
	"NAME",
	"NAME",
	"NAME",
	"NAME",
	"RESOURCE",
	"SCORE",
	"SCORES",
	"SHOOT",		//Kind of shooting
	"SPEED",
	"SPS",		//Shoots Per Second
	"TEXTURE",
	"TITLE",		//HTML tag
	"TYPE",
	"UNKNOWN",	//This tag has no matching. It is for management purpouses only. 
	"VERSION"
};