/* Header with definitions needed for the management of the characters

	Prephix = CHAR_

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#ifndef CHAR_INITIALIZATION
#define CHAR_INITIALIZATION

#define CHAR_TAG_MAX_LONG 15	//Maximun length of a sensitive tag

typedef enum {
	CHARACTER_UNKNOWN,
		// ENEMY SHIPS
	SHIP,
	SUPPLYSHIP,

	PLAYER,
	BONUS,
	WEAPON,
	MAX_CHARTYPE	//Only for character types management. No object exist in the game for this type
} CHAR_CharacterType;

#endif