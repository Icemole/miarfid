/*	Class that loads the values of a given level of game 

	Prefix CLaP_: Class Level Parser

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "LiteHTMLReader.h"
#include "Tags.h"

#define CLaP_DEBUG				//Class HTML Parser Debug activated
#define CLaP_MAXSTACK	  16	//Maximun deepness of the stack

/* Class that loads the values by default in the very beginning of every level in the game
*/

class CLanguageReader : public ILiteHTMLReaderEvents
{

private:
    void BeginParse(DWORD dwAppData, bool &bAbort);
    void StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void Characters(const CString &rText, DWORD dwAppData, bool &bAbort);
    void Comment(const CString &rComment, DWORD dwAppData, bool &bAbort);
    void EndParse(DWORD dwAppData, bool bIsAborted);

	//Translates from a string to the token that the string represents
	TagType String2Tag (const char *Name);
};