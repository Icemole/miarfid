/*	All the tags needed by the game in every situation: Initialization, level loading, internationalization,...

	Author: Ram�n Moll�
	Last update: 2009-05-01
*/

#define TAG_DEBUG				//Tags Debug activated

#define TAG_MAX_LONG 16			//Maximun length of any lexeme associated to any tag 

//Types of tags all the parsers can match during the parsing
//They can be used also as states of of the stack machine (parser) during the parsing
typedef enum {
				//INITIALIZATION	LEVEL	LANGUAGE
	NIL,		//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	BODY,		//HTML tag
	CHARACTER,	//INITIALIZATION	LEVEL
	CONTENT,	//							LANGUAGE
	DIR,		//INITIALIZATION	LEVEL	LANGUAGE
	EPS,		//Energy Per Shoot
	EXPLOSION,	//INITIALIZATION	LEVEL	
	FILE_LANGUAGE,//						LANGUAGE
	FILE_TAG,	//INITIALIZATION	LEVEL	LANGUAGE
	FILE_TEXTURE,//INITIALIZATION	LEVEL	
	GEOMETRY,	//INITIALIZATION	LEVEL	
	HAVE,		//INITIALIZATION	LEVEL				Recursive definition of a character
	HEAD,		//HTML tag
	HEALTH,		//INITIALIZATION	LEVEL				How strong is the character. How much has to be hurt before dieing
	HTML,		//HTML tag
	ID,			//							LANGUAGE
	LANGUAGE,	//INITIALIZATION			LANGUAGE
	LANGUAGES,	//INITIALIZATION			LANGUAGE
	LEVEL,		//					LEVEL	
	LIVES,		//INITIALIZATION	LEVEL	Amount of ships the player has still before finishing the game
	MAXBRICKS,	//INITIALIZATION	LEVEL	
	MAXPLAYERS,	//INITIALIZATION	LEVEL	
	MAXSHIPS,	//INITIALIZATION	LEVEL	
	MAXSUPPLYSHIPS,//INITIALIZATION	LEVEL	
	MESH,		//INITIALIZATION	LEVEL	
	MUSIC,		//INITIALIZATION	LEVEL	
	NAME,		//INITIALIZATION	LEVEL	
	NAME_CHARACTER,//INITIALIZATION	LEVEL	
	NAME_LANGUAGE,//INITIALIZATION	LEVEL	LANGUAGE
	NAME_SCORE,//INITIALIZATION	LEVEL	
	NAME_TEXTURE,//INITIALIZATION	LEVEL	
	RESOURCE,	//							LANGUAGE
	SCORE,		//INITIALIZATION	LEVEL	
	SCORES,		//INITIALIZATION	LEVEL	
	SHOOT,		//INITIALIZATION	LEVEL	Kind of shooting
	SPEED,		//INITIALIZATION	LEVEL	
	SPS,		//INITIALIZATION	LEVEL	Shoots Per Second
	TEXTURE,	//INITIALIZATION	LEVEL	
	TITLE,		//HTML tag
	TYPE,		//INITIALIZATION	LEVEL	
	UNKNOWN,	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	VERSION,	//INITIALIZATION	LEVEL	LANGUAGE
	MAXTAGS		//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
} TagType;