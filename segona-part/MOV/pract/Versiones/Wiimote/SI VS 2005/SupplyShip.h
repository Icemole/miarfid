/*	Definition of the class Super Ship

	Prefix: CSS_

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "Game.h"

#ifndef CSS_SUPPLYSHIP
#define CSS_SUPPLYSHIP

//Definitions for the game
#define CSS_MAX_FILE_NAME	25
#define CSS_MAX_SUPPLYSHIPS	10	//Amount of Super Ships available during the same playing level

namespace CSS {
class CSupplyShip
{
	//Atributos
public:
	bool	Alive;						// SupplyShip is still on play
	int		Health;						// Health
	float	Hit_duration;				// dur�e d'affichage du supplyship touch�
	bool	Explosion;					// explosion is still active
	int		Explosion_life;				// Explosion life duration
	int		Type;						// Type of supplyship
	float	Speed;						// Velocity
	float	x, y, z;					// Coordinates
	float	Size_x, Size_y;				// Size
	char	Mesh[CSS_MAX_FILE_NAME];	// Name of the file mesh associated to the SupplyShip. It does not hold the path to the file.

	//Methods
	void Init ();

	CSupplyShip(){Init();}		//Constructor of the class
};
}

#endif