/*LANGUAGE READER
	Author: Ramón Mollá
	Last update: 2007-09-11
*/
#include "LanguageParser.h"

extern char Tags[MAXTAGS][TAG_MAX_LONG];

//Translates from a string to the token that the string represents
TagType CLanguageReader::String2Tag (const char *Name)
{
	switch (Name[0])
	{
	case 'B': if (0 == strcmp(Name, Tags[BODY]))		return BODY;
		break;
	case 'C': if (0 == strcmp(Name, Tags[CONTENT]))		return CONTENT;	  
		break;
	case 'H': if ('E' == Name[1])
				  if (strcmp(Name, Tags[HEAD]))
					if (strcmp(Name, Tags[HTML]))		return UNKNOWN;
					else								return HTML;
				  else									return HEAD;
		break;
	case 'I': if (0 == strcmp(Name, Tags[ID]))			return ID;
		break;
	case 'N': if (0 == strcmp(Name, Tags[NAME]))		return NAME;
		break;
	case 'R': if (0 == strcmp(Name, Tags[RESOURCE]))	return RESOURCE;
		break;
	case 'T': if (strcmp(Name, Tags[TITLE]))
				if (strcmp(Name, Tags[TYPE]))			return UNKNOWN;
				else									return TYPE;
			  else										return TITLE;
		break;
	case 'V': if (0 == strcmp(Name, Tags[VERSION]))		return VERSION;
		break;
	};
	return UNKNOWN;
};

/* Class that loads the values by default in the very beginning of every level in the game

   LANGUAGE INITIALIZATION
*/

void CLanguageReader::BeginParse(DWORD dwAppData, bool &bAbort)
{
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
	printf("Comienza la interpretación del fichero de inicialización.\n");
}

void CLanguageReader::StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	printf("Etiqueta comienzo: %s\n", pTag->getTagName());
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

}

void CLanguageReader::EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	printf("Etiqueta fin: %s\n", pTag->getTagName());
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

}

void CLanguageReader::Characters(const CString &rText, DWORD dwAppData, bool &bAbort)
{
	printf("Texto: %s\n", rText);
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CLanguageReader::Comment(const CString &rComment, DWORD dwAppData, bool &bAbort)
{
	printf("Comentarios: %s\n", rComment);
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CLanguageReader::EndParse(DWORD dwAppData, bool bIsAborted)
{
	UNUSED_ALWAYS(dwAppData);
	if (bIsAborted) printf ("Se ha acabado la interpretación del fichero.\n");
};