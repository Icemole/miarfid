/*/////////////////////////////////////////////////////////////////////////////////////
//
// SupplyShip initialization
//
// These values are by default. They have to be overwritten in the initializatio phase
// when reading the default values from the "initialization.html" file

	Prefix: CSS_

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "SupplyShip.h"


void CSS::CSupplyShip::Init()
{
	Alive			=	true;
	Health			=	25;
	Hit_duration	=	500;
	Explosion		=	false; //At the beginning, the supplyship is not exploding
	Type			=	0;
	Speed			=	2;
	x				=	0;
	y				=	4;
	z				=	0.05f;
	Size_x			=	0.5;
	Size_y			=	0.3f;
}

//Variables globales de control del juego

CSS::CSupplyShip	 SupplyShip[CSS_MAX_SUPPLYSHIPS];	//Array of SupplyShips that can appear while playing

unsigned int MaxSupplyShips,	//Maximum amount of supplyships avialable in a given level of game
			 CurrentSupplyShip = CG_INIT_INITIAL;


void a()
{CurrentSupplyShip = 0;}