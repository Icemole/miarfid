/*LEVEL READER
	Author: Ram�n Moll�
	Last update: 2009-07-01
*/

#include "LevelParser.h"
#include "Character.h"
#include "Game.h"
#include "Player.h"
#include "SupplyShip.h"

extern char Tags[MAXTAGS][TAG_MAX_LONG];
extern CHAR_CharacterType CharType;
extern CGame Game;

extern CSS::CSupplyShip	SupplyShip[CSS_MAX_SUPPLYSHIPS];	//Array of SupplyShips that can appear while playing
extern unsigned int MaxSupplyShips;						//Maximum amount of supplyships avialable in a given level of game
extern unsigned int CurrentSupplyShip;

extern unsigned char MaxPlayers; //Amount of players currently used in a game playing
extern char CHAR_Tags[MAX_CHARTYPE][CHAR_TAG_MAX_LONG];

//Stores the new state onto the stack
void CLevelReader::PushState (TagType State)
{	++LastState;
	if (CLeP_MAXSTACK == LastState) 
	{	//ERROR. Amount of states stored in the stack has outrun the maximun stack deepness
		//Report to the player

	}
	else StackState[LastState] = State;
}

//Take out the last state from the stack. Shorts the stack. Returns current state
TagType CLevelReader::PopState  (void)
{	TagType CurrentState = StackState[LastState];
	LastState--;
	if (0 > LastState)
	{	//First state reached. Reset stack
		LastState = 0;
		StackState[0]=NIL;
	}
	return CurrentState;
}

//Translates from a string to the token that the string represents
TagType CLevelReader::String2Tag (const char *Name)
{
switch (Name[0])
	{
	case 'B': if (0 == strcmp(Name, Tags[BODY]))			return BODY;
		break;
	case 'C': if (0 == strcmp(Name, Tags[CHARACTER]))		return CHARACTER;	  
		break;
	case 'D': if (0 == strcmp(Name, Tags[DIR]))				return DIR;
		break;
	case 'E': if (strcmp(Name, Tags[EPS]))
				if (strcmp(Name, Tags[EXPLOSION]))			return UNKNOWN;
				else										return EXPLOSION;
			  else											return EPS;
		 break;
	case 'F': if (0 == strcmp(Name, Tags[FILE_TAG]))		return FILE_TAG;
		break;
	case 'G': if (0 == strcmp(Name, Tags[GEOMETRY]))		return GEOMETRY;
		break;
	case 'H': if ('E' == Name[1])
				  if (strcmp(Name, Tags[HEAD]))		
					  if (strcmp(Name, Tags[HEALTH]))		return UNKNOWN;
					   else									return HEALTH;
				  else										return HEAD;
			  else if (strcmp(Name, Tags[HAVE]))
						if (strcmp(Name, Tags[HTML]))		return UNKNOWN;
						else								return HTML;
				   else										return HAVE;
		break;
	case 'L': if (0 == strcmp(Name, Tags[LIVES]))			return LIVES;
		break;
	case 'M': if ('A' == Name[1])
				  //Serie de MAX*
				  if ('S' == Name[3])
					   if (strcmp(Name, Tags[MAXSHIPS]))
							if (strcmp(Name, Tags[MAXSUPPLYSHIPS]))	return UNKNOWN;
					        else									return MAXSUPPLYSHIPS;
					   else											return MAXSHIPS;
				  else if (strcmp(Name, Tags[MAXBRICKS]))
							if (strcmp(Name, Tags[MAXPLAYERS]))		return UNKNOWN;
					        else									return MAXPLAYERS;
					   else											return MAXBRICKS;
			  else if (strcmp(Name, Tags[MESH]))
					  if (strcmp(Name, Tags[MUSIC]))				return UNKNOWN;
					  else											return MUSIC;
				   else												return MESH;
		break;
	case 'N': if (0 == strcmp(Name, Tags[NAME]))					return NAME;
		break;
	case 'S': if (strcmp(Name, Tags[SCORE]))
				if (strcmp(Name, Tags[SCORES]))
				  if (strcmp(Name, Tags[SHOOT]))
					if (strcmp(Name, Tags[SPEED]))
					  if (strcmp(Name, Tags[SPS]))	return UNKNOWN;
					  else							return SPS;
					else							return SPEED;
				  else								return SHOOT;			  
				else								return SCORES;
			  else									return SCORE;
		break;
	case 'T': if  (strcmp(Name, Tags[TEXTURE]))
				if (strcmp(Name, Tags[TITLE]))
					if (strcmp(Name, Tags[TYPE]))	return UNKNOWN;
					else							return TYPE;
				else								return TITLE;
			  else									return TEXTURE;
		break;
	case 'V': if (0 == strcmp(Name, Tags[VERSION]))	return VERSION;
		break;
	};
	return UNKNOWN;
};


/* Class that loads the values by default in the very beginning of every level in the game

   LEVEL INITIALIZATION
*/

void CLevelReader::Init(char *FileName)
{
	LogFile.open(FileName);
	if (!LogFile)	//NAME the file
		exit(0);
	InitializeState();
};

void CLevelReader::BeginParse(DWORD dwAppData, bool &bAbort)
{
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
#ifdef CLeP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Begining the initialization file parsing.\n";
#endif
}

void CLevelReader::StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	CString TagString;
	TagType Tag;

	TagString = pTag->getTagName();

#ifdef CLeP_DEBUG	//Class HTML Parser Debug activated
	LogFile << "Start tag: " << TagString << "\n";
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	Tag = String2Tag(LPCTSTR(TagString));
	switch(StackState[LastState])
	{case NIL:		//Starting and ending states
				if (HTML == Tag)
					//It is a HTML file. Change to HTML state
					PushState(HTML);
				else ErrorParser("First tag was not an HTML tag.");
		break;
	 case BODY:		//HTML tag
					switch (Tag)
					{
					case DIR:
					case MAXPLAYERS:
					case MAXSHIPS:
					case MAXSUPPLYSHIPS:
					case MAXBRICKS:
					case CHARACTER:
					case SCORES: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a BODY group.");
					}
		break;
	case CHARACTER:
					switch (Tag)
					{
					case NAME:
						PushState(NAME_CHARACTER); //Change to the state NAME used to identify a character
						break;
					case EXPLOSION:
					case LIVES:
					case HEALTH:
					case SPEED:
					case MESH:
					case TEXTURE:
					case TYPE:
					case SHOOT:
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a CHARACTER group.");
					}
		break;
	case DIR:
					switch (Tag)
					{
					case MUSIC:
					case TEXTURE:
					case GEOMETRY: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a DIR group.");
					}
		break;
	case HEAD:		//HTML tag
					switch (Tag)
					{
					case TITLE:
					case VERSION:
					case TYPE:
					case LEVEL: PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Tag not allowed in a HEAD group.");
					}
		break;		
	case HTML:		//HTML Tag
					//The parser is at the very beginning of the iniitalization file
					switch (Tag)
					{
					case HEAD:		//A HEAD Tag is expected
					case BODY: 
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Only the attribute HEAD or BODY may appear in a group HTML.");
					}					
		break;
	case SCORES:	//A NAME Tag is expected
					if (NAME == Tag)
						//Change to NAME state
						PushState(NAME_SCORE);

					//May be that no NAME tag appears or that the NAME section is finished.
					//There is a SCORE part also
					else if (SCORE == Tag)
						//Change to SCORE state
						PushState(SCORE);
					else ErrorParser("Only a NAME or SCORE command may appear in a SCORES group.");
		break;
	case SHOOT:		//Kind of shooting
					switch (Tag)
					{
					case SPS:	//Change to SPS state
					case EPS:	//May be that no SPS tag appears or that the SPS section is finished.
								//There is a EPS part also
						PushState(Tag); //Change to the state specified by the Tag
						break;
					default: ErrorParser("Only the attribute SPS or EPS may appear in a SHOOT group. Nothing more.");
					}
		break;
	case TEXTURE:
					if (NAME == Tag)
						//Change to NAME state
						PushState(NAME_TEXTURE);

					//May be that no NAME tag appears or that the NAME section is finished.
					//There is a FILE part also
					else if (FILE_TAG == Tag)
						//Change to FILE state
						PushState(FILE_TEXTURE);
					else ErrorParser("Only the attribute NAME or FILE may appear in a group TEXTURE. Nothing more.");
		break;
	default:;
	};
}

void CLevelReader::EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	printf("Etiqueta fin: %s\n", pTag->getTagName());
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

}

void CLevelReader::Characters(const CString &rText, DWORD dwAppData, bool &bAbort)
{
char	msj[128], aux[20];
float	f;

if (bAbort) return;

#ifdef CIP_DEBUG	//Class HTML Parser Debug activated
LogFile << "Texto: " << rText << std::endl;
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	switch(int(StackState[LastState]))
	{case EPS:		//Energy Per Shoot
					f = atof (rText);
		break;
	case EXPLOSION:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 SupplyShip[CurrentSupplyShip].Hit_duration = atof (rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case FILE_TEXTURE:
					
		break;
	case GEOMETRY:
					strcpy(Game.GeometryDir, rText);
		break;
	case HEALTH:	//How strong is the character. How much has to be hurt before dieing
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 SupplyShip[CurrentSupplyShip].Health = atoi (rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case LIVES:		//Amount of ships the player has still before finishing the game
		break;
	case MAXBRICKS:
		break;
	case MAXPLAYERS:
					MaxPlayers = atoi(rText);
					//Max and Min truncation
					if (CP_MAX_PLAYERS < MaxPlayers)
					{	strcpy(msj, "Cantidad m�xima de jugadores indicada (");
						strcat(msj, rText);
						strcat(msj, ") supera la cantidad m�xima permitida por el programa(");
						strcat(msj, itoa(CP_MAX_PLAYERS, aux, 10));
						strcat(msj, "). Valor truncado al m�ximo.");
						ErrorParser (msj);
						MaxPlayers = CP_MAX_PLAYERS;
					}
					if (0 > MaxPlayers)
					{	strcpy(msj, "Cantidad m�xima de jugadores indicada (");
						strcat(msj, rText);
						strcat(msj, ") es inferior a la cantidad m�nima necesaria para jugar(1). Valor truncado al m�nimo.");
						ErrorParser (msj);
						MaxPlayers = 1;
					}
		break;
	case MAXSHIPS:
		break;
	case MAXSUPPLYSHIPS:
					MaxSupplyShips = atoi(rText);
					if (CSS_MAX_SUPPLYSHIPS < MaxSupplyShips)
					{	strcpy(msj, "Cantidad m�xima de supernaves indicada (");
						strcat(msj, rText);
						strcat(msj, ") supera la cantidad m�xima permitida por el programa(");
						strcat(msj, itoa(CSS_MAX_SUPPLYSHIPS, aux, 10));
						strcat(msj, "). Valor truncado al m�ximo.");
						ErrorParser (msj);
						MaxSupplyShips = CSS_MAX_SUPPLYSHIPS;
					}
					if (0 > MaxSupplyShips)
					{	strcpy(msj, "Cantidad m�xima de naves nodriza indicada (");
						strcat(msj, rText);
						strcat(msj, ") es inferior a la cantidad m�nima necesaria para jugar(1). Valor truncado al m�nimo.");
						ErrorParser (msj);
						MaxSupplyShips = 1;
					}
		break;
	case MESH:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 strcpy(SupplyShip[CurrentSupplyShip].Mesh, rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case MUSIC:
					strcpy(Game.MusicDir, rText);
		break;
	case NAME_CHARACTER:
			switch (rText[0])
			{
			case 'S':	if (strcmp(rText,CHAR_Tags[SHIP]))
							if (strcmp(rText,CHAR_Tags[SUPPLYSHIP]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
							else {
									CharType = SUPPLYSHIP;
									//Reset all the values by default in case the file does not cover every object aspect
									SupplyShip[CurrentSupplyShip].Init();
							}
						else CharType = SHIP;
				break;
			case 'B':	if (strcmp(rText,CHAR_Tags[BONUS]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
						else CharType = BONUS;
				break;
			default:	if (strcmp(rText,CHAR_Tags[PLAYER]))
							if (strcmp(rText,CHAR_Tags[WEAPON]))
							{	strcpy(msj, "Character Type (");
								strcat(msj, rText);
								strcat(msj, ") not compatible.");
								ErrorParser (msj);
								CharType = CHARACTER_UNKNOWN;
							}
							else CharType = WEAPON;
						else CharType = PLAYER;
			};
		break;
	case NAME_SCORE:
		break;
	case NAME_TEXTURE:
		break;
	case SCORE:
		break;
	case SPEED:
					switch (CharType)
					{case SHIP:
					 case SUPPLYSHIP:
						 SupplyShip[CurrentSupplyShip].Speed = atof (rText);
						 break;
					 case PLAYER:
					 case BONUS:
					 case WEAPON:
					 default:;
					}
		break;
	case SPS:		//Shoots Per Second
		break;
	case TEXTURE:
					strcpy(Game.TextureDir, rText);
		break;
	case TITLE:		//HTML tag
					if (strcmp(rText, Game.Title))
					{ErrorParser ("El fichero no corresponde a este videojuego.");
					 bAbort = true;}
					else ;	//File content corresponds to the expected one for this videogame.
							//Go on. Nothing is done
		break;
	case TYPE:		if (strcmp(rText, "Level"))
					{
						strcpy(msj,"El fichero es de tipo \"");
						strcat(msj, rText);
						strcat(msj, "\". Se requiere un fichero de tipo \"inicializaci�n\".");
						ErrorParser (msj);
					 bAbort = true;}
					else ;	//File type is "Initialization"
							//Go on. Nothing is done
		break;
	case VERSION:	if (strcmp(rText, Game.Version))
					{
						strcpy(msj, "La versi�n del fichero es la v");
						strcat(msj, rText);
						strcat(msj, ", que no corresponde con la v");
						strcat(msj, Game.Version);
						strcat(msj, " de este videojuego.");
						ErrorParser (msj);
					 bAbort = true;}
					else ;	//File version corresponds to the one from this videogame.
							//Go on. Nothing is done
		break;
	default:;		//Tags not supported are not managed, even if they have content associated
	};
}

void CLevelReader::Comment(const CString &rComment, DWORD dwAppData, bool &bAbort)
{
	printf("Comentarios: %s\n", rComment);
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CLevelReader::EndParse(DWORD dwAppData, bool bIsAborted)
{
	UNUSED_ALWAYS(dwAppData);
	if (bIsAborted) printf ("Se ha acabado la interpretaci�n del fichero.\n");
};
