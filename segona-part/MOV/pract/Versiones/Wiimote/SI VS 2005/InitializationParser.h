/*	Class that loads the values by default in the very beginning of the game

	Prefix CIP_: Class Initialization Parser

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "LiteHTMLReader.h"
#include "Tags.h"

#include <fstream>

#define CIP_DEBUG			//Class HTML Parser Debug activated
#define CIP_MAXSTACK	 16	//Maximun deepness of the stack

class CInitializationReader : public ILiteHTMLReaderEvents
{
private:
	std::ofstream	LogFile;
	TagType			StackState[CIP_MAXSTACK];	//Stack automata. Stack deepness	
	unsigned char	LastState;				//Pointer to the last stack. Never negative

	inline void		InitializeState(){LastState = 0; StackState[LastState]=NIL;};	//Starts up the stack
	void			PushState (TagType State);	//Stores the new state onto the stack
													//Overflow never happens
	TagType			PopState  (void);	//Take out the last state from the stack. Shorts the stack
									//Returns current state
    void BeginParse(DWORD dwAppData, bool &bAbort);
    void StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void Characters(const CString &rText, DWORD dwAppData, bool &bAbort);
    void Comment(const CString &rComment, DWORD dwAppData, bool &bAbort);
    void EndParse(DWORD dwAppData, bool bIsAborted);
	void ErrorParser(char *msj) {LogFile << "Parser error: " << msj << std::endl;}
	void ErrorClosingTag(CString TagString); //Logs a parser error of not matching closing tags

	//Translates from a string to the token that the string represents
	TagType			String2Tag (const char *Name);

public:
	void Init(char *FileName);

	//Constructor and destructor
	CInitializationReader() {InitializeState();};
	~CInitializationReader(){if (NULL != LogFile) LogFile.close();};

};