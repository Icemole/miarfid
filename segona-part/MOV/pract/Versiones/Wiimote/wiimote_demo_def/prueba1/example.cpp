//sincronizacion
#include <windows.h>

#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <process.h>

#ifndef WIN32
	#include <unistd.h>
#endif

#include "wiiuse.h"

#include <math.h>
#define PI 3.14159265
#define DEG2RAD(x) (((x)*PI)/180.0)
#define RAD2DEG(x) (((x)*180.0)/PI)

#define MAX_WIIMOTES				4


//ventanas glut
int winIR, winACC;
//vector de wiimotes
wiimote** wiimotes;
//wiimotes encontrados y conectados
int encontrados, conectados;


//coordenadas de las fuentes infrarrojas
float x_ir[4]={0.0,0.0,0.0,0.0}, y_ir[4]={0.0,0.0,0.0,0.0};
//tamanyos de las luces infrarrojas
float dot_size[4]={0.0,0.0,0.0,0.0};
//angulos obtenidos por acelerometros
float roll_wiimote=0.0, pitch_wiimote=0.0, yaw_wiimote=0.0;


float halfx=10.24/2;
float halfy=7.68/2;


//===============================================================================================================//
//===========================================FUNCIONES WIIUSE====================================================//
//===============================================================================================================//



// callback del manejador de eventos
// esta funcion es llamada automaticamente por la libreria wiiuse cuando sucede un evento en un wiimote espec�fico
void handle_event(struct wiimote_t* wm) {
	printf("\n\n--- EVENTO [wiimote id %i] ---\n", wm->unid);

	//si se ha pulsado un boton normal, informamos por pantalla
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_A))		printf("A pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_B))		printf("B pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_UP))		printf("UP pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_DOWN))	printf("DOWN pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_LEFT))	printf("LEFT pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_RIGHT))	printf("RIGHT pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_MINUS))	printf("MINUS pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_PLUS))	printf("PLUS pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_ONE))		printf("ONE pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_TWO))		printf("TWO pulsado\n");
	if (IS_PRESSED(wm, WIIMOTE_BUTTON_HOME))	printf("HOME pulsado\n");

	//si se ha soltado un boton normal, informamos por pantalla
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_A))		printf("A liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_B))		printf("B liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_UP))		printf("UP liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_DOWN))	printf("DOWN liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_LEFT))	printf("LEFT liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_RIGHT))	printf("RIGHT liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_MINUS))	printf("MINUS liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_PLUS))	printf("PLUS liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_ONE))	printf("ONE liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_TWO))	printf("TWO liberado\n");
	if (IS_RELEASED(wm, WIIMOTE_BUTTON_HOME))	printf("HOME liberado\n");

	//pulsar el boton MINUS desactiva los acelerometros
	if (IS_JUST_PRESSED(wm, WIIMOTE_BUTTON_MINUS))
		wiiuse_motion_sensing(wm, 0);

	//pulsar el boton PLUS activa los acelerometros
	if (IS_JUST_PRESSED(wm, WIIMOTE_BUTTON_PLUS))
		wiiuse_motion_sensing(wm, 1);

	//pulsar el boton B activa/desactiva la vibracion
	if (IS_JUST_PRESSED(wm, WIIMOTE_BUTTON_B))
		wiiuse_toggle_rumble(wm);

	//pulsar el boton UP activa activa el sensor infrarrojo
	if (IS_JUST_PRESSED(wm, WIIMOTE_BUTTON_UP))
		wiiuse_set_ir(wm, 1);
	//pulsar el boton DOWN activa desactiva el sensor infrarojo
	if (IS_JUST_PRESSED(wm, WIIMOTE_BUTTON_DOWN))
		wiiuse_set_ir(wm, 0);

	//si los acelerometros estan activos imprimimos y almacenados los angulos
	if (WIIUSE_USING_ACC(wm)) {
		printf("wiimote roll  = %f [%f]\n", wm->orient.roll, wm->orient.a_roll);
		printf("wiimote pitch = %f [%f]\n", wm->orient.pitch, wm->orient.a_pitch);
		printf("wiimote yaw   = %f\n", wm->orient.yaw);
		roll_wiimote=wm->orient.a_roll;
		pitch_wiimote=wm->orient.a_pitch;
		//el yaw se calcula solo si los infrarrojos estan activos
		//se esta usando el calculo de triangulacion por defecto de la libreria
		//es decir, se necesitan dos fuentes infrarrojas en la misma disposicion que la barra de infrarrojos de la wii
		yaw_wiimote= wm->orient.yaw;
	}else{
		roll_wiimote=yaw_wiimote=pitch_wiimote=0.0;
	}



	//si el sensor infrarrojo esta activo imprimimos y almacenados las coordenadas
	if (WIIUSE_USING_IR(wm)) {
		int i = 0;

		//buscamos entre las 4 posibles fuentes
		for (; i < 4; ++i) {
			//si es visible imrpimimos y almacenamos sus coordenadas...
			if (wm->ir.dot[i].visible){
				printf("IR fuente %i: (%u, %u)\n", i, wm->ir.dot[i].x, wm->ir.dot[i].y);
				x_ir[i]=(float)wm->ir.dot[i].x;
				y_ir[i]=(float)wm->ir.dot[i].y;
				dot_size[i]=(float)wm->ir.dot[i].size;
			}
			
		}
		//printf("IR cursor: (%u, %u)\n", wm->ir.x, wm->ir.y);
		//printf("IR distancia Z: %f\n", wm->ir.z);
	}

	
}




//este tipo de evento se maneja cuando hay un cambio de estado en el wiimote
//por ejemplo si se conecta/desconecta una expansion 
//tambien se puede llamar explicitamente a la funcion wiiuse_status()
void handle_ctrl_status(struct wiimote_t* wm) {
	printf("\n\n--- ESTADO [wiimote id %i] ---\n", wm->unid);

	printf("Expansiones:      %i\n", wm->exp.type); //1 o 0
	printf("Altavoz:         %i\n", WIIUSE_USING_SPEAKER(wm)); //1 o 0
	printf("IR:              %i\n", WIIUSE_USING_IR(wm)); //1 o 0
	printf("Leds:            %i %i %i %i\n", WIIUSE_IS_LED_SET(wm, 1), WIIUSE_IS_LED_SET(wm, 2), WIIUSE_IS_LED_SET(wm, 3), WIIUSE_IS_LED_SET(wm, 4)); // 0 o 1 / 0 o 1 / 0 o 1 / 0 o 1
	printf("Bateria:         %f %%\n", wm->battery_level); // [0,1]
}


//para manejar una desconexion
void handle_disconnect(wiimote* wm) {
	printf("\n\n--- DESCONEXION [wiimote id %i] ---\n", wm->unid);
}




//esta es la funcion que espera los eventos continuamente para todos los wiimotes
//normalmente se situara en el bucle principal del programa o en un hilo independiente
void wiimote_activity(){

	
	while (1) {
		//wiiuse_poll() necesita ser llamado con el array de wiimotes y el numero de los mismos 
		//tamanyo del vector, independientemente de que esten desconectados o no encontrados
		//la funcion activara el flag correspondiente cuando el wiimote haya reportado un evento
		if (wiiuse_poll(wiimotes, MAX_WIIMOTES)) {
			//si en algun momento se da un evento en alguno de los wiimotes...
			int i = 0;
			for (; i < MAX_WIIMOTES; ++i) {
				switch (wiimotes[i]->event) {
					case WIIUSE_EVENT:
						//evento generico
						handle_event(wiimotes[i]);
						break;

					case WIIUSE_STATUS:
						//evento de estado
						handle_ctrl_status(wiimotes[i]);
						break;

					case WIIUSE_DISCONNECT:
					case WIIUSE_UNEXPECTED_DISCONNECT:
						//evento de desconexion
						handle_disconnect(wiimotes[i]);
						break;

					default:
						break;
				}
			}
		}
	}



	return;
}

//funcion de inicializacion de los wiimotes
int ini_wiimotes(){
	
	//inicializamos el array de wiimotes 
	wiimotes =  wiiuse_init(MAX_WIIMOTES);
	//y los buscamos. Llamamos a la funcion de busqueda con el maximo de wiimotes
	//que esperamos encontrar. Se fija un timeout de 5 segundos.
	//esto nos devolver� el numero actual de wiimotes en modo "discovery"
	


	encontrados = wiiuse_find(wiimotes, MAX_WIIMOTES, 5);

	system("cls");
	printf("DEMO de WIIUSE v0.12\n---------------------\n");	
	if (!encontrados) {
		printf ("No se han encontrado wiimotes.");
		return 0;
	}
	//si se encuentran, nos conectamos a ellos
	//La funcion nos devuelve el numero de conexiones correctas.
	conectados = wiiuse_connect(wiimotes, MAX_WIIMOTES);
	if (conectados)
		printf("Conectado a %i wiimote (de %i encontrados).\n", conectados, encontrados);
	else {
		printf("Fallo al conectar con wiimotes.\n");
		return 0;
	}

	wiiuse_set_leds(wiimotes[0], WIIMOTE_LED_1);
		
	wiiuse_rumble(wiimotes[0], 1);
	
	#ifndef WIN32
		usleep(200000);
	#else
		Sleep(200);
	#endif
	wiiuse_rumble(wiimotes[0], 0);
	

	//Ahora ya se puede consultar el estado de cada wiimote para conocer
	//por ejemplo el estado de la bateria del wiimote 0.
	//Esto deberia ser el wiimote WIIMOTE_ID_1, pero para estar seguro
	//mejor utilizar la funcion wiiuse_get_by_id()
	//wiiuse_status(wiimotes[0]);
	
	//fijamos un timeout para los eventos del wiimote de 5 msg.
	wiiuse_set_timeout(wiimotes, 2, 5, 5);

	_beginthread((void (__cdecl *)(void *))wiimote_activity,0,NULL);

	return 0;

}



//===============================================================================================================//
//===========================================FUNCIONES OPENGL====================================================//
//===============================================================================================================//
void teclado (unsigned char tecla, int x, int y){
	
	

	if(tecla==27){
		
		wiiuse_cleanup(wiimotes, MAX_WIIMOTES);
		exit(0);
	}

	return;
}

void inicializaVentanaIR (void) 
{

	glutInitWindowSize(320,240);
	glutInitWindowPosition(700,50);
	
	winIR=glutCreateWindow("IR");
  	

}




void displayVentanaIR(void)
{

	

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, (6.0/2)/tan(DEG2RAD(30)), 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	glColor3f(1.0,1.0,1.0);
	for(int i=0;i<4;i++){
		glPushMatrix();
		glTranslatef(((float)(x_ir[i]/100))-halfx,((float)(y_ir[i]/100))-halfy,0.0);
		glScalef(1.0*dot_size[i],1.0*dot_size[i],1.0*dot_size[i]);
		glutSolidSphere(0.05,5,5);
		glPopMatrix();
	}



	glutSwapBuffers();
	
	


}




void reshapeVentanaIR(int x, int y)
{

	if (y == 0 || x == 0) return;  
	
	glMatrixMode(GL_PROJECTION);  
	glLoadIdentity();
	
	glOrtho(-10.24/2, 10.24/2, -7.68/2, 7.68/2, 0.1, 2000.0);

	glMatrixMode(GL_MODELVIEW);
	glViewport(0,0,x,y);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, (6.0/2)/tan(DEG2RAD(30)), 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

}



void inicializaVentanaACC (void) 
{

	glutInitWindowSize(320,240);
	glutInitWindowPosition(700,350);
	
	winACC=glutCreateWindow("ACC");
  	

}




void displayVentanaACC(void)
{

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0,(GLdouble)800/(GLdouble)600,0.01,90000000.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 300.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);


	//EJES DE COORDENADAS
			
			glColor3f(1.0,0.0,0.0);
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(200.0,0.0,0.0);
			glEnd();
			glColor3f(0.0,1.0,0.0);
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(0.0,200.0,0.0);
			glEnd();
			glColor3f(0.0,0.0,1.0);
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(0.0,0.0,200.0);
			glEnd();


			//POSICI�N DEL WIIMOTE DE EJEMPLO
			//en realidad aqui hay que aplicar roll, pitch y yaw (de acelerometros lo que se pueda).
			
			glRotatef(pitch_wiimote,1.0,0.0,0.0);
			glRotatef(yaw_wiimote,0.0,1.0,0.0);
			glRotatef(roll_wiimote,0.0,0.0,1.0);
		

			//CAJA WIIMOTE
			glPushMatrix();//2
			glColor3f(1.0,1.0,1.0);
			glTranslatef(0.0,0.0,70.0);
			glScalef(1.0,1.0,140.0/40.0);
			glutWireCube(40.0);
			glPopMatrix();
			//ORIGEN DE COORDENADAS DEL SENSOR WIIMOTE1
			glTranslatef(0.0,0.0,140.0);
			//EJES DE COORDENADAS
			glColor3f(1.0,0.0,0.0);
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(30.0,0.0,0.0);
			glEnd();
			glColor3f(0.0,1.0,0.0);
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(0.0,30.0,0.0);
			glEnd();
			glColor3f(0.0,0.0,1.0);
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(0.0,0.0,30.0);
			glEnd();
			//CAMPO DE VISION
			glColor3f(1.0,1.0,1.0);
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(-512.0,384.0,1236.72);
			glEnd();
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(512.0,384.0,1236.72);
			glEnd();
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(512.0,-384.0,1236.72);
			glEnd();
			glBegin(GL_LINES);
				glVertex3f(0.0,0.0,0.0);
				glVertex3f(-512.0,-384.0,1236.72);
			glEnd();

			glPopMatrix();//2


	glutSwapBuffers();

	
	


}




void reshapeVentanaACC(int x, int y)
{

	if (y == 0 || x == 0) return;  
	
	glMatrixMode(GL_PROJECTION);  
	glLoadIdentity();

	gluPerspective(60.0,(GLdouble)800/(GLdouble)600,0.01,90000000.0);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0,0,x,y);  
	
}





void funcionIdle(){

	//wiimote_activity();
	glutPostWindowRedisplay(winIR);
	glutPostWindowRedisplay(winACC);
		

}




//===============================================================================================================//
//===============================================FUNCION MAIN====================================================//
//===============================================================================================================//

int main(int argc, char** argv) {
	
   glutInit(&argc, argv);
   glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
	
   //ventana que muestra las luces infrarrojas
   inicializaVentanaIR();
   glutDisplayFunc(displayVentanaIR);
   glutReshapeFunc(reshapeVentanaIR);

   //ventana que muestra las orientaciones del wiimote
   inicializaVentanaACC();
   glutDisplayFunc(displayVentanaACC);
   glutReshapeFunc(reshapeVentanaACC);
   
   glutKeyboardFunc(teclado);

   glutIdleFunc(funcionIdle);

   //inicializar los wiimotes
   ini_wiimotes();

   glutMainLoop();
   
   //Desconectar los wiimotes
   wiiuse_cleanup(wiimotes, MAX_WIIMOTES);
	
   return 0;


	


}