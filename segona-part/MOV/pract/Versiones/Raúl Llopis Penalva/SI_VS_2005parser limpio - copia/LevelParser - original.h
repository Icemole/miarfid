/*	Class that loads the values of a given level of game 

	Prefix CLeP_: Class Level Parser

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "LiteHTMLReader.h"

#define CLeP_DEBUG	//Class HTML Parser Debug activated
#define CLeP_TAG_MAX_LONG 15	//Maximun length of a sensitive tag

//Types of tags the initialization parser can match during the parsing
//They can be used also as states of of the stack machine (parser) during the parsing
typedef enum {
	NIL,		//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	BODY,		//HTML tag
	CHARACTER,
	DIR,
	EPS,		//Energy Per Shoot
	EXPLOSION,
	FILE_TAG,
	GEOMETRY,
	HAVE,		//Recursive definition of a character
	HEAD,		//HTML tag
	HEALTH,		//How strong is the character. How much has to be hurt before dieing
	HTML,		//HTML tag
	LIVES,		//Amount of ships the player has still before finishing the game
	MAXBRICKS,
	MAXPLAYERS,
	MAXSHIPS,
	MAXSUPPLYSHIPS,
	MESH,
	MUSIC,
	NAME,
	NAME_CHARACTER,
	NAME_SCORE,
	NAME_TEXTURE,
	SCORES,
	SCORE,
	SHOOT,		//Kind of shooting
	SPEED,
	SPS,		//Shoots Per Second
	TEXTURE,
	TITLE,		//HTML tag
	TYPE,
	UNKNOWN,	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	VERSION,
	MAXTAGS		//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
} CLeP_TagType;

/* Class that loads the values by default in the very beginning of every level in the game
*/

class CLevelReader : public ILiteHTMLReaderEvents
{

private:
    void BeginParse(DWORD dwAppData, bool &bAbort);
    void StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void Characters(const CString &rText, DWORD dwAppData, bool &bAbort);
    void Comment(const CString &rComment, DWORD dwAppData, bool &bAbort);
    void EndParse(DWORD dwAppData, bool bIsAborted);

	//Translates from a string to the token that the string represents
	CLeP_TagType String2Tag (const char *Name);
};