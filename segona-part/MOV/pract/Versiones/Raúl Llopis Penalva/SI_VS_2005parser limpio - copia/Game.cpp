/*The place where resides the class game
  General class to support all the attribute general to all the levels, players, ships,...

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include <string.h>
#include "game.h"

CGame::CGame()
{
	//Game general attributes
	strcpy(Version, "0.6.1");		// CURRENT VERSION
	strcpy(Title, "Space Invaders");// TITLE
	Step			= FADING_IN;	// STAGE
	Level			= 1;			// LEVEL
	ReadLevel		= true;			// Activar/Desactivar lectura de nivel por parser
	LevelReaded		= false;		// Lectura de nivel realizada
	State			= INITIAL;		// The game is in initialization mode when starting

	//Visualization
	Tridimensional	= false;		// 3D GAME not ACTIVE
	Lines3D			= false;		// 3D GAME - MODE GL_LINES
	Mipmap			= true;			// MIPMAP available
	Vsync			= true;			// VSYNC available
	Vsync_active	= true;			// VSYNC ACTIVE
	Billboard		= true;			// BILLBOARDS 

	//Antialiasing
	Antialiasing		= true;		// ANTI-ALIASING available
	Antialiasing_mode	= AAx2;		// ANTI-ALIASING MODE
	Antialiasing_active	= true;		// ANTI-ALIASING ACTIVE

	//Music
	Stream				= false;	// MUSIC STREAMING
	Bitrate				= 0;		// MUSIC BITRATE 

	//WIVIK KEYBOARD 
	Wivik				= false;	// HANDLING ACTIVATED
	Holddown_wivik		= 100;		// HOLD DOWN KEY DURATION IN ms

	Text_2D				= true;		// 2D TEXT AFFICHE ?
	Message[0]			= 0;		// MESSAGE UTILISATEUR
	Spectrum			= false;	// SPECTRUM (BEAT DETECTION)?

	//Console
	Console				= false;	// CONSOLE INACTIVE
	Console_come_in		= true;		// CONSOLE on screen
	Console_moving		= false;	// CONSOLE EN DEPLACEMENT ?
	Console_start		= false;	// CONSOLE EN DEPLACEMENT ?

	Loading				= false;	// To make the Loading Thread called only once
	Loading_finished	= true;

	Lang				= 0;		// LANGUAGE (0 english, 1 french, 2 spanish)

}