/*	Class that loads the values by default in the very beginning of the game

	Prefix CIP_: Class Initialization Parser

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "LiteHTMLReader.h"
#include <fstream>

#define CIP_DEBUG	//Class HTML Parser Debug activated
#define CIP_TAG_MAX_LONG 20	//Maximun length of a sensitive tag

#define CG_MAX_LEVELS	99	// Max levels if Initialization.html is not correct

//Types of tags the initialization parser can match during the parsing
//They can be used also as states of of the stack machine (parser) during the parsing
typedef enum {
	NIL,		//Initial state. This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	BODY,		//HTML tag
	CHARACTER,
	DIR,
	EPS,		//Energy Per Shoot
	EXPLOSION,
	FILE_TAG,
	FILE_LANGUAGE,
	FILE_TEXTURE,
	GEOMETRY,
	HAVE,		//Recursive definition of a character
	HEAD,		//HTML tag
	HEALTH,		//How strong is the character. How much has to be hurt before dieing
	HTML,		//HTML tag
	LANGUAGE,
	LANGUAGES,
	LIVES,		//Amount of ships the player has still before finishing the game
	MAXBRICKS,
	MAXLEVELS,
	MAXPLAYERS,
	MAXSHIPS,
	MAXSUPPLYSHIPS,
	MESH,
	MUSIC,
	NAME,
	NAME_CHARACTER,
	NAME_LANGUAGE,
	NAME_SCORE,
	NAME_TEXTURE,
	SCORE,
	SCORES,
	SHOOT,		//Kind of shooting
	SPEED,
	SPS,		//Shoots Per Second
	TEXTURE,
	TITLE,		//HTML tag
	TYPE,
	TYPE_CHARACTER,
	UNKNOWN,	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	VERSION,
	MAXTAGS		//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
} CIP_TagType;

class CInitializationReader : public ILiteHTMLReaderEvents
{
private:
	std::ofstream	LogFile;
	CIP_TagType		StackState[MAXTAGS];	//Stack automata. Stack deepness	
	unsigned char	LastState;				//Pointer to the last stack. Never negative

	inline void		InitializeState(){LastState = 0; StackState[LastState]=NIL;};	//Starts up the stack
	inline void		PushState (CIP_TagType State);	//Stores the new state onto the stack
													//Overflow never happens
	inline CIP_TagType PopState  (void);//Take out the last state from the stack. Shorts the stack
										//Returns current state
    void BeginParse(DWORD dwAppData, bool &bAbort);
    void StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void Characters(const CString &rText, DWORD dwAppData, bool &bAbort);
    void Comment(const CString &rComment, DWORD dwAppData, bool &bAbort);
    void EndParse(DWORD dwAppData, bool bIsAborted);
	void ErrorParser(char *msj) {LogFile << "Parser error: " << msj << std::endl;}
	void ErrorClosingTag(CString TagString); //Logs a parser error of not matching closing tags

	//Translates from a string to the token that the string represents
	CIP_TagType String2Tag (const char *Name);

public:
	void Init(char *FileName);

	//Constructor and destructor
	CInitializationReader() {InitializeState();};
	~CInitializationReader(){if (NULL != LogFile) LogFile.close();};

};