/*/////////////////////////////////////////////////////////////////////////////////////
//
// SupplyShip initialization
//
// These values are by default. They have to be overwritten in the initializatio phase
// when reading the default values from the "initialization.html" file

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "SupplyShip.h"
#include "si_World.h"

void CSupplyShip::Init()
{
	Alive			=	true;
	Health			=	500;
	Hit_duration	=	500;
	Explosion		=	false; //At the beginning, the supplyship is not exploding
	Type			=	0;
	Speed			=	2;
	x				=	0;
	y				=	4;
	z				=	0.05f;
	Size_x			=	0.5;
	Size_y			=	0.3f;
}

void CSupplyShip::CreateFSM()
{
	State ssState[4];

	//State 0
	ssState[0].setName("Unborn");
	ssState[0].addAction(eOnExit, eAction, "Action: Unborn supplyships", NULL);	
	ssState[0].addTransition("Borning", &ssState[1]);

	// State 1
	ssState[1].setName("Born");
	ssState[1].addAction(eOnEntry, eAction, "Action: Initializing supplyships", init_supplyship);
	ssState[1].addTransition("Growing", &ssState[2]);

	// State 2
	ssState[2].setName("Living");
	ssState[2].addAction(eOnEvent, eAction, "Action: Moving-Shooting supplyships", "Moving", move_supplyship);
	ssState[2].addAction(eOnEvent, eAction, "Action: Displaying supplyships", "Displaying", display_supplyship2);
	ssState[2].addAction(eOnEvent, eAction, "Action: Healthing supplyships", "Healthing", healthing_supplyship);
	ssState[2].addTransition("Moving", &ssState[2]);
	ssState[2].addTransition("Displaying", &ssState[2]);
	ssState[2].addTransition("Healthing", &ssState[2]);
	ssState[2].addTransition("Dying", &ssState[3]);

	// State 3
	ssState[3].setName("Dead");
	ssState[3].addAction(eOnEntry, eAction, "Action: Dying supplyships", death_supplyship);
	ssState[3].addTransition("Resurrecting", &ssState[0]);


	// FSM
	ssFSM.addStates(ssState, 4);

	return;
}


//Variables globales de control del juego

unsigned int CSS_MaxSupplyShips,	//Maximum amount of supplyships avialable in a given level of game
			 CSS_CurrentSupplyShip = CG_INIT_INITIAL;
