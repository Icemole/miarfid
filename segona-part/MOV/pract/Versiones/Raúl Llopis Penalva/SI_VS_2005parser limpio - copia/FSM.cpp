/****************************************
*	Author:		Nathaniel Meyer			*
*	E-Mail:		nath_meyer@hotmail.com	*
*	Website:	http://www.nutty.ca		*
*										*
*   You are free to use, redistribute,  *
*   and alter this file in anyway, so   *
*   long as credit is given where due.	*
****************************************/


#include "FSM.h"

/*
	Constructor / Destructor
*/
FSM::FSM ()
{
	cStateList = NULL;
	cCurrentState = NULL;
}

FSM::~FSM ()
{
}

/*
	addStates
*/
void FSM::addStates (State *cStates, int numStates)
{
	/*cStateList = cStates;*/

	cStateList = new State[4];

	for (int i = 0; i < numStates; i++)
	{
		//Clone name
		cStateList[i].setName(cStates[i].getName());

		// Clone actions
		for (int k = 0; k < cStates[i].mMaxEvents+4; k++) {
			if (strcmp(cStates[i].mSpecification[k].name, "") != 0) {
				if (k < eOnEvent) {

					cStateList[i].addAction((eWhen)k, 
											(eType)cStates[i].mSpecification[k].type, 
											cStates[i].mSpecification[k].name, 
											cStates[i].mSpecification[(eType)k].func);
				}
				else {

					cStateList[i].addAction(eOnEvent, 
											(eType)cStates[i].mSpecification[k].type, 
											cStates[i].mSpecification[k].name,
											cStates[i].mSpecification[k].event,
											cStates[i].mSpecification[k].func);
				}
			}
		}
		

	}


	// Clone transitions
	// This code comes after because states must be clonated. 
	for (int i = 0; i < numStates; i++)	{

    	for (int j = 0; j < cStates[i].getNumTransitions(); j++) {
            //get the state destination for the current transition
			State aux=*cStates[i].mTransition[j].cTo;
			//look for the clonated state with the same name
			bool found=false;
			int ind = -1;
			for (int k=0; k<numStates && !found; k++){
				found=(strcmp(aux.getName(),cStateList[k].getName())==0);
				if (found) ind=k;
			}
			//we have the index of the destination transition, now we create the transition
			if (found){
			  cStateList[i].addTransition(cStates[i].mTransition[j].event, &cStateList[ind]);
			}
			
		}
	}

	cCurrentState = &cStateList[0];
}

/*
	event
*/
//bool FSM::inEvent (char *event, char *args)
bool FSM::inEvent (char *event, LPSTR *args, int count)
{
	// Input the event into the current state
	if ( cCurrentState != NULL )
	{
		return cCurrentState->incoming(event, args, count);
	}

	return false;
}

//bool FSM::outEvent (char *event, char *args)
bool FSM::outEvent (char *event, LPSTR *args, int count)
{
	// Output the event into the current state
	if ( (cStateList != NULL) && (cCurrentState != NULL) )
	{
		State *cTemp = cCurrentState->outgoing(event);
		if ( cTemp != NULL )
		{
			// Set the new output state as current and feed it the event
			cCurrentState = cTemp;
			cCurrentState->incoming(event, args, count);

			return true;
		}
	}

	return false;
}

/*
	Accessor Methods
*/
char *FSM::getCurrentStateName ()
{
	return cCurrentState->getName();
}