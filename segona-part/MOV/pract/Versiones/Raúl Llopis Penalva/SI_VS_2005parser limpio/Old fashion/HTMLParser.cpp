Subject:
ZS070906
From:
ZENIT <zenitespanol@zenit.org>
Date:
Thu, 6 Sep 2007 17:00:00 -0400
To:
diariohtml@mail6.zenit.org

ZENIT
El mundo visto desde Roma
Servicio diario - 06 de septiembre de 2007


SANTA SEDE
Benedicto XVI y Shimon Peres quieren acabar con 60 a�os de lutos
El Papa recibe al ministro de Asuntos Exteriores de Arabia Saudita
La Iglesia al anunciar la fe no quiere imponerse, aclara el Papa
Dejarse transformar por el amor de Cristo, �opci�n de fondo� de todo cristiano
La Santa Sede apoya los estudios sobre la salud global
Benedicto XVI alienta al cine a �filmar lo inefable�

MUNDO
Llamamiento del episcopado boliviano a la paz
Los cristianos europeos quieren dar testimonio de Cristo unidos
Por qu� no es posible decir �Cristo s�, la Iglesia no�

ENTREVISTAS
Teresa de Calcuta: Luz desde la oscuridad (I)

DOCUMENTACI�N
Mensaje del Papa por la muerte del patriarca ortodoxo de Ruman�a


Santa Sede

Benedicto XVI y Shimon Peres quieren acabar con 60 a�os de lutos
En una audiencia concedida en Castel Gandolfo

CASTEL GANDOLFO, jueves, 6 septiembre 2007 (ZENIT.org).- Benedicto XVI y el presidente de Israel Shimon Peres consideraron que ha llegado finalmente la hora de acabar con 60 a�os de lutos en Tierra Santa, en una audiencia de 35 minutos en la residencia pontificia de Castel Gandolfo.

El Papa recibi� al jefe de Estado en su primer viaje al extranjero desde su nombramiento. Sucesivamente Peres se entrevist� con el cardenal Tarcisio Bertone, secretario de Estado. En el encuentro estuvieron tambi�n presentes el arzobispo Dominique Mamberti, secretario para las Relaciones con los Estados, y Oded Ben-Hur, embajador de Israel ante la Santa Sede.

Seg�n ha explicado un comunica! do de la Oficina de Informaci�n de la Santa Sede, �los cordiales coloquios han permitido un intercambio de informaci�n sobre la reciente reanudaci�n de los contactos entre israel�es y palestinos para restablecer la paz en Tierra Santa, respetando las Resoluciones de las Naciones Unidad y los Acuerdos alcanzados hasta ahora�.

�Se ha auspiciado que en el contexto internacional actual que parece ser particularmente favorable gracias a la Conferencia Internacional programada el pr�ximo mes de noviembre, cada una de las partes implicadas haga todos los esfuerzos posibles para responder a las expectativas de las poblaciones, extenuadas por una crisis que dura desde hace 60 a�os y que sigue sembrando luto y destrucci�n�.

�Tambi�n se han examinado las relaciones entre el Estado de Israel y la Santa Sede --aclara--, manifestando el deseo de una conclusi�n r�pida de los importantes negociados, todav�a en curso, y de la instauraci�n de un di�logo constante entre las autoridades israel�es y las comunidades cristianas locales, de cara a la participaci�n plena de estas �ltimas en la construcci�n del bien com�n�.

El comunicado concluye revelando que �el presidente Peres ha renovado al Santo Padre la invitaci�n a visitar Tierra Santa�.

Benedicto XVI ha sido invitado en varias ocasiones por las autoridades israel�es, pero el Vaticano ha subrayado que este viaje podr�a tener lugar en tiempo de paz, o al menos con una tregua s�lida.


Env�a esta noticia a un amigo

top

El Papa recibe al ministro de Asuntos Exteriores de Arabia Saudita
Hablan sobre libertad religiosa y di�logo interreligioso

CASTEL GANDOLFO, jueves, 6 septiembre 2007 (ZENIT.org).- Benedicto XVI ha recibido en audiencia en la residencia pontificia de Castel Gandolfo al pr�ncipe Saud Al Faisal, ministro de Asuntos Exteriores de Arabia Saudita.

Un comunicado de la Sala de Prensa de la Santa Sede explica que, �en el curso del cordial coloquio� que dur� unos 20 minutos, �se afrontaron varios argumentos de inter�s com�n�.

�Principalmente --especifica--, la defensa de los valores religiosos y morales, el conflicto en Oriente Medio, la situaci�n pol�tica y religiosa en Arabia Saudita, la importancia del di�logo intercultural e interreligioso y la contribuci�n de los fieles de las di! ferentes religiones a la promoci�n del entendimiento entre los hombres y los pueblos�.

�Para lograr este objetivo se auspiciaron iniciativas comunes a favor de la paz�, concluye el comunicado.


Env�a esta noticia a un amigo

top

La Iglesia al anunciar la fe no quiere imponerse, aclara el Papa
Al recibir a los obispo de la Conferencia Episcopal de Laos y Camboya

CASTEL GANDOLFO, jueves, 6 septiembre 2007 (ZENIT.org).- La Iglesia al anunciar la fe no quiere imponerse, explic� Benedicto XVI este jueves al recibir en audiencia a los obispos de la Conferencia Episcopal de Laos y Camboya.

En el encuentro, con motivo de la quinquenal visita �ad limina apostolorum� al Papa y a sus colaboradores, el Santo Padre afront� �el anuncio de la fe cristiana en una cultura particular�.

Arraigada desde hace m�s de 400 a�os, la Iglesia en Camboya vivi� un periodo dram�tico entre los a�os setenta y ochenta del siglo pasado: el r�gimen de Pol Pot trat� de eliminarla, destruyendo lugares de culto.

La libertad r! eligiosa fue reconocida en 1992. En el pa�s de 13 millones de habitantes, 23 mil son cat�licos, particularmente comprometidos en obras de educaci�n y sociales.

�La fe cristiana no es una realidad extra�a a vuestros pueblos --dijo el Papa a los obispos asi�ticos--. Jes�s es la Buena Nueva para los hombres y mujeres de todo tiempo y lugar que buscan el sentido de la existencia y la verdad de su humanidad�.

�Y anunci�ndolo a todos los pueblos --a�adi�--, la Iglesia no quiere imponerse, sino que da testimonio de su estima por el ser humano y por la sociedad donde vive�.

�En el contexto social y religioso de vuestra regi�n es sumamente importante que los cat�licos manifiesten su propia identidad, respetando siempre las otras tradiciones religiosas y las culturas de los pueblos�.

�Esta identidad --subray�-- debe expresa! rse ante todo a trav�s de una experiencia espiritual aut�ntica, que se basa en la acogida de la Palabra de Dios y en los sacramentos de la Iglesia�.

Entre las prioridades para los obispos de Camboya y Laos, el Papa plante� �la formaci�n de los fieles, sobre todo la de los religiosos y catequistas�, pues �su papel para la vitalidad de las comunidades cristianas es de gran importancia�.

De esta manera, constat�, con �una s�lida fe cristiana pueden entablar un di�logo aut�ntico con los miembros de otras religiones para cooperar en la construcci�n de vuestros pa�ses y promover el bien com�n�.


Env�a esta noticia a un amigo

top

Dejarse transformar por el amor de Cristo, �opci�n de fondo� de todo cristiano
Afirma Benedicto XVI en su mensaje al prior general de la Orden Carmelita

CASTELGANDOLFO, jueves, 6 septiembre 2007 (ZENIT.org).- Dejarse transformar por el amor de Dios es la opci�n de fondo que se presenta a todo cristiano, escribe Benedicto XVI en un mensaje enviado al padre Joseph Chalmers, prior general de la Orden de los Hermanos de la Beata Virgen Mar�a del Monte Carmelo.

En septiembre pr�ximo, recuerda el Papa, la �antigua e ilustre Orden� celebrar� su Cap�tulo General, �con motivo del octavo centenario de la entrega por parte de san Alberto, Patriarca de Jerusal�n (1205-1214), de la �formula vit� en la que se inspiraron los eremitas latinos que se establecieron �junto a la Fuente del Monte Carmelo&rsq! uo; (Regla Carmelita, 1)�.

Fue el �primer reconocimiento por parte de la Iglesia de este grupo de hombres, que dejaron todo para vivir en obsequio de Jesucristo, imitando los sublimes ejemplos de la Beata Virgen Mar�a y del Profeta El�as�, indica el pont�fice.

Los primeros carmelitas, recuerda el Santo Padre, �acogiendo el se�or�o de Cristo en sus vidas, se hicieron disponibles a ser transformados por su amor�. �Es esta la opci�n de fondo que se presenta a cada cristiano�, subraya.

En su primera enc�clica, �Deus caritas est�, Benedicto XVI, afirma que �en el inicio del ser cristiano no hay decisi�n �tica o una gran idea, sino el encuentro con un acontecimiento, con una Persona, que da a la vida un nuevo horizonte y con esto la direcci�n decisiva� (n� 1).

�Si este desaf�o vale para el cristiano --indica el Papa Benedicto XVI en su mensaje--, cu�nto m�s debe sentirse interpelado por �l un carmelita, cuya vocaci�n es la subida al monte de la perfecci�n�.

El Papa reconoce que �no es para nada f�cil vivir fielmente esta llamada�. �En cierto sentido --afirma--, hay necesidad de protegerse con armaduras de las insidias del mundo�.

A pesar de las dificultades, admite, son muchos los hombres y mujeres que han alcanzado la santidad �viviendo con fidelidad creativa los valores de la Regla carmelita�.

El Papa recuerda la constituci�n conciliar �Lumen gentium� (n� 50) y a�ade que �por un motivo m�s, nos sentimos impulsados a buscar la Ciudad futura y a la vez se nos ense�a la v�a segur�sima por la que, entre las mudables cosas del mundo, podremos llegar a la perfecta uni�n con Cr! isto, es decir a la santidad, seg�n el estado y condici�n propia de cada uno�.

Seg�n el Papa, el tema del Cap�tulo de los Carmelitas ��In obsequio Jesu Christi�. Comunidad orante y prof�tica en un mundo que cambia�, pone bien de manifiesto el peculiar estilo con el que la Orden del Carmelo trata de responder al amor de Dios por medio de una vida en la que se entrelazan oraci�n, fraternidad y esp�ritu prof�tico�.

�Con la mirada fija en Cristo y confiando en la ayuda de los santos que, en el curso de estos ocho siglos, han encarnado los dict�menes de la Regla del Carmelo, que cada miembro de la Orden de los Hermanos de la Bienaventurada Virgen Mar�a del Monte Carmelo se sienta llamado a ser testigo cre�ble de la dimensi�n espiritual propia de cada ser humano�.

Los fieles laicos podr�n as� encontrar, en las comunidades carmelitas, aut�nticas ��escuelas� de oraci�n, donde el encuentro con Cristo no se exprese s�lo en petici�n de ayuda, sino tambi�n en acci�n de gracias, alabanza, adoraci�n�, a�ade, citando a Juan Pablo II (Carta Apost�lica �Novo millennio ineunte�, 33).

El fin principal del cap�tulo general de la Orden Carmelita, como escribe el padre Chalmers, en la convocatoria del evento, es el de �escucha y luego tratar de comprender lo que Dios pide a la Orden en este preciso momento de nuestra historia�.

�En los �ltimos a�os --explica el prior general--, hemos adquirido un nivel de conocimiento m�s profundo de nuestro carisma y, al mismo tiempo, la Orden se ha difundido en �reas geogr�ficas en las que antes no ten�a una presencia�.

�El desaf�o principal para! la Iglesia es el de proclamar la Buena Noticia a la gente de hoy en todas las culturas del mundo, y nuestra Orden est� tambi�n llamada a participar en esta misi�n�.

�Tenemos que tratar de ser fieles a nuestra vocaci�n carmelita en un mundo que est� en evoluci�n continua�, concluye.


Env�a esta noticia a un amigo

top

La Santa Sede apoya los estudios sobre la salud global
NUEVA YORK, jueves, 6 septiembre 2007 (ZENIT.org).- �Global Health in Focus� (La Salud Global a Examen) es el tema de una conferencia internacional que tendr� lugar en Nueva York el pr�ximo 25 de septiembre.

El evento est� copatrocinado por la Misi�n del Observador Permanente de la Santa Sede ante Naciones Unidas, la �Path to Peace Foundation� (Fundaci�n Camino hacia la Paz), la Universidad Purdue, de Lafayette, Indiana, Estados Unidos, y la Universidad de Notre Dame, Indiana

La convocatoria tiene lugar ante la apertura de la 62 sesi�n de la Asamblea General de Naciones Unidas sobre este mismo argumento.

Seg�n un comunicado recibido por Zenit, la reuni�n internacional pretende poner de manifiesto el trabajo de dos conocidas universidades estadounidenses sobre el tema de la salud global.

Estas instituciones, explica el texto, presentar�n una visi�n de conjunto del trabajo que est�n llevando a cabo, en un arco de temas que va desde el estudio del sida/vih hasta los m�todos para controlar la malaria y proporcionar asistencia m�dica en �frica, del modo m�s eficiente.

Entre los proyectos a discutir est�n aquellos que podr�an ser aplicados inmediatamente y aquellos que pueden servir como modelo.

En sesiones definidas como �breves pero intensas�, los ponentes �definir�n y debatir�n sobre un tema; apoyar�n la dignidad de la persona humana; describir�n las mejores pr�cticas actuales; explorar�n nuevas posibilidades; proporcionar�n un foro para el intercambio de experiencias entre las naciones y una mejor comunicaci�n entre oradores y participantes�.

Para m�s informaci�n, entrar en los sitios: de la Misi�n de la Santa Sede (www.holyseemission.org), de la �Path to Peace Foundation� (www.thepathtopeacefoundation.org), de la �Purdue University� (www.purdueedu), y de la Universidad de Notre Dame (www.nd.edu),


Env�a esta noticia a un amigo

top

Benedicto XVI alienta al cine a �filmar lo inefable�
En un mensaje de apoyo a una nueva iniciativa surgida en M�xico

GUADALAJARA, jueves, 6 septiembre 2007 (ZENIT.org).- Mediante un mensaje dirigido al arzobispo de Guadalajara, cardenal Juan Sandoval ��ig�ez, el Papa Benedicto XVI se ha hecho presente en el inicio de los trabajos del Simposio Internacional �Filmar lo Inefable 2007�.

La misiva, firmada por el secretario de Estado del Papa, el cardenal Tarcisio Bertone, representa un espaldarazo al Simposium y a la iniciativa llevada a cabo en esta ciudad mexicana de poner en marcha el Centro Superior de Producci�n Cinematogr�fica, Filmar lo inefable, A.C. (CSPC)

En la carta el Papa Benedicto XVI subraya que la iniciativa del Centro de Producci�n Cinematogr�fica cat�lico �favorece un di&aac! ute;logo eclesial con los profesionales de la comunicaci�n audiovisual, especialmente los cineastas, que buscan en una visi�n antropol�gica cristiana una fuente de inspiraci�n para su trabajo art�stico�.

En otra parte de su misiva, el mensaje pontificio invita a quienes trabajan en el campo de la cinematograf�a, �a desarrollar un cine de calidad que, evitando aspectos tantas veces reductivos, proponga valores universales y modelos de convivencia ciudadana, y favorezca tambi�n el di�logo entre los pueblos y una cultura de paz, presupuesto irrenunciable para la civilizaci�n del amor�.

En el documento se expresa que el mundo del cine, cuando se abre a la dimensi�n trascendente de la vida y al misterio que late en el fondo del ser humano, es capaz de promover un aut�ntico humanismo lleno de genuinos valores como la esperanza y la fraternidad, la concordia y la paz.
El Santo Padre env�a su bendici�n apost�lica hasta la sede de este Simposio y asegura que reza al Se�or para que ilumine a cuantos participan en �ste, especialmente a los j�venes cineastas, �para que compartan este mensaje en su propio ambiente�.

El cardenal Sandoval ��ig�ez, arzobispo de Guadalajara, inaugur� y bendijo el 3 de septiembre las instalaciones del Centro Superior de Producci�n Cinematogr�fica, Filmar lo inefable, A.C., as� como los trabajos que inician formalmente con los 21 cineastas que trabajar�n en estas instalaciones.

Antes de la lectura de la bendici�n de locales destinados a medios de comunicaci�n, el cardenal Sandoval encomend� los trabajos del Centro para que quienes ah� trabajan y los j�venes que vienen a estudiar, contribuyan en algo �a la luz que hace falta en este mundo�.
!
�Mucha gente --expres� el purpurado mexicano-- vive su vida sin sentido, sin fe, a veces los medios de comunicaci�n contribuyen mucho a eso mismo. Que los que aqu� estudien lleven ese prop�sito de sumarse y sumar sus capacidades que aqu� adquieran, a una gran obra para iluminar este mundo con los valores de la fe, los que sean creyentes, o con los valores humanos que est�n radicados en la misma naturaleza del hombre�.

Estos valores, dijo, pueden ser una gu�a para vivir decentemente y encontrarle un sentido a la existencia humana.

El Padre Javier Magdaleno Cueva, director del CSPC, present� al cardenal Sandoval ��ig�ez la primera generaci�n que conforma este Centro y que est� compuesta por j�venes provenientes de Venezuela, Itlaia, Uruguay, Chile, Brasil, Colombia y Cuba, as� como otros cineastas de origen mexicano, que han dejado sus tra! bajos, sus casas y familias, para iniciar este nuevo proyecto cinematogr�fico.

En el acto de inauguraci�n se hizo presente tambi�n, a travtag out from the HTML section.");
					ErrorParser(msj);
		break;
	default:;
	};
}

void CInitializationReader::Characters(const CString &rText, DWORD dwAppData, bool &bAbort)
{
char msj[100];
float Value;

#ifdef CHP_DEBUG	//Class HTML Parser Debug activated
	fprintf(LogFile, "Texto: %s\n", rText);
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

	switch(StackState)
	{case EPS:		//Energy Per Shoot
					Value = atof (rText);
		break;
	case EXPLOSION:
					
		break;
	case FILE_LANGUAGE:
					
		break;
	case FILE_TEXTURE:
					
		break;
	case GEOMETRY:
					
		break;
	case HEALTH:	//How strong is the character. How much has to be hurt before dieing
		break;
	case LEVEL:
		break;
	case LIVES:		//Amount of ships the player has still before finishing the game
		break;
	case MAXBRICKS:
		break;
	case MAXPLAYERS:
		break;
	case MAXSHIPS:
		break;
	case MAXSUPERSHIPS:
		break;
	case MESH:
		break;
	case MUSIC:
		break;
	case NAME_CHARACTER:
		break;
	case NAME_LANGUAGE:
		break;
	case NAME_SCORE:
		break;
	case NAME_TEXTURE:
		break;
	case SCORE:
		break;
	case SPEED:
		break;
	case SPS:		//Shoots Per Second
		break;
	case TEXTURE:
		break;
	case TITLE:		//HTML tag
		break;
	case TYPE:
		break;
	case VERSION:
		break;
	default:;
	};
}

void CInitializationReader::Comment(const CString &rComment, DWORD dwAppData, bool &bAbort)
{
#ifdef CHP_DEBUG	//Class HTML Parser Debug activated
	fprintf(LogFile, "Comentarios: %s\n", rComment);
#endif

	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CInitializationReader::EndParse(DWORD dwAppData, bool bIsAborted)
{
	UNUSED_ALWAYS(dwAppData);
#ifdef CHP_DEBUG	//Class HTML Parser Debug activated
	if (bIsAborted) fprintf (LogFile, "Se ha acabado la interpretaci�n del fichero.\n");
#endif
}

/* Class that loads the values by default in the very beginning of every level in the game

   LEVEL INITIALIZATION
*/

void CLevelReader::BeginParse(DWORD dwAppData, bool &bAbort)
{
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
	printf("Comienza la interpretaci�n del fichero de inicializaci�n.\n");
}

void CLevelReader::StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	printf("Etiqueta comienzo: %s\n", pTag->getTagName());
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

}

void CLevelReader::EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort)
{
	printf("Etiqueta fin: %s\n", pTag->getTagName());
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;

}

void CLevelReader::Characters(const CString &rText, DWORD dwAppData, bool &bAbort)
{
	printf("Texto: %s\n", rText);
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CLevelReader::Comment(const CString &rComment, DWORD dwAppData, bool &bAbort)
{
	printf("Comentarios: %s\n", rComment);
	UNUSED_ALWAYS(dwAppData);
	bAbort = false;
}

void CLevelReader::EndParse(DWORD dwAppData, bool bIsAborted)
{
	UNUSED_ALWAYS(dwAppData);
	if (bIsAborted) printf ("Se ha acabado la interpretaci�n del fichero.\n");
};


//Translates from a string to the token that the string represents
TagType String2Tag (const char *Name)
{
	unsigned char character;
	char UpString[CHP_TAG_MAX_LONG];

	//All the tag is set up
	for(character = 0; character < strlen(Name); character++)
      UpString[character] = toupper(Name[character]);

	switch (UpString[0])
	{
	case 'B': if (strcmp(UpString, Tags[BODY]))				return BODY;
		break;
	case 'C': if (strcmp(UpString, Tags[CHARACTER]))		return CHARACTER;	  
		break;
	case 'D': if (strcmp(UpString, Tags[DIR]))				return DIR;
		break;
		case 'E': if (strcmp(UpString, Tags[EPS]))			return EPS;
			  else if (strcmp(UpString, Tags[EXPLOSION]))	return EXPLOSION;
		 break;
	case 'F': if (strcmp(UpString, Tags[FILE_TAG]))			return FILE_TAG;
		break;
	case 'G': if (strcmp(UpString, Tags[GEOMETRY]))			return GEOMETRY;
		break;
	case 'H': if ('E' == UpString[1])
				  if (strcmp(UpString, Tags[HEAD]))			return HEAD;
				  else if (strcmp(UpString, Tags[HEALTH]))	return HEALTH;
			  else if (strcmp(UpString, Tags[HAVE]))		return HAVE;
				   else if (strcmp(UpString, Tags[HTML]))	return HTML;
		break;
	case 'L': if (strcmp(UpString, Tags[LEVEL]))			return LEVEL;
			  else if (strcmp(UpString, Tags[LANGUAGE]))	return LANGUAGE;
			  else if (strcmp(UpString, Tags[LIVES]))		return LIVES;
		break;
	case 'M': if ('A' == UpString[1])
				  //Serie de MAX*
				  if ('S' == UpString[3])
					   if (strcmp(UpString, Tags[MAXSHIPS]))			return MAXSHIPS;
					   else if (strcmp(UpString, Tags[MAXSUPERSHIPS]))	return MAXSUPERSHIPS;
				  else if (strcmp(UpString, Tags[MAXBRICKS]))			return MAXBRICKS;
				   else if (strcmp(UpString, Tags[MAXPLAYERS]))			return MAXPLAYERS;
			  else if (strcmp(UpString, Tags[MESH]))					return MESH;
				   else if (strcmp(UpString, Tags[MUSIC]))				return MUSIC;
		break;
	case 'N': if (strcmp(UpString, Tags[NAME]))			return NAME;
		break;
	case 'S': if (strcmp(UpString, Tags[SCORE]))		return SCORE;
			  else if ('S' == UpString[5])				return SCORES;
			  else if (strcmp(UpString, Tags[SHOOT]))	return SHOOT;
			  else if (strcmp(UpString, Tags[SPEED]))	return SPEED;
			  else if (strcmp(UpString, Tags[SPS]))		return SPS;
		break;
	case 'T': if (strcmp(UpString, Tags[TEXTURE]))		return TEXTURE;
			  else if (strcmp(UpString, Tags[TITLE]))	return TITLE;
			  else if (strcmp(UpString, Tags[TYPE]))	return TYPE;
		break;
	case 'V': if (strcmp(UpString, Tags[VERSION]))		return VERSION;
		break;
	};
	return UNKNOWN;
};

