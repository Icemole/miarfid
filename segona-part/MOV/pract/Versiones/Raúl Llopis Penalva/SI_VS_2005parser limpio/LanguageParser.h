/*	Class that loads the values of a given level of game 

	Prefix CLaP_: Class Level Parser

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "LiteHTMLReader.h"

#define CLaP_DEBUG	//Class HTML Parser Debug activated
#define CLaP_TAG_MAX_LONG 15	//Maximun length of a sensitive tag

//Types of tags the initialization parser can match during the parsing
//They can be used also as states of of the stack machine (parser) during the parsing
typedef enum {
	BODY,		//HTML tag
	CONTENT,
	HEAD,		//HTML tag
	HTML,		//HTML tag
	ID,
	NAME,
	RESOURCE,
	TITLE,		//HTML tag
	TYPE,
	UNKNOWN,	//This tag is for management purpouses only. There is no correspondence to any string label in the HTML file
	VERSION,
	MAXTAGS
} CLaP_TagType;

/* Class that loads the values by default in the very beginning of every level in the game
*/

class CLanguageReader : public ILiteHTMLReaderEvents
{

private:
    void BeginParse(DWORD dwAppData, bool &bAbort);
    void StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void Characters(const CString &rText, DWORD dwAppData, bool &bAbort);
    void Comment(const CString &rComment, DWORD dwAppData, bool &bAbort);
    void EndParse(DWORD dwAppData, bool bIsAborted);

	//Translates from a string to the token that the string represents
	CLaP_TagType String2Tag (const char *Name);
};