/*	Header with definitions needed for the language management

	Prephix = LANG_

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#ifndef LANG_LANGUAGES
#define LANG_LANGUAGES

#include <vector>

#define LANG_LONG_STRING 128
#define LANG_DEFAULT "English"
#define LANG_MAX_LANGUAGES 8

//Error codes for the management of the languages
typedef enum errors {
	LANG_NO_ERROR,		//No error has happened
	LANG_REPEATED,		//There is just an available language with the same name as the given one
	LANG_INEXISTENT,	//There is no langugae with the given name
	LANG_NO_LANGUAGE,	//There are no languages at all in the list of languages
	LANG_MAX_ERRORS		//For error management only
} LANG_ERRORS;

//Different languages supported by

class CLanguage
{
private:
	char	File	[LANG_LONG_STRING],
			Name	[LANG_LONG_STRING];

public:

	//Methods
	CLanguage(){File[0] = Name[0] = 0;}

	void SetFile	(char *f) {strcpy(File, f);}	//Sets the folder f where the language files are
	void SetName	(char *n) {strcpy(Name, n);}	//Sets the name n of the language
	char *GetFile	() {return File;}
	char *GetName	() {return Name;}
};

class CLanguages
{
private:
	std::vector<CLanguage> Languages;	//Languages structure to support all the languages available for the game
										//It uses the template vector (STL)
	char Active[LANG_LONG_STRING];		//Active language from all of available

public:

	//Methods
	LANG_ERRORS		Add		(char *file);	//Adds a new language to the list if possible
	LANG_ERRORS		Remove	(char *name);	//Removes an existing language from the list if possible
	unsigned int	Amount	();				//Quantity of languages that ara available in a given moment
};

#endif
