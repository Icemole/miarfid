// si_World.h - space invaders opengl

#ifndef _WORLD_H
#define _WORLD_H


//////////////////////////////////////////////////////////////////////////////////////
//
// WOLRD => Les fonctions d'initialisation et d'affichage des �l�ments du jeu
//
//

// Initialisation des �l�ments

void init_ships(LPSTR *args);
void living_ships(LPSTR *args);
void death_ships(LPSTR *args);
void display_ship(LPSTR *args);
void init_supplyship(LPSTR *args);
void healthing_supplyship(LPSTR *args);
void display_supplyship2(LPSTR *args);
//void display_supplyship2(int i);
void death_supplyship(LPSTR *args);
void move_supplyship(LPSTR *args);

void init_player();
void init_shoots();
void init_bonuses(bool game_3d_local);
void init_bunker(float x, float y, float z);

// Affichage des �l�ments

void display_player();
void display_supplyship();
void display_shoot(int i, bool viewport2);
void display_bonus_2d_to_3d();
void display_ring_2d_to_3d();
void display_bonus_3d();
void display_bunkers();
void display_score_hiscore_lives();
void display_below_lives();
void display_background(bool viewport2);
void display_image();
void display_cube(int texture_id, float size);
void display_console();
void display_console_mini_screen();
void display_text_frames();
void display_energy();
void display_spectrum();
void display_equalizer();

//////////////////////////////////////////////////////////////////////////////////////
//
// WORLD => Le splash screen
//
//
void display_splashscreen();
extern bool active_splash;
extern bool active_reverse_splash;

#endif