/*/////////////////////////////////////////////////////////////////////////////////////
//
// Player initialization or constructor
//
// These values are by default. They have to be overwritten in the initializatio phase
// when reading the default values from the "initialization.html" file

	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "Player.h"

bool CPlayer::Init ()	//Used when all the values are initialized by default
												//when reading the global initialization game file. 
												//Returns true if the initialization of the attirbute has been correctly done
{
	return true;
}

CPlayer::CPlayer()
{
	Alive			=	true;
	Type			=	0;
	Energy			=	20;
	Hit_duration	=	500;
	
	//Space position
	x				=	0;
	y				=  -6.15f;
	yi				=	0.00;
	yi_speed		=	0.01f;
	yi_cos			=	0;
	z				=	0.05f;
	zi				=	0.00;
	zi_speed		=	0.01f;
	Size_x			=	0.3f;
	Size_y			=	0.25;
	Movement		=	0;
	
	//Weapons
	laser_left		=	false;
	laser_right		=	false;
}
