/*
	Author: Ram�n Moll�
	Last update: 2007-09-11
*/

#include "Ship.h"
#include "si_World.h"

//////////////////////////////////////////////////////////////////////////////////////
//
// Ship initialization
//
// These values are by default. They have to be overwritten in the initialization phase
// when reading the default values from the "initialization.html" file

void CShip::Init()
{
	Alive			=	true;
	Health			=	25;
	Hit_duration	=	500;
	Type			=	HIGH;
	Speed			=	2;
	x				=	0;
	y				=	4;
	z				=	0.05f;
	Size_x			=	0.5;
	Size_y			=	0.3f;
}


void CShip::CreateFSM(int ship)
{
	State sState[4];

	//State 0
	sState[0].setName("Unborn");
	sState[0].addAction(eOnExit, eAction, "Action: Unborn ships", NULL);
	sState[0].addTransition("Borning", &sState[1]);

	// State 1
	sState[1].setName("Born");
	sState[1].addAction(eOnEntry, eAction, "Action: Initializing ships", init_ships);
	sState[1].addTransition("Growing", &sState[2]);

	// State 2
	sState[2].setName("Living");
	sState[2].addAction(eOnEvent, eAction, "Action: Moving-Shooting ships", "Moving", living_ships);
	sState[2].addAction(eOnEvent, eAction, "Action: Displaying ships", "Displaying", display_ship);
	sState[2].addTransition("Moving", &sState[2]);
	sState[2].addTransition("Displaying", &sState[2]);
	sState[2].addTransition("Dying", &sState[3]);

	// State 3
	sState[3].setName("Dead");
	sState[3].addAction(eOnEntry, eAction, "Action: Dying ships", death_ships);
	sState[3].addTransition("Resurrecting", &sState[0]); // Ready for...


	// FSM
	sFSM.addStates(sState, 4);

	return;
}

unsigned int CS_MaxShips,	//Maximum amount of ships avialable in a given level of game
			 CS_CurrentShip = CG_INIT_INITIAL;

