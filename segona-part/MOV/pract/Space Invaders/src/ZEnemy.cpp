/*
	@author Nahuel Unai Rosell� Beneitez
	@version 2020-11-28
*/

//#include <AI\FSMParser.h>

//#include <Navy.h>

#include <ZEnemy.h>

#include <SIGame.h>
//#include <TextureAnimationManager.h>
//#include <ShootsManager.h>	/// Header File class Manager for the shoots
#include <glext.h>

#define CSZ_XSIZE 1.0f
#define CSZ_YSIZE 1.0f
#define CSZ_ZSIZE 0.0f

#define CSZ_XSIZE3D 0.4f
#define CSZ_YSIZE3D 0.5f
#define CSZ_ZSIZE3D 0.0f

//extern CNavy					*Navy;			///<Singleton to save the general configuration of the enemy Navy
//extern CTextureAnimationManager	AnimationsManager;
//extern CShootsManager			*ShootsManager;

//////////////////////////////////////////////////////////////////////////////////////
//
// Ship initialization
//
// These values are by default. They have to be overwritten in the initialization phase
// when reading the default values from the "initialization.html" file
/*
UGKS_String CSZ_StateName[CSZ_MAXSTATE] =
{
	"UNBORN",
	"BORN",
	"LIVING",
	"BURSTING",
	"DEAD"
};

///Sensitive transition names
UGKS_String CSZ_NameTransition[CSZ_MAXTRANSITION] = 
{
	"DEFAULT",
	"BORNING",
	"GROWING",
	"MOVING",
	"DISPLAYING",
	"BURST",
	"REBORN",
	"DYING"
};
*/

void CZEnemy::Init()
{
	Health = MaxHealth	=	CSZ_MAX_HEALTH;
	Hit_duration		=	CSZ_MAX_MAX_HIT_DUR;
	Type				=	CHARS_ZENEMY;
	HealthRegen			=	CSZ_HEALTH_REGEN;
	Shield				=	CSZ_MAX_SHIELD;
	ShieldRegen			=	CSZ_SHIELD_REGEN;
	Fuel				=	CSZ_FUEL;
	Weight				=	CSZ_WEIGHT;

	Speed.v[XDIM]		=	0.015f;	//Units/ms

	AABB[CHAR_BBSIZE][XDIM].Value = CSZ_XSIZE;
	AABB[CHAR_BBSIZE][YDIM].Value = CSZ_YSIZE;
	AABB[CHAR_BBSIZE][ZDIM].Value = CSZ_ZSIZE;

#ifdef CHAR_USE_AABB
	UpdateAABB();
#endif

	Explosion.Health	=	CSZ_MAX_EXPLOSION_LIFE;

	UpdateSF(TimerManager.GetSF());
}

/**
* Destroys the 'Ship' object.
* The actions for destroying the Ship are performed through to the base class 'CCharacter'.
*/
CZEnemy::~CZEnemy()
{

};

///Physics methods
void CZEnemy::Collided (CCharacter *CollidedChar)
{
}

//Displays the ship while hit
void CZEnemy::DisplayHit()
{
	/*
	TimerManager.GetTimer(SIGLBT_APP_TIMING)->EndCounting();
	Hit_duration -= 10 * TimerManager.GetTimer(SIGLBT_APP_TIMING)->GetLastPeriodms();
	if (Hit_duration <= 0)
		Hit_duration = CSZ_MAX_MAX_HIT_DUR;

	Mesh->modelo.Draw();	// ship not textured, white
	*/
}

//Renders a ships in 3D or 2D way
void CZEnemy::Render ()
{
	/*
#ifdef XM_RND_TIME_DISC
	TimerManager.GetTimer(SIGLBT_RENDER_TIMING)->InitCounting();
#endif
	GLboolean Blending = glIsEnabled(GL_BLEND);

	if (!Alive())	//Although it is not active
		return;
	//Enable the navy ships lighting
	//glEnable(GL_LIGHTING);			// activate lights on
									//Disable the players lighting
	glDisable(SIGLB_PLAYER_LIGHT);
	//Enable the navy ships lighting
	glEnable(SIGLB_SHIP_LIGHT);
	glEnable(SIGLB_SHIP2_LIGHT);

	switch (RenderMode)
	{
	case CHAR_NO_RENDER:			//No render for this character
		break;
	case CHAR_2D:
		if (!Blending)
			glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		AnimationsManager.Animations[IndAnimation2D]->UpdateAnimation();
		AnimationsManager.Animations[IndAnimation2D]->SetCurrentPhotogram();

		Render2D();

		if (!Blending)	glDisable(GL_BLEND);
		break;
	case CHAR_LINES3D:
	case CHAR_3D:
		glEnable(GL_CULL_FACE);		// Back face culling set on
		glFrontFace(GL_CCW);		// The faces are defined counter clock wise
		glEnable(GL_DEPTH_TEST);	// Occlusion culling set on

		Render3D();

		break;
	default: return;
	}

	if (Explosion.Active())
		Explosion.Render();

#ifdef XM_RND_TIME_DISC
	TimerManager.EndAccCounting(SIGLBT_RENDER_TIMING);
#endif
	*/
}

//////////////////////////////////////////////////////////////////////////////////////
//
// Displays a ship 3DS
//
//
void CZEnemy::Render3D()
{
	/*
	Mesh->modelo.rot.v = Rotation.v;

	Mesh->modelo.pos.x = Position.v[XDIM];
	Mesh->modelo.pos.y = Position.v[YDIM];

	Mesh->modelo.scale.v = Scale.v;

	// affiche le ship touch�
	if (Hit_duration < CSZ_MAX_MAX_HIT_DUR)
		DisplayHit();
	else
		Mesh->modelo.Draw();
	*/
}

void CZEnemy::ChangeRenderMode(CHAR_RENDER_MODE Mode)
{
	/*
	RenderMode = Mode;
	Explosion.ChangeRenderMode(Mode);

	switch (RenderMode)
	{
	case CHAR_NO_RENDER:			//No render for this character
		break;
	case CHAR_2D:
		break;
	case CHAR_LINES3D:
		break;
	case CHAR_3D:

#ifdef CHAR_USE_QUADTREE
#elif defined CHAR_USE_AABB 
#elif defined CHAR_USE_OBB 
#endif
		FitMeshIntoBoundingBox();
		break;
	default: return;
	}
	*/
}

void CZEnemy::Update(void)
{
	/*
	double msUpdShip;

	if (Alive())					///If the Ship is alive
	{
		if (Timer[CSZ_UPD_PERIOD].IsSounding())
		{
			if (Explosion.Active())
				Explosion.Update();

#ifndef XM_CONTINUOUS_WITH_SIMULATE_TIME
			LastUpdTime = nowTime;

			//Next execution time calculation.
			double auxX = abs(AABB[CHAR_BBSIZE][XDIM].Value / Speed.v[XDIM]);
			double auxY = abs(AABB[CHAR_BBSIZE][YDIM].Value / Speed.v[YDIM]);
			double auxZ = abs(AABB[CHAR_BBSIZE][ZDIM].Value / Speed.v[ZDIM]);
			msUpdShip = sqrt((auxX * auxX) + (auxY * auxY));//+(auxZ*auxZ));
			if (msUpdShip > SIGLBD_MIN_UPDATETIME_OBJECTS) msUpdShip = SIGLBD_MIN_UPDATETIME_OBJECTS;
			Timer[CS_UPD_PERIOD].SetAlarm(Timer[CS_UPD_PERIOD].ms2Ticks(msUpdCShip));

			//Shooting calculation
			if ((floor((rand() % 100000 / (1 + Navy->ShootsFrequency)) / msUpdShip)) == 1) ///Has the Supply ship to fire?
			{
				Vector P, S;

				P.Set(Position.v[XDIM],
					Position.v[YDIM] - .3f,
					.05f);
				S.Set(0.0f,
					-CS_SHOOT_SPEED,
					0.0f);

				if (Navy->WithShoots)
					ShootsManager->NewShoot(CSH_SHIP, P, S);
			}

			OutEvent(CS_MOVING); //v 2->2

			if(((CSIGame*)Directory[CHARS_GAME_REF])->BouncingMode){
				for(unsigned int i=numId+1;i<Navy->Ship.size();i++)
					if(Navy->Ship[i]->Alive()) Collided(Navy->Ship[i]);
		}
#else

			//Next execution time calculation.
			double auxX = abs(AABB[CHAR_BBSIZE][XDIM].Value / Speed.v[XDIM]);
			double auxY = abs(AABB[CHAR_BBSIZE][YDIM].Value / Speed.v[YDIM]);
			msUpdShip = sqrt((auxX * auxX) + (auxY * auxY));
			if (msUpdShip > SIGLBD_MIN_UPDATETIME_OBJECTS) msUpdShip = SIGLBD_MIN_UPDATETIME_OBJECTS;
			Timer[CSZ_UPD_PERIOD].ResetAlarm();
#endif
		}
	}
	*/
}

void CZEnemy::DiscreteUpdate(void)
{
	/*
	double msUpdShip;

	if (Alive() && !EndByTime && !EndByFrame)					///If the Ship is alive
	{
#ifdef XM_UPD_TIME_DISC
		TimerManager.GetTimer(SIGLBT_UPDATE_TIMING)->InitCounting();
#endif	
	}

	msUpdShip = Timer[CSZ_UPD_PERIOD].GetAlarmPeriodms();

	PositionPrev = Position;

	UpdateCollisionDetection();

	double auxX = abs(AABB[CHAR_BBSIZE][XDIM].Value / Speed.v[XDIM]);
	double auxY = abs(AABB[CHAR_BBSIZE][YDIM].Value / Speed.v[YDIM]);
	//double auxZ= abs(AABB[CHAR_BBSIZE][ZDIM].Value/Speed.v[ZDIM]);

	msUpdShip = UGKALG_Min(auxX, auxY);

	if (msUpdShip > SIGLBD_MIN_UPDATETIME_OBJECTS) msUpdShip = SIGLBD_MIN_UPDATETIME_OBJECTS;
	Timer[CSZ_UPD_PERIOD].SetAlarm(Timer[CSZ_UPD_PERIOD].ms2Ticks(msUpdShip));

#ifdef XM_UPD_TIME_DISC
	TimerManager.EndAccCounting(SIGLBT_UPDATE_TIMING);
#endif
#ifdef DEF_RTD_TIME
	TimerManager.GetTimer(SIGLBT_RTDSKMM_TIMING].InitCounting();
#endif

	msgUpd = (cMsgNavy*)GetMsgToFill(UMSG_MSG_NAVY);
	msgUpd->Type = UMSG_MSG_NAVY;
	SendSelfMsg(msgUpd, Timer[CSZ_UPD_PERIOD].GetAlarmPeriod());

#ifdef DEF_RTD_TIME
	TimerManager.EndAccCounting(SIGLBT_RTDSKMM_TIMING);
#endif
	*/
}

void CZEnemy::ReceiveMessage(RTDESK_CMsg *pMsg){
	/*
	switch (pMsg->Type)
	{
	case UMSG_MSG_NAVY:
		cMsgNavy* auxMsg;
		auxMsg = (cMsgNavy*)pMsg;
		switch (auxMsg->SubType)
		{
		case UMSG_UPDSHIPS:

			DiscreteUpdate();

			break;
		case UMSG_TURNALL:
			break;
		}
		break;
	case UMSG_MSG_BASIC_TYPE:

		DiscreteUpdate();

		break;
	}
	*/
}
