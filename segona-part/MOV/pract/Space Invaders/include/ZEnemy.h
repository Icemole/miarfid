/**	Definition of the class ZEnemy

	Prefix: CSZ_
	@author Nahuel U. Rosell� Beneitez
	@version 2020-11-28
*/


#ifndef CSZ_ZENEMY
#define CSZ_ZENEMY

#include <ExplosiveChar.h>
#include <Textures\Texture.h>
#include <SIMessage.h>

#define CSZ_MAX_HEALTH			 25	//Maximun amount of health of a given Ship by default
#define CSZ_MAX_SHIELD			 15 //Maximum amount of shield
#define CSZ_HEALTH_REGEN		  1 //Health regeneration per second
#define CSZ_SHIELD_REGEN		  5 //Shield regeneration per second
#define CSZ_FUEL				 50	//Amount of fuel the ship has
#define CSZ_WEIGHT				 10	//Weight of the ship
#define CSZ_MAX_MAX_HIT_DUR		500	//Maximun amount of time when a given Ship is hit
#define CSZ_MAX_EXPLOSION_LIFE 10000	//Time in ms the ship may be exploding

/** \typedef CSZ_EXTRA_TIMERS
*	Types of different local timing managed by any ship
*/

typedef enum {
	CSZ_UPD_PERIOD,
	CSZ_RND_PERIOD,
	CSZ_MAX_TIMERS
} CSZ_EXTRA_TIMERS;

/*
typedef struct {
	int r1;
	int g1;
	int b1;
	int a1;
} C3DS_Color4f1;

typedef enum {
	CSZ_UNBORN=0,	///For management purpouses only
	CSZ_BORN,		///The character is just born but it is not still operative (living)
	CSZ_LIVING,		///Everything this character may do while it is alive 
	CSZ_BURSTING,	///The character has been touched so many times that its life has gone negative. So it has to burst before being died. Starts an explosion and dies
	CSZ_DEAD,		///The character is no operative. Reached after dying
	CSZ_MAXSTATE		///For management purpouses only
} CSZ_ZENEMY_STATE;

typedef enum {
	CSZ_DEFAULT=0,		///For management purpouses only
	CSZ_BORNING,			///The character is going to be born but it is not still operative (living)
	CSZ_GROWING,			///Change to LIVING State
	CSZ_MOVING,			///Any time this character is moving while it is alive 
	CSZ_DISPLAYING,		///Renders the ship on the screen
	CSZ_BURST,			///The character has been touched so many times that its life has gone negative. So it has to burst before being died. Starts an explosion and dies
	CSZ_REBORN,
	CSZ_DIE,				///The ship has got bursted and now has to be dead
	CSZ_MAXTRANSITION	///For management purpouses only
} CSZ_ZENEMY_TRANSITION;
*/

class CZEnemy: public CExplosiveChar
{
	//Atributes
public:
	float			HealthRegen;	// Health regeneration per second
	float			Shield;			// Amount of damage which the ship can take before taking damage as health
	float			ShieldRegen;	// Shield regeneration per second
	float			Fuel;			// Amount of fuel the ship has
	float			Weight;			// How much the ship weighs
	
	/*
	float			zi;				// The ship floats (coord z in 3D mode)
	float			zi_speed;		// The ship floats (speed)
	float			my_rand;		// random number
	float Radius;

	C3DS_Color4f1 	Color3D;		//For 3D mode
	
	//For united ships
	bool faseActual;
	*/

	//RT-DESK
	double timeRTdeskMsg;						//Tiempo Mensaje RTDESK
	RTDESK_CMsg *msgUpd;						//RTDESK Message Time

	//Methods
	
	//Constructor / Initializer
	void Init ();
	
	CZEnemy ()
	{
		Mesh = NULL;

#ifdef CHAR_USE_AABB
		InitializeAABB();
#endif
		Init();
	}	// Constructor of the class

	~CZEnemy();

	//Physics
	void Collided			(CCharacter *CollidedChar);

	///Shows the ship on the screen
	void Render				(void);
	///Change the way the ship is going to be rendered on the screen
	void ChangeRenderMode	(CHAR_RENDER_MODE);

	void Render3D			();
	void DisplayHit			();

	void Update				(void);
	void DiscreteUpdate		(void);

	void ReceiveMessage		(RTDESK_CMsg *pMsg);
};

/*
void *Zinit_ship		(LPSTR *args, CZEnemy *Ship);
void *CSZ_Move		(LPSTR *args, CZEnemy *Ship);
void *CSZ_Bursting	(LPSTR *args, CZEnemy *Ship);
void *CSZ_Kill		(LPSTR *args, CZEnemy *Ship);
void *CSZ_Display	(LPSTR *args, CZEnemy *Ship);
*/

#endif