/*Cabecera que incluye todo lo necesario para poder enlazar con el 
  LiteHTMLReader
*/

#include "..\LiteHTMLReader.h"

class CManejadorEventos : public ILiteHTMLReaderEvents
{
private:
    void BeginParse(DWORD dwAppData, bool &bAbort);
    void StartTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void EndTag(CLiteHTMLTag *pTag, DWORD dwAppData, bool &bAbort);
    void Characters(const CString &rText, DWORD dwAppData, bool &bAbort);
    void Comment(const CString &rComment, DWORD dwAppData, bool &bAbort);
    void EndParse(DWORD dwAppData, bool bIsAborted);
};