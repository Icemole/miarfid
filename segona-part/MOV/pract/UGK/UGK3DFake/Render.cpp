#include "Render.h"

#include <osg/Material>
#include <osg/StateSet>
#include <osgDB/ReadFile>
#include <osgSim/Impostor>

#include <iostream>

Render::Render()
{
}


Render::~Render()
{
}

osg::Node* Render::LoadSceneGraph(std::string str, osg::Texture2D* tex, int dim) {

	//Read 1 scene graph
	osg::Node* grafo = osgDB::readNodeFile(str);
	if (tex != NULL) {
		Render::ApplyTexture(grafo,tex);
	}

	// If we want the model in 3D:
	if (dim == 0) {
		std::cout << "3D Mode" << std::endl;
	}
	// If we want the impostor (2D):
	else if (dim == 1) {
		std::cout << "2D Mode" << std::endl;
		osgSim::Impostor* imp = new osgSim::Impostor();
		imp->setImpostorThreshold(0.0);
		imp->addChild(grafo);
		imp->setRange(0, 0.0, 1e7f);
		imp->setCenter(grafo->getBound().center());
		grafo = imp;
	}

	return grafo;

	// Read multiple scene graphs (not running atm)
	/*
	int count = NUMERO DE FICHEROS;
	char **ficheros = new char*[NUMERO DE FICHEROS];
	// Hacer bucle de lectura
	ficheros[0] = "cow.osg";
	ficheros[1] = "cloud/Clouddiss.obj";

	//Debug check read files
	std::cout << "Ficheros" << ficheros << std::endl;

	//osgDB::readNodeFiles()
	osg::ArgumentParser arguments(&count, ficheros);
	osg::Node* root = osgDB::readNodeFiles(arguments);
	return root;
	*/

	//viewer.setSceneData(osgDB::readNodeFile("cow.osg"));

}

void Render::LoadSceneData(osg::Node* n, osgViewer::Viewer* v) {
	v->setSceneData(n);
}

osg::PositionAttitudeTransform* Render::TranslateModel(osg::Node* n, osg::Vec3 vec) {
	osg::PositionAttitudeTransform* trans = new osg::PositionAttitudeTransform();
	trans->setPosition(vec);
	trans->addChild(n);
	return trans;
}

osg::Texture2D* Render::LoadTexture(std::string str) {

	osg::Texture2D* texture = new osg::Texture2D;
	osg::Image* image = osgDB::readImageFile(str);
	if (!image) {
		std::cout << "Problem loading image " << std::endl;
	}
	texture->setImage(image);

	texture->setTextureHeight(100);
	texture->setTextureWidth(100);

	texture->setWrap(osg::Texture2D::WrapParameter::WRAP_S, osg::Texture2D::WrapMode::MIRROR);
	texture->setWrap(osg::Texture2D::WrapParameter::WRAP_R, osg::Texture2D::WrapMode::MIRROR);
	texture->setWrap(osg::Texture2D::WrapParameter::WRAP_T, osg::Texture2D::WrapMode::MIRROR);

	texture->setFilter(osg::Texture2D::FilterParameter::MIN_FILTER, osg::Texture2D::FilterMode::NEAREST);
	texture->setFilter(osg::Texture2D::FilterParameter::MAG_FILTER, osg::Texture2D::FilterMode::NEAREST);

	return texture;
}

void Render::ApplyTexture(osg::Node* n, osg::Texture2D* texture) {

	osg::Material* material = new osg::Material;
	osg::StateSet* stateset = n->getOrCreateStateSet();

	stateset->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
	
	stateset->setTextureAttribute(0, texture, osg::StateAttribute::OVERRIDE);
	stateset->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	stateset->setTextureMode(0, GL_TEXTURE_GEN_S, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	stateset->setTextureMode(0, GL_TEXTURE_GEN_T, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	//stateset->setAttribute(material, osg::StateAttribute::OVERRIDE);
	
}