#pragma once

//#define UGKSG_OGRE	
//#define UGKSG_UDK
//#define UGKSG_CRY
//#define UGKSG_IRL
//#define UGKSG_TORQUE
#define UGKSG_OSG

#ifdef UGKSG_OGRE
#elif defined (UGKSG_UDK)
#elif defined (UGKSG_CRY)
#elif defined (UGKSG_IRL)
#elif defined (UGKSG_TORQUE)
#elif defined (UGKSG_OSG)

#include <osgViewer/Viewer>

#include <osg/PositionAttitudeTransform>

class Render
{
public:
	Render();
	~Render();
	
	// Function to load files, use string of file to be read, and integer: 0 to indicate 3D and 1 to indicate 2D
	static osg::Node* LoadSceneGraph(std::string str, osg::Texture2D* tex, int dim);
	// Load a node (n) into the data scene of the viewer v
	static void LoadSceneData(osg::Node* n, osgViewer::Viewer* v);
	// Translate the node n to the Vector3 vec 
	static osg::PositionAttitudeTransform* TranslateModel(osg::Node* n, osg::Vec3 vec);
	// Load texture
	static osg::Texture2D* LoadTexture(std::string str);
	// Apply texture
	static void ApplyTexture(osg::Node* n, osg::Texture2D* tex);
};

#endif

