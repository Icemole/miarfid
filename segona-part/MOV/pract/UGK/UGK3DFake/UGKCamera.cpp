#include "UGKCamera.h"

#include <iostream>

UGKCamera::UGKCamera()
{
}


UGKCamera::~UGKCamera()
{
}

osg::Camera* UGKCamera::getCamera(osgViewer::Viewer* v) {
	return v->getCamera();
}

void UGKCamera::setPerspectiveProj(osg::Camera* c, double fov, double aspectRatio, double near, double far) {
	c->setProjectionMatrixAsPerspective(fov, aspectRatio, near, far);
}

void UGKCamera::setOrthoProj(osg::Camera* c, double left, double right, double bottom, double top, double near, double far) {
	c->setProjectionMatrixAsOrtho(left, right, bottom, top, near, far);
}

void UGKCamera::SetupCamera(osg::Camera* c, osgViewer::Viewer* v) {

	//Load camera parameters
	osg::Vec3 eye, center, up;
	c->getViewMatrixAsLookAt(eye, center, up);
	//std::cout << "Eye: " << eye.v[XDIM] << "," << &eye.ptr.v[YDIM] << "," << &eye.ptr.v[ZDIM] << std::endl;
	//std::cout << "Center: " << &center.v[XDIM] << "," << &center.v[YDIM] << "," << &center.v[ZDIM] << std::endl;
	//std::cout << "Up: " << &up.v[XDIM] << "," << &up.v[YDIM] << "," << &up.v[ZDIM] << std::endl;

	v->setCameraManipulator(new osgGA::FirstPersonManipulator());

}


