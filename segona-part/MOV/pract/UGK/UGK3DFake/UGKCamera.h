#pragma once

#include <osg/Camera>
#include <osgViewer/Viewer>
#include <osgGA/FirstPersonManipulator>

class UGKCamera
{
public:
	UGKCamera();
	~UGKCamera();

	static osg::Camera* getCamera(osgViewer::Viewer* v);
	static void setPerspectiveProj(osg::Camera* c, double fov, double aspectRatio, double near, double far);
	static void setOrthoProj(osg::Camera* c, double left, double right, double bottom, double top, double near, double far);
	static void SetupCamera(osg::Camera* c, osgViewer::Viewer* v);
};

