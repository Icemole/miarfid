#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgDB/ReadFile>
#include <osgSim/Impostor>
#include <osg/Texture2D>

#include <osgGA/FirstPersonManipulator>

#include <osg/PositionAttitudeTransform>

#include "Render.h"
#include "UGKCamera.h"

#include <iostream>

int main(int argc, char **argv) {

	const osgGA::GUIEventAdapter* ea = new osgGA::GUIEventAdapter();

	// Initialize variables
	osgViewer::Viewer* viewer = new osgViewer::Viewer();
	osg::Group* root = new osg::Group();
	osg::Camera* cam = UGKCamera::getCamera(viewer);

	//Setup Camera
	UGKCamera::SetupCamera(cam, viewer);

	// Load textures
	osg::Texture2D* texBloc = Render::LoadTexture("modelos/basic_block/basic_block.png");
	osg::Texture2D* texTub = Render::LoadTexture("modelos/tuberia/tuberia.png");
	
	// Load our 3D model and apply textures
	osg::Node* bloque = Render::LoadSceneGraph("modelos/basic_block/BasicBlock.obj", texBloc, 0);
	osg::Node* caparazon = Render::LoadSceneGraph("modelos/caparazon/KoopaShell.obj", NULL, 0);
	osg::Node* goomba = Render::LoadSceneGraph("modelos/goomba/Goomba.obj", NULL, 0);
	osg::Node* tuberia = Render::LoadSceneGraph("modelos/tuberia/tuberia.obj", texTub, 0);
	osg::Node* mushroom = Render::LoadSceneGraph("modelos/mushroom/Mushrooms.obj", NULL, 0);
	osg::Node* bloqueInt = Render::LoadSceneGraph("modelos/quest_block/quest_block.obj", NULL, 0);
	osg::Node* skybox = Render::LoadSceneGraph("modelos/skybox/skyboxFunciona.obj", NULL, 0);

	// Convert it to an impostor aplying textures
	osg::Node* impostorBloque = Render::LoadSceneGraph("modelos/basic_block/BasicBlock.obj", texBloc, 1);
	osg::Node* impostorCaparazon = Render::LoadSceneGraph("modelos/caparazon/KoopaShell.obj", NULL, 1);
	osg::Node* impostorGoomba = Render::LoadSceneGraph("modelos/goomba/Goomba.obj", NULL, 1);
	osg::Node* impostorTuberia = Render::LoadSceneGraph("modelos/tuberia/tuberia.obj", texTub, 1);;
	osg::Node* impostorMushroom = Render::LoadSceneGraph("modelos/mushroom/Mushrooms.obj", NULL, 1);
	osg::Node* impostorBloqueInt = Render::LoadSceneGraph("modelos/quest_block/quest_block.obj", NULL, 1);

	/*
	// Illumination
	osg::StateSet* lightState = root->getOrCreateStateSet();
	lightState->setMode(GL_LIGHTING, osg::StateAttribute::ON);
	lightState->setMode(GL_LIGHT0, osg::StateAttribute::ON);

	//Create light
	osg::Light* light0 = new osg::Light;
	light0->setPosition(osg::Vec4(-1, 0, 0.5, 1));
	light0->setDirection(osg::Vec3(0, 1, 0));
	light0->setSpotCutoff(15);

	osg::LightSource* source0 = new osg::LightSource;
	source0->setLight(light0);
	root->addChild(source0);
	*/

	//Translate and copy our impostor
	osg::PositionAttitudeTransform* transImp2 = Render::TranslateModel(impostorBloque, osg::Vec3(100, 0, 0));
	osg::PositionAttitudeTransform* transImp3 = Render::TranslateModel(impostorBloque, osg::Vec3(200, 0, 0));
	osg::PositionAttitudeTransform* transImp4 = Render::TranslateModel(impostorBloque, osg::Vec3(300, 0, 0));
	osg::PositionAttitudeTransform* transImpMush = Render::TranslateModel(impostorMushroom, osg::Vec3(0, 0, 250));
	osg::PositionAttitudeTransform* transImp5 = Render::TranslateModel(impostorBloque, osg::Vec3(400, 0, 0));
	osg::PositionAttitudeTransform* transImp6 = Render::TranslateModel(impostorBloque, osg::Vec3(500, 0, 0));
	osg::PositionAttitudeTransform* transImpCap = Render::TranslateModel(impostorCaparazon, osg::Vec3(200, -400, -400));
	osg::PositionAttitudeTransform* transImpGoom = Render::TranslateModel(impostorGoomba, osg::Vec3(50, 0, -400));
	osg::PositionAttitudeTransform* transImpGoom2 = Render::TranslateModel(impostorGoomba, osg::Vec3(400, 0, -400));
	osg::PositionAttitudeTransform* transImpTub = Render::TranslateModel(impostorTuberia, osg::Vec3(200, 0, -400));
	root->addChild(impostorBloque);
	root->addChild(transImp2);
	root->addChild(transImp3);
	root->addChild(transImp4);
	root->addChild(transImp5);
	root->addChild(transImp6);
	root->addChild(transImpCap);
	root->addChild(transImpGoom);
	root->addChild(transImpGoom2);
	root->addChild(transImpTub);
	root->addChild(transImpMush);
	

	// Translate and copy our 3D model
	osg::PositionAttitudeTransform* trans = Render::TranslateModel(bloque, osg::Vec3(1000, 0, 0));
	osg::PositionAttitudeTransform* trans2 = Render::TranslateModel(bloque, osg::Vec3(1100, 0, 0));
	osg::PositionAttitudeTransform* trans3 = Render::TranslateModel(bloque, osg::Vec3(1200, 0, 0));
	osg::PositionAttitudeTransform* transMush = Render::TranslateModel(mushroom, osg::Vec3(1000, 0, 250));
	osg::PositionAttitudeTransform* transCap = Render::TranslateModel(caparazon, osg::Vec3(1250, -400, -400));
	osg::PositionAttitudeTransform* transGoom = Render::TranslateModel(goomba, osg::Vec3(1050, 0, -400));
	osg::PositionAttitudeTransform* transGoom2 = Render::TranslateModel(goomba, osg::Vec3(1400, 0, -400));
	osg::PositionAttitudeTransform* trans4 = Render::TranslateModel(bloque, osg::Vec3(1300, 0, 0));
	osg::PositionAttitudeTransform* trans5 = Render::TranslateModel(bloque, osg::Vec3(1400, 0, 0));
	osg::PositionAttitudeTransform* trans6 = Render::TranslateModel(bloque, osg::Vec3(1500, 0, 0));
	osg::PositionAttitudeTransform* transTub = Render::TranslateModel(tuberia, osg::Vec3(1200, 0, -400));
	osg::PositionAttitudeTransform* transSkyb = Render::TranslateModel(skybox, osg::Vec3(0, -1800, -400));
	transSkyb->setScale(osg::Vec3(2, 1, 0.5));
	root->addChild(trans);
	root->addChild(trans2);
	root->addChild(trans3);
	root->addChild(trans4);
	root->addChild(trans5);
	root->addChild(trans6);
	root->addChild(transCap);
	root->addChild(transGoom);
	root->addChild(transGoom2);
	root->addChild(transTub);
	root->addChild(transMush);

	//Skybox
	root->addChild(transSkyb);

	// Impostor try
	//osgSim::ImpostorSprite* sprite;
	//sprite = impostor->findBestImpostorSprite(0, eye);
	//imp->getImpostorSpriteList(0);
	//root->addChild(sprite);

	// Image try
	//osg::Image* im = osgDB::readImageFile("");
	//osgDB::writeImageFile(*im, "prueba.png");
	
	// Load scene data
	Render::LoadSceneData(root, viewer);

	// Select camera projection mode
	UGKCamera::setPerspectiveProj(cam, 40.0, 1.0, 0.01, 500.0);
	//UGKCamera::setOrthoProj(cam, -10, 10, -10, 10, 1, 100);

	//Stats handler
	osgViewer::StatsHandler* statHandler = new osgViewer::StatsHandler;
	viewer->addEventHandler(statHandler);

	viewer->realize();
	while (!viewer->done()) {
		//viewer.addEventHandler(new osgGA::GUIEventHandler());
		viewer->frame();
		//return viewer->run();
	}
	
	//return viewer.run();

	return 0;

}
