# Sudoku solver by means of a genetic algorithm.
# It is assumed that an individual is a 81-dimensional array with numbers from 1 to 9.
# The array will represent the sudoku board from left to right, top to bottom.

"""
BASIC SCHEMA:
- Selection: elitist.
- Crossover: 9 points (rows).
- Mutation: uniform.

DETAILED APPROACHES TAKEN, SORTED IN CHRONOLOGICAL ORDER:
- Crossover:
    FIRST APPROACH: cross in 9 points (uniform).
    Usually reaches ~200 when left running for a long time
    (mutation_prob = 0.1)
    SECOND APPROACH: cross in 9 points (random).
    Usually reaches ~200 when left running for a long time
    (mutation_prob = 0.1)
    THIRD APPROACH: uniform cross.
    Tends to work better than cross in 9 points: fitness ~220
    when left running for a long time (mutation_prob = 0.1)

- Population replacement:
    FIRST APPROACH: no approach. Reaches quite good fitness ~220.
    SECOND APPROACH: final jury. Reaches better fitness (~230)
    when left for quite a long time.

- Selection:
    FIRST APPROACH: elitist selection: pairs the best parents,
    the second best parents, etc. Works great
    (~200 to ~235 in average), but gets stuck towards the end.
    SECOND APPROACH: contest selection:

"""

import heapq
# Since heapq creates min-heaps, the fitness is stored in negative values.

import random

import cProfile, pstats
from io import StringIO
pr = cProfile.Profile()
pr.enable()

import signal

def signal_handler(signum, frame):
    # Prints to a file the best sudoku obtained after the user
    # has stopped the program through a SIGINT signal.
    signal.signal(signum, signal.SIG_IGN)  # Ignore additional signals.
    pr.disable()
    s = StringIO()
    ps = pstats.Stats(pr, stream = s).sort_stats("cumulative")
    ps.print_stats()
    print(s.getvalue())
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# Parent selection by contest:
# num_contests groups of indiv_in_contest individuals each.
num_contests = 40
indiv_in_contest = 2
max_boards_per_gen = num_contests * indiv_in_contest
max_generations_without_change = 10000
max_fitness = 81 * 3


mutation_prob = 0.05
crossover_prob = 0.9
copy_parent_prob = 0.1
seed_population = {0: 1, 1: 2, 2: 3, 9: 4, 10: 5, 11: 6, 18: 7, 19: 8, 20: 9, 60: 1, 61: 2, 62: 3, 69: 4, 70: 5, 71: 6, 78: 7, 79: 8, 80: 9}

def encode(individual):
    return individual

def crossover(individual1, individual2):
    # The crossover is defined as the grouping of the first row
    # of individual1 with the second row of individual2 with the
    # third row of individual1...
    child = []
    successor_method = random.random()
    previous_probability = 0
    if successor_method < copy_parent_prob:
        # Create a new sudoku as a descendent
        child = generate_random_sudoku()
    else:
        # Real crossover

        # CROSS IN 9 POINTS (UNIFORM).
        #
        # for i in range(4):
        #     child += individual1[9 * i : 9 * (i + 1)]
        #     child += individual2[9 * (i + 1) : 9 * (i + 2)]
        # child += individual1[72 : 81]

        # CROSS IN 9 POINTS (RANDOM).
        #
        # random_permutation = numpy.random.permutation([0, 1, 2, 3, 4, 5, 6, 7, 8])
        # counter = 0
        # for i in random_permutation:
        #     if counter % 2 == 0:
        #         child += individual1[9 * i : 9 * (i + 1)]
        #     else:
        #         child += individual2[9 * i : 9 * (i + 1)]
        #     counter += 1
        #     if counter == 8:
        #         break
        # child += individual1[9 * random_permutation[-1] : 9 * (random_permutation[-1] + 1)]
        
        # UNIFORM CROSS
        #
        for i in range(81):
            if random.random() < 0.5:
                child.append(individual1[i])
            else:
                child.append(individual2[i])

    return child


def mutate(individual, location):
    if location not in seed_population:
        non_mutated_value = individual[location]
        while individual[location] == non_mutated_value:
            individual[location] = random.randint(1, 9)
    
    return individual


def decode(individual):
    return individual


def generate_random_sudoku():
    # Generates a random sudoku with no restrictions applied to it.
    board = []
    for i in range(81):
        board.append(random.randint(1, 9))
    
    return board


def evaluate(individual):
    # The measure of evaluation is the number of different numbers
    # which are different on every row, column, and 3x3 square.
    # This makes for a maximum fitness of 81 * 3 = 243.
    fitness = 0

    # Keep track of the numbers at every row, column and 3x3 square.
    rows = {}
    cols = {}
    three_sq = {}

    # Analyze every row.
    for r in range(81):
        aux_list = rows.get(int(r / 9), [])
        aux_list.append(individual[r])
        rows[int(r / 9)] = aux_list
    
    # Analyze every column.
    for c in range(81):
        aux_list = cols.get(c % 9, [])
        aux_list.append(individual[c])
        cols[c % 9] = aux_list
    
    # Analyze every 3x3 square.
    for three in range(81):
        # General formula to access the 3x3 square:
        # int(current_row / 3) * 3 + int(current_col / 3)
        current_row = int(three / 9)
        current_col = three % 9
        aux_list = three_sq.get(int(current_row / 3) * 3 + int(current_col / 3), [])
        aux_list.append(individual[three])
        three_sq[int(current_row / 3) * 3 + int(current_col / 3)] = aux_list
    
    # Calculate the fitness
    for i in range(9):
        fitness += len(set(rows[i]))
        fitness += len(set(cols[i]))
        fitness += len(set(three_sq[i]))
    
    return fitness


def get_real_fitness(fitness):
    return -fitness


def initialize_population(num_boards, seed_pop = {}):
    # Seed population: dictionary with some of the values already set.
    # For instance, seed_population[0] = 3
    # means that the very first cell of the sudoku will always have a 3.
    boards = []
    for i in range(num_boards):
        # Generate n different boards.
        board = generate_random_sudoku()
        if seed_population != {}:
            for location, number in seed_population.items():
                board[location] = number
        heapq.heappush(boards, (get_real_fitness(evaluate(board)), board))
    
    return boards


def print_sudoku_board(sudoku_board):
    print("|", end = "")
    for _ in range(0, 23):
        print("-", end = "")
    print("|")
    for multiple_row in range(0, 3):
        for i in range(3 * multiple_row, 3 * multiple_row + 3):
            print("|", end = " ")
            for multiple_column in range(0, 3):
                for j in range(3 * multiple_column, 3 * multiple_column + 3):
                    print(int(sudoku_board[i * 9 + j]), end = " ")
                if multiple_column != 2:
                    print("|", end = " ")
            print("|")
        print("|", end = "")
        for _ in range(0, 23):
            print("-", end = "")
        print("|")
    print()


def compute_associated_probabilities(best_parents_fitness):
    # Gives a list (where the order matters) which contains
    # the probabilities that the individual i is chosen
    # according to the fitness i
    #
    # RANKING SELECTION
    #
    probabilities = []

    total_fitness = sum(best_parents_fitness)
    max_parameter = 1.1
    min_parameter = 2 - max_parameter
    for i in range(len(best_parents_fitness)):
        h = (max_parameter - min_parameter) * i / (len(best_parents_fitness) - 1)
        h = max_parameter - h
        probabilities.append(h / len(best_parents_fitness))
    
    return probabilities


def get_two_parents(parents, parents_fitness, crossover_probabilities):
    # Chooses two parents at random by using the roulette method.
    parent1 = []
    parent2 = []
    index_parent1 = 0
    index_parent2 = 0
    
    while index_parent1 == index_parent2:
        accum_prob = 0
        threshold_prob = random.random()
        # Pick the first parent
        for i in range(len(crossover_probabilities)):
            accum_prob += crossover_probabilities[i]
            if accum_prob > threshold_prob:
                parent1 = parents[i]
                fitness_parent1 = parents_fitness[i]
                index_parent1 = i
                break
        accum_prob = 0
        threshold_prob = random.random()
        # Pick the second parent
        for i in range(len(crossover_probabilities)):
            accum_prob += crossover_probabilities[i]
            if accum_prob > threshold_prob:
                parent2 = parents[i]
                fitness_parent2 = parents_fitness[i]
                index_parent2 = i
                break
        
    return (parent1, get_real_fitness(fitness_parent1), parent2, get_real_fitness(fitness_parent2))


def main():
    # General procedure of genetic algorithms:
    # Adapted from John McCall. Genetic algorithms for modelling and optimisation.

    # (1) Randomly generate an initial source population of P chromosomes.
    # (2) Calculate the fitness, F(c), of each chromosome c in the source population.
    boards = initialize_population(max_boards_per_gen, seed_population)
    
    # (3) Create an empty successor population and then repeat the following steps
    # until P chromosomes have been created.
    fitness_parent1 = 0
    num_generations_without_change = 0
    previous_best_fitness = 0
    while get_real_fitness(fitness_parent1) < max_fitness:
        successors = []
        contest_winners = []  # For selection by contest.
        iteration = 0
        """
        ELITIST SELECTION
        """
        # while len(boards) > 0:
        #     # (a) Select two chromosomes, c1 and c2
        #     fitness_parent1, parent1 = heapq.heappop(boards)
        #     fitness_parent2, parent2 = heapq.heappop(boards)

        #     # (a') from the source population.

        #     # print_sudoku_board(parent1)
        #     # print_sudoku_board(parent2)
        #     # print(fitness_parent1, fitness_parent2)

        #     # (b) Apply crossover to c1 and c2 with crossover rate pc to obtain a child chromosome c.
        #     child1 = crossover(parent1, parent2)
        #     child2 = crossover(parent2, parent1)

        #     # (c) Apply uniform mutation to c with mutation rate pm to produce c′.
        #     for i in range(81):
        #         if random.random() < mutation_prob:
        #             mutate(child1, i)
        #     for i in range(81):
        #         if random.random() < mutation_prob:
        #             mutate(child2, i)
        
        #     # (d) Add c′to the successor population.
        #     fitness_child1 = get_real_fitness(evaluate(child1))
        #     heapq.heappush(successors, (fitness_child1, child1))
        #     # if fitness_child1 < fitness_parent1:
        #     #     # Child is better than parent
        #     #     heapq.heappush(successors, (fitness_child1, child1))
        #     #     if iteration == 0:
        #     #         num_generations_without_change = 0
        #     # else:
        #     #     # Parent is better than child
        #     #     heapq.heappush(successors, (fitness_parent1, parent1))
        #     #     if iteration == 0:
        #     #         num_generations_without_change += 1
            
        #     fitness_child2 = get_real_fitness(evaluate(child2))
        #     heapq.heappush(successors, (fitness_child2, child2))
        #     # if fitness_child2 < fitness_parent2:
        #     #     # Child is better than parent.
        #     #     heapq.heappush(successors, (fitness_child2, child2))
        #     # else:
        #     #     # Parent is better than child.
        #     #     heapq.heappush(successors, (fitness_parent2, parent2))

        #     if iteration == 0 and previous_best_fitness != fitness_parent1:
        #         print_sudoku_board(parent1)
        #         print(get_real_fitness(fitness_parent1))
        #         previous_best_fitness = fitness_parent1
        #     iteration += 1

        """
        CONTEST + ROULETTE SELECTION
        """
        for i in range(num_contests):
            contest_participants = []
            # (a) Select two chromosomes, c1 and c2
            for j in range(indiv_in_contest):
                fitness_parent, parent = heapq.heappop(boards)
                heapq.heappush(contest_participants, (fitness_parent, parent))
            fitness_parent_winner, parent_winner = heapq.heappop(contest_participants)
            heapq.heappush(contest_winners, (fitness_parent_winner, parent_winner))
            contest_participants = []

        # (a') from the source population.
        # Note: at this point, we have all the winners in contest_winners.
        # This list (priority queue) is of length num_contests.
        best_parents = []
        best_parents_fitness = []
        best_parent = None
        best_parent_fitness = None
        for i in range(num_contests):
            fitness_parent_winner, parent_winner = heapq.heappop(contest_winners)
            best_parents.append(parent_winner)
            best_parents_fitness.append(get_real_fitness(fitness_parent_winner))
        
        current_winners_without_pair = num_contests
        parents_probabilities = compute_associated_probabilities(best_parents_fitness)

        while(current_winners_without_pair) > 0:
            # Choose at random two parents through wheel selection
            parent1, fitness_parent1, parent2, fitness_parent2 = get_two_parents(best_parents, best_parents_fitness, parents_probabilities)
            if best_parent is None:
                best_parent = parent1
                best_parent_fitness = fitness_parent1
            # All of the parameters are modified inside the method!
            current_winners_without_pair -= 2

            # (b) Apply crossover to c1 and c2 with crossover rate pc to obtain a child chromosome c.
            child1 = crossover(parent1, parent2)
            child2 = crossover(parent2, parent1)

            # (c) Apply uniform mutation to c with mutation rate pm to produce c′.
            for i in range(81):
                if random.random() < mutation_prob:
                    mutate(child1, i)
            for i in range(81):
                if random.random() < mutation_prob:
                    mutate(child2, i)
        
            # (d) Add c′to the successor population.
            fitness_child1 = get_real_fitness(evaluate(child1))
            # heapq.heappush(successors, (fitness_child1, child1))
            if get_real_fitness(fitness_child1) > get_real_fitness(fitness_parent1):
                # Child is better than parent
                heapq.heappush(successors, (fitness_child1, child1))
                if iteration == 0:
                    num_generations_without_change = 0
            else:
                # Parent is better than child
                heapq.heappush(successors, (fitness_parent1, parent1))
                if iteration == 0:
                    num_generations_without_change += 1
            
            if iteration == 0 and previous_best_fitness != best_parent_fitness:
                print_sudoku_board(best_parent)
                print(get_real_fitness(best_parent_fitness))
                previous_best_fitness = best_parent_fitness
            iteration += 1
            
            fitness_child2 = get_real_fitness(evaluate(child2))
            if len(successors) < max_boards_per_gen:
                # heapq.heappush(successors, (fitness_child2, child2))
                if get_real_fitness(fitness_child2) > get_real_fitness(fitness_parent2):
                    # Child is better than parent.
                    heapq.heappush(successors, (fitness_child2, child2))
                else:
                    # Parent is better than child.
                    heapq.heappush(successors, (fitness_parent2, parent2))
            
            num_children = len(successors)
            for i in range(max_boards_per_gen - num_children):
                child = generate_random_sudoku()
                heapq.heappush(successors, (get_real_fitness(evaluate(child)), child))

        # (4) Replace the source population with the successor population.
        
        # NO APPROACH
        #
        # boards = successors.copy()
        # successors = []
        
        # FINAL JURY
        #
        if num_generations_without_change == max_generations_without_change:
            print("Final jury done.")
            best_fitness, best_individual = heapq.heappop(successors)
            successors = []
            boards = initialize_population(max_boards_per_gen - 1, seed_population)
            heapq.heappush(boards, (best_fitness, best_individual))
            num_generations_without_change = 0
            previous_best_fitness = 0
        else:
            boards = successors.copy()
        successors = []
    
    print_sudoku_board(heapq.heappop(boards)[1])

        
    
    # (5) If stopping criteria have not been met, return to Step 2.


if __name__ == "__main__":
    main()