# Sudoku solver by means of a genetic algorithm.
# It is assumed that an individual is a 81-dimensional array with numbers from 1 to 9.
# The array will represent the sudoku board from left to right, top to bottom.

import heapq

import random

import sys

# Fancy stuff for printing.
import click

# For writing into disk "f(time) = fitness".
import time

# For catching control + c and writing the best sudoku to disk.
import signal

import cProfile, pstats
from io import StringIO
pr = cProfile.Profile()
pr.enable()

verbose = "-v" in sys.argv
solution_id = 0
previous_best_board = []


# selection_method = {"elitista", "torneo"}
selection_method = "elitista"

# crossover_method = {"nueve_puntos_uniforme", "nueve_puntos_aleatorio", "uniforme"}
crossover_method = "uniforme"

# mutation_method = {"muchas", "una", "intercambio", "intercambiar_en_fila"}
mutation_method = "intercambiar_en_fila"

# substitution_method = {"nada", "reemplazar_el_peor_por_el_mejor", "reemplazar_peor_por_mejor"}
substitution_method = "reemplazar_el_peor_por_el_mejor"

# replacement_method = {"nada", "juicio_final"}
replacement_method = "juicio_final"

mutation_prob = 0.2
crossover_prob = 1
# Sudoku difficulty: normal
seed_population = {8: 9, 10: 9, 16: 3, 18: 5, 19: 1, 20: 3, 21: 9, 29: 6, 31: 7, 33: 4, 34: 1, 35: 5, 37: 4, 39: 5, 40: 6, 47: 8, 51: 7, 52: 6, 58: 3, 59: 2, 60: 1, 66: 7, 70: 9, 71: 6, 72: 7, 73: 3, 75: 1, 77: 6, 78: 2, 80: 4}


# Parent selection by torneo:
# num_contests groups of indiv_in_contest individuals each.
num_contests = 20
indiv_in_contest = 2
max_boards_per_gen = num_contests * indiv_in_contest
max_generations_without_change = 1000
reward_factor = 1
max_fitness = 81 * 3 * reward_factor

legal_row_moves = {}
illegal_row_moves = set([i for i in seed_population])
for i in range(9):
    valid_cells = set()
    for valid_cell in range(9 * i, 9 * (i + 1)):
        valid_cells.add(valid_cell)
    valid_cells -= illegal_row_moves
    legal_row_moves[i] = valid_cells


def encode(individual):
    return individual

def crossover(individual1, individual2):
    child = []

    if crossover_method == "nueve_puntos_uniforme":
        """
        CROSS IN 9 POINTS (UNIFORM).
        """
        for i in range(4):
            child += individual1[9 * i : 9 * (i + 1)].copy()
            child += individual2[9 * (i + 1) : 9 * (i + 2)].copy()
        child += individual1[72 : 81].copy()

    elif crossover_method == "nueve_puntos_aleatorio":
        """
        CROSS IN 9 POINTS (RANDOM).
        """
        random_permutation = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        random.shuffle(random_permutation)
        counter = 0
        for i in random_permutation:
            if counter % 2 == 0:
                child += individual1[9 * i : 9 * (i + 1)]
            else:
                child += individual2[9 * i : 9 * (i + 1)]
            counter += 1
            if counter == 8:
                break
        child += individual1[9 * random_permutation[-1] : 9 * (random_permutation[-1] + 1)]
    
    elif crossover_method == "uniforme":
        """
        UNIFORM CROSS
        """
        for i in range(81):
            if random.random() < 0.5:
                child.append(individual1[i])
            else:
                child.append(individual2[i])

        for i in range(81):
            if random.random() < 0.5:
                child.append(individual1[i])
            else:
                child.append(individual2[i])
    
    return child


def mutate(individual):
    if mutation_method == "muchas":
        """
        MANY CELLS MUTATE AT THE SAME TIME
        """
        for i in range(81):
            if random.random() < mutation_prob:
                apply_mutation_to_cell(individual, i)
    
    elif mutation_method == "una":
        """
        ONLY ONE CELL MUTATES AT THE SAME TIME
        """
        if random.random() < mutation_prob:
            apply_mutation_to_cell(individual, random.randint(0, 80))
    
    elif mutation_method == "intercambio":
        """
        SWAP OF TWO RANDOM CELLS
        """
        i, j = random.randint(0, 80), random.randint(0, 80)
        aux = individual[i]
        individual[i] = individual[j]
        individual[j] = aux
    
    elif mutation_method == "intercambiar_en_fila":
        """
        SWAP OF TWO RANDOM CELLS IN THE SAME ROW
        """
        row_index = random.randint(0, len(legal_row_moves) - 1)
        i, j = random.sample(legal_row_moves[row_index], 2)
        individual[i], individual[j] = individual[j], individual[i]
    
    return individual


def apply_mutation_to_cell(individual, location):
    if location not in seed_population:
        non_mutated_value = individual[location]
        while individual[location] == non_mutated_value:
            individual[location] = random.randint(1, 9)
    
    return individual


def substitute(successors, child1, fitness_child1, child2, fitness_child2, parent1, fitness_parent1, parent2, fitness_parent2):
    # Returns True if any child has been added, False otherwise.

    child_added = False
    if substitution_method == "nada" or substitution_method == "reemplazar_el_peor_por_el_mejor":
        heapq.heappush(successors, (fitness_child1, child1))
        heapq.heappush(successors, (fitness_child2, child2))
        child_added = True
    
    elif substitution_method == "reemplazar_peor_por_mejor":
        """
        REPLACE WORSE CHILD BY BETTER PARENT
        """
        # Child 1
        if fitness_child1 < fitness_parent1 and fitness_child1 < fitness_parent2:
            # Child is better than some parent.
            heapq.heappush(successors, (fitness_child1, child1))
            child_added = True
        
        # Child 2
        if fitness_child2 < fitness_parent2 and fitness_child2 < fitness_parent1:
            # Child is better than parent.
            heapq.heappush(successors, (fitness_child2, child2))
            child_added = True
        
        if not child_added:
            if fitness_parent1 < fitness_parent2:
                heapq.heappush(successors, (fitness_parent1, parent1))
            else:
                heapq.heappush(successors, (fitness_parent2, parent2))
    
    return child_added


def decode(individual):
    return individual


def generate_seed_sudoku():
    # Generates a random sudoku with no restrictions applied to it.
    board = []
    for i in range(81):
        if i not in seed_population:
            board.append(random.randint(1, 9))
        else:
            board.append(seed_population[i])
    
    return board


row_permutes_from = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# Generates a sudoku by generating a row as a permutation of the vector given above.
def generate_seed_sudoku_row_permutations():
    board = []
    for i in range(9):  # 9 rows: i represents each row number.
        row = random.sample(row_permutes_from, 9)
        
        # Satisfy fixed cell constraints.
        for k, v in seed_population.items():
            # Check if the fixed cell to consider is in the row.
            if i == int(k / 9):
                # Work locally within the row.
                previous_cell_value = row[k % 9]
                if previous_cell_value != v:
                    # Swap the cell which is currently occupying the (k-th % 9) position of the row
                    # with the cell in the same row which has the value that we want to put
                    # in the (k-th % 9) position of the row.
                    for cell in range(9):
                        if row[cell] == v:
                            index_to_swap = cell
                            break
                    row[k % 9] = v
                    row[index_to_swap] = previous_cell_value

        board += row
    
    return board


def evaluate(individual):
    # The measure of evaluation is the number of different numbers
    # which are different on every row, column, and 3x3 square.
    # This makes for a maximum fitness of 81 * 3 = 243.
    fitness = 0

    # Keep track of the numbers at every row, column and 3x3 square.
    rows = {}
    cols = {}
    three_sq = {}
    
    for i in range(9):
        row = set()
        for j in range(9):
            row.add(individual[i * 9 + j])
        fitness += len(row)
    
    for i in range(9):
        col = set()
        for j in range(9):
            col.add(individual[j * 9 + i])
        fitness += len(col)
    
    three_sq = {}
    for i in range(9):
        for j in range(9):
            aux_list = three_sq.get(int(i / 3) * 3 + int(j / 3), set())
            aux_list.add(individual[i * 9 + j])
            three_sq[int(i / 3) * 3 + int(j / 3)] = aux_list
    for i in three_sq:
        fitness += len(three_sq[i])

    return max_fitness - fitness


    # for cell in range(81):
    #     # Analyze the row.
    #     aux_list = rows.get(int(cell / 9), set())
    #     aux_list.append(individual[cell])
    #     rows[int(cell / 9)] = aux_list
    #     # aux_set = rows.get(int(cell / 9), set([]))
    #     # aux_set.add(individual[cell])
    #     # rows[int(cell / 9)] = aux_set

    #     # Analyze the column.
    #     aux_list = cols.get(cell % 9, set())
    #     aux_list.append(individual[cell])
    #     cols[cell % 9] = aux_list
    #     # aux_set = cols.get(cell % 9, set())
    #     # aux_set.add(individual[cell])
    #     # cols[cell % 9] = aux_set

    #     # Analyze the 3x3 square.
    #     current_row = int(cell / 9)
    #     current_col = cell % 9
    #     aux_list = three_sq.get(int(current_row / 3) * 3 + int(current_col / 3), set())
    #     aux_list.add(individual[cell])
    #     three_sq[int(current_row / 3) * 3 + int(current_col / 3)] = aux_list
    #     # aux_set = three_sq.get(int(current_row / 3) * 3 + int(current_col / 3), set([]))
    #     # aux_set.add(individual[cell])
    #     # three_sq[int(current_row / 3) * 3 + int(current_col / 3)] = aux_set
    
    # # Calculate the fitness
    # for i in range(9):
    #     # Award those combinations that are permutations.
    #     fitness += len(set(rows[i]))
    #     fitness += len(set(cols[i]))
    #     fitness += len(set(three_sq[i]))
        
    # return max_fitness - fitness


def initialize_population(num_boards, seed_pop = {}):
    # Seed population: dictionary with some of the values already set.
    # For instance, seed_population[0] = 3
    # means that the very first cell of the sudoku will always have a 3.
    boards = []
    for i in range(num_boards):
        # Generate n different boards.
        # board = generate_seed_sudoku()
        board = generate_seed_sudoku_row_permutations()
        heapq.heappush(boards, (evaluate(board), board))
    
    return boards


def print_sudoku_board(sudoku_board):
    print("+", end = "")
    for i in range(0, 29):
        # 23 at first, but marking fixed cells with asterisks makes the board larger.
        if i == 9 or i == 19:
            print("+", end = "")
        else:
            print("-", end = "")
    print("+")
    for multiple_row in range(0, 3):
        for i in range(3 * multiple_row, 3 * multiple_row + 3):
            print("|", end = " ")
            for multiple_column in range(0, 3):
                for j in range(3 * multiple_column, 3 * multiple_column + 3):
                    if i * 9 + j in seed_population:
                        # click.secho(str(sudoku_board[i * 9 + j]) + "*", fg = "red", nl = False)
                        print(str(sudoku_board[i * 9 + j]) + "*", end = "")
                        if (j + 1) % 3 != 0:
                            print(end = " ")
                    else:
                        print(int(sudoku_board[i * 9 + j]), end = " ")
                        if (j + 1) % 3 != 0:
                            print(end = " ")
                if multiple_column != 2:
                    print("|", end = " ")
            print("|")
        print("+", end = "")
        for i in range(0, 29):
            # 23 at first, but marking fixed cells with asterisks makes the board larger.
            if i == 9 or i == 19:
                print("+", end = "")
            else:
                print("-", end = "")
        print("+")


def compute_associated_probabilities(best_parents_fitnesses):
    # Gives a list (where the order matters) which contains
    # the probabilities that the individual i is chosen
    # according to the fitness i
    #
    # RANKING SELECTION
    #
    probabilities = []

    total_fitness = sum(best_parents_fitnesses)
    max_parameter = 1.1
    min_parameter = 2 - max_parameter
    for i in range(len(best_parents_fitnesses)):
        h = (max_parameter - min_parameter) * i / (len(best_parents_fitnesses) - 1)
        h = max_parameter - h
        probabilities.append(h / len(best_parents_fitnesses))
    
    return probabilities


def get_two_parents(parents, parents_fitnesses, crossover_probabilities):
    # Chooses two parents at random by using the roulette method.
    parent1 = []
    parent2 = []
    index_parent1 = 0
    index_parent2 = 0
    
    while index_parent1 == index_parent2:
        accum_prob = 0
        threshold_prob = random.random()
        # Pick the first parent
        for i in range(len(crossover_probabilities)):
            accum_prob += crossover_probabilities[i]
            if accum_prob > threshold_prob:
                parent1 = parents[i]
                fitness_parent1 = parents_fitnesses[i]
                index_parent1 = i
                break
        accum_prob = 0
        threshold_prob = random.random()
        # Pick the second parent
        for i in range(len(crossover_probabilities)):
            accum_prob += crossover_probabilities[i]
            if accum_prob > threshold_prob:
                parent2 = parents[i]
                fitness_parent2 = parents_fitnesses[i]
                index_parent2 = i
                break
        
    return (parent1, fitness_parent1, parent2, fitness_parent2)


def signal_handler(signum, frame):
    # Prints to a file the best sudoku obtained after the user
    # has stopped the program through a SIGINT signal.
    signal.signal(signum, signal.SIG_IGN)  # Ignore additional signals.
    # orig_stdout = sys.stdout
    # with open("sudoku-solution-" + str(solution_id), "w") as f:
    #     sys.stdout = f
    #     print_sudoku_board(previous_best_board)
    #     sys.stdout = orig_stdout
    pr.disable()
    s = StringIO()
    ps = pstats.Stats(pr, stream = s).sort_stats("cumulative")
    ps.print_stats()
    print(s.getvalue())
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


def main():
    # Variables that will be used in the main loop.
    # Takes note of the generations without a major change,
    # in case a population cleanup is required (if it is stuck on a local minimum).
    num_generations_without_change = 0
    # Tracks the best previous fitness and board.
    previous_best_fitness = max_fitness
    global previous_best_board
    # Identifier of the solution to be stored on disk.
    global solution_id
    try:
        with open("sudoku-solution-id.data", "r") as f:
            solution_id = int(f.read())
    except FileNotFoundError:
        # Initialize the file: it is supposed to be the first solution.
        solution_id = 0
    with open("sudoku-solution-id.data", "w") as f:
        f.write(str(solution_id + 1))


    # GENERAL PROCEDURE OF GENETIC ALGORITHMS.
    # Adapted from John McCall. Genetic algorithms for modelling and optimisation.

    # (1) Randomly generate an initial source population of P chromosomes.
    # (2) Calculate the fitness, F(c), of each chromosome c in the source population.
    boards = initialize_population(max_boards_per_gen, seed_population)

    # Take note of the time elapsed to write to disk f(time) = fitness.
    # Do not take into account the time taken for initializing the population.
    
    # (3) Create an empty successor population and then repeat the following steps
    # until P chromosomes have been created.
    iteration = 0
    while previous_best_fitness > 0:
        # Children of the current fathers.
        successors = []
        # For doing the selection by contest.
        contest_winners = []
        best_fitness_in_parents_generation, best_board_in_parents_generation = boards[0]

        if selection_method == "elitista":
            """
            ELITIST SELECTION
            """
            while len(boards) > 0:
                # (a) Select two chromosomes, c1 and c2 from the source population.
                fitness_parent1, parent1 = heapq.heappop(boards)
                fitness_parent2, parent2 = heapq.heappop(boards)

                # (b) Apply crossover to c1 and c2 with crossover rate pc to obtain a child chromosome c.
                if random.random() < crossover_prob:
                    child1 = crossover(parent1, parent2)
                else:
                    child1 = parent1.copy()
                if random.random() < crossover_prob:
                    child2 = crossover(parent2, parent1)
                else:
                    child2 = parent2.copy()

                # (c) Apply uniform mutation to c with mutation rate pm to produce c′.
                if random.random() < mutation_prob:
                    mutate(child1)
                if random.random() < mutation_prob:
                    mutate(child2)
                
                # (d) Add c′to the successor population.
                successors_changed = substitute(successors, child1, evaluate(child1), child2, evaluate(child2), parent1, fitness_parent1, parent2, fitness_parent2)

        elif selection_method == "torneo":
            """
            CONTEST + ROULETTE SELECTION
            """
            for i in range(num_contests):
                contest_participants = []
                # (a) Select two chromosomes, c1 and c2
                for j in range(indiv_in_contest):
                    fitness_parent, parent = heapq.heappop(boards)
                    heapq.heappush(contest_participants, (fitness_parent, parent))
                fitness_parent_winner, parent_winner = heapq.heappop(contest_participants)
                heapq.heappush(contest_winners, (fitness_parent_winner, parent_winner))
            
            # (a') from the source population.
            # Note: at this point, we have all the winners in contest_winners.
            # This list (priority queue) is of length num_contests.
            best_parents = []
            best_parents_fitnesses = []
            for i in range(num_contests):
                fitness_parent_winner, parent_winner = heapq.heappop(contest_winners)
                best_parents.append(parent_winner)
                best_parents_fitnesses.append(fitness_parent_winner)
            
            current_winners_without_pair = num_contests
            parents_probabilities = compute_associated_probabilities(best_parents_fitnesses)

            while(current_winners_without_pair) > 0:
                # Choose at random two parents through wheel selection
                parent1, fitness_parent1, parent2, fitness_parent2 = get_two_parents(best_parents, best_parents_fitnesses, parents_probabilities)
                current_winners_without_pair -= 2

                # (b) Apply crossover to c1 and c2 with crossover rate pc to obtain a child chromosome c.
                child1 = crossover(parent1, parent2)
                child2 = crossover(parent2, parent1)

                # (c) Apply uniform mutation to c with mutation rate pm to produce c′.
                if random.random() < mutation_prob:
                    mutate(child1)
                if random.random() < mutation_prob:
                    mutate(child2)
                
                # (d) Add c′ to the successor population.
                successors_changed = substitute(successors, child1, evaluate(child1), child2, evaluate(child2), parent1, fitness_parent1, parent2, fitness_parent2)

                num_children = len(successors)
                for i in range(max_boards_per_gen - num_children):
                    # child = generate_seed_sudoku()
                    child = generate_seed_sudoku_row_permutations()
                    heapq.heappush(successors, (evaluate(child), child))

        # Insert the best parent in the successor list, get rid of the worst child.
        # Kay Wiese and Scott D. Goodwin. Keep-Best Reproduction: A Selection Strategy for Genetic Algorithms.
        if substitution_method == "reemplazar_el_peor_por_el_mejor":
            aux_heap = []
            heapq.heappush(aux_heap, (best_fitness_in_parents_generation, best_board_in_parents_generation))
            for _ in range(len(successors) - 1):
                heapq.heappush(aux_heap, heapq.heappop(successors))
            successors = aux_heap.copy()
        
        # Check if a solution has been improved.
        best_fitness_in_generation, best_board_in_generation = successors[0]
        if best_fitness_in_generation < previous_best_fitness:
            num_generations_without_change = 0
            previous_best_fitness = best_fitness_in_generation
            previous_best_board = best_board_in_generation
            # with open("popul=" + str(max_boards_per_gen)  + ",mut_p=" + str(mutation_prob) + "," + selection_method + "," + crossover_method + "," + mutation_method + "," + substitution_method, "a") as f:
            with open("popul=" + str(max_boards_per_gen)  + ",mut_p=" + str(mutation_prob), "a") as f:
                f.write(str(iteration) + "\t" + str(best_fitness_in_generation) + "\n")
            if verbose:
                print_sudoku_board(best_board_in_generation)
                print(best_fitness_in_generation)
        else:
            num_generations_without_change += 1
        
        iteration += 1

        # (4) Replace the source population with the successor population.
        
        if replacement_method == "nada":
            """
            NO APPROACH
            """
            boards = successors.copy()
        
        elif replacement_method == "juicio_final":
            """
            FINAL JURY
            """
            if num_generations_without_change == max_generations_without_change:
                print("Final jury done.")
                best_fitness, best_individual = heapq.heappop(successors)
                successors = []
                boards = initialize_population(max_boards_per_gen - 1, seed_population)
                heapq.heappush(boards, (best_fitness, best_individual))
                num_generations_without_change = 0
                previous_best_fitness = best_fitness
            else:
                boards = successors.copy()
        successors = []
    
    # (5) If stopping criteria have not been met, return to Step 2.

    # At this point we have found a valid solution. Return it.
    print("SUDOKU FINISHED. Solution:")
    print_sudoku_board(heapq.heappop(boards)[1])


if __name__ == "__main__":
    main()