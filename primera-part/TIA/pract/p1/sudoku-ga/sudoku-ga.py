# Genetic algorithm framework.
from deap import base, creator, tools

# Parallelism libraries.
from scoop import futures
import multiprocessing

import random

# Fancy printing
import click

import sys

import signal

import time


verbose = "-v" in sys.argv
solution_id = 0
generations_elapsed = 0


# fixed_cells = {0: 1, 1: 2, 2: 3, 9: 4, 10: 5, 11: 6, 18: 7, 19: 8, 20: 9, 60: 1, 61: 2, 62: 3, 69: 4, 70: 5, 71: 6, 78: 7, 79: 8, 80: 9}
fixed_cells = {8: 9, 10: 9, 16: 3, 18: 5, 19: 1, 20: 3, 21: 9, 29: 6, 31: 7, 33: 4, 34: 1, 35: 5, 37: 4, 39: 5, 40: 6, 47: 8, 51: 7, 52: 6, 58: 3, 59: 2, 60: 1, 66: 7, 70: 9, 71: 6, 72: 7, 73: 3, 75: 1, 77: 6, 78: 2, 80: 4}
tournament_size = 2
num_tournaments = 40
max_population = num_tournaments * tournament_size
max_generations = 1000000
crossover_probability = 0.9
mutation_probability = 1
change_probability = 0.05


row_permutes_from = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# Generates a sudoku with some restrictions applied, given by fixed_cells.
def generate_seed_sudoku():
    board = []
    for i in range(9):  # 9 rows: i represents each row number.
        row = random.sample(row_permutes_from, 9)
        
        # Satisfy fixed cell constraints.
        for k, v in fixed_cells.items():
            # Check if the fixed cell to consider is within boundaries.
            if i == int(k / 9):
                # Work locally within the row.
                previous_cell_value = row[k % 9]
                if previous_cell_value != v:
                    # Swap the cell which is currently occupying the (k-th % 9) position of the row
                    # with the cell in the same row which has the value that we want to put
                    # in the (k-th % 9) position of the row.
                    for cell in range(9):
                        if row[cell] == v:
                            index_to_swap = cell
                            break
                    row[k % 9] = v
                    row[index_to_swap] = previous_cell_value

        board += row

    return board


previous_best_board = []


def signal_handler(signum, frame):
    # Prints to a file the best sudoku obtained after the user
    # has stopped the program through a SIGINT signal.
    signal.signal(signum, signal.SIG_IGN)  # Ignore additional signals.
    orig_stdout = sys.stdout
    with open("sudoku-solution-" + str(solution_id), "w") as f:
        sys.stdout = f
        print_sudoku_board(previous_best_board)
        sys.stdout = orig_stdout
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


def fitness(individual):
    # The measure of evaluation is the number of different numbers
    # which are different on every row, column, and 3x3 square.
    # This makes for a maximum fitness of 81 * 3 = 243.
    fitness = 0

    # Keep track of the numbers at every row, column and 3x3 square.
    rows = {}
    cols = {}
    three_sq = {}

    # Analyze every row.
    for r in range(81):
        aux_list = rows.get(int(r / 9), [])
        aux_list.append(individual[r])
        rows[int(r / 9)] = aux_list
    
    # Analyze every column.
    for c in range(81):
        aux_list = cols.get(c % 9, [])
        aux_list.append(individual[c])
        cols[c % 9] = aux_list
    
    # Analyze every 3x3 square.
    for three in range(81):
        # General formula to access the 3x3 square:
        # int(current_row / 3) * 3 + int(current_col / 3)
        current_row = int(three / 9)
        current_col = three % 9
        aux_list = three_sq.get(int(current_row / 3) * 3 + int(current_col / 3), [])
        aux_list.append(individual[three])
        three_sq[int(current_row / 3) * 3 + int(current_col / 3)] = aux_list
    
    # Calculate the fitness
    for i in range(9):
        fitness += len(set(rows[i]))
        fitness += len(set(cols[i]))
        fitness += len(set(three_sq[i]))
    
    # DEAP requires a sequence of numbers to be returned
    # by the fitness function.
    return fitness,


def crossover(individual1, individual2):
    child1 = child2 = []
    
    for i in range(81):
        # First child
        if random.random() < 0.5:
            child1.append(individual1[i])
        else:
            child1.append(individual2[i])
        # Second child
        if random.random() < 0.5:
            child2.append(individual1[i])
        else:
            child2.append(individual2[i])
    return child1, child2


def mutate(individual):
    # The mutation is directed by the probability of changing
    # any cell's contents.
    for i in range(81):
        if random.random() < change_probability:
            if i not in fixed_cells:
                non_mutated_value = individual[i]
                while individual[i] == non_mutated_value:
                    individual[i] = random.randint(1, 9)
    
    return individual


def print_sudoku_board(sudoku_board):
    print("|", end = "")
    for i in range(0, 29):
        # 23 at first, but marking fixed cells with asterisks makes the board larger.
        if i == 9 or i == 19:
            print("+", end = "")
        else:
            print("-", end = "")
    print("|")
    for multiple_row in range(0, 3):
        for i in range(3 * multiple_row, 3 * multiple_row + 3):
            print("|", end = " ")
            for multiple_column in range(0, 3):
                for j in range(3 * multiple_column, 3 * multiple_column + 3):
                    if i * 9 + j in fixed_cells:
                        click.secho(str(sudoku_board[i * 9 + j]) + "*", fg = "red", nl = False)
                        # print(str(sudoku_board[i * 9 + j]) + "*", end = "")
                        if (j + 1) % 3 != 0:
                            print(end = " ")
                    else:
                        print(int(sudoku_board[i * 9 + j]), end = " ")
                        if (j + 1) % 3 != 0:
                            print(end = " ")
                if multiple_column != 2:
                    print("|", end = " ")
            print("|")
        print("|", end = "")
        for i in range(0, 29):
            # 23 at first, but marking fixed cells with asterisks makes the board larger.
            if i == 9 or i == 19:
                print("+", end = "")
            else:
                print("-", end = "")
        print("|")


# Fitness and individual declaration.
creator.create("FitnessMax", base.Fitness, weights = (1.0,))
creator.create("Individual", list, fitness = creator.FitnessMax)

# Population declaration.
toolbox = base.Toolbox()
# toolbox.register("attribute", lambda: random.randint(1, 9))
# toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attribute, n = 81)
toolbox.register("indices", generate_seed_sudoku)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

# Crossover, mutation, selection and evaluation functions.
toolbox.register("mate", crossover)
toolbox.register("mutate", mutate)
toolbox.register("select", tools.selTournament, tournsize = tournament_size)
toolbox.register("evaluate", fitness)

# Parallelism: SCOOP module.
toolbox.register("map", futures.map)

# Parallelism: multiprocessing module.
# pool = multiprocessing.Pool()
# toolbox.register("map", pool.map)


def main():
    # Best individual and fitness found so far.
    global previous_best_board
    previous_best_fitness = 0
    
    # Identifier of the solution to be stored on disk.
    global solution_id
    try:
        with open("sudoku-solution-id.data", "r") as f:
            solution_id = int(f.read())
    except FileNotFoundError:
        # Initialize the file: it is supposed to be the first solution.
        solution_id = 0
    with open("sudoku-solution-id.data", "w") as f:
        f.write(str(solution_id + 1))
    
    time_since_beginning = time.time()
    global generations_elapsed
    
    population = toolbox.population(n = max_population)
    # for individual in population:
    #     for position, number in fixed_cells.items():
    #         individual[position] = number

    fitnesses = list(map(toolbox.evaluate, population))
    for individual, fitness in zip(population, fitnesses):
        individual.fitness.values = fitness
    
    for generation in range(max_generations):
        # Select the next generation individuals.
        children = toolbox.select(population, len(population))
        # Clone the selected individuals.
        children = list(map(toolbox.clone, children))

        # Apply crossover and mutation on the children.
        for child1, child2 in zip(children[::2], children[1::2]):
            if random.random() < crossover_probability:
                toolbox.mate(child1, child2)
                del child1.fitness.values
                del child2.fitness.values

        for mutant in children:
            if random.random() < mutation_probability:
                toolbox.mutate(mutant)
                del mutant.fitness.values
        
        # Evaluate the individuals with an invalid fitness.
        invalid_fitness_individuals = [individual for individual in children if not individual.fitness.valid]
        fitnesses = map(toolbox.evaluate, invalid_fitness_individuals)
        for individual, fitness in zip(invalid_fitness_individuals, fitnesses):
            individual.fitness.values = fitness

        population[:] = children

        if len(invalid_fitness_individuals) > 0:
            current_best_fitness_index = invalid_fitness_individuals.index(max(invalid_fitness_individuals, key = lambda individual: individual.fitness))
            current_best_fitness = toolbox.evaluate(invalid_fitness_individuals[current_best_fitness_index])[0]
            if current_best_fitness > previous_best_fitness:
                previous_best_fitness = current_best_fitness
                previous_best_board = population[current_best_fitness_index]
                with open("sudoku-evolution-" + str(solution_id), "a") as f:
                    f.write(str(time.time() - time_since_beginning) + "\t" + str(current_best_fitness) + "\n")
                if verbose:
                    print_sudoku_board(population[current_best_fitness_index])
                    print(current_best_fitness)
    
    print("BEST INDIVIDUAL OBTAINED:")
    print_sudoku_board(previous_best_board)
    print(previous_best_fitness)
    orig_stdout = sys.stdout
    with open("sudoku-solution-" + str(solution_id), "w") as f:
        sys.stdout = f
        print_sudoku_board(previous_best_board)
        sys.stdout = orig_stdout


if __name__ == "__main__":
    main()