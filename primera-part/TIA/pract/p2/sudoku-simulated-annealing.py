# Sudoku solver by means of simulated annealing.
# It is assumed that an individual is a 81-dimensional array with numbers from 1 to 9.
# The array will represent the sudoku board from left to right, top to bottom.

import random, math, sys, copy, numpy

# For writing into disk "f(time) = fitness".
# import time

# For catching control + c and writing the best sudoku to disk.
import signal

import cProfile, pstats
from io import StringIO
pr = cProfile.Profile()
pr.enable()

verbose = "-v" in sys.argv
reheat = "-r" in sys.argv
solution_id = 0
f = None

# Sudoku difficulty: normal
seed_population = {8: 9, 10: 9, 16: 3, 18: 5, 19: 1, 20: 3, 21: 9, 29: 6, 31: 7, 33: 4, 34: 1, 35: 5, 37: 4, 39: 5, 40: 6, 47: 8, 51: 7, 52: 6, 58: 3, 59: 2, 60: 1, 66: 7, 70: 9, 71: 6, 72: 7, 73: 3, 75: 1, 77: 6, 78: 2, 80: 4}
# Sudoku difficulty: ???
# seed_population = {1: 3, 4: 7, 7: 5, 9: 5, 12: 1, 14: 6, 17: 9, 20: 1, 24: 4, 28: 9, 31: 5, 34: 6, 36: 6, 39: 4, 41: 2, 44: 7, 46: 4, 49: 1, 52: 3, 56: 2, 60: 8, 63: 9, 66: 3, 68: 5, 71: 2, 73: 1, 76: 2, 79: 7}
# Sudoku difficulty: order 3 ???
# seed_population = {8: 8, 9: 1, 10: 8, 14: 2, 15: 3, 19: 6, 22: 5, 23: 7, 26: 1, 28: 7, 30: 9, 31: 6, 37: 9, 39: 7, 41: 4, 43: 1, 49: 8, 50: 1, 52: 4, 54: 6, 57: 2, 58: 4, 61: 8, 65: 4, 66: 5, 70: 9, 71: 3, 72: 5}
reward_factor = 1
penalization_factor = 1
max_fitness = 81 * 3 * reward_factor
current_board = []
current_board_fitness = max_fitness

# initial_temperature = 1000
max_iterations_without_change = 30000
# temperature_decrement_schema = {"linear", "exponential", "fractional"}
temperature_decrement_schema = "exponential"
# - Fractional: very low k
# - Exponential: high k, usually in {0.8, 0.99}
# neighbor_generation_method = {"many", "one", "swap", "swap_same_row", "swap_3x3"}
neighbor_generation_method = "swap_3x3"

def get_three_sq_index_from_absolute_index(index):
    row = int(index / 9)
    col = index % 9
    three_sq_index = int(row / 3) * 3 + int(col / 3)
    return three_sq_index

# Dictionary with available cells to swap for each square.
# Only squares with more than two cells available are specified.
# This dictionary is used in the neighbor generation, "swap_3x3"
three_squares_with_swaps = {}
possible_cells_to_swap = [[0, 1, 2, 9, 10, 11, 18, 19, 20], [3, 4, 5, 12, 13, 14, 21, 22, 23], [6, 7, 8, 15, 16, 17, 24, 25, 26], [27, 28, 29, 36, 37, 38, 45, 46, 47], [30, 31, 32, 39, 40, 41, 48, 49, 50], [33, 34, 35, 42, 43, 44, 51, 52, 53], [54, 55, 56, 63, 64, 65, 72, 73, 74], [57, 58, 59, 66, 67, 68, 75, 76, 77], [60, 61, 62, 69, 70, 71, 78, 79, 80]]
three_square = 0
for i in range(9):
    possible_swaps = set(possible_cells_to_swap[i]) - set([cell for cell in seed_population])
    if len(possible_swaps) > 1:
        three_squares_with_swaps[three_square] = possible_swaps
        three_square += 1

legal_three_sq_moves = {}
# Initialize the 3x3 legal placements to be every number.
for i in range(9):
    legal_three_sq_moves[i] = set([1, 2, 3, 4, 5, 6, 7, 8, 9])

# Remove the illegal placements according to the seed configuration.
for k, v in seed_population.items():
    three_sq_index = get_three_sq_index_from_absolute_index(k)
    legal_three_sq_moves[three_sq_index] -= set([v])

possible_swaps = set()
for movs in three_squares_with_swaps.values():
    for i in movs:
        for j in movs:
            if i != j and (j, i) not in possible_swaps:
                possible_swaps.add((i, j))

initial_temperature = 10
k = 0.999
def decrease_temperature(iteration, prev_temp):
    new_temp = None
    if temperature_decrement_schema == "linear":
        new_temp = initial_temperature - iteration * k
    
    elif temperature_decrement_schema == "exponential":
        new_temp = k * prev_temp
    
    elif temperature_decrement_schema == "fractional":
        new_temp = prev_temp / (1 + k * prev_temp)
    
    return new_temp


def encode(individual):
    return individual


def generate_neighbor(individual, i, j):
    if neighbor_generation_method == "swap_3x3":
        # NEW METHOD
        # (swap indices provided in the method)
        # individual[i], individual[j] = individual[j], individual[i]


        # OLD METHOD
        # (using three_squares_with_swaps)
        #
        # Randomly get a square to swap the values from.
        index = random.randint(0, len(three_squares_with_swaps) - 1)
        # print(index)
        
        # Get the two cells to swap.
        first_cell, second_cell = random.sample(three_squares_with_swaps[index], 2)

        # Swap the values.
        individual[first_cell], individual[second_cell] = individual[second_cell], individual[first_cell]


        # OLDER METHOD
        # (raw checks)
        #
        # # Randomly choose a cell to change (and thus a 3x3 square).
        # first_cell = random.randint(0, 80)
        # while first_cell in seed_population:
        #     first_cell = random.randint(0, 80)
        # first_cell_row = int(first_cell / 9)
        # first_cell_col = first_cell % 9

        # # Get the boundaries (beginning) of the 3x3 square
        # # in which the chosen cell is.
        # square_row_beginning = int(first_cell_row / 3) * 3
        # square_col_beginning = int(first_cell_col / 3) * 3
        
        # # From the cell previously obtained,
        # # choose another cell which is in the same 3x3 square.
        # second_cell_row = random.randint(square_row_beginning, square_row_beginning + 2)
        # second_cell_col = random.randint(square_col_beginning, square_col_beginning + 2)
        # second_cell = second_cell_row * 9 + second_cell_col
        # while second_cell in seed_population:
        #     second_cell_row = random.randint(square_row_beginning, square_row_beginning + 2)
        #     second_cell_col = random.randint(square_col_beginning, square_col_beginning + 2)
        #     second_cell = second_cell_row * 9 + second_cell_col
        
        # # Swap the values.
        # individual[first_cell], individual[second_cell] = individual[second_cell], individual[first_cell]


    elif neighbor_generation_method == "many":
        """
        MANY CELLS CHANGE AT THE SAME TIME
        """
        change_prob = 0.05
        for i in range(81):
            if i not in seed_population and random.random() < change_prob:
                non_mutated_value = individual[i]
                while individual[i] == non_mutated_value:
                    individual[i] = random.randint(1, 9)
    
    elif neighbor_generation_method == "one":
        """
        ONLY ONE CELL CHANGES
        """
        # Get the index of mutation
        i = random.randint(0, 80)
        if i in seed_population:
            # Do not mutate
            return individual
    
        non_mutated_value = individual[i]
        while individual[i] == non_mutated_value:
            individual[i] = random.randint(1, 9)
    
    elif neighbor_generation_method == "swap":
        """
        SWAP OF TWO RANDOM CELLS
        """
        i, j = random.randint(0, 80), random.randint(0, 80)
        while i == j:
            i, j = random.randint(0, 80), random.randint(0, 80)
        if i in seed_population or j in seed_population:
            # Do not mutate
            return individual
        
        aux = individual[i]
        individual[i] = individual[j]
        individual[j] = aux
    
    elif neighbor_generation_method == "swap_same_row":
        """
        SWAP OF TWO RANDOM CELLS AT THE SAME ROW
        """
        row = random.randint(0, 8)
        i, j = random.randint(9 * row, 9 * (row + 1) - 1), random.randint(9 * row, 9 * (row + 1) - 1)
        while i == j or i in seed_population or j in seed_population:
            i, j = random.randint(9 * row, 9 * (row + 1) - 1), random.randint(9 * row, 9 * (row + 1) - 1)
        
        aux = individual[i]
        individual[i] = individual[j]
        individual[j] = aux
    
    return individual


def decode(individual):
    return individual


row_permutes_from = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# Generates a sudoku by generating a row as a permutation of the vector given above.
def generate_seed_sudoku_row_permutations():
    board = []
    for i in range(9):  # 9 rows: i represents each row number.
        row = random.sample(row_permutes_from, 9)
        
        # Satisfy fixed cell constraints.
        for k, v in seed_population.items():
            # Check if the fixed cell to consider is in the row.
            if i == int(k / 9):
                # Work locally within the row.
                previous_cell_value = row[k % 9]
                if previous_cell_value != v:
                    # Swap the cell which is currently occupying the (k-th % 9) position of the row
                    # with the cell in the same row which has the value that we want to put
                    # in the (k-th % 9) position of the row.
                    for cell in range(9):
                        if row[cell] == v:
                            index_to_swap = cell
                            break
                    row[k % 9] = v
                    row[index_to_swap] = previous_cell_value

        board += row
    
    return board


# Generates a sudoku by generating every 3x3 square as a permutation of the sequence [1..9].
def generate_seed_sudoku_square_permutations():
    board = []
    # current_legal_three_sq_moves = legal_three_sq_moves.copy()
    current_legal_three_sq_moves = copy.deepcopy(legal_three_sq_moves)
    for i in range(81):
        if i not in seed_population:
            three_sq_index = get_three_sq_index_from_absolute_index(i)
            number_to_place = random.sample(current_legal_three_sq_moves[three_sq_index], 1)
            current_legal_three_sq_moves[three_sq_index] -= set(number_to_place)
            board.append(number_to_place[0])  # There is only one value obtained from sample().
        else:
            board.append(seed_population[i])
    
    return board


def evaluate(individual):
    fitness = 0

    # Examine rows.
    for i in range(9):
        row = set()
        for j in range(9):
            row.add(individual[i * 9 + j])
        fitness += 9 - len(row)
    
    # Examine columns.
    for i in range(9):
        col = set()
        for j in range(9):
            col.add(individual[j * 9 + i])
        fitness += 9 - len(col)

    return fitness


def print_sudoku_board(sudoku_board):
    print("+", end = "")
    for i in range(0, 29):
        # 23 at first, but marking fixed cells with asterisks makes the board larger.
        if i == 9 or i == 19:
            print("+", end = "")
        else:
            print("-", end = "")
    print("+")
    for multiple_row in range(0, 3):
        for i in range(3 * multiple_row, 3 * multiple_row + 3):
            print("|", end = " ")
            for multiple_column in range(0, 3):
                for j in range(3 * multiple_column, 3 * multiple_column + 3):
                    if i * 9 + j in seed_population:
                        # click.secho(str(sudoku_board[i * 9 + j]) + "*", fg = "red", nl = False)
                        print(str(sudoku_board[i * 9 + j]) + "*", end = "")
                        if (j + 1) % 3 != 0:
                            print(end = " ")
                    else:
                        print(int(sudoku_board[i * 9 + j]), end = " ")
                        if (j + 1) % 3 != 0:
                            print(end = " ")
                if multiple_column != 2:
                    print("|", end = " ")
            print("|")
        print("+", end = "")
        for i in range(0, 29):
            # 23 at first, but marking fixed cells with asterisks makes the board larger.
            if i == 9 or i == 19:
                print("+", end = "")
            else:
                print("-", end = "")
        print("+")


def signal_handler(signum, frame):
    # Prints to a file the best sudoku obtained after the user
    # has stopped the program through a SIGINT signal.
    signal.signal(signum, signal.SIG_IGN)  # Ignore additional signals.
    orig_stdout = sys.stdout
    global f
    f.close()
    with open("FAILED - sudoku-solution-" + temperature_decrement_schema + "-t = " + str(initial_temperature) + "-k = " + str(k) + "- FAILED", "w") as f_one:
        sys.stdout = f_one
        print_sudoku_board(current_board)
        print(current_board_fitness)
        sys.stdout = orig_stdout
    pr.disable()
    s = StringIO()
    ps = pstats.Stats(pr, stream = s).sort_stats("cumulative")
    ps.print_stats()
    print(s.getvalue())
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


def print_details(current_board, current_board_fitness, current_temperature):
    print_sudoku_board(current_board)
    print(current_board_fitness)
    print("Current temperature:", current_temperature)


def main():
    # Identifier of the solution to be stored on disk.
    global solution_id
    try:
        with open("sudoku-solution-id.data", "r") as f_one:
            solution_id = int(f_one.read())
    except FileNotFoundError:
        # Initialize the file: it is supposed to be the first solution.
        solution_id = 0
    with open("sudoku-solution-id.data", "w") as f_one:
        f_one.write(str(solution_id + 1))

    global current_board
    global current_board_fitness
    current_board = generate_seed_sudoku_square_permutations()
    current_board_fitness = evaluate(current_board)
    iteration = 0
    num_iterations_without_change = 0
    current_temperature = initial_temperature

    global f
    f = open("sudoku-evolution-" + temperature_decrement_schema + "-t = " + str(initial_temperature) + "-k = " + str(k), "a")
    
    # time_since_beginning = time.time()
    
    if verbose:
        print("INITIAL SUDOKU")
        print_sudoku_board(current_board)
        print(current_board_fitness)

    # FOR CURRENT EVALUATION FUNCTION
    possible_swaps_from_individual = list(possible_swaps)

    while current_board_fitness > 0:
        random.shuffle(possible_swaps_from_individual)
        neighbor = generate_neighbor(current_board.copy(), None, None)
        neighbor_fitness = evaluate(neighbor)
        
        if neighbor_fitness < current_board_fitness:
            # Unconditionally expand the neighbor.
            current_board = neighbor
            current_board_fitness = neighbor_fitness

            f.write(str(iteration) + "\t" + str(current_board_fitness) + "\n")
            
            if verbose:
                print_details(current_board, current_board_fitness, current_temperature)
            num_iterations_without_change = -1
        
        else:
            # Expand the neighbor if the movement is accepted with a probability p.
            try:
                p = math.exp((current_board_fitness - neighbor_fitness) / current_temperature)
            except ZeroDivisionError:
                print("The temperature has reached zero. Exiting...")
                sys.exit(1)
            if random.random() < p:
                if neighbor_fitness < current_board_fitness:
                    num_iterations_without_change = -1
                current_board = neighbor
                current_board_fitness = neighbor_fitness

                f.write(str(iteration) + "\t" + str(current_board_fitness) + "\n")
                
                if verbose:
                    print("Worse or equal neighbor chosen.")
                    print_details(current_board, current_board_fitness, current_temperature)
        
        iteration += 1
        num_iterations_without_change += 1
            
        # Reheat (just a little).
        if reheat and num_iterations_without_change >= max_iterations_without_change:
            # current_temperature += 0.0001 * current_temperature
            current_temperature = initial_temperature
            if verbose:
                print("Reheating... Current temperature:", current_temperature)
            # current_temperature += 0.005

            num_iterations_without_change = 0
        else:
            current_temperature = decrease_temperature(iteration, current_temperature)
            if current_temperature <= 0:
                print("The temperature has reached zero. Exiting...")
                orig_stdout = sys.stdout
                with open("sudoku-solution-" + temperature_decrement_schema + "-t = " + str(initial_temperature) + "-k = " + str(k) + "-" + str(solution_id), "w") as f_one:
                    sys.stdout = f_one
                    print_sudoku_board(current_board)
                    print(current_board_fitness)
                    sys.stdout = orig_stdout
                sys.exit(1)
    
    print("SOLUTION FOUND")
    print("Iteration number", iteration)
    print_sudoku_board(current_board)
    print(current_board_fitness)
    orig_stdout = sys.stdout
    with open("sudoku-solution-" + temperature_decrement_schema + "-t = " + str(initial_temperature) + "-k = " + str(k), "w") as f_one:
        sys.stdout = f_one
        print_sudoku_board(current_board)
        print(current_board_fitness)
        sys.stdout = orig_stdout
    
    f.close()
    

if __name__ == "__main__":
    main()