import nltk
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords as sw
nltk.download("wordnet")
# nltk.download("punkt")
# remove_stopwords = False

def simplified_lesk(word, sentence):
    best_sense = wn.synsets(word)[0]
    max_overlap = 0
    context = set([wn.morphy(w) for w in nltk.word_tokenize(sentence) if w.isalnum()])
    for sense in wn.synsets(word):
        definition = [wn.morphy(w) if wn.morphy(w) is not None else w for w in nltk.word_tokenize(sense.definition()) if w.isalnum() and w is not None]
        examples = [wn.morphy(w) if wn.morphy(w) is not None else w for sent in sense.examples() for w in nltk.word_tokenize(sent) if w.isalnum() and w is not None]
        signature = set(definition + examples)
        overlap = len(context.intersection(signature))
        if overlap > max_overlap:
            max_overlap = overlap
            best_sense = sense
    
    return best_sense


print(simplified_lesk("arms", "i am human and thus my arms are not very long, but shorter are tyrannosaurus rex's arms").definition())
print(simplified_lesk("key", "the key problem is not you but me").definition())