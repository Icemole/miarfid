import gensim
import numpy as np
import nltk
from nltk.corpus import wordnet as wn
from nltk.data import find
nltk.download("wordnet")
nltk.download("word2vec_sample")
# nltk.download("punkt")
# remove_stopwords = False

word2vec_sample = str(find("models/word2vec_sample/pruned.word2vec.txt"))
model = gensim.models.KeyedVectors.load_word2vec_format(word2vec_sample, binary = False)

remove_stopwords = False

def simplified_lesk_embeddings(word, sentence):
    best_sense = wn.synsets(word)[0]
    max_overlap = 0
    context = []
    for w in nltk.word_tokenize(sentence):
        if not w.isalnum():
            continue
        morphed_word = wn.morphy(w)
        try:
            if morphed_word and model[morphed_word] is not None:
                context.append(morphed_word)
        except KeyError:  # Thrown when getting the embedding of a word which is not in the model.
            try:
                if model[w] is not None:
                    context.append(w)
            except KeyError:  # TypeError
                pass
    context = sum([model[w] for w in context])
    
    for sense in wn.synsets(word):
        definition = []
        for w in nltk.word_tokenize(sense.definition()):
            if not w.isalnum():
                continue
            morphed_word = wn.morphy(w)
            try:
                if morphed_word and model[morphed_word] is not None:
                    definition.append(morphed_word)
            except KeyError:  # Thrown when getting the embedding of a word which is not in the model.
                try:
                    if model[w] is not None:
                        definition.append(w)
                except KeyError:  # TypeError
                    pass
        definition = sum([model[w] for w in definition])

        examples = []
        for example in sense.examples():
            for w in nltk.word_tokenize(example):
                if not w.isalnum():
                    continue
                morphed_word = wn.morphy(w)
                try:
                    if morphed_word and model[morphed_word] is not None:
                        examples.append(morphed_word)
                except KeyError:  # Thrown when getting the embedding of a word which is not in the model.
                    try:
                        if model[w] is not None:
                            examples.append(w)
                    except KeyError:  # TypeError
                        pass
        examples = sum([model[w] for w in examples])
        
        signature = definition + examples

        # overlap = model.similarity(context, signature)  # Cannot calculate similarity directly between two vector embeddings
        overlap = np.dot(context, signature) / (np.linalg.norm(context) * np.linalg.norm(signature))
        print(sense.definition(), "........overlap:", overlap)
        if overlap > max_overlap:
            print("better overlap")
            max_overlap = overlap
            best_sense = sense
    
    return best_sense


# print(simplified_lesk_embeddings("arms", "i am human and thus my arms are not very long but shorter are tyrannosaurus rex arms").definition())
print(simplified_lesk_embeddings("key", "the key problem is not you but me").definition())