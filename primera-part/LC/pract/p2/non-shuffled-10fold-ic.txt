HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23854 output symbols> Time taken: 0.9930438995361328
HMM ACCURACY FOR TEST PARTITION 1 = 0.9283174828904097 Time taken: 44.034037828445435
Confidence interval at 95%: (0.9249469357971696, 0.9316880299836499)
Confidence interval range: (-0.0033705470932401844, 0.0033705470932401844)

TnT model trained. Time taken: 2.1063525676727295
TNT ACCURACY FOR TEST PARTITION 0 = 0.8963203270820371 Time taken: 165.94100618362427
Confidence interval at 95%: (0.8923372020142407, 0.9003034521498335)
Confidence interval range: (-0.003983125067796388, 0.003983125067796388)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23788 output symbols> Time taken: 0.8914163112640381
HMM ACCURACY FOR TEST PARTITION 2 = 0.9237111533931321 Time taken: 39.569690227508545
Confidence interval at 95%: (0.9202091692885093, 0.927213137497755)
Confidence interval range: (-0.0035019841046228274, 0.0035019841046228274)

TnT model trained. Time taken: 2.073667287826538
TNT ACCURACY FOR TEST PARTITION 1 = 0.8886019751744133 Time taken: 164.2912209033966
Confidence interval at 95%: (0.8844514033400825, 0.8927525470087441)
Confidence interval range: (-0.004150571834330744, 0.004150571834330744)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 65 states and 23657 output symbols> Time taken: 0.8685083389282227
HMM ACCURACY FOR TEST PARTITION 3 = 0.9228301472203911 Time taken: 40.05060267448425
Confidence interval at 95%: (0.919362759760781, 0.9262975346800013)
Confidence interval range: (-0.0034673874596102077, 0.0034673874596102077)

TnT model trained. Time taken: 2.0718040466308594
TNT ACCURACY FOR TEST PARTITION 2 = 0.8864864864864865 Time taken: 155.34616780281067
Confidence interval at 95%: (0.8823647758933791, 0.8906081970795939)
Confidence interval range: (-0.004121710593107412, 0.004121710593107412)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23775 output symbols> Time taken: 0.8821461200714111
HMM ACCURACY FOR TEST PARTITION 4 = 0.9251465103889185 Time taken: 40.315415143966675
Confidence interval at 95%: (0.9217097868670494, 0.9285832339107876)
Confidence interval range: (-0.0034367235218691074, 0.0034367235218691074)

TnT model trained. Time taken: 2.075808525085449
TNT ACCURACY FOR TEST PARTITION 3 = 0.88949564908542 Time taken: 183.68583297729492
Confidence interval at 95%: (0.8854012052793707, 0.8935900928914693)
Confidence interval range: (-0.004094443806049327, 0.004094443806049327)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23799 output symbols> Time taken: 0.9590933322906494
HMM ACCURACY FOR TEST PARTITION 5 = 0.9228631913204551 Time taken: 43.45111680030823
Confidence interval at 95%: (0.9193902975760133, 0.926336085064897)
Confidence interval range: (-0.0034728937444418557, 0.0034728937444418557)

TnT model trained. Time taken: 2.398897171020508
TNT ACCURACY FOR TEST PARTITION 4 = 0.8928287906853665 Time taken: 184.24469685554504
Confidence interval at 95%: (0.8888024069873715, 0.8968551743833615)
Confidence interval range: (-0.004026383697995073, 0.004026383697995073)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 65 states and 23658 output symbols> Time taken: 0.9097635746002197
HMM ACCURACY FOR TEST PARTITION 6 = 0.8781806196440343 Time taken: 32.16708469390869
Confidence interval at 95%: (0.8729757075597437, 0.8833855317283248)
Confidence interval range: (-0.005204912084290573, 0.005204912084290573)

TnT model trained. Time taken: 2.1143429279327393
TNT ACCURACY FOR TEST PARTITION 5 = 0.8389584706657878 Time taken: 92.37370538711548
Confidence interval at 95%: (0.8331091900657132, 0.8448077512658624)
Confidence interval range: (-0.005849280600074668, 0.005849280600074668)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 24000 output symbols> Time taken: 0.999967098236084
HMM ACCURACY FOR TEST PARTITION 7 = 0.887891425743974 Time taken: 32.012507915496826
Confidence interval at 95%: (0.8826923847675009, 0.893090466720447)
Confidence interval range: (-0.005199040976473055, 0.005199040976473055)

TnT model trained. Time taken: 2.3476860523223877
TNT ACCURACY FOR TEST PARTITION 6 = 0.8583445253410618 Time taken: 89.18489575386047
Confidence interval at 95%: (0.8525984489267501, 0.8640906017553734)
Confidence interval range: (-0.005746076414311684, 0.005746076414311684)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23716 output symbols> Time taken: 0.9736537933349609
HMM ACCURACY FOR TEST PARTITION 8 = 0.8913107511045656 Time taken: 39.08429169654846
Confidence interval at 95%: (0.8866284531506591, 0.8959930490584721)
Confidence interval range: (-0.004682297953906487, 0.004682297953906487)

TnT model trained. Time taken: 2.3013627529144287
TNT ACCURACY FOR TEST PARTITION 7 = 0.8506627393225331 Time taken: 70.9030830860138
Confidence interval at 95%: (0.8453008977779809, 0.8560245808670853)
Confidence interval range: (-0.005361841544552204, 0.005361841544552204)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 64 states and 23840 output symbols> Time taken: 0.9829633235931396
HMM ACCURACY FOR TEST PARTITION 9 = 0.8911650954798163 Time taken: 36.28235459327698
Confidence interval at 95%: (0.8864199831311015, 0.895910207828531)
Confidence interval range: (-0.004745112348714809, 0.004745112348714809)

TnT model trained. Time taken: 2.3763034343719482
TNT ACCURACY FOR TEST PARTITION 8 = 0.8591370558375635 Time taken: 129.9724555015564
Confidence interval at 95%: (0.8538366036480841, 0.8644375080270429)
Confidence interval range: (-0.005300452189479411, 0.005300452189479411)

===========================================

HMM model trained. Resulting model: <HiddenMarkovModelTagger 65 states and 23793 output symbols> Time taken: 1.2320771217346191
HMM ACCURACY FOR TEST PARTITION 10 = 0.8784427571832664 Time taken: 34.53532838821411
Confidence interval at 95%: (0.8729168885911929, 0.8839686257753399)
Confidence interval range: (-0.005525868592073538, 0.005525868592073538)

TnT model trained. Time taken: 2.348149538040161
TNT ACCURACY FOR TEST PARTITION 9 = 0.8255173440524044 Time taken: 88.0954008102417
Confidence interval at 95%: (0.8190994486885868, 0.831935239416222)
Confidence interval range: (-0.006417895363817674, 0.006417895363817674)

===========================================

BEST ACCURACY FOR HMM: 0.9283174828904097 for the n-th test partition number = 1
Mean HMM accuracy: 0.9049859134368964
Confidence interval at 95%: (0.8925391114671274, 0.9174327154066654)
Confidence interval range: (-0.012446801969768967, 0.012446801969768967)

=========================================

BEST ACCURACY FOR TNT: 0.8963203270820371 for the n-th test partition number = 2
Mean TNT accuracy: 0.8686353363733075
Confidence interval at 95%: (0.8537557542269009, 0.8835149185197141)
Confidence interval range: (-0.014879582146406558, 0.014879582146406558)
