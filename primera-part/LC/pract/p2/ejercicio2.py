import nltk
import numpy as np
from random import shuffle

nltk.download("cess_esp")

from nltk.corpus import cess_esp as cess

# Statistical models
from nltk.tag import hmm
from nltk.tag import tnt

import time, math

# Shuffle flag.
shuffle_corpus = True
# Cross-validation parameters.
# Whether to perform cross-validation or normal training.
cross_validation = True
# Number of partitions when cross-validating.
k = 10

# Get the tagged sentences from the corpus and shuffle them.
tagged_sents = cess.tagged_sents()
tagged_sents = list(tagged_sents)
if shuffle_corpus:
    shuffle(tagged_sents)

# Preprocess the corpus:
# - Every gramatical category must be of length 2, unless it
#   starts with "v" or "F", in which case it must be of length 3.
# - Words such as "*0*" with their respective category being "sn[...]"
#   must be removed.
preprocessed_sents = []
for sentence in tagged_sents:
    new_sentence = []
    for word, category in sentence:
        substr_end = 2
        if category[0] == "v" or category[0] == "F":
            substr_end = 3
        category = category[ : substr_end]

        if word != "*0*" and category[ : 2] != "sn":
            new_sentence.append((word, category))
    preprocessed_sents.append(new_sentence)

# Declare the percentage of samples devoted to train and test.
total_samples = len(preprocessed_sents)
percentage_training = 0.9
num_training = int(len(preprocessed_sents) * percentage_training)
# num_test = len(preprocessed_sents) - num_training
# train_sents = preprocessed_sents[ : num_training]
# test_sents = preprocessed_sents[num_training : ]

# Store the best accuracies obtained by the HMM and the TNT models,
# as well as the best partition where it has been obtained (for k-fold).
best_hmm_accuracy = 0
best_hmm_partition = -1
best_tnt_accuracy = 0
best_tnt_partition = -1

# Auxiliary structure to get the number of words by partition.
num_words_by_partition = []
for i in range(k):
    sents = preprocessed_sents[int(i * total_samples / k) : int((i + 1) * total_samples / k)]
    word_counter = 0
    for sentence in sents:
        word_counter += len(sentence)
    num_words_by_partition.append(word_counter)
hmm_accuracies = []
tnt_accuracies = []

if not cross_validation:
    train_sents = preprocessed_sents[ : num_training]
    test_sents = preprocessed_sents[num_training : ]
    print("Length of the test corpus:", len(test_sents))
    print()

    # HMM
    # Declare the HMM model.
    t0 = time.time()
    hmm_model = hmm.HiddenMarkovModelTagger.train(train_sents)
    print("HMM model trained. Time taken:", time.time() - t0)
    # Evaluate the HMM model.
    t0 = time.time()
    hmm_accuracy = hmm_model.evaluate(test_sents)
    print("HMM ACCURACY:", hmm_accuracy)
    print("Time taken to evaluate:", time.time() - t0)
    print()

    # TNT
    # Declare the TNT model.
    t0 = time.time()
    tnt_model = tnt.TnT()
    tnt_model.train(train_sents)
    print("TnT model trained. Time taken:", time.time() - t0)
    # Evaluate the TNT model.
    t0 = time.time()
    tnt_accuracy = tnt_model.evaluate(test_sents)
    print("TNT ACCURACY:", tnt_accuracy)
    print("Time taken to evaluate:", time.time() - t0)

else:
    # Cross-validation
    for i in range(k):
        train_sents = preprocessed_sents[ : int(i * total_samples / k)] + preprocessed_sents[int((i + 1) * total_samples / k) : ]
        test_sents = preprocessed_sents[int(i * total_samples / k) : int((i + 1) * total_samples / k)]

        # HMM
        # Train the HMM model.
        t0 = time.time()
        hmm_model = hmm.HiddenMarkovModelTagger.train(train_sents)
        print("HMM model trained. Resulting model:", hmm_model, "Time taken:", time.time() - t0)
        # Evaluate the HMM model.
        t0 = time.time()
        hmm_accuracy = hmm_model.evaluate(test_sents)
        print("HMM ACCURACY FOR TEST PARTITION", (i + 1), "=", hmm_accuracy, "Time taken:", time.time() - t0)
        num_test_sents = num_words_by_partition[i]
        conf_int = 1.96 * math.sqrt(hmm_accuracy * (1 - hmm_accuracy) / num_test_sents)
        print("Confidence interval at 95%: (" + str(hmm_accuracy - conf_int) + ", " + str(hmm_accuracy + conf_int) + ")")
        print("Confidence interval range: (" + str(-conf_int) + ", " + str(conf_int) + ")")
        if hmm_accuracy > best_hmm_accuracy:
            best_hmm_accuracy = hmm_accuracy
            best_hmm_partition = i
        hmm_accuracies.append(hmm_accuracy)
        print()

        # TNT
        # Train the TNT model.
        t0 = time.time()
        tnt_model = tnt.TnT()
        tnt_model.train(train_sents)
        print("TnT model trained. Time taken:", time.time() - t0)
        # Evaluate the TNT model.
        t0 = time.time()
        tnt_accuracy = tnt_model.evaluate(test_sents)
        print("TNT ACCURACY FOR TEST PARTITION", i, "=", tnt_accuracy, "Time taken:", time.time() - t0)
        num_test_sents = num_words_by_partition[i]
        conf_int = 1.96 * math.sqrt(tnt_accuracy * (1 - tnt_accuracy) / num_test_sents)
        print("Confidence interval at 95%: (" + str(tnt_accuracy - conf_int) + ", " + str(tnt_accuracy + conf_int) + ")")
        print("Confidence interval range: (" + str(-conf_int) + ", " + str(conf_int) + ")")
        if tnt_accuracy > best_tnt_accuracy:
            best_tnt_accuracy = tnt_accuracy
            best_tnt_partition = (i + 1)
        tnt_accuracies.append(tnt_accuracy)
        print()
        print("===========================================")
        print()

    print("BEST ACCURACY FOR HMM:", best_hmm_accuracy, "for the n-th test partition number =", (best_hmm_partition + 1))
    mean_hmm_accuracy = sum(hmm_accuracies) / len(hmm_accuracies)
    print("Mean HMM accuracy:", mean_hmm_accuracy)
    hmm_variance = 0
    for accuracy in hmm_accuracies:
        hmm_variance += math.pow(accuracy - mean_hmm_accuracy, 2)
    hmm_variance /= 10
    hmm_conf_int = 1.96 * math.sqrt(hmm_variance / 10)
    print("Confidence interval at 95%: (" + str(mean_hmm_accuracy - hmm_conf_int) + ", " + str(mean_hmm_accuracy + hmm_conf_int) + ")")
    print("Confidence interval range: (" + str(-hmm_conf_int) + ", " + str(hmm_conf_int) + ")")
    print()
    print("=========================================")
    print()
    print("BEST ACCURACY FOR TNT:", best_tnt_accuracy, "for the n-th test partition number =", (best_tnt_partition + 1))
    mean_tnt_accuracy = sum(tnt_accuracies) / len(tnt_accuracies)
    print("Mean TNT accuracy:", mean_tnt_accuracy)
    tnt_variance = 0
    for accuracy in tnt_accuracies:
        tnt_variance += math.pow(accuracy - mean_tnt_accuracy, 2)
    tnt_variance /= 10
    tnt_conf_int = 1.96 * math.sqrt(tnt_variance / 10)
    print("Confidence interval at 95%: (" + str(mean_tnt_accuracy - tnt_conf_int) + ", " + str(mean_tnt_accuracy + tnt_conf_int) + ")")
    print("Confidence interval range: (" + str(-tnt_conf_int) + ", " + str(tnt_conf_int) + ")")