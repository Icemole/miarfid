Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 65 states and 23950 output symbols> Time taken: 1.4014482498168945
HMM ACCURACY FOR TEST PARTITION 1 = 0.926420528934276 Time taken: 56.37430191040039

TnT model trained. Time taken: 3.4289703369140625
TNT ACCURACY FOR TEST PARTITION 0 = 0.9039015449070438 Time taken: 211.60806012153625

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23996 output symbols> Time taken: 1.5353124141693115
HMM ACCURACY FOR TEST PARTITION 2 = 0.9332166211832552 Time taken: 54.18709850311279

TnT model trained. Time taken: 3.2357609272003174
TNT ACCURACY FOR TEST PARTITION 1 = 0.9053086864734051 Time taken: 228.7559187412262

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23964 output symbols> Time taken: 1.4310684204101562
HMM ACCURACY FOR TEST PARTITION 3 = 0.9297964417374579 Time taken: 52.827367305755615

TnT model trained. Time taken: 3.5327422618865967
TNT ACCURACY FOR TEST PARTITION 2 = 0.9026019126996848 Time taken: 202.9162302017212

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23975 output symbols> Time taken: 1.4296813011169434
HMM ACCURACY FOR TEST PARTITION 4 = 0.9214406959473795 Time taken: 52.71466040611267

TnT model trained. Time taken: 3.4433302879333496
TNT ACCURACY FOR TEST PARTITION 3 = 0.9003288775726713 Time taken: 196.11906170845032

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23970 output symbols> Time taken: 1.4234561920166016
HMM ACCURACY FOR TEST PARTITION 5 = 0.9244108729187723 Time taken: 53.30371594429016

TnT model trained. Time taken: 3.4533541202545166
TNT ACCURACY FOR TEST PARTITION 4 = 0.8998350976115751 Time taken: 206.0453977584839

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 65 states and 23877 output symbols> Time taken: 1.407500982284546
HMM ACCURACY FOR TEST PARTITION 6 = 0.9245060302797024 Time taken: 51.95177984237671

TnT model trained. Time taken: 3.227355718612671
TNT ACCURACY FOR TEST PARTITION 5 = 0.8999230177059276 Time taken: 209.36381435394287

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 65 states and 23940 output symbols> Time taken: 1.4390051364898682
HMM ACCURACY FOR TEST PARTITION 7 = 0.9241222504230119 Time taken: 51.117194414138794

TnT model trained. Time taken: 3.562661647796631
TNT ACCURACY FOR TEST PARTITION 6 = 0.9008565989847716 Time taken: 210.89590620994568

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 24081 output symbols> Time taken: 1.503051996231079
HMM ACCURACY FOR TEST PARTITION 8 = 0.9239609483960949 Time taken: 51.247506856918335

TnT model trained. Time taken: 3.369729518890381
TNT ACCURACY FOR TEST PARTITION 7 = 0.9030962343096234 Time taken: 191.07536458969116

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 66 states and 23908 output symbols> Time taken: 1.4249179363250732
HMM ACCURACY FOR TEST PARTITION 9 = 0.9247311827956989 Time taken: 51.70834040641785

TnT model trained. Time taken: 3.377091407775879
TNT ACCURACY FOR TEST PARTITION 8 = 0.8978759468192171 Time taken: 195.11446022987366

===========================================

Length of the test corpus: 603
HMM model trained. Resulting model: <HiddenMarkovModelTagger 65 states and 23949 output symbols> Time taken: 1.4045155048370361
HMM ACCURACY FOR TEST PARTITION 10 = 0.9237097980553478 Time taken: 53.894481897354126

TnT model trained. Time taken: 3.4954686164855957
TNT ACCURACY FOR TEST PARTITION 9 = 0.8990276738967838 Time taken: 164.97252750396729

===========================================

BEST ACCURACY FOR HMM: 0.9332166211832552 for the n-th test partition number = 2
BEST ACCURACY FOR TNT: 0.9053086864734051 for the n-th test partition number = 3
