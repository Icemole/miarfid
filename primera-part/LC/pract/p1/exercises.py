def exercise1(string):
    string = string.replace("/", " ").split()
    categories_dict = {}
    i = 0
    while i < len(string):
        categories_dict[string[i + 1]] = categories_dict.get(string[i + 1], 0) + 1
        i += 2
    for k in sorted(categories_dict.keys()):
        print(k, categories_dict[k])

def exercise2(string):
    string = string.replace("/", " ").split()
    categories_dict = {}
    i = 0
    while i < len(string):
        word = string[i].lower()
        category = string[i + 1]
        word_categories_dict = categories_dict.get(word, (0, {}))
        frequency = word_categories_dict[0] + 1
        #word_list = word_categories_dict[1].get(category, 0)
        word_list = word_categories_dict[1]
        word_list[category] = word_list.get(category, 0) + 1
        categories_dict[word] = (frequency, word_list)
        i += 2
    for k in sorted(categories_dict.keys()):
        print(k, "\t", categories_dict[k][0], end = "\t")
        for ktwo in sorted(categories_dict[k][1].keys()):
            print(ktwo, categories_dict[k][1][ktwo], end = "\t")
        print()

def exercise3(string):
    string = string.replace("/", " ").split()
    string.insert(0, "<s>")
    string.append("</s>")
    bigram_dict = {}
    i = 0
    
    while i < len(string):
        if i == len(string) - 2:
            bigram_dict[(string[i], string[i + 1])] = bigram_dict.get((string[i], string[i + 1]), 0) + 1
        else:
            bigram_dict[(string[i], string[i + 2])] = bigram_dict.get((string[i], string[i + 2]), 0) + 1
        i += 2
    for k in sorted(bigram_dict.keys()):
        print(k, bigram_dict[k])
    
    return bigram_dict

def exercise4(string, word_to_search):
    string = string.replace("/", " ").split()
    #string.insert(0, "<s>")
    #string.append("</s>")
    categories_dict = {}
    word_categories_frequency = {}
    i = 0
    total_words = 0
    while i < len(string):
        word = string[i].lower()
        category = string[i + 1]
        word_categories_dict = categories_dict.get(word, (0, {}))
        # Update the word frequency
        frequency = word_categories_dict[0] + 1
        # Update the dictionary of word frequencies
        word_list = word_categories_dict[1]
        word_list[category] = word_list.get(category, 0) + 1
        categories_dict[word] = (frequency, word_list)
        # Update the word category's frequency
        word_categories_frequency[category] = word_categories_frequency.get(category, 0) + 1
        i += 2
        total_words += 1
    
    if categories_dict.get(word_to_search, None):
        # P(gramatical_category | word_to_search)
        word_frequency = categories_dict[word_to_search][0]
        gramatical_category_dict = categories_dict[word_to_search][1]
        for i in gramatical_category_dict.keys():
            print("P(", i, "|", word_to_search, ") =", gramatical_category_dict[i] / word_frequency)
        for i in gramatical_category_dict.keys():
            conditional_probability = gramatical_category_dict[i] / word_frequency
            print("P(", word_to_search, "|", i, ") =", conditional_probability * word_frequency / word_categories_frequency[i])
    else:
        print("La palabra no ha sido encontrada en el texto.")
    

def main():
    string = "El/DT perro/N come/V carne/N de/P la/DT carnicería/N  y/C de/P la/DT nevera/N y/C canta/V el/DT la/N la/N la/N ./Fp"
    # exercise1(string)
    # exercise2(string)
    # exercise3(string)
    # exercise4(string, "la")


if __name__ == "__main__":
    main()