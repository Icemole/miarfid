#!/usr/bin/octave
addpath("./");
arg_list = argv();

if(nargin != 1)
    printf("Usage: experiment.m <dataset>\n");
    exit(1);
endif

load(["../datos/", arg_list{1}, ".gz"]);
[N,L]=size(data);
D=L-1;
ll=unique(data(:,L));
C=numel(ll);
rand("seed",23);
data=data(randperm(N),:);
NTr=round(.7*N);
M=N-NTr;
te=data(NTr+1:N,:);
best_num_errors = N;
best_alpha = 0.1;
best_beta = 0.1;
printf("#\ta\tb\tE\tk\tEte\n");
printf("#------------------------------------------\n");
for a=[.1 1 10 100 1000 10000 100000]
    for b=[.1 1 10 100 1000 10000 100000]
        [w,E,k]=perceptron(data(1:NTr,:),b,a);
        rl=zeros(M,1);
        for n=1:M
            rl(n)=ll(linmach(w,[1 te(n,1:D)]'));
        end
        [nerr m]=confus(te(:,L),rl);
        printf("%7.1f\t%7.1f\t%3d\t%3d\t%3d\n",a,b,E,k,nerr);

        # Update alpha and beta if the perceptron has converged
        # and the number of errors is lesser than the previous best
        # and beta is higher than the previous best.
        if (E == 0 && nerr < best_num_errors && b > best_beta)
            best_num_errors = nerr;
            best_alpha = a;
            best_beta = b;
        # elseif (nerr < best_num_errors )
        endif
    end
end
printf("a*: %8.1f, b*: %8.1f, E*: %8.1f\n", best_alpha, best_beta, best_num_errors);

# Calculate the most optimal perceptron according to
# the best alpha and beta previously calculated.
[w, E, k] = perceptron(data, best_beta, best_alpha);
save_precision(4);
save([arg_list{1}, "_w"], "w");