#!/usr/bin/octave -qf
if (nargin!=3) printf("%s <tr> <te> <k>\n",program_name()); exit; end
arg_list=argv(); Tr=arg_list{1}; Te=arg_list{2}; k=str2num(arg_list{3});
load(sprintf(Tr)); tr=data; [NTr,L]=size(tr); D=L-1;
labs=unique(data(:,L)); C=numel(labs);
load(sprintf(Te)); te=data; NTe=rows(te); clear data;
recolabs=zeros(1,NTe);
for i=1:NTe
  tei=te(i,1:D)';
  nmin=1; min=inf;
  distances = [];
  for n=1:NTr
    trn=tr(n,1:D)'; aux=tei-trn; d=aux'*aux;
    distances(n) = d;
    %if (d<min) min=d; nmin=n; endif
  end
  [distances, best_rel_indices] = sort(distances);
  classes = tr(best_rel_indices(1:k), L);
  best_class = 1; max_samples = -inf;
  for c=1:C
    aux_samples = sum(find(classes == c));
    if aux_samples > max_samples
      max_samples = aux_samples;
      best_class = c;
    end
  end
  recolabs(i) = best_class;
  % recolabs(i)=tr(nmin,L);
end
[Nerr m]=confus(te(:,L),recolabs);
printf("%s %s %d %d %.1f\n",Tr,Te,Nerr,NTe,100.0*Nerr/NTe);
m
